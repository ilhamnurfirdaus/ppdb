<?php

namespace Database\Seeders;

use App\Models\SyaratPendaftaran;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class SyaratPendaftaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $syarat_pendaftaran = new SyaratPendaftaran;
        $syarat_pendaftaran->id = Str::uuid();
        $syarat_pendaftaran->name = "Scan Kartu Keluarga";
        $syarat_pendaftaran->keterangan = "Scan Kartu Keluarga dengan format file .jpg .jpeg .png";
        $syarat_pendaftaran->save();

        $syarat_pendaftaran = new SyaratPendaftaran;
        $syarat_pendaftaran->id = Str::uuid();
        $syarat_pendaftaran->name = "Scan Rapor Kelas 4 Semester 1";
        $syarat_pendaftaran->keterangan = "Scan Rapor Kelas 4 Semester 1 dengan format file .jpg .jpeg .png";
        $syarat_pendaftaran->save();

        $syarat_pendaftaran = new SyaratPendaftaran;
        $syarat_pendaftaran->id = Str::uuid();
        $syarat_pendaftaran->name = "Scan Rapor Kelas 5 Semester 1";
        $syarat_pendaftaran->keterangan = "Scan Rapor Kelas 6 Semester 1 dengan format file .jpg .jpeg .png";
        $syarat_pendaftaran->save();

        $syarat_pendaftaran = new SyaratPendaftaran;
        $syarat_pendaftaran->id = Str::uuid();
        $syarat_pendaftaran->name = "Scan Rapor Kelas 6 Semester 1";
        $syarat_pendaftaran->keterangan = "Scan Rapor Kelas 6 Semester 1 dengan format file .jpg .jpeg .png";
        $syarat_pendaftaran->save();
    }
}
