<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Pengumuman;

class PengumumanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pengumuman = new Pengumuman;
        $pengumuman->id = Str::uuid();
        $pengumuman->judul = "Pendaftaran";
        $pengumuman->deskripsi = "Test";
        $pengumuman->status = "Aktif";
        $pengumuman->save();
    }
}
