<?php

namespace Database\Seeders;

use App\Models\Jurusan;
use App\Models\Biaya;
use App\Models\SettingBerkas;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class JurusanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jurusan = new Jurusan;
        $jurusan->id = "8ac17b8a-ce6e-4506-9e96-85ba3c342187";
        $jurusan->name = "MIPA";
        $jurusan->status = "Aktif";
        $jurusan->keterangan = "";
        $jurusan->minimal_nilai = 30.5;
        $jurusan->maksimal_jumlah_siswa = 30;
        $jurusan->tahun_ajaran = "2022-2023";
        $jurusan->akreditasi = "Terakreditasi A";
        $jurusan->save();

        $setting_berkas = new SettingBerkas;
        $setting_berkas->setting_berkas_id  = "SB000001";
        $setting_berkas->jenis_berkas  = "Berkas Pendaftaran";
        $setting_berkas->nama_berkas = "Kartu Keluarga";
        $setting_berkas->jurusan_id  = "Default";
        $setting_berkas->tahun_ajaran = "2022-2023";
        $setting_berkas->save();

        $setting_berkas = new SettingBerkas;
        $setting_berkas->setting_berkas_id  = "SB000002";
        $setting_berkas->jenis_berkas  = "Berkas Pendaftaran";
        $setting_berkas->nama_berkas = "Akte Kelahiran";
        $setting_berkas->jurusan_id  = "Default";
        $setting_berkas->tahun_ajaran = "2022-2023";
        $setting_berkas->save();

        $setting_berkas = new SettingBerkas;
        $setting_berkas->setting_berkas_id  = "SB000003";
        $setting_berkas->jenis_berkas  = "Berkas Pendaftaran";
        $setting_berkas->nama_berkas = "Surat Keterangan Tidak Buta Warna";
        $setting_berkas->jurusan_id  = $jurusan->id;
        $setting_berkas->tahun_ajaran = "2022-2023";
        $setting_berkas->save();

        // $setting_berkas = new SettingBerkas;
        // $setting_berkas->setting_berkas_id  = "SB000005";
        // $setting_berkas->jenis_berkas  = "Berkas Daftar Ulang";
        // $setting_berkas->nama_berkas = "KTP Ayah";
        // $setting_berkas->jurusan_id  = "Default";
        // $setting_berkas->tahun_ajaran = "2022-2023";
        // $setting_berkas->save();

        // $setting_berkas = new SettingBerkas;
        // $setting_berkas->setting_berkas_id  = "SB000006";
        // $setting_berkas->jenis_berkas  = "Berkas Daftar Ulang";
        // $setting_berkas->nama_berkas = "KTP Ibu";
        // $setting_berkas->jurusan_id  = "Default";
        // $setting_berkas->tahun_ajaran = "2022-2023";
        // $setting_berkas->save();

        $biaya = new Biaya;
        $biaya->id = "BJ202101";
        $biaya->jenis_biaya = "Lab";
        $biaya->program_studi = $jurusan->id;
        $biaya->biaya = "400000";
        $biaya->tahun_ajaran = "2022-2023";
        $biaya->save();

        $biaya = new Biaya;
        $biaya->id = "BL202101";
        $biaya->jenis_biaya = "Seragam";
        $biaya->program_studi = "Default";
        $biaya->biaya = "500000";
        $biaya->tahun_ajaran = "2022-2023";
        $biaya->save();

        // $biaya = new Biaya;
        // $biaya->id = Str::uuid();
        // $biaya->jenis_biaya = "Biaya Pendaftaran";
        // $biaya->program_studi = "Default";
        // $biaya->biaya = "800000";
        // $biaya->nama_gelombang = "Gelombang 1";
        // $biaya->mulai_tanggal = "2022-03-01";
        // $biaya->sampai_tanggal = "2022-04-01";
        // $biaya->tahun_ajaran = "2022-2023";
        // $biaya->save();

        // $biaya = new Biaya;
        // $biaya->id = Str::uuid();
        // $biaya->jenis_biaya = "Biaya Daftar Ulang";
        // $biaya->program_studi = "Default";
        // $biaya->biaya = "1000000";
        // $biaya->nama_gelombang = "Gelombang 2";
        // $biaya->mulai_tanggal = "2022-04-02";
        // $biaya->sampai_tanggal = "2022-05-01";
        // $biaya->tahun_ajaran = "2022-2023";
        // $biaya->save();

        // $biaya = new Biaya;
        // $biaya->id = Str::uuid();
        // $biaya->jenis_biaya = "Biaya Pendaftaran";
        // $biaya->program_studi = $jurusan->id;
        // $biaya->biaya = "800000";
        // $biaya->nama_gelombang = "Gelombang 1";
        // $biaya->mulai_tanggal = "2022-03-02";
        // $biaya->sampai_tanggal = "2022-04-01";
        // $biaya->tahun_ajaran = "2022-2023";
        // $biaya->save();

        // $biaya = new Biaya;
        // $biaya->id = Str::uuid();
        // $biaya->jenis_biaya = "Biaya Daftar Ulang";
        // $biaya->program_studi = $jurusan->id;
        // $biaya->biaya = "1000000";
        // $biaya->nama_gelombang = "Gelombang 1";
        // $biaya->mulai_tanggal = "2022-03-02";
        // $biaya->sampai_tanggal = "2022-04-01";
        // $biaya->tahun_ajaran = "2022-2023";
        // $biaya->save();

        $jurusan = new Jurusan;
        $jurusan->id = '7b2dde12-bdba-4e72-993c-8871f40adc1f';
        $jurusan->name = "IPS";
        $jurusan->status = "Aktif";
        $jurusan->keterangan = "";
        $jurusan->minimal_nilai = 66.5;
        $jurusan->maksimal_jumlah_siswa = 19;
        $jurusan->tahun_ajaran = "2022-2023";
        $jurusan->akreditasi = "Terdaftar";
        $jurusan->save();

        // $biaya = new Biaya;
        // $biaya->id = Str::uuid();
        // $biaya->jenis_biaya = "Biaya Pendaftaran";
        // $biaya->program_studi = $jurusan->id;
        // $biaya->biaya = "700000";
        // $biaya->nama_gelombang = "Gelombang 1";
        // $biaya->mulai_tanggal = "2022-03-01";
        // $biaya->sampai_tanggal = "2022-04-01";
        // $biaya->tahun_ajaran = "2022-2023";
        // $biaya->save();

        // $biaya = new Biaya;
        // $biaya->id = Str::uuid();
        // $biaya->jenis_biaya = "Biaya Daftar Ulang";
        // $biaya->program_studi = $jurusan->id;
        // $biaya->biaya = "3000000";
        // $biaya->nama_gelombang = "Gelombang 1";
        // $biaya->mulai_tanggal = "2022-03-01";
        // $biaya->sampai_tanggal = "2022-04-01";
        // $biaya->tahun_ajaran = "2022-2023";
        // $biaya->save();

        // $biaya = new Biaya;
        // $biaya->id = Str::uuid();
        // $biaya->jenis_biaya = "Biaya Pendaftaran";
        // $biaya->program_studi = $jurusan->id;
        // $biaya->biaya = "750000";
        // $biaya->nama_gelombang = "Gelombang 2";
        // $biaya->mulai_tanggal = "2022-04-02";
        // $biaya->sampai_tanggal = "2022-05-01";
        // $biaya->tahun_ajaran = "2022-2023";
        // $biaya->save();

        // $biaya = new Biaya;
        // $biaya->id = Str::uuid();
        // $biaya->jenis_biaya = "Biaya Daftar Ulang";
        // $biaya->program_studi = $jurusan->id;
        // $biaya->biaya = "3500000";
        // $biaya->nama_gelombang = "Gelombang 2";
        // $biaya->mulai_tanggal = "2022-04-02";
        // $biaya->sampai_tanggal = "2022-05-01";
        // $biaya->tahun_ajaran = "2022-2023";
        // $biaya->save();

        $setting_berkas = new SettingBerkas;
        $setting_berkas->setting_berkas_id  = "SB000007";
        $setting_berkas->jenis_berkas  = "Berkas Pendaftaran";
        $setting_berkas->nama_berkas = "Surat Keterangan Hafal Huruf";
        $setting_berkas->jurusan_id  = $jurusan->id;
        $setting_berkas->tahun_ajaran = "2022-2023";
        $setting_berkas->save();
    }
}
