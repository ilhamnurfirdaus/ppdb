<?php

namespace Database\Seeders;

use App\Models\Test;
use App\Models\TestChoice;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class TestSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $test = new Test;
        $test->id = Str::uuid();
        $test->pertanyaan = "Jumlah Kaki Jerapah";
        $test->tahun_ajaran = "2021-2022";
        $test->save();

        for ($i=1; $i <= 4 ; $i++) { 
            $test_choice = new TestChoice;
            $test_choice->id = Str::uuid();
            $test_choice->test_id = $test->id;
            $test_choice->name = $i;
            $test_choice->score = ($i == 4 ? 1 : 0);
            $test_choice->save();
        }
        
        $test = new Test;
        $test->id = Str::uuid();
        $test->pertanyaan = "Jumlah Kaki Macan";
        $test->tahun_ajaran = "2021-2022";
        $test->save();

        for ($i=1; $i <= 4 ; $i++) { 
            $test_choice = new TestChoice;
            $test_choice->id = Str::uuid();
            $test_choice->test_id = $test->id;
            $test_choice->name = $i;
            $test_choice->score = ($i == 4 ? 1 : 0);
            $test_choice->save();
        }

        $test = new Test;
        $test->id = Str::uuid();
        $test->pertanyaan = "Jumlah Kaki Kepiting";
        $test->tahun_ajaran = "2021-2022";
        $test->save();

        for ($i=1; $i <= 4 ; $i++) { 
            $test_choice = new TestChoice;
            $test_choice->id = Str::uuid();
            $test_choice->test_id = $test->id;
            $test_choice->name = $i;
            $test_choice->score = ($i == 2 ? 1 : 0);
            $test_choice->save();
        }
    }
}
