<?php

namespace Database\Seeders;

use App\Models\TahunAjaran;
use Illuminate\Database\Seeder;

class TahunAjaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=2021; $i <= intval(date("Y")); $i++) { 
            $tahun_ajaran = new TahunAjaran;
            $tahun_ajaran->tahun_ajaran = $i."-".($i+1);
            $tahun_ajaran->waktu_soal = 2;
            $tahun_ajaran->jml_raport = 6;
            $tahun_ajaran->minimal_nilai = 50;
            $tahun_ajaran->maksimal_jumlah_siswa = 500;
            $tahun_ajaran->save();
        }
    }
}
