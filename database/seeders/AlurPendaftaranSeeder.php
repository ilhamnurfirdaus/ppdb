<?php

namespace Database\Seeders;

use App\Models\AlurPendaftaran;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class AlurPendaftaranSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $alur_pendaftaran = new AlurPendaftaran;
        $alur_pendaftaran->id = Str::uuid();
        $alur_pendaftaran->name = "1. Isi Form Pendaftaran";
        $alur_pendaftaran->keterangan = "Isi Form Pendaftaran pada register";
        $alur_pendaftaran->save();

        $alur_pendaftaran = new AlurPendaftaran;
        $alur_pendaftaran->id = Str::uuid();
        $alur_pendaftaran->name = "2. Pembayaran Pendaftaran";
        $alur_pendaftaran->keterangan = "Bayar Pendaftaran sesuai jurusan masing - masing";
        $alur_pendaftaran->save();

        $alur_pendaftaran = new AlurPendaftaran;
        $alur_pendaftaran->id = Str::uuid();
        $alur_pendaftaran->name = "3. Berkas Pendaftaran";
        $alur_pendaftaran->keterangan = "Masukan berkas sesuai dengan syarat pendaftaran";
        $alur_pendaftaran->save();

        $alur_pendaftaran = new AlurPendaftaran;
        $alur_pendaftaran->id = Str::uuid();
        $alur_pendaftaran->name = "4. Test Pendaftaran";
        $alur_pendaftaran->keterangan = "Mengerjakan test pendaftaran berupa pilihan ganda, jika waktu habis maka akan kesimpan jawaban anda.";
        $alur_pendaftaran->save();

        $alur_pendaftaran = new AlurPendaftaran;
        $alur_pendaftaran->id = Str::uuid();
        $alur_pendaftaran->name = "5. Seleksi";
        $alur_pendaftaran->keterangan = "Tunggu hasil seleksi lolos / tidak lolos";
        $alur_pendaftaran->save();
    }
}
