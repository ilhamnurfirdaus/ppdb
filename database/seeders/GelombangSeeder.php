<?php

namespace Database\Seeders;

use App\Models\Biaya;
use App\Models\Gelombang;
use Illuminate\Database\Seeder;

class GelombangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $gelombang = new Gelombang;
        $gelombang->gelombang_id = "GEL202101";
        $gelombang->nama_gelombang = "Gelombang 1";
        $gelombang->tanggal_mulai = "2022-03-01";
        $gelombang->tanggal_selesai = "2022-04-01";
        $gelombang->tahun_ajaran = "2022-2023";
        $gelombang->save();

        $biaya = new Biaya;
        $biaya->id = "BP202101";
        $biaya->jenis_biaya = "Biaya Pendaftaran";
        $biaya->program_studi = "Default";
        $biaya->biaya = 800000;
        $biaya->nama_gelombang = $gelombang->nama_gelombang;
        $biaya->mulai_tanggal = $gelombang->tanggal_mulai;
        $biaya->sampai_tanggal = $gelombang->tanggal_selesai;        
        $biaya->tahun_ajaran = $gelombang->tahun_ajaran;
        $biaya->save();

        $biaya = new Biaya;
        $biaya->id = "BU202101";
        $biaya->jenis_biaya = "Biaya Daftar Ulang";
        $biaya->program_studi = "Default";
        $biaya->biaya = 8000000;
        $biaya->nama_gelombang = $gelombang->nama_gelombang;
        $biaya->mulai_tanggal = $gelombang->tanggal_mulai;
        $biaya->sampai_tanggal = $gelombang->tanggal_selesai;        
        $biaya->tahun_ajaran = $gelombang->tahun_ajaran;
        $biaya->save();

        $gelombang = new Gelombang;
        $gelombang->gelombang_id = "GEL202102";
        $gelombang->nama_gelombang = "Gelombang 2";
        $gelombang->tanggal_mulai = "2022-04-02";
        $gelombang->tanggal_selesai = "2022-05-01";
        $gelombang->tahun_ajaran = "2022-2023";
        $gelombang->save();

        $biaya = new Biaya;
        $biaya->id = "BP202102";
        $biaya->jenis_biaya = "Biaya Pendaftaran";
        $biaya->program_studi = "Default";
        $biaya->biaya = 900000;
        $biaya->nama_gelombang = $gelombang->nama_gelombang;
        $biaya->mulai_tanggal = $gelombang->tanggal_mulai;
        $biaya->sampai_tanggal = $gelombang->tanggal_selesai;        
        $biaya->tahun_ajaran = $gelombang->tahun_ajaran;
        $biaya->save();

        $biaya = new Biaya;
        $biaya->id = "BU202102";
        $biaya->jenis_biaya = "Biaya Daftar Ulang";
        $biaya->program_studi = "Default";
        $biaya->biaya = 9000000;
        $biaya->nama_gelombang = $gelombang->nama_gelombang;
        $biaya->mulai_tanggal = $gelombang->tanggal_mulai;
        $biaya->sampai_tanggal = $gelombang->tanggal_selesai;        
        $biaya->tahun_ajaran = $gelombang->tahun_ajaran;
        $biaya->save();

        $gelombang = new Gelombang;
        $gelombang->gelombang_id = "GEL202103";
        $gelombang->nama_gelombang = "Gelombang 3";
        $gelombang->tanggal_mulai = "2022-05-02";
        $gelombang->tanggal_selesai = "2022-12-01";
        $gelombang->tahun_ajaran = "2022-2023";
        $gelombang->save();

        $biaya = new Biaya;
        $biaya->id = "BP202103";
        $biaya->jenis_biaya = "Biaya Pendaftaran";
        $biaya->program_studi = "Default";
        $biaya->biaya = 1000000;
        $biaya->nama_gelombang = $gelombang->nama_gelombang;
        $biaya->mulai_tanggal = $gelombang->tanggal_mulai;
        $biaya->sampai_tanggal = $gelombang->tanggal_selesai;        
        $biaya->tahun_ajaran = $gelombang->tahun_ajaran;
        $biaya->save();

        $biaya = new Biaya;
        $biaya->id = "BU202103";
        $biaya->jenis_biaya = "Biaya Daftar Ulang";
        $biaya->program_studi = "Default";
        $biaya->biaya = 10000000;
        $biaya->nama_gelombang = $gelombang->nama_gelombang;
        $biaya->mulai_tanggal = $gelombang->tanggal_mulai;
        $biaya->sampai_tanggal = $gelombang->tanggal_selesai;        
        $biaya->tahun_ajaran = $gelombang->tahun_ajaran;
        $biaya->save();

        $gelombang = new Gelombang;
        $gelombang->gelombang_id = "GEL202201";
        $gelombang->nama_gelombang = "Gelombang 1";
        $gelombang->tanggal_mulai = "2022-03-01";
        $gelombang->tanggal_selesai = "2022-04-01";
        $gelombang->tahun_ajaran = "2022-2023";
        $gelombang->save();

        $gelombang = new Gelombang;
        $gelombang->gelombang_id = "GEL202202";
        $gelombang->nama_gelombang = "Gelombang 2";
        $gelombang->tanggal_mulai = "2022-04-02";
        $gelombang->tanggal_selesai = "2022-05-01";
        $gelombang->tahun_ajaran = "2022-2023";
        $gelombang->save();

        $gelombang = new Gelombang;
        $gelombang->gelombang_id = "GEL202203";
        $gelombang->nama_gelombang = "Gelombang 3";
        $gelombang->tanggal_mulai = "2022-05-02";
        $gelombang->tanggal_selesai = "2022-06-01";
        $gelombang->tahun_ajaran = "2022-2023";
        $gelombang->save();

        $gelombang = new Gelombang;
        $gelombang->gelombang_id = "GEL202204";
        $gelombang->nama_gelombang = "Gelombang 4";
        $gelombang->tanggal_mulai = "2022-06-02";
        $gelombang->tanggal_selesai = "2022-12-01";
        $gelombang->tahun_ajaran = "2022-2023";
        $gelombang->save();
    }
}
