<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\WebProfil;

class WebProfilSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = new WebProfil;
        $data->id = Str::uuid();
        $data->name = "SochaEdu";
        $data->alamat = "Jl. Puspanjolo Timur VII, Bojongsalaman, Kec. Semarang Bar., Kota Semarang, Jawa Tengah 50141";
        $data->email = "admin@gmail.com";
        $data->cp = "089127382921";
        $data->telp = "(0245) 8148 1852 ";
        $data->jenjang_sekolah = "SLTA";
        $data->jenjang_sekolah2 = "SMK";
        $data->jml_raport = 3;
        $data->minimal_nilai = 30.5;
        $data->maksimal_jumlah_siswa = 19;
        $data->waktu_soal = 2;
        $data->logo = "logo-header.png";
        $data->banner_1 = "background.jpg";
        $data->tampil_seleksi = "Tidak";
        $data->informasi_pendaftaran = '<p style="margin-bottom: 10px; outline: 0px; text-align: center; font-family: Poppins, sans-serif; font-size: 14px;"><span arial="" black";"="" style="outline: 0px; font-size: 24px; background-color: rgb(255, 255, 0);"><span style="outline: 0px;"><u style="outline: 0px;">PPDB</u></span></span></p><p style="margin-bottom: 10px; outline: 0px; text-align: center; font-family: Poppins, sans-serif; font-size: 14px;"><span arial="" black";"="" style="outline: 0px; font-size: 24px; background-color: rgb(255, 255, 0);"><span style="outline: 0px;"><u style="outline: 0px;">SEKOLAH TAHUN AKADEMIK 2021 / 2022</u></span></span></p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;"><br style="outline: 0px;"></p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;"><span style="outline: 0px; font-size: 0.875rem; background-color: rgb(255, 255, 0);"><span style="outline: 0px;">Kamu bisa daftar di bulan berikut :</span></span></p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;"><span style="outline: 0px; font-size: 0.875rem;">Pendaftaran siswa baru terbagi menjadi 3 Gelombang&nbsp;</span></p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;"><span style="outline: 0px; font-size: 0.875rem;">- Gelombang I : Desember 2020 s/d April 2021</span></p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;"><span style="outline: 0px; font-size: 0.875rem;">- Gelombang II : Mei s/d Juli 2021</span></p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;"><span style="outline: 0px; font-size: 0.875rem;">- Gelombang III : Agustus s/d September 2021</span></p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;"><span style="outline: 0px; font-size: 0.875rem;"><br style="outline: 0px;"></span></p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;"><span style="outline: 0px; font-size: 0.875rem; background-color: rgb(255, 255, 0);"><span style="outline: 0px;">Untuk Program Studi Pilihan nya ada :</span></span><br style="outline: 0px;"></p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;">Ada 3 Prodi favorit yang sudah terakreditasi BAN-PT yang bisa kamu pilih&nbsp;</p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;">- Prodi S1 Manajemen (Terakreditasi B)</p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;">- Prodi D3 Manajemen Perusahaan (Terakreditasi B)</p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;">- Prodi S1 Akuntansi (Terakreditasi BAIK)</p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;"><br style="outline: 0px;"></p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;"><span style="outline: 0px; background-color: rgb(255, 255, 0);">Dan ada 3 jenis kelas yang bisa kamu pilih :</span></p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;">Bagi kamu yang sudah bekerja, tidak perlu bingung karena ada kelas sore &amp; malam nya</p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;">- Kelas PAGI&nbsp; : Jam 08.00 s/d Selesai</p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;">- Kelas SORE&nbsp; : Jam 16.30 s/d Selesai</p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;">- Kelas MALAM&nbsp; : Jam 18.30 s/d Selesai</p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;"><br style="outline: 0px;"></p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;"><span style="outline: 0px; background-color: rgb(255, 255, 0);">ini untuk syarat pendaftarannya ya :</span></p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;">Untuk syarat pendaftaran sangat mudah, cek daftar berikut ya, pendaftaran bisa online atau datang langsung ke Kampus</p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;">- Mengisi Formulir Pendaftaran Online atau Formulir Dapat Diambil di Kampus</p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;">- Membayar Biaya Pendaftaran Sebesar Rp. 200.000,-</p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;">- Fotocopy Ijazah Terlegalisir, Kartu Keluarga dan KTP (masing-masing 2 lembar)</p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;">- Pas foto Berwarna Ukuran 3x4 Sebanyak 3 Lembar</p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;"><br style="outline: 0px;"></p><p style="margin-bottom: 10px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;"><span style="outline: 0px;">Nah, mudahkan, AYO SEGERA daftarkan dirimu untuk Sekolah. kalau kamu masih bingung bisa bertanya langsung via Whatsapp ya di 0857 1388 2666</span></p><p style="margin-bottom: 0px; outline: 0px; font-family: Poppins, sans-serif; font-size: 14px;"><span style="outline: 0px;">Ayo bergabung bersama menjadi SISWA Sekolah.</span></p>';
        $data->warna_header = "#33ff92";
        $data->save();
    }
}
