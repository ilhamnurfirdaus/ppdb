<?php

namespace Database\Seeders;

use App\Models\NilaiBerkas;
use Illuminate\Database\Seeder;

class NilaiBerkasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $nilai_berkas = new NilaiBerkas;
        $nilai_berkas->nilai_berkas_id = "NIBE000001";
        $nilai_berkas->min_nilai = 1;
        $nilai_berkas->max_nilai = 2;
        $nilai_berkas->nilai = 1.5;
        $nilai_berkas->save();

        $nilai_berkas = new NilaiBerkas;
        $nilai_berkas->nilai_berkas_id = "NIBE000002";
        $nilai_berkas->min_nilai = 2;
        $nilai_berkas->max_nilai = 3;
        $nilai_berkas->nilai = 2.5;
        $nilai_berkas->save();

        $nilai_berkas = new NilaiBerkas;
        $nilai_berkas->nilai_berkas_id = "NIBE000003";
        $nilai_berkas->min_nilai = 3;
        $nilai_berkas->max_nilai = 4;
        $nilai_berkas->nilai = 3.5;
        $nilai_berkas->save();

        $nilai_berkas = new NilaiBerkas;
        $nilai_berkas->nilai_berkas_id = "NIBE000004";
        $nilai_berkas->min_nilai = 4;
        $nilai_berkas->max_nilai = 5;
        $nilai_berkas->nilai = 4.5;
        $nilai_berkas->save();

        $nilai_berkas = new NilaiBerkas;
        $nilai_berkas->nilai_berkas_id = "NIBE000005";
        $nilai_berkas->min_nilai = 5;
        $nilai_berkas->max_nilai = 6;
        $nilai_berkas->nilai = 5.5;
        $nilai_berkas->save();

        $nilai_berkas = new NilaiBerkas;
        $nilai_berkas->nilai_berkas_id = "NIBE000006";
        $nilai_berkas->min_nilai = 6;
        $nilai_berkas->max_nilai = 7;
        $nilai_berkas->nilai = 6.5;
        $nilai_berkas->save();

        $nilai_berkas = new NilaiBerkas;
        $nilai_berkas->nilai_berkas_id = "NIBE000007";
        $nilai_berkas->min_nilai = 7;
        $nilai_berkas->max_nilai = 8;
        $nilai_berkas->nilai = 7.5;
        $nilai_berkas->save();

        $nilai_berkas = new NilaiBerkas;
        $nilai_berkas->nilai_berkas_id = "NIBE000008";
        $nilai_berkas->min_nilai = 8;
        $nilai_berkas->max_nilai = 9;
        $nilai_berkas->nilai = 8.5;
        $nilai_berkas->save();

        $nilai_berkas = new NilaiBerkas;
        $nilai_berkas->nilai_berkas_id = "NIBE000009";
        $nilai_berkas->min_nilai = 9;
        $nilai_berkas->max_nilai = 10;
        $nilai_berkas->nilai = 9.5;
        $nilai_berkas->save();
    }
}
