<?php

namespace Database\Seeders;

use App\Models\SettingRaport;
use App\Models\TahunAjaran;
use Illuminate\Database\Seeder;

class SettingRaportSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $tahun_ajarans = TahunAjaran::get();
        for ($i=1; $i <= 3; $i++) { 
            foreach ($tahun_ajarans as $tahun_ajaran) {
                $setting_raports = SettingRaport::select('*')->where("tahun_ajaran", $tahun_ajaran->tahun_ajaran)->get();
                $th = str_split($tahun_ajaran->tahun_ajaran, 4);
                $id = "SR".$th[0]."0".$i;
                $setting_raport = new SettingRaport;
                $setting_raport->setting_raport_id = $id;
                $setting_raport->nama_raport = "Raport Kelas ".$i;
                $setting_raport->tahun_ajaran = $tahun_ajaran->tahun_ajaran;
                $setting_raport->save();
            }
        }
    }
}
