<?php

namespace Database\Seeders;

use App\Models\Bayar;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class BayarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bayar = new Bayar();
        $bayar->id = Str::uuid();
        $bayar->siswa_id = "79d56104-79cd-4880-85c8-5aae2771d167";
        $bayar->rekening_id = "12cd3811-2021-4421-90d2-902b6a4280c0";
        $bayar->img_bayar = "account(ae18c0b2-aaa4-4ab3-8573-463ab46b84bf).png";
        $bayar->nama_bank = "BANK MANDIRI";
        $bayar->nomor_rekening = "1234567890";
        $bayar->atas_nama = "Bapak";
        $bayar->status = "Lunas";
        $bayar->tiga_digit_angka = "133";
        $bayar->tgl_bayar = "2021-09-09";
        $bayar->jml_byr = 800000;
        $bayar->jenis_biaya = "Biaya Pendaftaran";
        $bayar->save();
    }
}
