<?php

namespace Database\Seeders;

use App\Models\Biaya;
use App\Models\Jabatan;
use App\Models\Pegawai;
use App\Models\Level;
use App\Models\Siswa;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $jabatan = new Jabatan;
        $jabatan->id = Str::uuid();
        $jabatan->name = "Admin Operator";
        $jabatan->save();

        $jabatan2 = new Jabatan;
        $jabatan2->id = Str::uuid();
        $jabatan2->name = "Guru";
        $jabatan2->save();

        $jabatan3 = new Jabatan;
        $jabatan3->id = Str::uuid();
        $jabatan3->name = "Kepala Sekolah";
        $jabatan3->save();

        $jabatan4 = new Jabatan;
        $jabatan4->id = Str::uuid();
        $jabatan4->name = "Seketaris";
        $jabatan4->save();

        $level = new Level;
        $level->id = Str::uuid();
        $level->name = "Admin";
        $level->scope = "";
        $level->save();

        $level1 = $level->id;

        $level = new Level;
        $level->id = Str::uuid();
        $level->name = "Kepala Sekolah";
        $level->scope = "";
        $level->save();

        $level2 = $level->id;

        $level = new Level;
        $level->id = Str::uuid();
        $level->name = "Keuangan";
        $level->scope = "";
        $level->save();

        $level3 = $level->id;

        $pegawai = new Pegawai;
        $pegawai->id = Str::uuid();
        $pegawai->tanggal_masuk = "2021-07-30";
        $pegawai->nip = "898928319839238";
        $pegawai->name = "Jumarno";
        $pegawai->tempat_lahir = "KABUPATEN DEMAK";
        $pegawai->tanggal_lahir = "1998-07-30";
        $pegawai->jabatan_id = $jabatan2->id;
        $pegawai->telp = "087890789665";
        $pegawai->email = "jumarno@gmail.com";
        $pegawai->save();

        $pegawai1 = $pegawai->id;

        $pegawai = new Pegawai;
        $pegawai->id = Str::uuid();
        $pegawai->tanggal_masuk = "2021-08-03";
        $pegawai->nip = "898928319839238";
        $pegawai->name = "Admin";
        $pegawai->tempat_lahir = "KOTA SEMARANG";
        $pegawai->tanggal_lahir = "2000-02-01";
        $pegawai->jabatan_id = $jabatan->id;
        $pegawai->telp = "087890789005";
        $pegawai->email = "admin@gmail.com";
        $pegawai->save();

        $pegawai2 = $pegawai->id;

        $pegawai = new Pegawai;
        $pegawai->id = Str::uuid();
        $pegawai->tanggal_masuk = "2017-05-13";
        $pegawai->nip = "898928319839238";
        $pegawai->name = "Bapak Kepala Sekolah";
        $pegawai->tempat_lahir = "KOTA SEMARANG";
        $pegawai->tanggal_lahir = "1981-05-13";
        $pegawai->jabatan_id = $jabatan3->id;
        $pegawai->telp = "087890789655";
        $pegawai->email = "kepalasekolah@gmail.com";
        $pegawai->save();

        $pegawai3 = $pegawai->id;

        $pegawai = new Pegawai;
        $pegawai->id = Str::uuid();
        $pegawai->tanggal_masuk = "2017-05-13";
        $pegawai->nip = "898928319839238";
        $pegawai->name = "Keuangan";
        $pegawai->tempat_lahir = "KOTA SEMARANG";
        $pegawai->tanggal_lahir = "1981-05-13";
        $pegawai->jabatan_id = $jabatan4->id;
        $pegawai->telp = "087890789655";
        $pegawai->email = "keuangan@gmail.com";
        $pegawai->save();

        $pegawai4 = $pegawai->id;

        $pegawai = Pegawai::find($pegawai2);

        $user = new User;
        $user->id = Str::uuid();
        $user->name = $pegawai->name;
        $user->email = $pegawai->email;
        $user->profil_id = $pegawai->id;
        $user->level_id = $level1;
        $user->jenis = "Admin";
        $user->username = "admin";
        $user->password = Hash::make("123456");
        $user->save();

        $pegawai = Pegawai::find($pegawai3);

        $user = new User;
        $user->id = Str::uuid();
        $user->name = $pegawai->name;
        $user->email = $pegawai->email;
        $user->profil_id = $pegawai->id;
        $user->level_id = $level2;
        $user->jenis = "Admin";
        $user->username = "kepsek";
        $user->password = Hash::make("123456");
        $user->save();

        $pegawai = Pegawai::find($pegawai4);

        $user = new User;
        $user->id = Str::uuid();
        $user->name = $pegawai->name;
        $user->email = $pegawai->email;
        $user->profil_id = $pegawai->id;
        $user->level_id = $level3;
        $user->jenis = "Admin";
        $user->username = "keuangan";
        $user->password = Hash::make("123456");
        $user->save();

        $user = new User;
        $user->id = Str::uuid();
        $user->name = "Test Siswa";
        $user->email = "siswa@gmail.com";
        $user->profil_id = "79d56104-79cd-4880-85c8-5aae2771d167";
        $user->jenis = "NonAdmin";
        $user->password = Hash::make("123456");
        $user->save();

        $siswa = new Siswa();
        $siswa->id = "79d56104-79cd-4880-85c8-5aae2771d167";
        $siswa->nik = "812391900011312";
        $siswa->no_kk = "848954773900012";
        $siswa->jenis_kelamin = "Laki-laki";
        $siswa->agama = "Islam";
        $siswa->asal_sekolah = "SMP Futuhiyyah";
        $siswa->jurusan = "8ac17b8a-ce6e-4506-9e96-85ba3c342187";
        // $siswa->tahun_ajaran = Carbon::now()->isoFormat('Y')."/".Carbon::now()->addYear()->isoFormat('Y');
        $siswa->no_hp = "087687384744";
        $siswa->cek_pengumuman = "0";
        $siswa->total_score = 0;
        $siswa->seleksi = "Tidak Lolos";

        $cari_gelombang = Biaya::select('*')
        ->where('sampai_tanggal', '>=', Carbon::now()->format('Y-m-d'))
        ->where('mulai_tanggal', '<=', Carbon::now()->format('Y-m-d'))
        ->get();

        $biaya_lain = Biaya::select('*')
        ->where('tahun_ajaran', '2021-2022')
        ->where('jenis_biaya', '!=', "Biaya Pendaftaran")
        ->where('jenis_biaya', '!=', "Biaya Daftar Ulang")
        ->where(function($q){
            $q->where('program_studi', 'Default')
            ->orWhere('program_studi', '8ac17b8a-ce6e-4506-9e96-85ba3c342187');
        })
        ->get();

        foreach ($cari_gelombang as $key => $value) {
            $tagihan[] = $value;
        }

        foreach ($biaya_lain as $key => $value) {
            $tagihan[] = $value;
        }

        $siswa->tagihan = $tagihan;
        $siswa->tahun_ajaran = $cari_gelombang->count() > 0 ? $cari_gelombang->first()->tahun_ajaran : '';

        $siswa->save();
    }
}
