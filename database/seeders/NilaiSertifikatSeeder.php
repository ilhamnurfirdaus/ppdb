<?php

namespace Database\Seeders;

use App\Models\NilaiSertifikat;
use Illuminate\Database\Seeder;

class NilaiSertifikatSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i <= 15; $i++) {
            $no = $i;
            
            if (strlen($i) == 1) {
                $no = "0".$i;
            }

            $nilai_sertifikat = new NilaiSertifikat;
            $nilai_sertifikat->nilai_sertifikats_id = "NISE2021".$no;

            if ($i >= 1 && $i <= 3) {
                $nilai_sertifikat->nama_tingkat = "Kecamatan";
            } else if ($i >= 4 && $i <= 6) {
                $nilai_sertifikat->nama_tingkat = "Kota/Kabupaten";
            } else if ($i >= 7 && $i <= 9) {
                $nilai_sertifikat->nama_tingkat = "Provinsi";
            } else if ($i >= 10 && $i <= 12) {
                $nilai_sertifikat->nama_tingkat = "Nasional";
            } else if ($i >= 13 && $i <= 15) {
                $nilai_sertifikat->nama_tingkat = "Internasional";
            } 

            if ($i >= 1 && $i <= 3 || $i >= 7 && $i <= 9 || $i >= 13 && $i <= 15) {
                if ($i % 3 == 0) {
                    $nilai_sertifikat->peringkat = 1;
                } else if ($i % 2 == 0) {
                    $nilai_sertifikat->peringkat = 2;
                } else {
                    $nilai_sertifikat->peringkat = 3;
                }
            } else {
                if ($i % 3 == 0) {
                    $nilai_sertifikat->peringkat = 1;
                } else if ($i % 2 == 0) {
                    $nilai_sertifikat->peringkat = 3;
                } else {
                    $nilai_sertifikat->peringkat = 2;
                }
            }

            $nilai_sertifikat->nilai = $i;
            $nilai_sertifikat->tahun_ajaran = "2021-2022";
            $nilai_sertifikat->save();
        }
    }
}
