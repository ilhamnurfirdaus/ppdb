<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();

        $this->call([
            AlurPendaftaranSeeder::class,
            JurusanSeeder::class,
            PengumumanSeeder::class,
            RekeningSeeder::class,
            SyaratPendaftaranSeeder::class,
            TestSeeder::class,
            WebProfilSeeder::class,
            NilaiBerkasSeeder::class,
            NilaiSertifikatSeeder::class,
            GelombangSeeder::class,
            BayarSeeder::class,
            UserSeeder::class,
            TahunAjaranSeeder::class,
            SettingRaportSeeder::class,
        ]);
    }
}
