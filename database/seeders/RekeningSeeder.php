<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\Rekening;

class RekeningSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $rekening = new Rekening;
        $rekening->id = "12cd3811-2021-4421-90d2-902b6a4280c0";
        $rekening->nama_rekening = "BANK BNI";
        $rekening->nomor_rekening = "881237048928301";
        $rekening->atas_nama = "Bapak";
        $rekening->save();
    }
}
