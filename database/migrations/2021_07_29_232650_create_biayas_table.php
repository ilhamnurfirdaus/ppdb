<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBiayasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('biayas', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('jenis_biaya');
            $table->string('program_studi');
            $table->string('biaya');
            $table->string('nama_gelombang')->nullable();
            $table->date('mulai_tanggal')->nullable();
            $table->date('sampai_tanggal')->nullable();
            $table->string('tahun_ajaran')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('biayas');
    }
}
