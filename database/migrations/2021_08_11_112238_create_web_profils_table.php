<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWebProfilsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('web_profils', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name')->nullable();
            $table->text('alamat')->nullable();
            $table->string('email')->nullable();
            $table->string('cp')->nullable();
            $table->string('telp')->nullable();
            $table->string("logo")->nullable();
            $table->string("banner_1")->nullable();
            $table->text("informasi_pendaftaran")->nullable();
            $table->string("jenjang_sekolah")->nullable();
            $table->string("jenjang_sekolah2")->nullable();
            $table->integer("jml_raport")->nullable();
            $table->float('minimal_nilai')->nullable();
            $table->integer('maksimal_jumlah_siswa')->nullable();
            $table->integer('waktu_soal')->nullable();
            $table->string('jumlah_lolos')->nullable();
            $table->enum('tampil_test', ['Tidak', 'Ya'])->default('Ya');
            $table->enum('tampil_seleksi', ['Tidak', 'Ya'])->default('Tidak');
            $table->date('waktu_seleski')->nullable();
            $table->string('warna_header')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('web_profils');
    }
}
