<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiswaBerkasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswa_berkas', function (Blueprint $table) {
            $table->uuid('siswa_berkas_id')->primary();
            $table->string('siswa_id')->nullable();
            $table->string('name')->nullable();
            $table->string('file')->nullable();
            $table->string('score')->nullable();
            $table->text('keterangan')->nullable();
            $table->string('jenis_berkas')->nullable();
            $table->string('jurusan_id')->nullable();
            $table->string('status')->nullable();
            $table->text('catatan_admin')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswa_berkas');
    }
}
