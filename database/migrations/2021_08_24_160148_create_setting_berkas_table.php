<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingBerkasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('setting_berkas', function (Blueprint $table) {
            $table->uuid('setting_berkas_id')->primary();
            $table->string('jenis_berkas');
            $table->string('nama_berkas');
            $table->string('jurusan_id');
            $table->string('tahun_ajaran')->nullable();
            $table->text('keterangan')->nullable();
            $table->enum('status_nilai', ['Tidak Ada', 'Ada'])->default('Tidak Ada');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('setting_berkas');
    }
}
