<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSiswasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('siswas', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('nisn')->nullable();
            $table->string('nis')->nullable();
            $table->string('no_kip')->nullable();
            $table->string('no_kk')->nullable();
            $table->string('nik')->nullable();
            // $table->string('nama_lengkap')->nullable();
            $table->string('tempat_lahir')->nullable();
            $table->date("tanggal_lahir")->nullable();
            $table->string('jenis_kelamin')->nullable();
            $table->string('agama')->nullable();
            $table->string('no_hp')->nullable();
            // $table->string('no_wa')->nullable();
            // $table->string('email')->nullable();
            $table->string('provinsi')->nullable();
            $table->string('kota_kabupaten')->nullable();
            $table->string('kecamatan')->nullable();
            $table->string('desa_kelurahan')->nullable();
            $table->string('alamat')->nullable();
            // $table->string('tinggal_bersama')->nullable();
            $table->string('ukuran_baju')->nullable();
            // $table->string('kewarganegaraan')->nullable();
            // $table->string('jenis_slta')->nullable();
            // $table->string('nama_sekolah')->nullable();
            // $table->string('tahun_ajaran')->nullable();
            $table->string('jurusan')->nullable();

            // $table->string('nama_ayah')->nullable();
            // $table->string('pekerjaan_ayah')->nullable();
            // $table->string('nama_ibu')->nullable();
            // $table->string('pekerjaan_ibu')->nullable();
            // $table->string('nama_wali')->nullable();
            // $table->string('pekerjaan_wali')->nullable();
            // $table->string('transport')->nullable();  

            // $table->string('berkas_score')->nullable();
            $table->string('cek_soal')->nullable();
            $table->dateTime('start_date')->nullable();
            $table->dateTime('end_date')->nullable();
            $table->string('soal_score')->nullable();
            $table->string("jenjang")->nullable();
            $table->string('cek_pengumuman')->nullable();
            $table->enum("jenis_kepsek", ['Tidak', 'Auto', 'Manual'])->default('Tidak');
            $table->string("total_score")->nullable();
            $table->string("poin_kepsek")->nullable();
            
            // $table->string('ukuran_baju_cm')->nullable();
            // $table->string('ukuran_celana_cm')->nullable();
            $table->string('asal_sekolah')->nullable();
            // $table->string('anak_ke')->nullable();
            // $table->string('tinggi_badan_cm')->nullable();
            // $table->string('jumlah_saudara')->nullable();
            // $table->string('berat_badan')->nullable();

            // $table->string('rt')->nullable();
            // $table->string('rw')->nullable();
            // $table->string('kode_pos')->nullable();
            // $table->string('jarak_ke_sekolah_meter')->nullable();

            // $table->string('nik_ayah')->nullable();
            // $table->string('tempat_lahir_ayah')->nullable();
            // $table->date("tanggal_lahir_ayah")->nullable(); 
            // $table->string('pendidikan_ayah')->nullable();
            // $table->string('penghasilan_ayah')->nullable();
            // $table->string('no_hp_ayah')->nullable();

            // $table->string('nik_ibu')->nullable();
            // $table->string('tempat_lahir_ibu')->nullable();
            // $table->date("tanggal_lahir_ibu")->nullable(); 
            // $table->string('pendidikan_ibu')->nullable();
            // $table->string('penghasilan_ibu')->nullable();
            // $table->string('no_hp_ibu')->nullable();

            // $table->string('file_ijazah')->nullable();
            // $table->string('file_ktp')->nullable();
            // $table->string('file_ktp_ayah')->nullable();
            // $table->string('file_ktp_ibu')->nullable();

            $table->string('seleksi')->nullable();
            
            $table->string('tahun_ajaran')->nullable();
            $table->text('tagihan')->nullable();
            
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('siswas');
    }
}
