<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTahunAjaransTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tahun_ajarans', function (Blueprint $table) {
            $table->uuid('tahun_ajaran')->primary();
            $table->enum('tampil_raport', ['Tidak', 'Ya'])->default('Ya');
            $table->enum('tampil_sertifikat', ['Tidak', 'Ya'])->default('Ya');
            $table->enum('tampil_berkas', ['Tidak', 'Ya'])->default('Ya');
            $table->enum('tampil_test', ['Tidak', 'Ya'])->default('Ya');
            $table->integer('waktu_soal')->nullable();
            $table->integer("jml_raport")->nullable();
            $table->float('minimal_nilai')->nullable();
            $table->integer('maksimal_jumlah_siswa')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tahun_ajarans');
    }
}
