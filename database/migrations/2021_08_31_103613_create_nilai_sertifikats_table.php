<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateNilaiSertifikatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('nilai_sertifikats', function (Blueprint $table) {
            $table->uuid('nilai_sertifikats_id')->primary();
            $table->string('nama_tingkat')->nullable();
            $table->integer('peringkat')->nullable();
            $table->integer('nilai')->nullable();
            $table->string('tahun_ajaran')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('nilai_sertifikats');
    }
}
