<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateJurusansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jurusans', function (Blueprint $table) {
            $table->uuid('id')->primary();
            $table->string('name');
            $table->text('keterangan')->nullable();
            $table->string('status')->nullable();
            $table->integer('waktu')->nullable();
            $table->float('minimal_nilai')->nullable();
            $table->integer('maksimal_jumlah_siswa')->nullable();
            $table->string('tahun_ajaran')->nullable();
            $table->string('akreditasi')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('jurusans');
    }
}
