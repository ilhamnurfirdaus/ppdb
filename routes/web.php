<?php

use App\Http\Controllers\AlurPendaftaranController;
use App\Http\Controllers\BayarAkhirController;
use App\Http\Controllers\BayarController;
use App\Http\Controllers\BayarsAkhirController;
use App\Http\Controllers\BayarsAwalController;
use App\Http\Controllers\BerandaController;
use App\Http\Controllers\BerkasController;
use App\Http\Controllers\BerkassController;
use App\Http\Controllers\BiayaController;
use App\Http\Controllers\BiayaNewController;
use App\Http\Controllers\DaftarController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\DisplayJurusanController;
use App\Http\Controllers\JabatanController;
use App\Http\Controllers\LevelController;
use App\Http\Controllers\PegawaiController;
use App\Http\Controllers\PengumumanController;
use App\Http\Controllers\RekeningController;
use App\Http\Controllers\SearchController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\InvoiceController;
use App\Http\Controllers\JurusanController;
use App\Http\Controllers\KepsekController;
use App\Http\Controllers\NilaiBerkasController;
use App\Http\Controllers\NilaiSertifikatController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\SeleksiController;
use App\Http\Controllers\SeleksisController;
use App\Http\Controllers\SyaratPendaftaranController;
use App\Http\Controllers\TestController;
use App\Http\Controllers\TestsController;
use App\Http\Controllers\GelombangController;
use App\Http\Controllers\WebProfilController;
use App\Http\Controllers\ProfilController;
use App\Http\Controllers\GrafikController;
use App\Http\Controllers\PendaftaranController;
use App\Http\Controllers\TahunAjaranController;
use App\Http\Middleware\CheckAdmin;
use App\Http\Middleware\CheckNonAdmin;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [HomeController::class, 'index'])->name('home');
Route::get('informasi-pendaftaran-pdf/{tahun_ajaran}', [HomeController::class, 'informasi_pendaftaran_pdf']);
Route::get('pendaftaran', [HomeController::class, 'pendaftaran']);

Auth::routes();

Route::prefix("search")->group(function () {
    Route::get('provinsi', [SearchController::class, 'provinsi']);
    Route::get('kotas', [SearchController::class, 'kotas']);
    Route::get('kota_kabupaten/{provinsi}', [SearchController::class, 'kota_kabupaten']);
    Route::get('kecamatan/{kota_kabupaten}', [SearchController::class, 'kecamatan']);
    Route::get('desa_kelurahan/{kecamatan}', [SearchController::class, 'desa_kelurahan']);
});

Route::prefix("siswa")->middleware(CheckNonAdmin::class)->group(function () {
    Route::get('/', [BerandaController::class, 'index']);

    Route::prefix("daftar")->group(function () {
        Route::get('/awal', [DaftarController::class, 'awal']);
        Route::post('/update-awal/{id}', [DaftarController::class, 'update_awal']);

        Route::get('/data-diri', [DaftarController::class, 'data_diri']);
        Route::post('/update-data-diri/{id}', [DaftarController::class, 'update_data_diri']);

        Route::get('/data-alamat', [DaftarController::class, 'data_alamat']);
        Route::post('/update-data-alamat/{id}', [DaftarController::class, 'update_data_alamat']);

        Route::get('/data-ortu', [DaftarController::class, 'data_ortu']);
        Route::post('/update-data-ortu/{id}', [DaftarController::class, 'update_data_ortu']);
    });

    Route::prefix("bayar-awal")->group(function () {
        Route::get('/', [BayarController::class, 'index']);
        Route::post('/store', [BayarController::class, 'store']);
        Route::get('/edit/{bayar}', [BayarController::class, 'edit']);
        Route::post('/update/{id}', [BayarController::class, 'update']);
        Route::get('/detil/{bayar}', [BayarController::class, 'detil']);
        Route::get('/bukti/{bayar}', [BayarController::class, 'bukti']);
        Route::get('/destroy/{id}', [BayarController::class, 'destroy']);
    });

    Route::prefix("bayar-akhir")->group(function () {
        Route::get('/', [BayarAkhirController::class, 'index']);
        Route::post('/store', [BayarAkhirController::class, 'store']);
        Route::post('/update/{id}', [BayarAkhirController::class, 'update']);
        Route::get('/destroy/{id}', [BayarAkhirController::class, 'destroy']);
    });

    Route::prefix("berkas")->group(function () {
        Route::get('/awal', [BerkasController::class, 'awal']);
        Route::post('/update-awal/{id}', [BerkasController::class, 'update_awal']);

        Route::post('/update-raport/{id}', [BerkasController::class, 'update_raport']);
        Route::post('/update-sertifikat/{id}', [BerkasController::class, 'update_sertifikat']);

        Route::post('/sertifikat-store', [BerkasController::class, 'sertifikat_store']);
        Route::post('/sertifikat-update/{id}', [BerkasController::class, 'sertifikat_update']);
        Route::get('/sertifikat-destroy/{id}', [BerkasController::class, 'sertifikat_destroy']);

        Route::get('/akhir', [BerkasController::class, 'akhir']);
        Route::post('/update-akhir/{id}', [BerkasController::class, 'update_akhir']);

        Route::get('/gambar_berkas/{siswa_berkas}', [BerkasController::class, 'gambar_berkas']);
        Route::get('/gambar_raport/{siswa_raport}', [BerkasController::class, 'gambar_raport']);
        Route::get('/gambar_sertifikat/{sertifikat}', [BerkasController::class, 'gambar_sertifikat']);
    });

    Route::prefix("soal")->group(function () {
        Route::get('/', [TestController::class, 'index']);
        Route::get('/mulai/{id}', [TestController::class, 'mulai']);
        Route::post('/selesai/{id}', [TestController::class, 'selesai']);
    });

    Route::prefix("pendaftaran/{tahun_ajaran}")->group(function () {
        
    });

    Route::prefix("seleksi")->group(function () {
        Route::get('/', [SeleksiController::class, 'index']);
    });

    Route::prefix("pengumuman")->group(function () {
        Route::get('/', [NotificationController::class, 'index']);        
    });
});

Route::prefix("admin")->middleware(CheckAdmin::class)->group(function () {
    Route::get('/', [DashboardController::class, 'index']);
    Route::get('grafik_sekolah', [GrafikController::class, 'grafik_sekolah']);
    Route::get('grafik_jurusan', [GrafikController::class, 'grafik_jurusan']);
    Route::get('grafik_jenis_kelamin', [GrafikController::class, 'grafik_jenis_kelamin']);
    
    Route::get('pendaftaran', [PendaftaranController::class, 'pendaftaran']);
    Route::get('save_switch_raport/{id}', [PendaftaranController::class, 'save_switch_raport']);
    Route::get('save_switch_sertifikat/{id}', [PendaftaranController::class, 'save_switch_sertifikat']);
    Route::get('save_switch_berkas/{id}', [PendaftaranController::class, 'save_switch_berkas']);
    Route::get('save_switch_test/{id}', [PendaftaranController::class, 'save_switch_test']);
    Route::post('save_waktu_soal/{id}', [PendaftaranController::class, 'save_waktu_soal']);
    Route::post('save_jml_raport/{id}', [PendaftaranController::class, 'save_jml_raport']);
    Route::get('get_nilai_sertifikat', [PendaftaranController::class, 'get_nilai_sertifikat']);
    Route::get('get_syarat_pendaftaran', [PendaftaranController::class, 'get_syarat_pendaftaran']);
    Route::get('get_setting_raport', [PendaftaranController::class, 'get_setting_raport']);
    Route::post('save_setting_raport', [PendaftaranController::class, 'save_setting_raport']);
    Route::get('edit_sertifikat/{id}', [PendaftaranController::class, 'edit_sertifikat']);
    Route::post('update_sertifikat/{id}', [PendaftaranController::class, 'update_sertifikat']);
    Route::get('add_syarat_pendaftaran', [PendaftaranController::class, 'add_syarat_pendaftaran']);



    Route::prefix("tahun-ajaran")->group(function () {
        Route::get('/', [TahunAjaranController::class, 'index']);
        Route::get('/gelombang/{tahun_ajaran}', [TahunAjaranController::class, 'gelombang']);
        Route::get('/create', [TahunAjaranController::class, 'create']);
        Route::post('/store', [TahunAjaranController::class, 'store']);
        Route::get('/edit/{tahun_ajaran}', [TahunAjaranController::class, 'edit']);
        Route::post('/update/{tahun_ajaran}', [TahunAjaranController::class, 'update']);
        Route::get('/destroy/{tahun_ajaran}', [TahunAjaranController::class, 'destroy']);
    });

    Route::prefix("setting-biaya")->group(function () {
        Route::get('/', [BiayaNewController::class, 'index']);
        Route::get('/uang-pendaftaran/{tahun_ajaran}', [BiayaNewController::class, 'uang_pendaftaran']);
        Route::get('/uang-gedung/{tahun_ajaran}', [BiayaNewController::class, 'uang_gedung']);
        Route::get('/biaya-jurusan/{tahun_ajaran}', [BiayaNewController::class, 'biaya_jurusan']);
        Route::get('/biaya-lain/{tahun_ajaran}', [BiayaNewController::class, 'biaya_lain']);
        Route::get('/biayas/{tahun_ajaran}', [BiayaNewController::class, 'biayas']);
        Route::get('/create', [BiayaNewController::class, 'create']);
        Route::post('/store', [BiayaNewController::class, 'store']);
        Route::get('/edit/{tahun_ajaran}', [BiayaNewController::class, 'edit']);
        Route::get('/destroy/{tahun_ajaran}', [BiayaNewController::class, 'destroy']);
    });

    Route::prefix("rekening")->group(function () {
        Route::get('/', [RekeningController::class, 'index']);
        Route::post('/store', [RekeningController::class, 'store']);
        Route::get('/edit/{rekening}', [RekeningController::class, 'edit']);
        Route::post('/update/{id}', [RekeningController::class, 'update']);
        Route::get('/destroy/{id}', [RekeningController::class, 'destroy']);
    });

    Route::prefix("pengumuman")->group(function () {
        Route::get('/', [PengumumanController::class, 'index']);
        Route::post('/store', [PengumumanController::class, 'store']);
        Route::post('/update/{id}', [PengumumanController::class, 'update']);
        Route::get('/destroy/{id}', [PengumumanController::class, 'destroy']);
    });

    Route::prefix("konten-management")->group(function () {
        Route::get('/edit-web-profil', [WebProfilController::class, 'edit_web_profil']);
        Route::post('/update-web-profil/{id}', [WebProfilController::class, 'update_web_profil']);

        Route::get('/edit-banner', [WebProfilController::class, 'edit_banner']);
        Route::post('/update-banner/{id}', [WebProfilController::class, 'update_banner']);

        Route::get('/edit-informasi-pendaftaran', [WebProfilController::class, 'edit_informasi_pendaftaran']);
        Route::post('/update-informasi-pendaftaran/{id}', [WebProfilController::class, 'update_informasi_pendaftaran']);
    });

    Route::prefix("alur-pendaftaran")->group(function () {
        Route::get('/', [AlurPendaftaranController::class, 'index']);
        Route::post('/store', [AlurPendaftaranController::class, 'store']);
        Route::post('/update/{id}', [AlurPendaftaranController::class, 'update']);
        Route::get('/destroy/{id}', [AlurPendaftaranController::class, 'destroy']);
    });

    Route::prefix("syarat-pendaftaran")->group(function () {
        Route::get('/', [SyaratPendaftaranController::class, 'index']);
        Route::post('/store', [SyaratPendaftaranController::class, 'store']);
        Route::get('/edit/{id}', [SyaratPendaftaranController::class, 'edit']);
        Route::post('/update/{id}', [SyaratPendaftaranController::class, 'update']);
        Route::get('/destroy/{id}', [SyaratPendaftaranController::class, 'destroy']);
    });

    Route::prefix("nilai-berkas")->group(function () {
        Route::get('/', [NilaiBerkasController::class, 'index']);
        Route::post('/store', [NilaiBerkasController::class, 'store']);
        Route::get('/edit/{id}', [NilaiBerkasController::class, 'edit']);
        Route::post('/update/{id}', [NilaiBerkasController::class, 'update']);
        Route::get('/destroy/{id}', [NilaiBerkasController::class, 'destroy']);
    });

    Route::prefix("nilai-sertifikat")->group(function () {
        Route::get('/', [NilaiSertifikatController::class, 'index']);
        Route::post('/update/{id}', [NilaiSertifikatController::class, 'update']);
    });

    Route::prefix("jurusan")->group(function () {
        Route::get('/', [JurusanController::class, 'index']);
        Route::get('/berkas/{jurusan}', [JurusanController::class, 'berkas']);
        Route::get('/tahun_ajaran/{tahun_ajaran}', [JurusanController::class, 'tahun_ajaran']);
        Route::post('/store', [JurusanController::class, 'store']);
        Route::get('/edit/{id}', [JurusanController::class, 'edit']);
        Route::post('/update/{id}', [JurusanController::class, 'update']);
        Route::get('/destroy/{id}', [JurusanController::class, 'destroy']);

        Route::get('/update-status/{id}/{status}', [JurusanController::class, 'update_status']);
    });

    Route::prefix("display-jurusan")->group(function () {
        Route::get('/', [DisplayJurusanController::class, 'index']);
        Route::get('/update-status/{id}/{status}', [DisplayJurusanController::class, 'update_status']);
    });

    Route::prefix("biaya")->group(function () {
        Route::get('/', [BiayaController::class, 'index']);
        Route::post('/store', [BiayaController::class, 'store']);
        Route::get('/edit/{id}', [BiayaController::class, 'edit']);
        Route::post('/update/{id}', [BiayaController::class, 'update']);
        Route::get('/destroy/{id}', [BiayaController::class, 'destroy']);
    });

    Route::prefix("gelombang")->group(function () {
        Route::get('/', [GelombangController::class, 'index']);
        Route::post('/store', [GelombangController::class, 'store']);
        Route::get('/edit/{id}', [GelombangController::class, 'edit']);
        Route::post('/update/{id}', [GelombangController::class, 'update']);
        Route::get('/destroy/{id}', [GelombangController::class, 'destroy']);
    });

    Route::prefix("level")->group(function () {
        Route::get('/', [LevelController::class, 'index']);
        Route::post('/store', [LevelController::class, 'store']);
        Route::post('/update/{id}', [LevelController::class, 'update']);
        Route::get('/destroy/{id}', [LevelController::class, 'destroy']);
    });

    Route::prefix("jabatan")->group(function () {
        Route::get('/', [JabatanController::class, 'index']);
        Route::post('/store', [JabatanController::class, 'store']);
        Route::post('/update/{id}', [JabatanController::class, 'update']);
        Route::get('/destroy/{id}', [JabatanController::class, 'destroy']);
    });

    Route::prefix("pegawai")->group(function () {
        Route::get('/', [PegawaiController::class, 'index']);
        Route::post('/store', [PegawaiController::class, 'store']);
        Route::post('/update/{id}', [PegawaiController::class, 'update']);
        Route::get('/destroy/{id}', [PegawaiController::class, 'destroy']);

        Route::get('export-excel', [PegawaiController::class, 'export_excel']);
        Route::post('import-excel', [PegawaiController::class, 'import_excel']);

        Route::get('/export_pdf/{q}/{tgl_mulai}/{tgl_berakhir}', [PegawaiController::class, 'export_pdf']);
    });

    Route::prefix("user")->group(function () {
        Route::get('/', [UserController::class, 'index']);
        Route::post('/store', [UserController::class, 'store']);
        Route::post('/update/{id}', [UserController::class, 'update']);
        Route::get('/destroy/{id}', [UserController::class, 'destroy']);
    });
    
    Route::prefix("bayar-awal")->group(function () {
        Route::get('/', [BayarsAwalController::class, 'index']);        
        Route::get('/bukti/{bayar}', [BayarsAwalController::class, 'bukti']);
        Route::get('/detil/{bayar}', [BayarsAwalController::class, 'detil']);
        Route::get('/edit/{bayar}', [BayarsAwalController::class, 'edit']);
        Route::post('/update/{id}', [BayarsAwalController::class, 'update']);

        Route::get('/export-excel', [BayarsAwalController::class, 'export_excel']);
        Route::post('/import-excel', [BayarsAwalController::class, 'import_excel']);

        Route::get('/export_pdf', [BayarsAwalController::class, 'export_pdf']);
    });

    Route::prefix("bayar-akhir")->group(function () {
        Route::get('/', [BayarsAkhirController::class, 'index']);        
        Route::post('/update/{id}', [BayarsAkhirController::class, 'update']);

        Route::get('/export_siswa', [BayarsAkhirController::class, 'export_siswa'])->name('export_siswa');
        Route::get('/generate_nis', [BayarsAkhirController::class, 'generate_nis']);
        Route::get('/export-excel', [BayarsAkhirController::class, 'export_excel']);
        Route::post('/import-excel', [BayarsAkhirController::class, 'import_excel']);
    });

    Route::prefix("berkas")->group(function () {
        Route::prefix("awal")->group(function () {
            Route::get('/', [BerkassController::class, 'awal_index']);
            Route::get('/seleksi-index/{id}', [BerkassController::class, 'awal_seleksi_index']);
            Route::post('/seleksi-post/{id}', [BerkassController::class, 'awal_seleksi_post']);
            Route::get('pdf', [BerkassController::class, 'pdf']);
            Route::get('export_excel', [BerkassController::class, 'export_excel']);
        });

        Route::prefix("akhir")->group(function () {
            Route::get('/', [BerkassController::class, 'akhir_index']);        
            Route::get('/seleksi-index/{id}', [BerkassController::class, 'akhir_seleksi_index']);
            Route::post('/seleksi-post/{siswa_berkas_id}', [BerkassController::class, 'akhir_seleksi_post']);
        });

        Route::get('grafik', [BerkassController::class, 'grafik']);
        Route::get('{id}/tahun_ajaran/{tahun_ajaran}', [BerkassController::class, 'tahun_ajaran']);
    });

    Route::prefix("soal")->group(function () {
        Route::get('/', [TestsController::class, 'index']);
        Route::post('/waktu_soal', [TestsController::class, 'waktu_soal']);
        Route::post('/store', [TestsController::class, 'store']);
        Route::get('/edit/{test}', [TestsController::class, 'edit']);
        Route::post('/update/{id}', [TestsController::class, 'update']);
        Route::get('/destroy/{id}', [TestsController::class, 'destroy']);
        Route::post('/import', [TestsController::class, 'import']);
    });

    Route::prefix("seleksi")->group(function () {
        Route::get('/', [SeleksisController::class, 'index']);        
        Route::post('/store', [SeleksisController::class, 'store']);
        Route::post('/buka', [SeleksisController::class, 'buka']);

        Route::get('/export_pdf', [SeleksisController::class, 'export_pdf']);
    });

    Route::prefix("kepsek")->group(function () {
        Route::get('/', [KepsekController::class, 'index']);
        
        Route::get('/berkass/{siswa}', [KepsekController::class, 'berkass']);
        Route::get('/raports/{siswa}', [KepsekController::class, 'raports']);
        Route::get('/sertifikats/{siswa}', [KepsekController::class, 'sertifikats']);
        Route::get('/sertifikat/{sertifikat}', [KepsekController::class, 'sertifikat']);

        Route::get('/edit/{siswa}', [KepsekController::class, 'edit']);
        Route::post('/update/{id}', [KepsekController::class, 'update']);
    });

});

Route::middleware(['auth'])->group(function () {
    Route::get('invoice/{id}/{type}', [InvoiceController::class, 'invoice']);

    Route::prefix("profile")->group(function () {
        Route::get('/', [ProfilController::class, 'index']);        
        Route::post('/update/{id}', [ProfilController::class, 'update']);
    });

    Route::prefix("bayar")->group(function () {
        Route::get('/invoice/{bayar}', [BayarsAwalController::class, 'invoice']);
        Route::get('/bukti/{bayar}', [BayarsAwalController::class, 'bukti']);
        Route::get('/detil/{bayar}', [BayarsAwalController::class, 'detil']);
    });

    Route::get('gambar_berkas/{siswa_berkas}', [BerkasController::class, 'gambar_berkas']);
    Route::get('gambar_raport/{siswa_raport}', [BerkasController::class, 'gambar_raport']);
    Route::get('gambar_sertifikat/{sertifikat}', [BerkasController::class, 'gambar_sertifikat']);
});
