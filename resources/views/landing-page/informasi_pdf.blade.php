@php
	function rupiah($angka){
		$hasil_rupiah = "Rp " . number_format($angka,2,',','.');
		return $hasil_rupiah;	
	}
@endphp
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Data Pegawai</title>
        <body>
            <style type="text/css">
                .footer { position: fixed; left: 0px; bottom: -50px; right: 0px; height: 50px;text-align: center;font-size: 10px;}
                .footer .pagenum:before { content: counter(page); }

                .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
                .tg td{font-family:Arial;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
                .tg th{font-family:Arial;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
                .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
            </style>

            <div class="footer">
              Page <span class="pagenum"></span>
            </div>
  
            <div style="font-family:Arial; font-size:12px;">
                <center><h2>PPDB SEKOLAH TAHUN AJARAN {{ $tahun_ajaran->tahun_ajaran }}</h2></center>  
            </div>
            <br>
            <div style="text-align: center;font-size: 12px;">
                <div style="text-align: left; margin-top: 40px;">
                    <p style="text-align: left;">
                        Siswa yang diterima pada tahun ajaran ini yaitu jumlah kuota maksimal {{ $tahun_ajaran->maksimal_jumlah_siswa }} siswa
                        @if ($tahun_ajaran->tampil_raport == "Ya")
                        , dengan syarat minimal nilai raport {{ $tahun_ajaran->minimal_nilai }}
                        @endif
                        .
                    </p>
                    <p style="margin-top: 10px; ">Biaya Pendaftaran :</p>
                </div>
                <div class="table-responsive-sm" style="margin-top: 10px;font-size: 12px;">
                    <table class="tg">
                        <thead>
                        <tr>
                            <th class="tg-3wr7">Nama Gelombang</th>
                            <th class="tg-3wr7">Nominal</th>
                            <th class="tg-3wr7">Tanggal Mulai</th>
                            <th class="tg-3wr7">Tanggal Selesai</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($tahun_ajaran->biaya
                            ->where('jenis_biaya', 'Biaya Pendaftaran')
                            ->where('program_studi', 'Default') as $key=>$item)
                            <tr>
                                <td class="tg-rv4w">{{$item->nama_gelombang}}</td>
                                <td class="tg-rv4w">{{ rupiah($item->biaya) }}</td>
                                <td class="tg-rv4w">
                                    {{Carbon\Carbon::parse($item->mulai_tanggal)->isoFormat('D MMMM Y')}}
                                </td>
                                <td class="tg-rv4w">
                                    {{Carbon\Carbon::parse($item->sampai_tanggal)->isoFormat('D MMMM Y')}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <p style="margin-top: 20px; text-align: left">Biaya Uang Gedung :</p>
                <div class="table-responsive-sm" style="margin-top: 10px;font-size: 12px;">
                    <table class="tg">
                        <thead>
                        <tr>
                            <th class="tg-3wr7">Nama Gelombang</th>
                            <th class="tg-3wr7">Nominal</th>
                            <th class="tg-3wr7">Tanggal Mulai</th>
                            <th class="tg-3wr7">Tanggal Selesai</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($tahun_ajaran->biaya
                            ->where('jenis_biaya', 'Biaya Daftar Ulang')
                            ->where('program_studi', 'Default') as $key=>$item)
                            <tr>
                                <td class="tg-rv4w">{{$item->nama_gelombang}}</td>
                                <td class="tg-rv4w">{{ rupiah($item->biaya) }}</td>
                                <td class="tg-rv4w">
                                    {{Carbon\Carbon::parse($item->mulai_tanggal)->isoFormat('D MMMM Y')}}
                                </td>
                                <td class="tg-rv4w">
                                    {{Carbon\Carbon::parse($item->sampai_tanggal)->isoFormat('D MMMM Y')}}
                                </td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
                <p style="margin-top: 20px; text-align: left">Biaya Lain-Lain :</p>
                <div class="table-responsive-sm" style="margin-top: 10px;font-size: 12px;">
                    <table class="tg">
                        <thead>
                        <tr>
                            <th class="tg-3wr7">Nama Biaya</th>
                            <th class="tg-3wr7">Nominal</th>
                        </tr>
                        </thead>
                        <tbody>
                            @foreach ($tahun_ajaran->bls() as $key=>$item)
                            <tr>
                                <td class="tg-rv4w">{{$item->jenis_biaya}}</td>
                                <td class="tg-rv4w">{{ rupiah($item->biaya) }}</td>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                </div>
            </div>            
        </body>
    </head>
</html>