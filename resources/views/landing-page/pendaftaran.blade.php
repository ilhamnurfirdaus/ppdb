@extends('layouts.landing-page')
@section('main')

<section class="features-area section-bg">
	<div class="container">
		{{-- <div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12 d-flex">
				<h3><i class="fa fa-info-circle"></i> Ayo Segera Daftar!</h3>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 d-flex justify-content-end">
                <button type="button" class="btn btn-primary" onclick="openLogin();">MASUK</button>
			</div>
		</div>
		<hr> --}}
        
        @if ($tutup_pendaftaran == "Ya")
        <div class="row justify-content-center">
			<div class="col-lg-9 col-md-9 col-sm-9">
                <div class="main-card mb-3 card bg-warning text-white">
                    <div class="card-body">
                        <strong>Maaf, Anda belum bisa mengakses halaman ini - </strong> Tunggu Gelombang Selanjutnya!
                    </div>
                </div>
            </div>
        </div>
        @else
		<div class="row justify-content-center">
			<div class="col-lg-9 col-md-9 col-sm-9">
                <div class="card">
                    <div class="card-header">
						<div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 d-flex">
                                <h5>Ayo Segera Daftar!</h5>
                            </div>
                            {{-- <div class="col-lg-6 col-md-6 col-sm-12 d-flex justify-content-end">
                                @if (!Auth::check())
                                <button type="button" class="btn btn-sm btn-primary" onclick="openLogin();">MASUK</button>
                                @endif
                            </div> --}}
                        </div>
					</div>
                    <div class="card-body">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf 
        
                            <input type="hidden" name="type" value="register">
        
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="name">{{ __('Nama Lengkap') }} *</label>
            
                                    <input id="name" type="text" class=" form-control form-control-sm @if (old("type") == "register") @error('name') is-invalid @enderror @endif" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Isi nama lengkap anda">
            
                                    @if (old("type") == "register")
                                    @error('name')
                                        <span class="invalid-feedback " role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    @endif
                                </div>
                                
                                <div class="form-group col-md-6">
                                    <label>NIK *</label>
                                    <input type="number" class="form-control form-control-sm" name="nik" value="{{old('nik')}}" min="0" max="999999999999" required>
                                </div>
                                
                            </div>

                            
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Provinsi Asal Sekolah*</label>
                                    <select id="provinsi" name="provinsi" class="form-control form-control-sm" style="width: 100%;padding: 10px;margin-top: 10px ; font-size: 19px;" required>
                                        <option value="">Cari Provinsi</option>
                                        @if (old("provinsi"))
                                        <option value="{{ old("provinsi") }}" selected>{{ old("provinsi") }}</option>
                                        @endif
                                    </select>
                                </div>
    
                                <div class="form-group col-md-6">
                                    <label>Kota / Kabupaten Asal Sekolah*</label>
                                    <select id="kota_kabupaten" name="kota_kabupaten" class="form-control form-control-sm" style="width: 100%;padding: 10px;margin-top: 10px ; font-size: 19px;" @if(old("kota_kabupaten")) @else disabled @endif required>
                                        @if (old("kota_kabupaten"))
                                        <option value="{{ old("kota_kabupaten") }}" selected>{{ old("kota_kabupaten") }}</option>
                                        @endif
                                    </select>
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="name">Asal Sekolah *</label>
                                    <select id="institusi" name="asal_sekolah" class="form-control form-control-sm" style="width: 100%;padding: 10px;margin-top: 10px ; font-size: 19px;" @if(old("kota_kabupaten")) @else disabled @endif required>
                                        @if (old("asal_sekolah"))
                                        <option value="{{ old("asal_sekolah") }}" selected>{{ old("asal_sekolah") }}</option>
                                        @endif
                                    </select>
            
                                    {{-- <input type="text" class="form-control form-control-sm" name="asal_sekolah" value="{{old('asal_sekolah')}}" required> --}}
                                </div>
    
                                <div class="form-group col-md-6">
                                    <label>Nomor KK *</label>
                                    <input type="number" class="form-control form-control-sm" name="no_kk" value="{{old('no_kk')}}" min="0" required>
                                </div>
                            </div>

                            {{-- <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="tempat">Tempat Lahir</label>
                                    <input type="text" value="{{old('tempat_lahir')}}" class="form-control form-control-sm" name="tempat_lahir" required>
                                </div>
                                <div class="form-group col-md-6">
                                    <label for="tgllahir">Tanggal Lahir</label>
                                    <input type="date" class="form-control form-control-sm" value="{{Carbon\Carbon::parse(old('tanggal_lahir'))->format('Y-m-d')}}" name="tanggal_lahir" required>
                                </div>
                            </div> --}}

                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label>Jenis Kelamin *</label>
                                    <div class="form-row">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="jenis_kelamin" value="Laki-laki" @if (old('jenis_kelamin') == "Laki-laki") checked @endif required>
                                            <label class="form-check-label">
                                                Laki-laki
                                            </label>
                                        </div>
                                        <div class="form-check mx-3">
                                            <input class="form-check-input" type="radio" name="jenis_kelamin" value="Perempuan" @if (old('jenis_kelamin') == "Perempuan") checked @endif>
                                            <label class="form-check-label">
                                                Perempuan
                                            </label>
                                        </div>
                                    </div>
                                    {{-- <select class="form-control form-control-sm" name="jenis_kelamin" required>
                                        <option value="">--- Pilih Jenis Kelamin ---</option>
                                        <option value="Laki-laki" @if (old('jenis_kelamin') == "Laki-laki") selected @endif>Laki-Laki</option>
                                        <option value="Perempuan" @if (old('jenis_kelamin') == "Perempuan") selected @endif>Perempuan</option>
                                    </select> --}}
                                </div>
    
                                <div class="form-group col-md-8">
                                    <label>Agama *</label>
                                    <div class="form-row">
                                        <div class="form-check">
                                            <input class="form-check-input" type="radio" name="agama" value="Budha" @if (old('agama') == "Budha") checked @endif required>
                                            <label class="form-check-label">
                                                Budha
                                            </label>
                                        </div>
                                        <div class="form-check mx-2">
                                            <input class="form-check-input" type="radio" name="agama" value="Hindu" @if (old('agama') == "Hindu") checked @endif>
                                            <label class="form-check-label">
                                                Hindu
                                            </label>
                                        </div>
                                        <div class="form-check mx-2">
                                            <input class="form-check-input" type="radio" name="agama" value="Islam" @if (old('agama') == "Islam") checked @endif>
                                            <label class="form-check-label">
                                                Islam
                                            </label>
                                        </div>
                                        <div class="form-check mx-2">
                                            <input class="form-check-input" type="radio" name="agama" value="Katholik" @if (old('agama') == "Katholik") checked @endif>
                                            <label class="form-check-label">
                                                Katholik
                                            </label>
                                        </div>
                                        <div class="form-check mx-2">
                                            <input class="form-check-input" type="radio" name="agama" value="Kristen" @if (old('agama') == "Kristen") checked @endif>
                                            <label class="form-check-label">
                                                Kristen
                                            </label>
                                        </div>
                                        <div class="form-check mx-2">
                                            <input class="form-check-input" type="radio" name="agama" value="Konghucu" @if (old('agama') == "Konghucu") checked @endif>
                                            <label class="form-check-label">
                                                Konghucu
                                            </label>
                                        </div>
                                    </div>
                                    {{-- <select class="form-control form-control-sm" name="agama" required>
                                        <option value="">--- Pilih Agama ---</option>
                                        <option value="Budha" @if (old('agama') == "Budha") selected @endif>Budha</option>
                                        <option value="Hindu" @if (old('agama') == "Hindu") selected @endif>Hindu</option>
                                        <option value="Islam" @if (old('agama') == "Islam") selected @endif>Islam</option>
                                        <option value="Katholik" @if (old('agama') == "Katholik") selected @endif>Katholik</option>
                                        <option value="Kristen" @if (old('agama') == "Kristen") selected @endif>Kristen</option>
                                        <option value="Konghucu" @if (old('agama') == "Konghucu") selected @endif>Konghucu</option>
                                    </select> --}}
                                </div>
                            </div>

                            @if ($web_profil->jenjang_sekolah == "SLTA")
                            <div class="form-group">
                                <label>Jurusan *</label>
                                <select class="form-control form-control-sm" name="jurusan" required>
                                    <option value="">--- Pilih Jurusan ---</option>
                                    @foreach ($jurusans as $item)
                                    <option value="{{ $item->id }}" @if (old('jurusan') == $item->id) selected @elseif(Request::input('jurusan') == $item->id) selected @endif>{{ $item->name }}</option>    
                                    @endforeach
                                </select>
                            </div>
                            @endif

                            {{-- <div class="form-group">
                                <label>Alamat *</label>
                                <textarea name="alamat" class="form-control form-control-sm" rows="3" required>{{old("alamat") ? old("alamat") : ''}}</textarea>
                                
                            </div> --}}


                            {{--
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label>Kecamatan *</label>
                                    <select id="kecamatan" name="kecamatan" class="form-control form-control-sm" style="width: 100%;padding: 10px;margin-top: 10px ; font-size: 19px;" @if(old("kecamatan")) @else disabled @endif required>
                                        @if (old("kecamatan"))
                                        <option value="{{ old("kecamatan") }}" selected>{{ old("kecamatan") }}</option>
                                        @endif
                                    </select>
                                </div>
    
                                <div class="form-group col-md-6">
                                    <label>Desa / Kelurahan *</label>
                                    <select id="desa_kelurahan" name="desa_kelurahan" class="form-control form-control-sm" style="width: 100%;padding: 10px;margin-top: 10px ; font-size: 19px;" @if(old("desa_kelurahan")) @else disabled @endif  required>
                                        @if (old("desa_kelurahan"))
                                        <option value="{{ old("desa_kelurahan") }}" selected>{{ old("desa_kelurahan") }}</option>
                                        @endif
                                    </select>
                                </div>
                            </div> --}}
        
                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="email">{{ __('E-Mail Address') }}  *</label>
            
                                    <input id="email" type="email" class=" form-control form-control-sm @if (old("type") == "register") @error('email') is-invalid @enderror @endif" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Isi email sebagai username akun anda">
            
                                    @if (old("type") == "register")
                                    @error('email')
                                        <span class="invalid-feedback " role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    @endif
                                </div>

                                <div class="form-group col-md-6">
                                    <label>Nomor Hp *</label>
                                    <input id="no_hp" name="no_hp" type="text" class=" form-control form-control-sm @if (old("type") == "register") @error('no_hp') is-invalid @enderror @endif" placeholder="08 . . ." oninput="numberOnly(this.id);" maxlength="15" value="{{old("no_hp")}}" required autocomplete="no_hp">
            
                                    @if (old("type") == "register")
                                    @error('no_hp')
                                        <span class="invalid-feedback " role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    @endif
                                </div>
                            </div>

                            <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="password">{{ __('Password') }}  *</label>
            
                                    <input id="password" type="password" class="  form-control form-control-sm @if (old("type") == "register") @error('password') is-invalid @enderror @endif" name="password" required autocomplete="new-password" placeholder="Minimal 6 Karakter">
            
                                    @if (old("type") == "register")
                                    @error('password')
                                        <span class="invalid-feedback " role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                    @endif
                                </div>
            
                                <div class="form-group col-md-6">
                                    <label for="password-confirm">{{ __('Confirm Password') }}  *</label>
                                    
                                    <input id="password-confirm" type="password" class=" form-control form-control-sm" name="password_confirmation" required autocomplete="new-password" placeholder="Ketik ulang password akun anda">
                                </div>
                            </div>
        
                            <div class="form-group my-3">
                            <button type="submit" class="btn btn-sm btn-primary">
                                {{ __('Register') }}
                            </button>
                            </div>

                            <div class="form-group mb-0">
                                <p>* HARAP ISIKAN DATA DENGAN BENAR</p>
                                <p>* EMAIL DAN PASSWORD AKAN DIGUNAKAN UNTUK LOGIN</p>
                            </div>
                        </form>
                    </div>
				</div>
            </div>
		</div>
        @endif
	</div>
<input type="hidden" id="jenjang_sekolah" value="{{$web_profil->jenjang_sekolah}}">
</section>
@endsection
@section('script')
    <script>
        $(document).ready(function(){
        $.get("https://nyala.sochainformatika.com/public/search/provinsi", function(data){
            $.each(data, function( key, value ){
                // console.log(value.name);
                if ($('#provinsi').val() == value.name) {
                    $('#provinsi').append("<option value='"+value.name+"' selected>"+value.name+"</option>");
                } else {
                    $('#provinsi').append("<option value='"+value.name+"'>"+value.name+"</option>");
                }
            });
        });

        $('#provinsi').select2({
            placeholder: "Pilih Provinsi",
        });

        $('#provinsi').change(function(){
            $('#kota_kabupaten').val(null).trigger('change');
            $('#kota_kabupaten').prop( "disabled", false );

            $('#jenjang_sekolah').val(null).trigger('change');
            $('#jenjang_sekolah').prop( "disabled", true );
            
            $('#alamat').val(null);
            $('#institusi').val(null).trigger('change');
            $('#institusi').prop( "disabled", true );

            let provinsi = $("#provinsi").val();
            var id_pesan = $(this).data("id_pesan");
            var url  = '{{url("toko/update-provinsi")}}/'+id_pesan;
            var token = '{{ csrf_token() }}';
        });

        $('#kota_kabupaten').select2({
            minimumInputLength: 1,
            language: { 
                inputTooShort: function () { 
                    return 'Cari Kota/Kabupaten asal sekolah anda.'; 
                } 
            },
            ajax: {
                url: "https://nyala.sochainformatika.com/public/search/kota_kabupaten",
                data: function (params) {
                    return {
                        q: params.term, // search term
                        provinsi: $('#provinsi').val()
                    };
                },
                dataType: 'json',
                delay: 250,
                processResults: pResultUpdate,
                cache: true
            },
        });

        let kota_kabupaten_val_update = "";
        if ($('#kota_kabupaten').val() != null) {
            let kota_kabupaten_explode_update = $('#kota_kabupaten').val().split(' ');
            if (kota_kabupaten_explode_update[0] == "KABUPATEN") {
                kota_kabupaten_val_update = "Kab. "+kota_kabupaten_explode_update[1];
            } else {
                kota_kabupaten_val_update = $('#kota_kabupaten').val();
            }
            console.log(kota_kabupaten_val_update);   
        }

        $('#kota_kabupaten').change(function(){
            $('#jenjang_sekolah').val(null).trigger('change');
            $('#jenjang_sekolah').prop( "disabled", false );

            $('#institusi').val(null).trigger('change');
            $('#institusi').prop( "disabled", false );

            kota_kabupaten_val_update = "";
            console.log(kota_kabupaten_val_update);
            if ($(this).val() != null) {
                let kota_kabupaten_explode_update = $(this).val().split(' ');
                if (kota_kabupaten_explode_update[0] == "KABUPATEN") {
                    kota_kabupaten_val_update = "Kab. "+kota_kabupaten_explode_update[1];
                } else {
                    kota_kabupaten_val_update = $(this).val();
                }
                console.log(kota_kabupaten_val_update);      
            }
        });

        // $('#jenjang_sekolah').select2({
        //     placeholder: "Pilih Jenjang Sekolah",
        // });

        // $('#jenjang_sekolah').change(function(){
        //     $('#institusi').val(null).trigger('change');
        //     $('#institusi').prop( "disabled", false );
        // });

        var jenjang = $("#jenjang_sekolah").val();
        // console.log(jenjang);

        $('#institusi').select2({
            minimumInputLength: 1,
            language: { 
                inputTooShort: function () { 
                    return 'Input bila tidak ada dalam pilihan.'; 
                } 
            },
            tags: true,
            ajax: {
                url: "https://nyala.sochainformatika.com/public/search/sekolah",
                data: function (params) {
                    
                    if (jenjang == 'SLTA') {
                        return {
                            q: params.term, // search term
                            kota_kabupaten: kota_kabupaten_val_update,
                            jenjang_sekolah: 'SMP'
                        };
                    }else if (jenjang == 'SLTP') {
                        return {
                            q: params.term, // search term
                            kota_kabupaten: kota_kabupaten_val_update,
                            jenjang_sekolah: 'SD'
                        };
                    }else{
                        return {
                            q: params.term, // search term
                            kota_kabupaten: kota_kabupaten_val_update,
                            jenjang_sekolah: 'TK'
                        };
                    }
                    
                },
                dataType: 'json',
                delay: 250,
                processResults: pResultUpdate2,
                cache: true
            },
        });

        function pResultUpdate(data) {
            return {
                results: $.map(data, function (item) {
                    // console.log(item);
                        return {
                            text: item.name,
                            id: item.name
                        }
                })
            };
        };

        function pResultUpdate2(data) {
            return {
                results: $.map(data, function (item) {
                        return {
                            text: item.sekolah,
                            // id: item.npsn+" - "+item.sekolah
                            id: item.sekolah
                        }
                })
            };
        };
    })
    </script>
@endsection