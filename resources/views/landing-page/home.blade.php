@extends('layouts.landing-page')
@section('main')
<style>
	@media only screen and (min-width: 575.98px) {
		.modal_margin_top {
			margin-top: 145px !important;
			height: 500px !important;
		}
	}
	.features-area {
		padding: 30px 0 30px !important;
	}
</style>
@php
	function rupiah($angka){
		$hasil_rupiah = "Rp " . number_format($angka,2,',','.');
		return $hasil_rupiah;	
	}
@endphp
{{-- <section>
	<div class="main-image text-center">
		<!-- Width: 1300px Height 500px -->
		<img src="{{ asset('templates/landing') }}/images/1607085082_e2d90f72ce6ab1fb915c.png" alt="" class="img-fluid">
	</div>
</section> --}}
<!-- Features Area -->
<section class="features-area section-bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12 d-flex">
				<h3><i class="fa fa-info-circle"></i> Ayo Segera Daftar!</h3>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 d-flex justify-content-end">
				@if (!Auth::check())
				<a href="{{ url('pendaftaran') }}" class="btn btn-danger">DAFTAR SEKARANG</a> &nbsp;
                <button type="button" class="btn btn-primary" onclick="openLogin();">MASUK</button>
				@endif
			</div>
		</div>
		<hr>
		<div class="row">
			<div class="col-lg-8 col-md-8 col-sm-12 d-flex">
				<img src="{{ isset($web_profil->banner_1) ? asset('templates/landing/images/banner/'.$web_profil->banner_1) : asset('templates/landing/images/background.jpg') }}" alt="" 
				{{-- class="img-fluid" --}}
				style="min-width: 100%;height: 250px;object-fit: cover;"
				>
			</div>
			<div class="col-lg-4 col-md-4 col-sm-12 d-flex">
				<div class="single-feature flex-fill" style="text-align: left;">
					<h4>Kontak Pendaftaran</h4>
					<p>Telp: {{ $web_profil->telp }}</p>
					<p>Cp: {{ $web_profil->cp }}</p>
					<p>Email: {{ $web_profil->email }}</p>
				</div>
			</div>
		</div>
	</div>
</section>

@if (isset($tahun_ajaran))
<section class="features-area section-bg" id="informasi-pendaftaran">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-sm-12 d-flex">
				<h3><i class="fa fa-info-circle"></i> Informasi Pendaftaran</h3>
			</div>
			<div class="col-lg-6 col-md-6 col-sm-12 d-flex justify-content-end">
				<a href="{{ url('informasi-pendaftaran-pdf/'.$tahun_ajaran->tahun_ajaran) }}" class="btn btn-danger">UNDUH</a> &nbsp;
			</div>
		</div>
		<hr>
		<div class="row d-flex justify-content-center">
			<div class="col-lg-12 col-md-12 col-sm-12">
				<div class="card">
					<div class="card-body text-center">
						<h4>PPDB SEKOLAH TAHUN AJARAN {{ $tahun_ajaran->tahun_ajaran }}</h4>
						<div style="text-align: left; margin-top: 40px;">
							<p style="text-align: left;">
								Siswa yang diterima pada tahun ajaran ini yaitu jumlah kuota maksimal {{ $tahun_ajaran->maksimal_jumlah_siswa }} siswa
								@if ($tahun_ajaran->tampil_raport == "Ya")
								, dengan syarat minimal nilai raport {{ $tahun_ajaran->minimal_nilai }}
								@endif
								.
							</p>
							<p style="margin-top: 10px; ">Biaya Pendaftaran :</p>
						</div>
						<div class="table-responsive-sm" style="margin-top: 10px;font-size: 12px;">
							<table class="table table-hover table-striped table-bordered table-default">
								<thead>
								<tr>
									<th>Nama Gelombang</th>
									<th>Nominal</th>
									<th>Tanggal Mulai</th>
									<th>Tanggal Selesai</th>
								</tr>
								</thead>
								<tbody>
									@foreach ($tahun_ajaran->biaya
									->where('jenis_biaya', 'Biaya Pendaftaran')
									->where('program_studi', 'Default') as $key=>$item)
									<tr>
										<td>{{$item->nama_gelombang}}</td>
										<td>{{ rupiah($item->biaya) }}</td>
										<td>
											{{Carbon\Carbon::parse($item->mulai_tanggal)->isoFormat('D MMMM Y')}}
										</td>
										<td>
											{{Carbon\Carbon::parse($item->sampai_tanggal)->isoFormat('D MMMM Y')}}
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						<p style="margin-top: 20px; text-align: left">Biaya Uang Gedung :</p>
						<div class="table-responsive-sm" style="margin-top: 10px;font-size: 12px;">
							<table class="table table-hover table-striped table-bordered table-default">
								<thead>
								<tr>
									<th>Nama Gelombang</th>
									<th>Nominal</th>
									<th>Tanggal Mulai</th>
									<th>Tanggal Selesai</th>
								</tr>
								</thead>
								<tbody>
									@foreach ($tahun_ajaran->biaya
									->where('jenis_biaya', 'Biaya Daftar Ulang')
									->where('program_studi', 'Default') as $key=>$item)
									<tr>
										<td>{{$item->nama_gelombang}}</td>
										<td>{{ rupiah($item->biaya) }}</td>
										<td>
											{{Carbon\Carbon::parse($item->mulai_tanggal)->isoFormat('D MMMM Y')}}
										</td>
										<td>
											{{Carbon\Carbon::parse($item->sampai_tanggal)->isoFormat('D MMMM Y')}}
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
						<p style="margin-top: 20px; text-align: left">Biaya Lain-Lain :</p>
						<div class="table-responsive-sm" style="margin-top: 10px;font-size: 12px;">
							<table class="table table-hover table-striped table-bordered table-default">
								<thead>
								<tr>
									<th>Nama Biaya</th>
									<th>Nominal</th>
								</tr>
								</thead>
								<tbody>
									@foreach ($tahun_ajaran->bls() as $key=>$item)
									<tr>
										<td>{{$item->jenis_biaya}}</td>
										<td>{{ rupiah($item->biaya) }}</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
@endif

<section class="features-area section-bg">
	<div class="container">
		<h3><i class="fa fa-info-circle"></i> Alur Pendaftaran PMB</h3>
		<hr>
		<div class="row">
			@foreach ($alur_pendaftarans as $item)
			<div class="col-lg-4 col-md-6 col-12 d-flex">
				<!-- Single Feature -->
				<div class="single-feature flex-fill">
					<h4>{{ $item->name }}</h4>
					<p>{{ $item->keterangan }}</p>
				</div>
				<!--/ End Single Feature -->
			</div>
			@endforeach
		</div>
	</div>
</section>

@if (isset($tahun_ajaran) && $tahun_ajaran->tampil_berkas == "Ya")
<section class="features-area section-bg">
	<div class="container">
		<h3><i class="fa fa-info-circle"></i> Syarat Pendaftaran PMB</h3>
		<hr>
			<div class="row">
				{{-- @foreach ($syarat_pendaftarans as $item)
				<div class="col-lg-4 col-md-6 col-12 d-flex">
					<!-- Single Feature -->
					<div class="single-feature flex-fill">
						<h4>{{ $item->name }}</h4>
						<p>{{ $item->keterangan }}</p>
					</div>
					<!--/ End Single Feature -->
				</div>
				@endforeach	 --}}
				<div class="col-lg-12 col-md-12 col-sm-12">
					<div class="list-group">
						@foreach ($setting_berkass as $number => $item)
						<div class="list-group-item list-group-item-action">{{ $number + 1 }}. {{$item->nama_berkas }}</div>
						@endforeach						
					</div>
				</div>
			</div>
		</h3>
	</div>
</section>
@endif

@if ($web_profil->jenjang_sekolah == "SLTA" && $jurusans->count() > 0)
<section class="features-area section-bg" id="jurusan">
	<div class="container">
		<h3><i class="fa fa-info-circle"></i> Jurusan Yang Tersedia</h3>
		<hr>
			<div class="row">
				<div class="col-12">
				<div id="carouselTestimonials" class="carousel slide" data-ride="carousel">
                    {{-- <ol class="carousel-indicators">
                    <li data-target="#carouselTestimonials" data-slide-to="0" class="active"></li>
                    @for ($i = 0; $i < count($jurusan) - 1; $i++)
                        <li data-bs-target="#carouselTestimonials" data-bs-slide-to="{{$i+1}}"></li>
                    @endfor
                    </ol> --}}
                    <div class="carousel-inner">
                    <div class="carousel-item active">
                        <div class="row d-flex justify-content-center">
                            @foreach ($jurusan[0] as $row)
                                <div class="col-md-3">
                                    <div class="card">
                                        <div class="card-body text-center">
                                            <h3 class="card-title mb-5">{{$row->name}}</h3>
                                            {{-- <p class="card-text" style="margin-top: 10px;">
                                                @php
                                                    $detail = implode(' ', array_slice(explode(' ', $row->keterangan), 0, 2));
                                                @endphp
                                                @if (count(explode(' ', $row->keterangan)) > 2)
                                                    {{$detail}}... <a href="#" data-toggle="modal" data-target="#keteranganModal-{{ $row->id }}">detail</a>
                                                @else
                                                    {{$row->keterangan}}
                                                @endif    
                                            </p> --}}
											<hr>

											<div class="modal fade modal_margin_top" id="detail_jurusan_{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
												<div class="modal-dialog modal-lg" role="document">
													<div class="modal-content">
														<div class="modal-header">
															<h6 class="modal-title" id="deleteModalLabel">Informasi Detail {{$row->name}}</h6>
															<button type="button" class="close" data-dismiss="modal" aria-label="Close">
															<span aria-hidden="true">&times;</span>
															</button>
														</div>
														<div class="modal-body">
															<div class="table-responsive-sm" style="text-align: left;">
																<table class="table table-borderless">
																	<tr>
																		<td>Kuota Siswa</td>
																		<td>:&nbsp;&nbsp;&nbsp;{{ $row->maksimal_jumlah_siswa }} Siswa</td>
																	</tr>
																	@if ($row->tahunAjaran->tampil_raport == "Ya")
																	<tr>
																		<td>Min Nilai Raport</td>
																		<td>:&nbsp;&nbsp;&nbsp;{{ $row->minimal_nilai }} Siswa</td>
																	</tr>
																	@endif
																	<tr>
																		<td>Akreditasi</td>
																		<td>:&nbsp;&nbsp;&nbsp;{{ $row->akreditasi }}</td>
																	</tr>
																	@if ($row->settingBerkas->count() > 0 && $row->tahunAjaran->tampil_berkas == "Ya")
																	<tr>
																		<td>Berkas Jurusan</td>
																		<td>
																			@foreach ($row->settingBerkas as $key=>$item)
																			{!! $key == 0 ? ":&nbsp;&nbsp;&nbsp;" : ""!!} {{$item->nama_berkas}} {{ $key != $row->settingBerkas->count()-1 ? ", " : ""}}
																			@endforeach
																		</td>
																	</tr>
																	@endif
																</table>
																@if ($row->biaya->count() > 0)
																<p style="margin-top: 20px; text-align: left">Biaya Jurusan:</p>
																<div class="table-responsive-sm" style="margin-top: 10px;font-size: 12px;">
																	<table class="table table-hover table-striped table-bordered table-default">
																		<thead>
																		<tr>
																			<th>Nama</th>
																			<th>Nominal</th>
																		</tr>
																		</thead>
																		<tbody>
																			@foreach ($row->biaya as $key=>$item)
																			<tr>
																				<td>{{$item->jenis_biaya}}</td>
																				<td>{{ rupiah($item->biaya) }}</td>
																			</tr>
																			@endforeach
																		</tbody>
																	</table>
																</div>
																@endif
															</div>
															{{-- @foreach ($jenis_biaya as $jb)
																<strong> {{$jb->jenis_biaya}} : </strong><br>
																@foreach ($biaya as $item)
																@if ($item->program_studi == $row->id && $item->jenis_biaya == $jb->jenis_biaya)
																{{$item->nama_gelombang}} ({{Carbon\Carbon::parse($item->mulai_tanggal)->isoFormat('D MMMM Y')}} - {{Carbon\Carbon::parse($item->selesai_tanggal)->isoFormat('D MMMM Y')}}) : {{rupiah($item->biaya)}} <br>
																@endif
																@endforeach
															@endforeach --}}
														</div>
													</div>
												</div>
											</div>

											<button type="button" data-toggle="modal" data-target="#detail_jurusan_{{$row->id}}" style="color: blue">Informasi Detail</button>
                                            <div class="row d-flex justify-content-center mt-5">
                                                {{-- <button type="button" class="btn btn-success w-75" @if (!Auth::check()) data-toggle="modal" data-target="#signModal" @endif >Daftar</button> --}}
												@if (!Auth::check()) 
												<a href="{{ url('pendaftaran').'?jurusan='.$row->id }}" class="btn btn-success w-75">Daftar</a>
												@else
												<button type="button" class="btn btn-success w-75">Daftar</button>
												@endif 
                                            </div>
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    @for ($i = 0; $i < count($jurusan) - 1; $i++)
                    <div class="carousel-item">
                        <div class="row d-flex justify-content-center">
                            @foreach ($jurusan[$i+1] as $row)
							<div class="col-md-3">
								<div class="card">
									<div class="card-body text-center">
										<h3 class="card-title mb-5">{{$row->name}}</h3>
										{{-- <p class="card-text" style="margin-top: 10px;">
											@php
												$detail = implode(' ', array_slice(explode(' ', $row->keterangan), 0, 2));
											@endphp
											@if (count(explode(' ', $row->keterangan)) > 2)
												{{$detail}}... <a href="#" data-toggle="modal" data-target="#keteranganModal-{{ $row->id }}">detail</a>
											@else
												{{$row->keterangan}}
											@endif    
										</p> --}}
										<hr>
										<div class="modal fade modal_margin_top" id="detail_jurusan_{{$row->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
											<div class="modal-dialog modal-lg" role="document">
												<div class="modal-content">
													<div class="modal-header">
														<h6 class="modal-title" id="deleteModalLabel">Informasi Detail {{$row->name}}</h6>
														<button type="button" class="close" data-dismiss="modal" aria-label="Close">
														<span aria-hidden="true">&times;</span>
														</button>
													</div>
													<div class="modal-body">
														<div class="table-responsive-sm" style="text-align: left;">
															<table class="table table-borderless">
																<tr>
																	<td>Kuota Siswa</td>
																	<td>:&nbsp;&nbsp;&nbsp;{{ $row->maksimal_jumlah_siswa }} Siswa</td>
																</tr>
																@if ($row->tahunAjaran->tampil_raport == "Ya")
																<tr>
																	<td>Min Nilai Raport</td>
																	<td>:&nbsp;&nbsp;&nbsp;{{ $row->minimal_nilai }} Siswa</td>
																</tr>
																@endif
																<tr>
																	<td>Akreditasi</td>
																	<td>:&nbsp;&nbsp;&nbsp;{{ $row->akreditasi }}</td>
																</tr>
																@if ($row->settingBerkas->count() > 0 && $row->tahunAjaran->tampil_berkas == "Ya")
																<tr>
																	<td>Berkas Jurusan</td>
																	<td>
																		@foreach ($row->settingBerkas as $key=>$item)
																		{!! $key == 0 ? ":&nbsp;&nbsp;&nbsp;" : ""!!} {{$item->nama_berkas}} {{ $key != $row->settingBerkas->count()-1 ? ", " : ""}}
																		@endforeach
																	</td>
																</tr>
																@endif
															</table>
															@if ($row->biaya->count() > 0)
															<p style="margin-top: 20px; text-align: left">Biaya Jurusan:</p>
															<div class="table-responsive-sm" style="margin-top: 10px;font-size: 12px;">
																<table class="table table-hover table-striped table-bordered table-default">
																	<thead>
																	<tr>
																		<th>Nama</th>
																		<th>Nominal</th>
																	</tr>
																	</thead>
																	<tbody>
																		@foreach ($row->biaya as $key=>$item)
																		<tr>
																			<td>{{$item->jenis_biaya}}</td>
																			<td>{{ rupiah($item->biaya) }}</td>
																		</tr>
																		@endforeach
																	</tbody>
																</table>
															</div>
															@endif
														</div>													
													</div>
												</div>
											</div>
										</div>

										<button type="button" data-toggle="modal" data-target="#detail_jurusan_{{$row->id}}" style="color: blue">Informasi Detail</button>
										<div class="row d-flex justify-content-center mt-5">
											{{-- <button type="button" class="btn btn-success w-75" @if (!Auth::check()) data-toggle="modal" data-target="#signModal" @endif >Daftar</button> --}}
											@if (!Auth::check()) 
												<a href="{{ url('pendaftaran') }}" class="btn btn-success w-75">Daftar</a>
											@else
												<button type="button" class="btn btn-success w-75">Daftar</button>
											@endif 
										</div>
										
										
									</div>
								</div>
							</div>
                            @endforeach
                        </div>
                    </div>
                    @endfor
                    @if (isset($jurusan[1]))
                        {{-- <div class="carousel-item">
                        <img class="d-block w-100" src="..." alt="Second slide">
                    </div>
                    <div class="carousel-item">
                        <img class="d-block w-100" src="..." alt="Third slide">
                    </div> --}}
                    </div>
                    <a  href="#carouselTestimonials" data-slide="prev" class="carousel-control-prev btn btn-sm">&#10094;</a>
                    <a  href="#carouselTestimonials"  data-slide="next" class="carousel-control-next btn btn-sm">&#10095;</a>
                    {{-- <a class="carousel-control-prev" href="#carouselTestimonials" role="button" data-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carouselTestimonials" role="button" data-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="sr-only">Next</span>
                    </a> --}}
                    <div class="row flex-center mt-5">
                        <div class="col-auto position-relative z-index-2">
                            <ol class="carousel-indicators me-xxl-7 me-xl-4 me-lg-7">
                            <li class="active" data-bs-target="#carouselTestimonials" data-bs-slide-to="0"></li>
                            @for ($i = 0; $i < count($jurusan) - 1; $i++)
                                <li data-bs-target="#carouselTestimonials" data-bs-slide-to="{{$i+1}}"></li>
                            @endfor
                            </ol>
                        </div>
                    </div>
                    @endif
                </div>
				</div>
			</div>
		</h3>
	</div>
</section>
<!--/ End Features Area -->
@endif


{{-- <section class="video-feature side overlay section-bg">
	<div class="container">
		<div class="row">
			<div class="col-lg-6 col-md-6 col-12">
				<div class="img-feature ">
					<img src="{{ asset('templates/landing') }}/images/1607085918_f8bf3eb62fb654c8542e.jpg" alt="">
				</div>
			</div>
			<div class="col-lg-6 col-md-6 col-12">
				<div class="features-main">
					<div class="title">
						<h2>Jurusan yang Tersedia</h2>
						<p>Memiliki berbagai macam pilihan untuk menggapai pendidikan jauh lebih tinggi.</p>
					</div>
					<div class="row mt-3 text-center">
												<div class="col-lg-6 col-md-6 col-6 mb-3">
							<!-- Single List Feature -->
							<i class="fas fa-book-reader fa-2x"></i>
							<h4 class="text-light">S1 MANAJEMEN</h4>
							<!--/ End Single List Feature -->
						</div>
												<div class="col-lg-6 col-md-6 col-6 mb-3">
							<!-- Single List Feature -->
							<i class="fas fa-book-reader fa-2x"></i>
							<h4 class="text-light">S1 AKUNTANSI</h4>
							<!--/ End Single List Feature -->
						</div>
												<div class="col-lg-6 col-md-6 col-6 mb-3">
							<!-- Single List Feature -->
							<i class="fas fa-book-reader fa-2x"></i>
							<h4 class="text-light">D3 MANAJEMEN PERUSAHAAN</h4>
							<!--/ End Single List Feature -->
						</div>
											</div>
					<!-- <div class="feature-btn">
						<a href="javascript:void(0)" class="bizwheel-btn theme-2">Know More</a>
					</div> -->
				</div>
			</div>
		</div>
	</div>
</section> --}}
@endsection