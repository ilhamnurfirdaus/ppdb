<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Cetak</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</head>
<body>
    @php
        function rupiah($num) {
            return 'Rp. '.number_format($num, 0, ',', '.');
        }
    @endphp
    <div class="text-center">
    <img src="{{ public_path('templates/landing') }}/images/logo-header.png" alt="" class="img-fluid">
    </div>
    <!--  -->
    <hr class="mt-3 mb-3">
    <!--  -->
    <div class="text-center" id="message">
        <h3>Invoice Pembayaran {{ $bayar->jenis_biaya == "Biaya Pendaftaran" ? 'Pendaftaran' : 'Daftar Ulang' }}</h3>
        <h5>Kode Pendaftaran: {{ $bayar->id }}</h5>
        <h5>{{ $bayar->siswa->user->name }}</h5>
    </div>
    <!--  -->
    <hr>
    <!--  -->
    <div class="text-center" id="nota">
        <h3>Pembayaran {{ $bayar->status }}</h3>
        <p class="mt-3 mb-3">Jumlah yang harus dibayar</p>
        {{-- <h5>{{ rupiah($bayar->siswa->jurusanTo->biayaAwal->biaya) }}</h5> --}}
        <h5>{{ rupiah($bayar->jml_byr + intval($bayar->tiga_digit_angka)) }}</h5>
        <hr>
        {{$bayar->rekening->atas_nama}} <br>
        {{$bayar->rekening->nomor_rekening}} <br>
        {{$bayar->rekening->nama_rekening}}
    </div>
</body>
</html>