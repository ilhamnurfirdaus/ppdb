@extends('layouts.admin')
@section('main')
@php
    function rupiah($num) {
        return 'Rp. '.number_format(intval($num), 0, ',', '.');
    }
@endphp
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>Seting Pengumuman
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
                <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#createModal">
                    Create
                </button>
            </div>
        </div>
        <div class="page-title-actions">
            
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>
<div class="main-card mb-3 card">
    <div class="card-body">
        <table style="width: 100%;" class="table table-hover table-striped table-bordered table-default">
            <thead>
            <tr>
                <th>Judul</th>
                <th>Deskripsi</th>
                <th>Status</th>
                <th>Tgl DIbuat</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($pengumumans as $item)
                <tr>
                    <td>{{$item->judul}}</td>
                    <td><button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#deskripsiModal-{{$item->id}}"><i class="fas fa-eye"></i></button></td>
                    <td>{{$item->status}}</td>
                    <td>
                        {{Carbon\Carbon::parse($item->created_at)->isoFormat('D MMMM Y')}}
                    </td>
                    <td>
                        <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#updateModal-{{$item->id}}"><i class="fas fa-pencil-alt"></i></button>
                        <button type="button" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#deleteModal-{{$item->id}}"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('modal')
<!-- Modal -->
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="createModalLabel">Buat Data Baru</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/pengumuman/store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type" type="text" value="create" hidden>
            <div class="form-group">
                <label>Judul  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="judul" type="text" class="form-control form-control-sm" placeholder="Isi Judul Pengumuman" value="{{old('judul')}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Deskripsi  <span class='text-danger' title='This field is required'>*</span></label>
                {{-- <textarea id="summernote-0" name="deskripsi" class="summernote">{!! old("deskripsi") !!}</textarea> --}}
                <textarea name="deskripsi" class="form-control form-control-sm" rows="5" required>{!! old("deskripsi") !!}</textarea>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Status <span class='text-danger' title='This field is required'>*</span></label>
                <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="status" style="width: 100%;" required>
                    <option value="">**Pilih</option>
                    <option value="Aktif" @if("Aktif" == old("status")) selected @endif>Aktif</option>
                    <option value="Tidak Aktif" @if("Tidak Aktif" == old("status")) selected @endif>Tidak Aktif</option>
                </select>
                <div class="text-danger"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

@foreach ($pengumumans as $item)
<div class="modal fade" id="deskripsiModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="deskripsiModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="deskripsiModal-{{$item->id}}Label">Deskripsi Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            {!! $item->deskripsi !!}
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        </div>
        </div>
    </div>
</div>

<div class="modal fade" id="updateModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="updateModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="updateModal-{{$item->id}}Label">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/pengumuman/update/'.$item->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type-{{$item->id}}" type="text" value="update" hidden>
            <div class="form-group">
                <label>Judul  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="judul" type="text" class="form-control form-control-sm" placeholder="Isi Judul Pengumuman" value="{{$item->judul}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Deskripsi  <span class='text-danger' title='This field is required'>*</span></label>
                {{-- <textarea id="summernote-0" name="deskripsi" class="summernote">{!! $item->deskripsi !!}</textarea> --}}
                <textarea name="deskripsi" class="form-control form-control-sm" rows="5" required>{!! $item->deskripsi !!}</textarea>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Status <span class='text-danger' title='This field is required'>*</span></label>
                <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="status" style="width: 100%;" required>
                    <option value="">**Pilih</option>
                    <option value="Aktif" @if("Aktif" == $item->status) selected @endif>Aktif</option>
                    <option value="Tidak Aktif" @if("Tidak Aktif" == $item->status) selected @endif>Tidak Aktif</option>
                </select>
                <div class="text-danger"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="deleteModal-{{$item->id}}Label">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Apakah anda yakin untuk menghapus data ini? 
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <a href="{{url('admin/pengumuman/destroy/'.$item->id)}}" class="btn btn-danger btn-sm">Hapus</a>
        </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        if ('{{old("type-".$item->id)}}' == 'update') {
            $(document).ready(function () {
                $("#updateModal-{{$item->id}}").modal('show');
            });
        };

        $('.select2-update-{{$item->id}}').select2({
            placeholder: "**Silahkan Pilih",
            dropdownParent: $("#updateModal-{{$item->id}}")
        });

        // $('#updateModal-{{$item->id}}').on('shown.bs.modal', function() {
        //     $('.summernote').summernote();
        // });
    });
</script>
@endforeach
@endsection
@section('script')
{{-- <script>
$(document).on("show.bs.modal", '.modal', function (event) {
    console.log("Global show.bs.modal fire");
    var zIndex = 100000 + (10 * $(".modal:visible").length);
    $(this).css("z-index", zIndex);
    setTimeout(function () {
        $(".modal-backdrop").not(".modal-stack").first().css("z-index", zIndex - 1).addClass("modal-stack");
    }, 0);
}).on("hidden.bs.modal", '.modal', function (event) {
    console.log("Global hidden.bs.modal fire");
    $(".modal:visible").length && $("body").addClass("modal-open");
});
$(document).on('inserted.bs.tooltip', function (event) {
    console.log("Global show.bs.tooltip fire");
    var zIndex = 100000 + (10 * $(".modal:visible").length);
    var tooltipId = $(event.target).attr("aria-describedby");
    $("#" + tooltipId).css("z-index", zIndex);
});
$(document).on('inserted.bs.popover', function (event) {
    console.log("Global inserted.bs.popover fire");
    var zIndex = 100000 + (10 * $(".modal:visible").length);
    var popoverId = $(event.target).attr("aria-describedby");
    $("#" + popoverId).css("z-index", zIndex);
});
</script> --}}
@endsection