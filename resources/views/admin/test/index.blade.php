@extends('layouts.admin')
@section('main')
@php
    function rupiah($num) {
        return 'Rp. '.number_format(intval($num), 0, ',', '.');
    }
@endphp
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>Master Soal
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
                <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#createModal">
                    Tambah
                </button>
                <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#importModal">
                    Import
                </button>
                
            </div>
        </div>
        <div class="page-title-actions">
            
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>
<div class="main-card mb-3 card">
    <div class="card-header form-inline">
        <form action="{{url('admin/soal/waktu_soal')}}" method="POST" enctype="multipart/form-data">
            @csrf    

            <div class="input-group input-group-sm">
                <div class="input-group-prepend">
                <span class="input-group-text" style="text-transform: capitalize !important;">Waktu Soal</span>
                </div>
                <input type="number" name="waktu_soal" class="form-control" value="{{ $web_profil->waktu_soal }}" min="0" style="width: 100px;" required>
                <div class="input-group-append">
                    <span class="input-group-text" style="text-transform: capitalize !important;">Jam</span>
                    <button class="btn btn-outline-success" type="submit">Simpan</button>
                </div>
            </div>
        </form>
    </div>
    <div class="card-body">
        <table style="width: 100%;" class="table table-hover table-striped table-bordered table-default">
            <thead>
            <tr>
                <th style="width: 50%;">Pertanyaan</th>
                <th style="width: 25%;">Tgl DIbuat</th>
                <th style="width: 25%;">Aksi</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($tests as $item)
                <tr>
                    <td>{{$item->pertanyaan}}</td>
                    <td>
                        {{Carbon\Carbon::parse($item->created_at)->isoFormat('D MMMM Y')}}
                    </td>
                    <td>
                        <button type="button" class="btn_update_modal btn btn-xs btn-success" data-toggle="modal" data-target="#updateModal" data-remote="{{url('admin/soal/edit/'.$item->id)}}"><i class="fas fa-pencil-alt"></i></button>
                        <button type="button" class="btn_delete_modal btn btn-xs btn-warning" data-toggle="modal" data-target="#deleteModal" data-remote="{{url('admin/soal/destroy/'.$item->id)}}"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('modal')
<!-- Modal -->
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="createModalLabel">Buat Data Baru</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/soal/store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type" type="text" value="create" hidden>
            <div class="form-group">
                <label>Pertanyaan</label>
                <textarea name="pertanyaan" id="pertanyaan" class='form-control form-control-sm' rows='5' required></textarea>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Pilihan Ganda</label>
                {{-- <div class="row mt-3 row-input" style="display: none;">
                    <div class="col-sm-7">
                        <input type="text" class='name form-control form-control-sm' placeholder="Nama">
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class='score form-control form-control-sm' placeholder="Nilai" max="10">
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-sm btn-secondary btn-delete" type="button" style="display: none;">X</button>
                    </div>
                </div> --}}
                <div class="row">
                    <div class="col-sm-4">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="score" id="flexRadioDefault1" value="1000">
                            <label class="form-check-label" for="flexRadioDefault1">
                              Jawaban Benar
                            </label>
                        </div>
                        {{-- <input type="number" name="score[]" class='score form-control form-control-sm' placeholder="Nilai" max="10" required> --}}
                    </div>
                    <div class="col-sm-8">
                        <input type="text" name="name[]" class='name form-control form-control-sm' placeholder="Nama Jawaban" required>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-4">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="score" id="flexRadioDefault1" value="0100">
                            <label class="form-check-label" for="flexRadioDefault1">
                              Jawaban Benar
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" name="name[]" class='name form-control form-control-sm' placeholder="Nama Jawaban" required>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-4">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="score" id="flexRadioDefault1" value="0010">
                            <label class="form-check-label" for="flexRadioDefault1">
                              Jawaban Benar
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" name="name[]" class='name form-control form-control-sm' placeholder="Nama Jawaban" required>
                    </div>
                </div>
                <div class="row mt-3">
                    <div class="col-sm-4">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="score" id="flexRadioDefault1" value="0001">
                            <label class="form-check-label" for="flexRadioDefault1">
                              Jawaban Benar
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" name="name[]" class='name form-control form-control-sm' placeholder="Nama Jawaban" required>
                    </div>
                </div>
                {{-- <div class="row mt-3 row-add">
                    <div class="col-sm-12">
                        <button type="button" class="btn btn-sm btn-info btn-add">Tambah Input</button>
                    </div>                        
                </div> --}}
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="createModalLabel">Import Soal</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/soal/import')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="file" type="file" value="create">
            <p>Download format import <a href="{{url('excel/format_soal.xlsx')}}">Disini</a></p>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

{{-- @foreach ($tests as $item)
<div class="modal fade" id="updateModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="updateModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="updateModal-{{$item->id}}Label">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/soal/update/'.$item->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type-{{$item->id}}" type="text" value="update" hidden>

            <div class="form-group">
                <label>Pertanyaan</label>
                <textarea name="pertanyaan" id="pertanyaan" class='form-control form-control-sm' rows='5' required>{{ $item->pertanyaan }}</textarea>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Pilihan Ganda</label> --}}
                {{-- <div class="row mt-3 row-input" style="display: none;">
                    <div class="col-sm-7">
                        <input type="text" class='name form-control form-control-sm' placeholder="Nama">
                    </div>
                    <div class="col-sm-3">
                        <input type="number" class='score form-control form-control-sm' placeholder="Nilai" max="10">
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-sm btn-secondary btn-delete" type="button" style="display: none;">X</button>
                    </div>
                </div> --}}
                {{-- @foreach ($item->testChoice as $key => $row)
                <div class="row {{$key != '0' ? 'mt-3' : ''}}">
                    <div class="col-sm-4">
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="score" id="flexRadioDefault1" 
                            value="{{ $key == '0' ? '1000' : ($key == '1' ? '0100' : ($key == '2' ? '0010' : ($key == '3' ? '0001' : '0000'))) }}" 
                            {{ $row->score == '1' ? 'checked' : ''}}>
                            <label class="form-check-label" for="flexRadioDefault1">
                              Jawaban Benar
                            </label>
                        </div>
                    </div>
                    <div class="col-sm-8">
                        <input type="text" name="name[]" class='name form-control form-control-sm' placeholder="Nama Jawaban" value="{{ $row->name }}" required>
                    </div> --}}
                    {{-- <div class="col-sm-7">
                        <input type="text" name="name[]" class='name form-control form-control-sm' placeholder="Nama" value="{{ $row->name }}" required>
                    </div>
                    <div class="col-sm-3">
                        <input type="number" name="score[]" class='score form-control form-control-sm' placeholder="Nilai" max="10" value="{{ intval($row->score) }}" required>
                    </div> --}}
                    {{-- @if ($key != 0)
                    <div class="col-sm-2">
                        <button class="btn btn-sm btn-secondary btn-delete" type="button">X</button>
                    </div>
                    @endif --}}
                {{-- </div>
                @endforeach --}}
                {{-- <div class="row mt-3 row-add">
                    <div class="col-sm-12">
                        <button type="button" class="btn btn-sm btn-info btn-add">Tambah Input</button>
                    </div>                        
                </div> --}}
            {{-- </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="deleteModal-{{$item->id}}Label">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Apakah anda yakin untuk menghapus data ini? 
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <a href="{{url('admin/soal/destroy/'.$item->id)}}" class="btn btn-danger btn-sm">Hapus</a>
        </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        if ('{{old("type-".$item->id)}}' == 'update') {
            $(document).ready(function () {
                $("#updateModal-{{$item->id}}").modal('show');
            });
        }

        $('.select2-update-{{$item->id}}').select2({
            placeholder: "**Silahkan Pilih",
            dropdownParent: $("#updateModal-{{$item->id}}")
        });
    });
</script>
@endforeach --}}
@endsection
@section('script')
<script>
    $(".score").on('change', function(){
        let att = parseInt($(this).attr("max"));
        let val = parseInt($(this).val());
        if (val > att) {
            $(this).val(att);
        }
    });

    $(document).on('click', '.btn-delete', function()
    { 
        $(this).parent().parent().remove(); 
    });

    $(document).ready(function(){
        $('.btn-add').click(function () {
            console.log($(this).parent().parent().parent().find('.row-input'));
            let tambah_input = $(this).parent().parent().parent().find('.row-input').clone();
            tambah_input.show();
            tambah_input.removeClass("row-input");
            tambah_input.find(".btn-delete").show();
            tambah_input.find(".name").attr('required',true);
            tambah_input.find(".name").attr('name','name[]');
            tambah_input.find(".score").attr('required',true);
            tambah_input.find(".score").attr('name','score[]');
            tambah_input.insertBefore($(this).parent().parent().parent().find(".row-add"));
        });    
    });
</script>
@endsection