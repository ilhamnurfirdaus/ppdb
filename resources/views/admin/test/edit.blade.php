<form action="{{url('admin/soal/update/'.$test->id)}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal-body">
        <input name="type-{{$test->id}}" type="text" value="update" hidden>

        <div class="form-group">
            <label>Pertanyaan</label>
            <textarea name="pertanyaan" id="pertanyaan" class='form-control form-control-sm' rows='5' required>{{ $test->pertanyaan }}</textarea>
            <div class="text-danger"></div>
        </div>
        <div class="form-group">
            <label>Pilihan Ganda</label>
            @foreach ($test->testChoiceAdmin as $key => $row)
            <div class="row {{$key != '0' ? 'mt-3' : ''}}">
                <div class="col-sm-4">
                    <div class="form-check">
                        <input class="form-check-input" type="radio" name="score" id="flexRadioDefault1" 
                        value="{{ $key == '0' ? '1000' : ($key == '1' ? '0100' : ($key == '2' ? '0010' : ($key == '3' ? '0001' : '0000'))) }}" 
                        {{ $row->score == '1' ? 'checked' : ''}}>
                        <label class="form-check-label" for="flexRadioDefault1">
                          Jawaban Benar
                        </label>
                    </div>
                </div>
                <div class="col-sm-8">
                    <input type="text" name="name[]" class='name form-control form-control-sm' placeholder="Nama Jawaban" value="{{ $row->name }}" required>
                </div>
            </div>
            @endforeach
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-success btn-sm">Simpan</button>
    </div>
</form>