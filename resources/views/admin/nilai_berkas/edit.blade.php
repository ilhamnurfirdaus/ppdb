<form action="{{url('admin/nilai-berkas/update/'.$nilai_berkas->nilai_berkas_id)}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal-body">
        <input name="type-{{$nilai_berkas->nilai_berkas_id}}" type="text" value="update" hidden>
        <div class="form-group">
            <label>Range Nilai <span class='text-danger' title='This field is required'>*</span></label>
            <div class="form-group row">
                <div class="col-sm-6">
                    <div class="input-group input-group-sm">
                        <input name="min_nilai" type="number" class="min_nilai form-control" placeholder="1" min="0" max="10" value="{{ $nilai_berkas->min_nilai }}" required>
                        <div class="input-group-prepend input-group-append">
                            <span class="input-group-text" id="">Sampai</span>
                        </div>
                        <input name="max_nilai" type="number" class="max_nilai form-control" placeholder="10" min="0" max="10" value="{{ $nilai_berkas->max_nilai }}" required>
                    </div>
                </div>
            </div>
        </div>
        <div class="form-group">
            <label>Nilai Berkas <span class='text-danger' title='This field is required'>*</span></label>
            <input name="nilai" type="number" class="form-control form-control-sm" placeholder="0.00" min="0" max="10" step=".01" value="{{ $nilai_berkas->nilai }}" required>
            <div class="text-danger"></div>
        </div>
        <div class="form-group">
            <label>keterangan</label>
            <textarea name="keterangan" id="keterangan" class='form-control form-control-sm' rows='5'>{{$nilai_berkas->keterangan}}</textarea>
            <div class="text-danger"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-success btn-sm">Simpan</button>
    </div>
</form>

<script>
    $(document).ready(function () {
        $(".min_nilai").change(function(){
            if ($(this).parent().find('.max_nilai').val() < 1) {
                $(this).parent().find('.max_nilai').val($(this).val());
            }

            if (parseInt($(this).val()) < $(this).attr("min")) {
                $(this).val($(this).attr("min"));
            } else if (parseInt($(this).val()) > parseInt($(this).parent().find('.max_nilai').val())) {
                $(this).val($(this).parent().find('.max_nilai').val());
            }
        });

        $(".max_nilai").change(function(){
            if ($(this).parent().find('.min_nilai').val() < 1) {
                $(this).parent().find('.min_nilai').val($(this).val());
            }

            if (parseInt($(this).val()) < parseInt($(this).parent().find('.min_nilai').val())) {
                $(this).val($(this).parent().find('.min_nilai').val());
            } else if (parseInt($(this).val()) > $(this).attr("max")) {
                $(this).val($(this).attr("max"));
            }
        });
    });
</script>