@extends('layouts.admin')
@section('main')
@php
    function rupiah($num) {
        return 'Rp. '.number_format(intval($num), 0, ',', '.');
    }
@endphp
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>Seting Poin Item Berkas Pendaftaran
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
                <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#createModal">
                    Create
                </button>
            </div>
        </div>
        <div class="page-title-actions">
            
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>
<div class="main-card mb-3 card">
    <div class="card-body">
        <table style="width: 100%;" class="table table-hover table-striped table-bordered table-default">
            <thead>
            <tr>
                <th>Range Nilai</th>
                <th>Nilai Berkas</th>
                <th>Keterangan</th>
                <th>Tgl DIbuat</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($nilai_berkass as $item)
                <tr>
                    <td>{{$item->min_nilai}} - {{$item->max_nilai}}</td>
                    <td>{{$item->nilai}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>
                        {{Carbon\Carbon::parse($item->created_at)->isoFormat('D MMMM Y')}}
                    </td>
                    <td>
                        <button type="button" class="btn_update_modal btn btn-xs btn-success" data-toggle="modal" data-target="#updateModal" data-remote="{{url('admin/nilai-berkas/edit/'.$item->nilai_berkas_id)}}"><i class="fas fa-pencil-alt"></i></button>
                        <button type="button" class="btn_delete_modal btn btn-xs btn-warning" data-toggle="modal" data-target="#deleteModal" data-remote="{{url('admin/nilai-berkas/destroy/'.$item->nilai_berkas_id)}}"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('modal')
<!-- Modal -->
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="createModalLabel">Buat Data Baru</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/nilai-berkas/store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type" type="text" value="create" hidden>
            <div class="form-group">
                <label>Range Nilai <span class='text-danger' title='This field is required'>*</span></label>
                <div class="form-group row">
                    <div class="col-sm-6">
                        <div class="input-group input-group-sm">
                            <input name="min_nilai" type="number" class="min_nilai form-control" placeholder="1" min="0" max="10" value="{{ old('min_nilai') }}" required>
                            <div class="input-group-prepend input-group-append">
                                <span class="input-group-text" id="">Sampai</span>
                            </div>
                            <input name="max_nilai" type="number" class="max_nilai form-control" placeholder="10" min="0" max="10" value="{{ old('max_nilai') }}" required>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Nilai Berkas <span class='text-danger' title='This field is required'>*</span></label>
                <input name="nilai" type="number" class="form-control form-control-sm" placeholder="0.00" min="0" max="10" step=".01" value="{{ old('nilai') ? old('nilai') : '0.0' }}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>keterangan</label>
                <textarea name="keterangan" id="keterangan" class='form-control form-control-sm' rows='5'>{{old('keterangan')}}</textarea>
                <div class="text-danger"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

{{-- @foreach ($nilai_berkass as $item)
<div class="modal fade" id="updateModal-{{$item->nilai_berkas_id}}" tabindex="-1" role="dialog" aria-labelledby="updateModal-{{$item->nilai_berkas_id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="updateModal-{{$item->nilai_berkas_id}}Label">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/nilai-berkas/update/'.$item->nilai_berkas_id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type-{{$item->nilai_berkas_id}}" type="text" value="update" hidden>
            <div class="form-group">
                <label>Range Nilai <span class='text-danger' title='This field is required'>*</span></label>
                <div class="form-group row">
                    <div class="col-sm-2">
                        <input name="min_nilai" type="number" class="min_nilai form-control form-control-sm" placeholder="1" min="0" max="10" value="{{ $item->min_nilai }}" required>
                      </div>
                    <label for="staticEmail" class="col-sm-2 col-form-label">-</label>
                    <div class="col-sm-2">
                        <input name="max_nilai" type="number" class="max_nilai form-control form-control-sm" placeholder="10" min="0" max="10" value="{{ $item->max_nilai }}" required>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Nilai Berkas <span class='text-danger' title='This field is required'>*</span></label>
                <input name="nilai" type="number" class="form-control form-control-sm" placeholder="0.00" min="0" max="10" step=".01" value="{{ $item->nilai }}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>keterangan</label>
                <textarea name="keterangan" id="keterangan" class='form-control form-control-sm' rows='5'>{{$item->keterangan}}</textarea>
                <div class="text-danger"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal-{{$item->nilai_berkas_id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModal-{{$item->nilai_berkas_id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="deleteModal-{{$item->nilai_berkas_id}}Label">Hapus Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Apakah anda yakin untuk menghapus data ini? 
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <a href="{{url('admin/nilai-berkas/destroy/'.$item->nilai_berkas_id)}}" class="btn_hapus btn btn-danger btn-sm">Hapus</a>
        </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        if ('{{old("type-".$item->nilai_berkas_id)}}' == 'update') {
            $(document).ready(function () {
                $("#updateModal-{{$item->nilai_berkas_id}}").modal('show');
            });
        }

        $('.select2-update-{{$item->nilai_berkas_id}}').select2({
            placeholder: "**Silahkan Pilih",
            dropdownParent: $("#updateModal-{{$item->nilai_berkas_id}}")
        });
    });
</script>
@endforeach --}}
<script>
    $(document).ready(function () {
        $(".min_nilai").change(function(){
            if ($(this).parent().find('.max_nilai').val() < 1) {
                $(this).parent().find('.max_nilai').val($(this).val());
            }

            if (parseInt($(this).val()) < $(this).attr("min")) {
                $(this).val($(this).attr("min"));
            } else if (parseInt($(this).val()) > parseInt($(this).parent().find('.max_nilai').val())) {
                $(this).val($(this).parent().find('.max_nilai').val());
            }
        });

        $(".max_nilai").change(function(){
            if ($(this).parent().find('.min_nilai').val() < 1) {
                $(this).parent().find('.min_nilai').val($(this).val());
            }

            if (parseInt($(this).val()) < parseInt($(this).parent().find('.min_nilai').val())) {
                $(this).val($(this).parent().find('.min_nilai').val());
            } else if (parseInt($(this).val()) > $(this).attr("max")) {
                $(this).val($(this).attr("max"));
            }
        });
    });
</script>
@endsection