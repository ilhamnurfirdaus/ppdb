@extends('layouts.admin')
@section('main')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>
                Dashboard
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
            </div>
        </div>
        <div class="page-title-actions">

        </div>
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="mb-3 card">
            <div class="card-header-tab card-header bg-danger text-white">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    Grafik Pendaftar Berdasarkan Sekolah
                </div>
            </div>
            <div class="card-body">
                <canvas id="myChart" height="80px"></canvas>
                
            </div>
        </div>            
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="mb-3 card">
            <div class="card-header-tab card-header bg-danger text-white">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    Grafik Pendaftar Berdasarkan Jurusan
                </div>
            </div>
            <div class="card-body">
                <canvas id="grafik_jurusan" height="80px"></canvas>
                
            </div>
        </div>            
    </div>
</div>

<div class="row">
    <div class="col-12">
        <div class="mb-3 card">
            <div class="card-header-tab card-header bg-danger text-white">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    Grafik Pendaftar Berdasarkan Jenis Kelamin
                </div>
            </div>
            <div class="card-body">
                <canvas id="grafik_jenis_kelamin" height="80px"></canvas>
                
            </div>
        </div>            
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-lg-4">
        <div class="mb-3 card">
            <div class="card-header-tab card-header bg-danger text-white">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    Pendaftar Berdasarkan Sekolah
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Sekolah</th>
                            <th>Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($siswa as $item)
                        <tr>
                            <th>{{$item->asal_sekolah}}</th>
                            @php
                                $i = 0;
                                foreach ($siswa_all as $col) {
                                    if ($item->asal_sekolah == $col->asal_sekolah) {
                                        $i++;
                                    }
                                }
                            @endphp
                            <td>{{$i}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>            
    </div>

    <div class="col-sm-12 col-lg-4">
        <div class="mb-3 card">
            <div class="card-header-tab card-header bg-danger text-white">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    Pendaftar Berdasarkan Jurusan
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Jurusan</th>
                            <th>Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($jurusan as $item)
                        <tr>
                            <th>{{$item->name}}</th>
                            @php
                                $i=0;
                                foreach($item->siswa as $col){
                                    $i++;
                                }
                            @endphp
                            <td>{{$i}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>            
    </div>

    <div class="col-sm-12 col-lg-4">
        <div class="mb-3 card">
            <div class="card-header-tab card-header bg-danger text-white">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    Pendaftar Berdasarkan Jenis Kelamin
                </div>
            </div>
            <div class="card-body">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Jenis Kelamin</th>
                            <th>Jumlah</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th>Laki-Laki</th>
                            <td>{{$lk}}</td>
                        </tr>
                        <tr>
                            <th>Perempuan</th>
                            <td>{{$pr}}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>            
    </div>
</div>
@endsection

@section('script')
{{-- <script src="https://cdn.jsdelivr.net/npm/chart.js"></script> --}}
<script src="{{asset('templates/chartjs/chart.js')}}"></script>
<script>
    var label = [];
    var isi = [];
    $.get("{{url('admin/grafik_sekolah')}}", function(get_data){
        // console.log(get_data);
        $.each( get_data.label, function( key, value ) {
            label.push(value.asal_sekolah);
        });
        // console.log(label);
        const data = {
        labels: label,
        datasets: [{
            label: 'Asal Sekolah',
            backgroundColor: 'rgb(255, 99, 132)',
            borderColor: 'rgb(255, 99, 132)',
            data: get_data.isi,
        }]
        };

        const config = {
        type: 'line',
        data: data,
        options: {}
        };

        const myChart = new Chart(
            document.getElementById('myChart'),
            config
        );
    })

    
    $.get("{{url('admin/grafik_jurusan')}}", function(get_data){
        // console.log(get_data);
        // const labels = Utils.months({count: 7});
        const data = {
        labels: get_data.label,
        datasets: [{
            label: 'Data Jurusan',
            data: get_data.isi,
            backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            'rgba(255, 205, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(201, 203, 207, 0.2)'
            ],
            borderColor: [
            'rgb(255, 99, 132)',
            'rgb(255, 159, 64)',
            'rgb(255, 205, 86)',
            'rgb(75, 192, 192)',
            'rgb(54, 162, 235)',
            'rgb(153, 102, 255)',
            'rgb(201, 203, 207)'
            ],
            borderWidth: 1
        }]
        };

        const config = {
        type: 'bar',
        data: data,
        options: {
            scales: {
            y: {
                beginAtZero: true
            }
            }
        },
        };
        
        const myChart = new Chart(
            document.getElementById('grafik_jurusan'),
            config
        );
    });
        
    $.get("{{url('admin/grafik_jenis_kelamin')}}", function(get_data){
        console.log(get_data)
        const data = {
        labels: ['Laki-laki', 'Perempuan'],
        datasets: [{
            label: 'Data Jenis Kelamin',
            data: [get_data.lk, get_data.pr],
            backgroundColor: [
            'rgba(255, 99, 132, 0.2)',
            'rgba(255, 159, 64, 0.2)',
            'rgba(255, 205, 86, 0.2)',
            'rgba(75, 192, 192, 0.2)',
            'rgba(54, 162, 235, 0.2)',
            'rgba(153, 102, 255, 0.2)',
            'rgba(201, 203, 207, 0.2)'
            ],
            borderColor: [
            'rgb(255, 99, 132)',
            'rgb(255, 159, 64)',
            'rgb(255, 205, 86)',
            'rgb(75, 192, 192)',
            'rgb(54, 162, 235)',
            'rgb(153, 102, 255)',
            'rgb(201, 203, 207)'
            ],
            borderWidth: 1
        }]
        };

        const config = {
        type: 'bar',
        data: data,
        options: {
            scales: {
            y: {
                beginAtZero: true
            }
            }
        },
        };
        
        const myChart = new Chart(
            document.getElementById('grafik_jenis_kelamin'),
            config
        );
    });
</script>
@endsection