@extends('layouts.admin')
@section('main')
@php
    function rupiah($num) {
        return 'Rp. '.number_format(intval($num), 0, ',', '.');
    }
@endphp
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>Master User
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
                <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#createModal">
                    Create
                </button>
            </div>
        </div>
        <div class="page-title-actions">
            
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>
<div class="main-card mb-3 card">
    <div class="card-body">
        <table style="width: 100%;" class="table table-hover table-striped table-bordered table-default">
            <thead>
            <tr>
                <th>Nama User</th>                
                <th>Email</th>
                <th>Pegawai</th>
                <th>Level</th>
                <th>Username</th>
                <th>Tgl Dibuat</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($users as $item)
                <tr>
                    <td>{{$item->name}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{$item->pegawai->name}}</td>
                    <td>{{$item->level->name}}</td>
                    <td>{{$item->username}}</td>
                    <td>
                        {{Carbon\Carbon::parse($item->created_at)->isoFormat('D MMMM Y')}}
                    </td>
                    <td>
                        {{-- <button type="button" class="btn btn-xs btn-info" data-toggle="modal" data-target="#infoModal-{{$item->id}}"><i class="fas fa-eye"></i></button> --}}
                        <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#updateModal-{{$item->id}}"><i class="fas fa-pencil-alt"></i></button>
                        <button type="button" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#deleteModal-{{$item->id}}"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('modal')
<!-- Modal -->
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="createModalLabel">Buat Data Baru</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/user/store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type" type="text" value="create" hidden>
            <div class="form-group">
                <label>Nama <span class='text-danger' title='This field is required'>*</span></label>
                <input name="name" type="text" class="form-control form-control-sm" placeholder="Isi nama user" value="{{old('name')}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Email  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="email" type="email" class="form-control form-control-sm" placeholder="Isi email user" value="{{old('email')}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Pegawai <span class='text-danger' title='This field is required'>*</span></label>
                <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="profil_id" style="width: 100%;" required>
                    <option value="">**Pilih</option>
                    @foreach ($pegawais as $row)
                    <option value="{{$row->id}}" @if($row->id == old("profil_id")) selected @endif>
                        {{$row->name}}
                    </option>
                    @endforeach
                </select>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Level <span class='text-danger' title='This field is required'>*</span></label>
                <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="level_id" style="width: 100%;" required>
                    <option value="">**Pilih</option>
                    @foreach ($levels as $row)
                    <option value="{{$row->id}}" @if($row->id == old("level_id")) selected @endif>
                        {{$row->name}}
                    </option>
                    @endforeach
                </select>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Username <span class='text-danger' title='This field is required'>*</span></label>
                <input name="username" type="text" class="form-control form-control-sm" placeholder="Isi username untuk login" value="{{old('username')}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Password <span class='text-danger' title='This field is required'>*</span></label>
                <input name="password" type="password" class="form-control form-control-sm" placeholder="Isi password untuk login" required>
                <div class="text-danger"></div>
                <p class="help-block"><span class='text-danger' title='This field is required'>*</span> Minimal 5 karakter</p>
            </div>
            <div class="form-group">
                <label>Konfirmasi Password <span class='text-danger' title='This field is required'>*</span></label>
                <input name="password_confirmation" type="password" class="form-control form-control-sm" placeholder="Ketik ulang password untuk konfirmasi" required>
                <p class="help-block"><span class='text-danger' title='This field is required'>*</span> Minimal 5 karakter</p>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

@foreach ($users as $item)
<div class="modal fade" id="infoModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="infoModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="infoModal-{{$item->id}}Label">Info Lanjut</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        </div>
        </div>
    </div>
</div>

<div class="modal fade" id="updateModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="updateModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="updateModal-{{$item->id}}Label">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/user/update/'.$item->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type-{{$item->id}}" type="text" value="update" hidden>
            <div class="form-group">
                <label>Nama <span class='text-danger' title='This field is required'>*</span></label>
                <input name="name" type="text" class="form-control form-control-sm" placeholder="Isi nama user" value="{{$item->name}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Email  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="email" type="email" class="form-control form-control-sm" placeholder="Isi email user" value="{{$item->email}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Pegawai <span class='text-danger' title='This field is required'>*</span></label>
                <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="profil_id" style="width: 100%;" required>
                    <option value="">**Pilih</option>
                    @foreach ($pegawais as $row)
                    <option value="{{$row->id}}" @if($row->id == $item->profil_id) selected @endif>
                        {{$row->name}}
                    </option>
                    @endforeach
                </select>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Level <span class='text-danger' title='This field is required'>*</span></label>
                <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="level_id" style="width: 100%;" required>
                    <option value="">**Pilih</option>
                    @foreach ($levels as $row)
                    <option value="{{$row->id}}" @if($row->id == $item->level_id) selected @endif>
                        {{$row->name}}
                    </option>
                    @endforeach
                </select>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Username <span class='text-danger' title='This field is required'>*</span></label>
                <input name="username" type="text" class="form-control form-control-sm" placeholder="Isi username untuk login" value="{{$item->username}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Password</label>
                <input name="password" type="password" class="form-control form-control-sm" placeholder="Isi password untuk login">
                <div class="text-danger"></div>
                <p class="help-block">Minimal 5 karakter. Kosongkan jika tidak ganti password</p>
            </div>
            <div class="form-group">
                <label>Konfirmasi Password</label>
                <input name="password_confirmation" type="password" class="form-control form-control-sm" placeholder="Ketik ulang password untuk konfirmasi">
                <p class="help-block">Minimal 5 karakter. Kosongkan jika tidak ganti password</p>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="deleteModal-{{$item->id}}Label">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Apakah anda yakin untuk menghapus data ini? 
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <a href="{{url('admin/user/destroy/'.$item->id)}}" class="btn btn-danger btn-sm">Hapus</a>
        </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        if ('{{old("type-".$item->id)}}' == 'update') {
            $(document).ready(function () {
                $("#updateModal-{{$item->id}}").modal('show');
            });
        }

        $('.select2-update-{{$item->id}}').select2({
            placeholder: "**Silahkan Pilih",
            dropdownParent: $("#updateModal-{{$item->id}}")
        });
    });
</script>
@endforeach
@endsection
@section('script')
<script>
    $(document).ready(function(){
        if ('{{old("type")}}' == 'create') {
            $("#createModal").modal('show');
        } 
    });
</script>
@endsection