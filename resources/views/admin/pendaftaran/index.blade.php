@extends('layouts.admin')
@section('main')
@php
    function rupiah($num) {
        return 'Rp. '.number_format(intval($num), 0, ',', '.');
    }
@endphp
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div>Pendaftaran
                {{-- <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#createModal">
                    Create
                </button> --}}
            </div>
        </div>
        <div class="page-title-actions">
            
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>
<div class="main-card mb-3 card">
    <div class="card-body">
        <form action="" class="form-inline">
            <label for="">Tahun Ajaran</label>
            <select name="tahun_ajaran" id="tahun_ajaran" class="form-control form-control-sm ml-2" style="width: 20%" onchange="this.form.submit()">
                @foreach ($tahun_ajaran_all as $item)
                    <option value="{{$item->tahun_ajaran}}" @if ($item->tahun_ajaran == Request::input('tahun_ajaran')) selected @endif>{{$item->tahun_ajaran}}</option>
                @endforeach
            </select>
        </form>
        <div class="custom-control custom-switch mb-2">
            <input type="checkbox" class="custom-control-input" id="raport" @if ($tahun_ajaran->tampil_raport == 'Ya') checked @endif>
            <label class="custom-control-label" for="raport">Raport</label>
        </div>
        
        {{--  <form action="{{url('admin/save_jml_raport', $tahun_ajaran->tahun_ajaran)}}" class="mb-2" method="POST" enctype="multipart/form-data">
            @csrf   
            <div class="input-group input-group-sm" id="form_raport" style="width: 30%">
                <div class="input-group-prepend">
                <span class="input-group-text" style="text-transform: capitalize !important;">Jumlah Raport</span>
                </div>
                <input type="number" name="jml_raport" style="width: 10%" class="form-control" value="{{ $tahun_ajaran->jml_raport }}" min="0" style="width: 100px;" required>
                <div class="input-group-append">
                    <button class="btn btn-outline-success" type="submit">Simpan</button>
                </div>
            </div>
        </form> --}}

        <div class="load_raport"></div>

        <div class="custom-control custom-switch mb-2">
            <input type="checkbox" class="custom-control-input" id="sertifikat" @if ($tahun_ajaran->tampil_sertifikat == 'Ya') checked @endif>
            <label class="custom-control-label" for="sertifikat">Sertifikat</label>
        </div>

        <div class="load_sertifikat"></div>

        <div class="custom-control custom-switch mb-2">
            <input type="checkbox" class="custom-control-input" id="berkas" @if ($tahun_ajaran->tampil_berkas == 'Ya') checked @endif>
            <label class="custom-control-label" for="berkas">Berkas</label>
        </div>
        
        <div class="load_berkas"></div>

        <div class="custom-control custom-switch mb-2">
            <input type="checkbox" class="custom-control-input" id="test" @if ($tahun_ajaran->tampil_test == 'Ya') checked @endif>
            <label class="custom-control-label" for="test">Test</label>
        </div>

        <form action="{{url('admin/save_waktu_soal', $tahun_ajaran->tahun_ajaran)}}" method="POST" enctype="multipart/form-data">
            @csrf   
            <div class="input-group input-group-sm" id="form_test" style="width: 30%">
                <div class="input-group-prepend">
                <span class="input-group-text" style="text-transform: capitalize !important;">Waktu Soal</span>
                </div>
                <input type="number" name="waktu_soal" style="width: 10%" class="form-control" value="{{ $tahun_ajaran->waktu_soal }}" min="0" style="width: 100px;" required>
                <div class="input-group-append">
                    <span class="input-group-text" style="text-transform: capitalize !important;">Jam</span>
                    <button class="btn btn-outline-success" type="submit">Simpan</button>
                </div>
            </div>
        </form>

    </div>
</div>
@endsection

@section('script')
    <script>
        $(document).on('click', '.btn-delete', function() { 
            $(this).parent().parent().remove(); 
        });
        
        $(".edit_sertifikat").click(function(){
            console.log('cekkkkk')
        })
        
        var id = $('#tahun_ajaran').val();
        if ($('#raport').is(':checked')) {
            // $('#form_raport').show();

            let url = "{{url('admin/get_setting_raport')}}?tahun_ajaran="+id;
            $('.load_raport').load(url, function( data, textStatus, xhr ) {
                if ( textStatus == "error" ) {
                    alert( "Koneksi Internet anda terputus." );
                } else {
                    console.log("berhasil");
                }
            });
        }else{
            // $('#form_raport').hide();
        }

        $('#raport').change(function(){
            if ($('#raport').is(':checked')) {
                // $('#form_raport').show();
                
                let url = "{{url('admin/get_setting_raport')}}?tahun_ajaran="+id;
                $('.load_raport').load(url, function( data, textStatus, xhr ) {
                    if ( textStatus == "error" ) {
                        alert( "Koneksi Internet anda terputus." );
                    } else {
                        console.log("berhasil");
                    }
                });
            }else{
                // $('#form_raport').hide();
            }

            $.get('{{url("admin/save_switch_raport")}}/'+id, function(data){
                console.log(data);
            });
        });
        
        if ($('#sertifikat').is(':checked')) {
            let url = "{{url('admin/get_nilai_sertifikat')}}/";
            $('.load_sertifikat').load(url, function( data, textStatus, xhr ) {
                if ( textStatus == "error" ) {
                        alert( "Koneksi Internet anda terputus." );
                    } else {
                        console.log("berhasil");
                    }
            });
        }

        $('#sertifikat').change(function(){
            $('.load_sertifikat').html('');
            if ($('#sertifikat').is(':checked')) {
            let url = "{{url('admin/get_nilai_sertifikat')}}/";
            $('.load_sertifikat').load(url, function( data, textStatus, xhr ) {
                if ( textStatus == "error" ) {
                        alert( "Koneksi Internet anda terputus." );
                    } else {
                        console.log("berhasil");
                    }
            });
            }else{
                $('.load_sertifikat').html('');
            }

            $.get('{{url("admin/save_switch_sertifikat")}}/'+id, function(data){
                    console.log(data);
                });
        });

        if ($('#berkas').is(':checked')) {
        let url = "{{url('admin/get_syarat_pendaftaran')}}/";
        $('.load_berkas').load(url, function( data, textStatus, xhr ) {
            if ( textStatus == "error" ) {
                    alert( "Koneksi Internet anda terputus." );
                } else {
                    console.log("berhasil");
                }
        });
        }
        $('#berkas').change(function(){
            $('.load_berkas').html('');
            if ($('#berkas').is(':checked')) {
            let url = "{{url('admin/get_syarat_pendaftaran')}}/";
            $('.load_berkas').load(url, function( data, textStatus, xhr ) {
                if ( textStatus == "error" ) {
                        alert( "Koneksi Internet anda terputus." );
                    } else {
                        console.log("berhasil");
                    }
            });
            }else{
                $('.load_berkas').html('');
            }

            $.get('{{url("admin/save_switch_berkas")}}/'+id, function(data){
                    console.log(data);
                });
        });

        if ($('#test').is(':checked')) {
            $('#form_test').show();
        }else{
            $('#form_test').hide();
        }
        $('#test').change(function(){
            if ($('#test').is(':checked')) {
                $('#form_test').show();
            }else{
                $('#form_test').hide();
            }
            $.get('{{url("admin/save_switch_test")}}/'+id, function(data){
                    console.log(data);
                });
        });

    </script>
@endsection
