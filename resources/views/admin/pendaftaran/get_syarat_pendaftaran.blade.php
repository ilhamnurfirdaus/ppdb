{{-- @extends('layouts.admin')
@section('main')
@php
    function rupiah($num) {
        return 'Rp. '.number_format(intval($num), 0, ',', '.');
    }
@endphp --}}

{{-- <div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div>Syarat Pendaftaran
                <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#createModal">
                    Create
                </button>
            </div>
        </div>
        <div class="page-title-actions">
            
        </div>    
    </div>
</div> --}}
<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>
<div class="main-card mb-3 card">
    
    <div class="card-body">
        <button type="button" class="btn_update_modal btn-shadow mb-2 btn btn-sm btn-success" data-toggle="modal" data-target="#updateModal" data-remote="{{url('admin/add_syarat_pendaftaran')}}">
            Create
        </button>
        <table style="width: 100%;" class="table table-hover table-striped table-bordered table-default">
            <thead>
            <tr>
                <th>Judul</th>
                <th style="width: 50%">Deskripsi</th>
                <th>Tgl DIbuat</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($setting_berkass as $item)
                <tr>
                    <td>{{$item->nama_berkas}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>
                        {{Carbon\Carbon::parse($item->created_at)->isoFormat('D MMMM Y')}}
                    </td>
                    <td>
                        <button type="button" class="btn_update_modal btn btn-xs btn-success" data-toggle="modal" data-target="#updateModal" data-remote="{{url('admin/syarat-pendaftaran/edit/'.$item->setting_berkas_id)}}"><i class="fas fa-pencil-alt"></i></button>
                        <button type="button" class="btn_delete_modal btn btn-xs btn-warning" data-toggle="modal" data-target="#deleteModal" data-remote="{{url('admin/syarat-pendaftaran/destroy/'.$item->setting_berkas_id)}}"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
{{-- @endsection --}}

{{-- @section('modal') --}}
<!-- Modal -->


{{-- @endsection --}}

<script>
    $(".btn_update_modal").click(function() {
        $("#model_content_update").empty(); 
        let url = $(this).data("remote");
        console.log(url);
        $('#model_content_update').load(url, function( data, textStatus, xhr ) {
            if ( textStatus == "error" ) {
                alert( "Koneksi Internet anda terputus." );
            } else {
                console.log("berhasil");
            }
        });
    });
</script>