<form action="{{url('admin/syarat-pendaftaran/store')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal-body">
        <input name="type" type="text" value="create" hidden>
        <div class="form-group">
            <label>Judul  <span class='text-danger' title='This field is required'>*</span></label>
            <input name="nama_berkas" type="text" class="form-control form-control-sm" placeholder="Isi judul syarat pendaftaran" value="{{old('nama_berkas')}}" required>
            <div class="text-danger"></div>
        </div>
        <div class="form-group">
            <label>Deksripsi</label>
            <textarea name="keterangan" id="keterangan" class='form-control form-control-sm' rows='5'>{{old('keterangan')}}</textarea>
            <div class="text-danger"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-success btn-sm">Simpan</button>
    </div>
    </form>