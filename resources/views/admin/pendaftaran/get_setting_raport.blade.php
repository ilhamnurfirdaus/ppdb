<div class="main-card mb-3 card">
    <div class="card-body">
        <form action="{{url('admin/save_setting_raport')}}" method="POST" enctype="multipart/form-data">
        @csrf
            <table style="width: 100%;" class="table table-hover table-striped table-bordered table-default">
                <thead>
                <tr>
                    <th>Nama Raport</th>
                    <th>Keterangan</th>
                </tr>
                </thead>
                <tbody>
                    <input type="hidden" name="tahun_ajaran" value="{{ Request::input('tahun_ajaran') }}">
                    @foreach ($setting_raports as $item)
                    <tr>
                        <td>
                            <input name="setting_raport_id[]" type="hidden" value="{{ $item->setting_raport_id }}">
                            <input name="nama_raport[]" type="text" class="form-control form-control-sm" placeholder="Isi nama raport" value="{{ $item->nama_raport }}" required>
                        </td>
                        <td>
                            <input name="keterangan[]" type="text" class="form-control form-control-sm" placeholder="isi keterangan raport" value="{{ $item->keterangan }}">
                        </td>
                        <td>
                            <button class="btn btn-sm btn-secondary btn-delete" type="button">X</button>
                        </td>
                    </tr>
                    @endforeach
                    <tr class="row_sr" style="display: none;">
                        <td>
                            <input class="setting_raport_id" type="hidden" value="Baru">
                            <input type="text" class="form-control form-control-sm nama_raport" placeholder="Isi nama raport">
                        </td>
                        <td>
                            <input type="text" class="form-control form-control-sm keterangan" placeholder="isi keterangan raport">
                        </td>
                        <td>
                            <button class="btn btn-sm btn-secondary btn-delete" type="button">X</button>
                        </td>
                    </tr>
                </tbody>
            </table>

            <div class="form-group my-3">
                <button id="btn-tambah" type="button" class="btn btn-success">
                    Tambah
                </button>
                <button type="submit" class="btn btn-success">
                    Simpan
                </button>
            </div>
        </form>
    </div>
</div>

<script>
    $('#btn-tambah').click(function () {
        let tambah_input = $(this).parent().parent().find('.row_sr').clone();
        tambah_input.show();
        tambah_input.removeClass("row_sr");
        tambah_input.find(".setting_raport_id").attr('required',true);
        tambah_input.find(".setting_raport_id").attr('name','setting_raport_id[]');
        tambah_input.find(".nama_raport").attr('required',true);
        tambah_input.find(".nama_raport").attr('name','nama_raport[]');
        tambah_input.find(".keterangan").attr('name','keterangan[]');
        tambah_input.insertBefore($(this).parent().parent().find(".row_sr"));
    });
</script>