{{-- @extends('layouts.admin') --}}
{{-- @section('main') --}}
{{-- @php
    function rupiah($num) {
        return 'Rp. '.number_format(intval($num), 0, ',', '.');
    }
@endphp --}}
{{-- <div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div>Seting Poin Item Sertifikat Pendaftaran</div>
        </div>   
    </div>
</div> --}}
<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>
<div class="main-card mb-3 card">
    <div class="card-body">
        <table style="width: 100%;" class="table table-hover table-striped table-bordered table-default">
            <thead>
            <tr>
                <th>Nama Tingkat</th>
                <th>Peringkat</th>
                <th>Nilai Tingkat</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($nilai_sertifikats as $item)
                <tr>
                    <td>{{$item->nama_tingkat}}</td>
                    <td>{{$item->peringkat}}</td>
                    <td>{{$item->nilai}}</td>
                    <td>
                        
                        <button type="button" class="btn_update_modal btn btn-xs btn-success" data-toggle="modal" data-target="#updateModal" data-remote="{{url('admin/edit_sertifikat/'.$item->nilai_sertifikats_id)}}"><i class="fas fa-pencil-alt"></i></button>
                        {{-- <button type="button" class="btn btn-xs btn-success" id="edit_sertifikat"  data-id="{{$item->nilai_sertifikats_id}}" data-toggle="modal" data-target="#editModal"><i class="fas fa-pencil-alt"></i></button> --}}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
{{-- @endsection --}}
{{-- 
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <div class="form-group">
                <label>Nama Tingkat</label>
                <input name="nama_tingkat" type="text" class="form-control form-control-sm" placeholder="Isi nama tingkat sertifikat" value="{{ $item->nama_tingkat }}" disabled>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Peringkat</label>
                <input name="peringkat" type="number" class="form-control form-control-sm" placeholder="Isi peringkat" value="{{ $item->peringkat }}" disabled>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Nilai Tingkat <span class='text-danger' title='This field is required'>*</span></label>
                <input name="nilai" type="number" class="nilai form-control form-control-sm" placeholder="Isi nilai tingkat sertifikat" min="1" max="10" value="{{ $item->nilai }}" required>
                <div class="text-danger"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div> --}}

{{-- @section('modal') --}}
{{-- @foreach ($nilai_sertifikats as $item)
<div class="modal fade" id="updateModal-{{$item->nilai_sertifikats_id}}" tabindex="-1" role="dialog" aria-labelledby="updateModal-{{$item->nilai_sertifikats_id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="updateModal-{{$item->nilai_sertifikats_id}}Label">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/nilai-sertifikat/update/'.$item->nilai_sertifikats_id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type-{{$item->nilai_sertifikats_id}}" type="text" value="update" hidden>
            <div class="form-group">
                <label>Nama Tingkat</label>
                <input name="nama_tingkat" type="text" class="form-control form-control-sm" placeholder="Isi nama tingkat sertifikat" value="{{ $item->nama_tingkat }}" disabled>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Peringkat</label>
                <input name="peringkat" type="number" class="form-control form-control-sm" placeholder="Isi peringkat" value="{{ $item->peringkat }}" disabled>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Nilai Tingkat <span class='text-danger' title='This field is required'>*</span></label>
                <input name="nilai" type="number" class="nilai form-control form-control-sm" placeholder="Isi nilai tingkat sertifikat" min="1" max="10" value="{{ $item->nilai }}" required>
                <div class="text-danger"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>
<script>
    $(document).ready(function () {
        if ('{{old("type-".$item->nilai_sertifikats_id)}}' == 'update') {
            $(document).ready(function () {
                $("#updateModal-{{$item->nilai_sertifikats_id}}").modal('show');
            });
        }

        $('.select2-update-{{$item->nilai_sertifikats_id}}').select2({
            placeholder: "**Silahkan Pilih",
            dropdownParent: $("#updateModal-{{$item->nilai_sertifikats_id}}")
        });
    });
</script>
@endforeach
<script>
    $(document).ready(function () {
        $(".nilai").change(function(){
            if (parseInt($(this).val()) < $(this).attr("min")) {
                $(this).val($(this).attr("min"));
            } else if (parseInt($(this).val()) > $(this).attr("max")) {
                $(this).val($(this).attr("max"));
            }
        });
    });
    nilai
</script> --}}
{{-- @endsection --}}