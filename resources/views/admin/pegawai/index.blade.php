@extends('layouts.admin')
@section('main')
@php
    function rupiah($num) {
        return 'Rp. '.number_format(intval($num), 0, ',', '.');
    }
@endphp
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>Master Pegawai
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
                <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#createModal">
                    Create
                </button>

                <button type="button" data-toggle="modal" data-target="#modal-upload" class='btn-shadow ml-3 btn btn-sm btn-success'>
                    <i class='fa fa-upload'></i> Import
                </button>

                <a href="{{url('admin/pegawai/export_pdf')}}{{Request::input('q') != null ? '/'.Request::input('q') : '/kosong'}}{{Request::input('tgl_mulai') != null ? '/'.Request::input('tgl_mulai') : '/kosong'}}{{Request::input('tgl_berakhir') != null ? '/'.Request::input('tgl_berakhir') : '/kosong'}}" target="blank" id='import' class='btn-shadow ml-3 btn btn-sm btn-danger'><i class='fa fa-upload'></i> Cetak</a>
            </div>
        </div>
        <div class="page-title-actions">
            
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>
<div class="main-card mb-3 card">
    <div class="card-header">
        <h3 class="card-title"></h3>

        <div class="card-tools">
            <form class="form-inline" method="get" action="{{ Request::fullUrl() }}" style="font-size: 12px;">
                Tanggal masuk :
                <div class="input-group input-group-sm ml-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Mulai</span>
                    </div>
                    <input id="tgl_mulai" name="tgl_mulai" type="date" class="tgl_mulai form-control" value="{{Carbon\Carbon::parse(Request::input('tgl_mulai'))->format('Y-m-d')}}">
                </div>

                <div class="input-group input-group-sm ml-3">
                    <div class="input-group-prepend">
                        <span class="input-group-text" id="basic-addon1">Sampai</span>
                    </div>
                    <input id="tgl_berakhir" name="tgl_berakhir" type="date" class="tgl_berakhir form-control" value="{{Carbon\Carbon::parse(Request::input('tgl_berakhir'))->format('Y-m-d')}}">
                </div>

                <div class="input-group input-group-sm ml-3" style="width: 150px;">
                    <input type="text" name="search" class="form-control" placeholder="Search" value="{{ Request::input('search') }}">
        
                    <div class="input-group-append">
                        <button type="submit" class="btn btn-default btn-primary">
                        <i class="fas fa-search"></i>
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    <div class="card-body">
        
        <table style="width: 100%; font-size: 10px;" class="table table-hover table-striped table-bordered table-export">
            <thead>
            <tr>
                <th>Tgl Masuk</th>
                <th>NIP</th>
                <th>Nama</th>
                <th style="width: 150px;">Tempat, Tgl lahir</th>
                <th>Jabatan</th>
                <th>Email</th>
                <th>Telp</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($pegawais as $item)
                <tr>
                    <td>
                        {{Carbon\Carbon::parse($item->tanggal_masuk)->isoFormat('D MMMM Y')}}
                    </td>
                    <td>{{$item->nip}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->tempat_lahir}}, {{Carbon\Carbon::parse($item->tanggal_lahir)->isoFormat('D MMMM Y')}}</td>
                    <td>{{$item->jabatan->name}}</td>
                    <td>{{$item->email}}</td>
                    <td>{{$item->telp}}</td>
                    <td>
                        <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#updateModal-{{$item->id}}"><i class="fas fa-pencil-alt"></i></button>
                        <button type="button" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#deleteModal-{{$item->id}}"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('modal')
<div class="modal fade" id="modal-upload">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Import Data</h4>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;
                </button>
            </div>
            <form method='post' enctype='multipart/form-data' action='{{url('admin/pegawai/import-excel')}}'>
            @csrf
            <div class="modal-body">
                <div class='form-group'>
                    <input type='file' name='file' required accept='.xls,.xlsx' class='form-control-file'>
                    <hr>
                    File format yang didukung: xls, xlsx. <a href='{{url('admin/pegawai/export-excel')}}'>Klik disini</a> unduh contoh format file xls import.  <br>
                    <strong>Perhatian =</strong> <br>
                    1. Mohon import data pegawai yang belum pernah diimport sebelumnya. <br>
                    2. Mohon tetap menggunakan header kolom sesuai format sampel (contoh) <br>
                    3. Ganti data dengan data Anda, mulai dari urutan ke 2 dan seterusnya <br>
                    4. Jika ukuran file Anda besar ( > 5 MB), mohon dapat dibagi menjadi beberapa file kecil ( 1 MB ) <br>
                    5. Pastikan penulisan nama orang, nama perusahaan, department, divisi, pegawai ditulis dengan huruf yang 'konsisten'
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary">Import</button>
            </div>
            </form>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<!-- Modal -->
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="createModalLabel">Buat Data Baru</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/pegawai/store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type" type="text" value="create" hidden>
            <div class="form-group">
                <label>Tanggal Masuk <span class='text-danger' title='This field is required'>*</span></label>
                <input name="tanggal_masuk" type="date" class="form-control form-control-sm" value="{{Carbon\Carbon::parse(old('tanggal_masuk'))->format('Y-m-d')}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>NIP <span class='text-danger' title='This field is required'>*</span></label>
                <input name="nip" type="number" class="form-control form-control-sm" placeholder="Isi NIP" value="{{old('nip')}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Nama <span class='text-danger' title='This field is required'>*</span></label>
                <input name="name" type="text" class="form-control form-control-sm" placeholder="Isi nama pegawai" value="{{old('name')}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Tempat Lahir <span class='text-danger' title='This field is required'>*</span></label>
                <select name="tempat_lahir" class="tempat_lahir-create" style="width: 100%;" required>
                    @if (old("tempat_lahir"))
                    <option value="{{ old("tempat_lahir") }}" selected>{{ old("tempat_lahir") }}</option>
                    @endif
                </select>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Tanggal Lahir <span class='text-danger' title='This field is required'>*</span></label>
                <input name="tanggal_lahir" type="date" class="form-control form-control-sm" value="{{Carbon\Carbon::parse(old('tanggal_lahir'))->format('Y-m-d')}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Jabatan <span class='text-danger' title='This field is required'>*</span></label>
                <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="jabatan_id" style="width: 100%;" required>
                    <option value="">**Pilih</option>
                    @foreach ($jabatans as $row)
                    <option value="{{$row->id}}" @if($row->id == old("jabatan_id")) selected @endif>
                        {{$row->name}}
                    </option>
                    @endforeach
                </select>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Telp <span class='text-danger' title='This field is required'>*</span></label>
                <input name="telp" type="number" class="form-control form-control-sm" placeholder="Isi no telp" value="{{old('telp')}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Email  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="email" type="email" class="form-control form-control-sm" placeholder="Isi email pegawai" value="{{old('email')}}" required>
                <div class="text-danger"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

@foreach ($pegawais as $item)
<div class="modal fade" id="updateModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="updateModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="updateModal-{{$item->id}}Label">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/pegawai/update/'.$item->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type-{{$item->id}}" type="text" value="update" hidden>
            <div class="form-group">
                <label>Tanggal Masuk <span class='text-danger' title='This field is required'>*</span></label>
                <input name="tanggal_masuk" type="date" class="form-control form-control-sm" value="{{Carbon\Carbon::parse($item->tanggal_masuk)->format('Y-m-d')}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>NIP <span class='text-danger' title='This field is required'>*</span></label>
                <input name="nip" type="number" class="form-control form-control-sm" placeholder="Isi NIP" value="{{$item->nip}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Nama <span class='text-danger' title='This field is required'>*</span></label>
                <input name="name" type="text" class="form-control form-control-sm" placeholder="Isi nama pegawai" value="{{$item->name}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Tempat Lahir <span class='text-danger' title='This field is required'>*</span></label>
                <select name="tempat_lahir" class="tempat_lahir-update" style="width: 100%;" required>
                @if (isset($item->tempat_lahir))
                <option value="{{ $item->tempat_lahir }}" selected>{{ $item->tempat_lahir }}</option>
                @endif
                </select>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Tanggal Lahir <span class='text-danger' title='This field is required'>*</span></label>
                <input name="tanggal_lahir" type="date" class="form-control form-control-sm" value="{{Carbon\Carbon::parse($item->tanggal_lahir)->format('Y-m-d')}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Jabatan <span class='text-danger' title='This field is required'>*</span></label>
                <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="jabatan_id" style="width: 100%;" required>
                    <option value="">**Pilih</option>
                    @foreach ($jabatans as $row)
                    <option value="{{$row->id}}" @if($row->id == $item->jabatan_id) selected @endif>
                        {{$row->name}}
                    </option>
                    @endforeach
                </select>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Telp <span class='text-danger' title='This field is required'>*</span></label>
                <input name="telp" type="number" class="form-control form-control-sm" placeholder="Isi no telp" value="{{$item->telp}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Email  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="email" type="email" class="form-control form-control-sm" placeholder="Isi email pegawai" value="{{$item->email}}" required>
                <div class="text-danger"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="deleteModal-{{$item->id}}Label">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Apakah anda yakin untuk menghapus data ini? 
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <a href="{{url('admin/pegawai/destroy/'.$item->id)}}" class="btn btn-danger btn-sm">Hapus</a>
        </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        if ('{{old("type-".$item->id)}}' == 'update') {
            $(document).ready(function () {
                $("#updateModal-{{$item->id}}").modal('show');
            });
        }

        $('.select2-update-{{$item->id}}').select2({
            placeholder: "**Silahkan Pilih",
            dropdownParent: $("#updateModal-{{$item->id}}")
        });

        $('.tempat_lahir-update').select2({
            minimumInputLength: 1,
            ajax: {
                url: "{{ url('search/kotas') }}",
                dataType: 'json',
                delay: 250,
                processResults: pResult,
                cache: true
            },
            dropdownParent: $("#updateModal-{{$item->id}}")
        });
        
        function pResult(data) {
            return {
                results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.name
                        }
                })
            };
        };
    });
</script>
@endforeach
@endsection
@section('script')
<script>
    $(document).ready(function(){
        $(".tgl_mulai").change(function()
        {
            var tgl_mulai = $('#tgl_mulai').val();
            var tgl_berakhir = $('#tgl_berakhir').val();
            if (tgl_berakhir == "") {
                $('#tgl_berakhir').val(tgl_mulai);
            } else if (tgl_mulai > tgl_berakhir){
                $('#tgl_berakhir').val(tgl_mulai);
            }
        });

        $(".tgl_berakhir").change(function()
        {
            var tgl_mulai = $('#tgl_mulai').val();
            var tgl_berakhir = $('#tgl_berakhir').val();
            if (tgl_berakhir == "") {
                $('#tgl_mulai').val(tgl_berakhir);
            } else if (tgl_mulai > tgl_berakhir){
                $('#tgl_mulai').val(tgl_berakhir);
            }
        });

        $('.tempat_lahir-create').select2({
            minimumInputLength: 1,
            ajax: {
                url: "{{ url('search/kotas') }}",
                dataType: 'json',
                delay: 250,
                processResults: pResult,
                cache: true
            },
            dropdownParent: $("#createModal")
        });

        function pResult(data) {
            return {
                results: $.map(data, function (item) {
                        return {
                            text: item.name,
                            id: item.name
                        }
                })
            };
        };
    });
</script>
@endsection