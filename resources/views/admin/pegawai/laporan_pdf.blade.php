<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
        <title>Data Pegawai</title>
        <body>
            <style type="text/css">
                .footer { position: fixed; left: 0px; bottom: -50px; right: 0px; height: 50px;text-align: center;font-size: 10px;}
                .footer .pagenum:before { content: counter(page); }

                .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
                .tg td{font-family:Arial;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
                .tg th{font-family:Arial;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
                .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
            </style>

            <div class="footer">
              Page <span class="pagenum"></span>
            </div>
  
            <div style="font-family:Arial; font-size:12px;">
                <center><h2>Data Pegawai</h2></center>  
            </div>
            <br>
            <table class="tg">
              <tr>
                <th class="tg-3wr7">NIP<br></th>
                <th class="tg-3wr7">Nama<br></th>
                <th class="tg-3wr7">Tempat, Tgl Lahir<br></th>
                <th class="tg-3wr7">Telpon<br></th>
                <th class="tg-3wr7">Email<br></th>
                <th class="tg-3wr7">Jabatan<br></th>
                <th class="tg-3wr7">Tanggal Masuk<br></th>
              </tr>
              <?php $no=1 ?>
              @foreach($pegawais as $item)
              <tr>
                <td class="tg-rv4w">{{$item->nip}}</td>
                <td class="tg-rv4w">{{$item->name}}</td>
                <td class="tg-rv4w">{{$item->tempat_lahir}}, {{Carbon\Carbon::parse($item->tanggal_lahir)->isoFormat('D MMMM Y')}}</td>
                <td class="tg-rv4w">{{$item->email}}</td>
                <td class="tg-rv4w">{{$item->telp}}</td>
                <td class="tg-rv4w">{{$item->jabatan->name}}</td>
                <td class="tg-rv4w">{{Carbon\Carbon::parse($item->tanggal_masuk)->isoFormat('D MMMM Y')}}</td>
              </tr>
              @endforeach
            </table>
        </body>
    </head>
</html>