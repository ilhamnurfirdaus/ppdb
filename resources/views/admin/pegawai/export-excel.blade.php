<table>
    <thead>
    <tr>
        <th>Tanggal Masuk</th>
        <th>NIP</th>
        <th>Nama</th>
        <th>Tempat Lahir</th>
        <th>Tanggal Lahir</th>
        <th>Jabatan</th>
        <th>Telp</th>
        <th>Email</th>
        <th></th>
        <th></th>
        <th></th>
        <th></th>
        <th>List Jabatan</th>
        <th></th>
    </tr>
    </thead>
    <tbody>
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>id</td>
            <td>Nama</td>
        </tr>
        @php
            $no = 0;
        @endphp
        @foreach ($jabatans as $item)
        <tr>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td></td>
            <td>{{ $item->id }}</td>
            <td>{{ $item->name }}</td>
            @php
                $no++
            @endphp
        </tr>
        @endforeach
    </tbody>
</table>