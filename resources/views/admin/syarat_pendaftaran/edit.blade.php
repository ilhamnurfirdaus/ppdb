<form action="{{url('admin/syarat-pendaftaran/update/'.$setting_berkas->setting_berkas_id)}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal-body">
        <input name="type-{{$setting_berkas->setting_berkas_id}}" type="text" value="update" hidden>
        <div class="form-group">
            <label>Judul  <span class='text-danger' title='This field is required'>*</span></label>
            <input name="nama_berkas" type="text" class="form-control form-control-sm" placeholder="Isi judul syarat pendaftaran" value="{{ $setting_berkas->nama_berkas }}" required>
            <div class="text-danger"></div>
        </div>
        <div class="form-group">
            <label>Deskripsi</label>
            <textarea name="keterangan" id="keterangan" class='form-control form-control-sm' rows='5'>{{$setting_berkas->keterangan}}</textarea>
            <div class="text-danger"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-success btn-sm">Simpan</button>
    </div>
</form>