@extends('layouts.admin')
@section('main')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>Master Rekening
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
                <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#createModal">
                    Create
                </button>
            </div>
        </div>
        <div class="page-title-actions">
            
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>
<div class="main-card mb-3 card">
    <div class="card-body">
        <table style="width: 100%;" class="table table-hover table-striped table-bordered table-default">
            <thead>
            <tr>
                <th>Nama Bank</th>
                <th>Nama Rekening</th>
                <th>Atas Nama</th>
                <th>Tgl DIbuat</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($rekenings as $item)
                <tr>
                    <td>{{$item->nama_rekening}}</td>
                    <td>{{$item->nomor_rekening}}</td>
                    <td>{{$item->atas_nama}}</td>
                    {{-- <td>
                        @if (isset($item->logo))
                        <a data-toggle="modal" data-target="#imageModal-{{$item->id}}" style="cursor: pointer;">
                            <img src="{{asset('penyimpanan/rekenings/'.$item->logo)}}" alt="Responsive image" height="20px">
                        </a>
                        @endif    
                    </td> --}}
                    <td>
                        {{Carbon\Carbon::parse($item->created_at)->isoFormat('D MMMM Y')}}
                    </td>
                    <td>
                        <button type="button" class="btn btn-xs btn-success btn_update_modal" data-toggle="modal" data-target="#updateModal" data-remote="{{url('admin/rekening/edit/'.$item->id)}}"><i class="fas fa-pencil-alt"></i></button>
                        <button type="button" class="btn btn-xs btn-warning btn_delete_modal" data-toggle="modal" data-target="#deleteModal" data-remote="{{url('admin/rekening/destroy/'.$item->id)}}"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('modal')
<!-- Modal -->
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="createModalLabel">Buat Data Baru</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/rekening/store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type" type="text" value="create" hidden>
            <div class="form-group">
                <label>Nama Bank <span class='text-danger' title='This field is required'>*</span></label>
                <select id="nama_bank" class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="nama_rekening" style="width: 100%;" required>
                    <option value="">**Pilih Bank</option>
                </select>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Nomor Rekening <span class='text-danger' title='This field is required'>*</span></label>
                <input id="nomor_rekening" name="nomor_rekening" type="number" class="form-control form-control-sm" placeholder="Isi nomor rekening" value="{{old('nomor_rekening')}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Atas Nama <span class='text-danger' title='This field is required'>*</span></label>
                <input name="atas_nama" type="text" class="form-control form-control-sm" placeholder="Isi atas nama bank" value="{{old('atas_nama')}}" required>
                <div class="text-danger"></div>
            </div>
            {{-- <div class="form-group">
                <label for="exampleInputFile">Logo Rekening <span class='text-danger' title='This field is required'>*</span></label>
                <div class="custom-file">
                    <input name="image"  type="file" class="custom-file-input" id="exampleInputFile">
                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                </div>
                <div class="text-danger">(*format: jpeg,png,jpg. maksimal ukuran 512 KB)</div>
            </div> --}}
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

<script>
    $.get("https://nyala.sochainformatika.com/public/search/bank", function(data){
        $.each(data, function( key, value ){
            $('#nama_bank').append("<option value='"+value.name+"'>"+value.name+"</option>");
        });
    });
</script>

{{-- @foreach ($rekenings as $item)
<div class="modal fade" id="updateModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="updateModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="updateModal-{{$item->id}}Label">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/rekening/update/'.$item->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type-{{$item->id}}" type="text" value="update" hidden>
            <div class="form-group">
                <label>Nama Bank <span class='text-danger' title='This field is required'>*</span></label>
                <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="nama_rekening" style="width: 100%;" required>
                    <option value="">**Pilih Bank</option>
                    @foreach ($banks as $row)
                    <option value="{{$row->nama_bank}}" @if($row->nama_bank == $item->nama_rekening) selected @endif>
                        {{$row->nama_bank}}
                    </option>
                    @endforeach
                </select>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Nomor Rekening <span class='text-danger' title='This field is required'>*</span></label>
                <input id="nomor_rekening" name="nomor_rekening" type="number" class="form-control form-control-sm" placeholder="Isi nomor rekening" value="{{$item->nomor_rekening}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Atas Nama <span class='text-danger' title='This field is required'>*</span></label>
                <input name="atas_nama" type="text" class="form-control form-control-sm" placeholder="Isi atas nama bank" value="{{$item->atas_nama}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label for="exampleInputFile">Logo Rekening <span class='text-danger' title='This field is required'>*</span></label>
                <div class="custom-file">
                    <input name="image"  type="file" class="custom-file-input" id="exampleInputFile">
                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                </div>
                <div class="text-danger">(*format: jpeg,png,jpg. maksimal ukuran 512 KB)</div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="deleteModal-{{$item->id}}Label">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Apakah anda yakin untuk menghapus data ini? 
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <a href="{{url('admin/rekening/destroy/'.$item->id)}}" class="btn btn-danger btn-sm">Hapus</a>
        </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        if ('{{old("type-".$item->id)}}' == 'update') {
            $(document).ready(function () {
                $("#updateModal-{{$item->id}}").modal('show');
            });
        }

        $('.select2-update-{{$item->id}}').select2({
            placeholder: "**Silahkan Pilih",
            dropdownParent: $("#updateModal-{{$item->id}}")
        });
    });
</script>
@endforeach --}}
@endsection