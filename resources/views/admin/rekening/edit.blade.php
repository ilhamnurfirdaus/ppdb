<form action="{{url('admin/rekening/update/'.$rekening->id)}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal-body">
        <div class="form-group">
            <label>Nama Bank <span class='text-danger' title='This field is required'>*</span></label>
            <select id="nama_bank2" class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="nama_rekening" style="width: 100%;" required>
                <option value="">**Pilih Bank</option>
            </select>
            <div class="text-danger"></div>
        </div>
        <div class="form-group">
            <label>Nomor Rekening <span class='text-danger' title='This field is required'>*</span></label>
            <input id="nomor_rekening" name="nomor_rekening" type="number" class="form-control form-control-sm" placeholder="Isi nomor rekening" value="{{$rekening->nomor_rekening}}" required>
            <div class="text-danger"></div>
        </div>
        <div class="form-group">
            <label>Atas Nama <span class='text-danger' title='This field is required'>*</span></label>
            <input name="atas_nama" type="text" class="form-control form-control-sm" placeholder="Isi atas nama bank" value="{{$rekening->atas_nama}}" required>
            <div class="text-danger"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-success btn-sm">Simpan</button>
    </div>
</form>

<script>
    $(document).ready(function () {
        $.get("https://nyala.sochainformatika.com/public/search/bank", function(data){
            $.each(data, function( key, value ){
                $('#nama_bank2').append("<option value='"+value.name+"'>"+value.name+"</option>");
            });

            $('#nama_bank2').select2();
            $('#nama_bank2').val("{{ $rekening->nama_rekening }}").trigger('change');
        });
    });
</script>