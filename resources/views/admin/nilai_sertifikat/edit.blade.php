
<form action="{{url('admin/update_sertifikat', $sertifikat->nilai_sertifikats_id)}}" method="POST" enctype="multipart/form-data">
@csrf
<div class="modal-body">
    <div class="form-group">
        <label>Nama Tingkat</label>
        <input name="nama_tingkat" type="text" class="form-control form-control-sm" placeholder="Isi nama tingkat sertifikat" value="{{ $sertifikat->nama_tingkat }}" disabled>
        <div class="text-danger"></div>
    </div>
    <div class="form-group">
        <label>Peringkat</label>
        <input name="peringkat" type="number" class="form-control form-control-sm" placeholder="Isi peringkat" value="{{ $sertifikat->peringkat }}" disabled>
        <div class="text-danger"></div>
    </div>
    <div class="form-group">
        <label>Nilai Tingkat <span class='text-danger' title='This field is required'>*</span></label>
        <input name="nilai" type="number" class="nilai form-control form-control-sm" placeholder="Isi nilai tingkat sertifikat" min="1" max="10" value="{{ $sertifikat->nilai }}" required>
        <div class="text-danger"></div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
    <button type="submit" class="btn btn-success btn-sm">Simpan</button>
</div>
</form>
