@extends('layouts.admin')
@section('main')
@php
    function rupiah($num) {
        return 'Rp. '.number_format(intval($num), 0, ',', '.');
    }
@endphp
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>Seting Periode/Gelombang Pendaftaran (Biaya Pendaftaran dan Uang Gedung)
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
                <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#createModal">
                    Create
                </button>
            </div>
        </div>
        <div class="page-title-actions">
            
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>
<div class="main-card mb-3 card">
    <div class="card-body">
        <table style="width: 100%;" class="table table-hover table-striped table-bordered table-default">
            <thead>
            <tr>
                <th>Jenis</th>
                @if ($web_profil->jenjang_sekolah == "SLTA")
                <th>Jurusan</th>
                @endif
                <th>Biaya</th>
                <th>Gelombang</th>
                <th>Tgl Mulai</th>
                <th>Tgl Berakhir</th>
                {{-- <th>Tgl DIbuat</th> --}}
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($biayas as $item)
                <tr>
                    <td>{{$item->jenis_biaya}}</td>
                    @if ($web_profil->jenjang_sekolah == "SLTA")
                    <td>{{$item->jurusanTo->name}}</td>
                    @endif
                    <td>{{rupiah($item->biaya)}}</td>
                    <td>{{$item->nama_gelombang}}</td>
                    <td>
                        {{Carbon\Carbon::parse($item->mulai_tanggal)->isoFormat('D MMMM Y')}}
                    </td>
                    <td>
                        {{Carbon\Carbon::parse($item->sampai_tanggal)->isoFormat('D MMMM Y')}}
                    </td>
                    {{-- <td>
                        {{Carbon\Carbon::parse($item->created_at)->isoFormat('D MMMM Y')}}
                    </td> --}}
                    <td>
                        <button type="button" class="btn_update_modal btn btn-xs btn-success" data-toggle="modal" data-target="#updateModal" data-remote="{{url('admin/biaya/edit/'.$item->id)}}"><i class="fas fa-pencil-alt"></i></button>
                        <button type="button" class="btn_delete_modal btn btn-xs btn-warning" data-toggle="modal" data-target="#deleteModal" data-remote="{{url('admin/biaya/destroy/'.$item->id)}}"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('modal')
<!-- Modal -->
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="createModalLabel">Buat Data Baru</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/biaya/store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type" type="text" value="create" hidden>
            @if ($web_profil->jenjang_sekolah == "SLTA")
            <div class="form-group">
                <label>Jurusan <span class='text-danger' title='This field is required'>*</span></label>
                <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="program_studi" style="width: 100%;" required>
                    <option value="">**Pilih</option>
                    @foreach ($jurusans as $row)
                    <option value="{{ $row->id }}" @if($row->id == old("program_studi")) selected @endif>{{ $row->name }}</option>    
                    @endforeach
                </select>
                <div class="text-danger"></div>
            </div>
            @endif
            <div class="form-group">
                <label>Nama Gelombang  <span class='text-danger' title='This field is required'>*</span></label>
                <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="nama_gelombang" style="width: 100%;" required>
                    <option value="">**Pilih</option>
                    @foreach ($gelombangs as $row)
                    <option value="{{ $row->gelombang_id }}" @if($row->nama_gelombang == old("nama_gelombang")) selected @endif>{{ $row->nama_gelombang }} ({{ $row->tanggal_mulai }} s/d {{ $row->tanggal_selesai }})</option>    
                    @endforeach
                </select>
                <div class="text-danger"></div>
            </div>

            <div class="form-group">
                <label>Tagihan  <span class='text-danger' title='This field is required'>*</span></label>
                <div class="form-inline mb-1" id="inputFormRow">
                    <input name="nama_tagihan[]" style="width: 50%" type="text" class="form-control form-control-sm mr-2" placeholder="Nama Tagihan">
                    <input name="nominal[]" style="width: 45%" type="number" class="form-control form-control-sm mr-2" placeholder="Nominal">
                    {{-- <a class="btn btn-danger btn-xs" id="removeRow" style="color: white">X</i></a> --}}
                </div>
                <div id="newRow"></div>
                <a class="btn btn-success btn-xs mt-1" id="addRow" style="color: white"><i class="fa fa-plus"></i></a>
            </div>

            {{-- <div class="form-group">
                <label>Jenis Biaya <span class='text-danger' title='This field is required'>*</span></label>
                <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="jenis_biaya" style="width: 100%;" required>
                    <option value="">**Pilih</option>
                    <option value="Biaya Pendaftaran" @if("Biaya Pendaftaran" == old("jenis_biaya")) selected @endif>Biaya Pendaftaran</option>
                    <option value="Biaya Daftar Ulang" @if("Biaya Daftar Ulang" == old("jenis_biaya")) selected @endif>Biaya Daftar Ulang</option>
                </select>
                <div class="text-danger"></div>
            </div> --}}

            {{-- <div class="form-group">
                <label>Biaya Pendaftaran  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="biaya_pendaftaran" type="number" class="form-control form-control-sm" placeholder="Isi biaya pendaftaran" value="{{old('biaya_pendaftaran')}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Biaya Daftar Ulang  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="biaya_daftar_ulang" type="number" class="form-control form-control-sm" placeholder="Isi biaya daftar ulang" value="{{old('biaya_daftar_ulang')}}" required>
                <div class="text-danger"></div>
            </div> --}}
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

{{-- <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="updateModalLabel">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div id="model_content_update">

        </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="deleteModalLabel">Hapus Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Apakah anda yakin untuk menghapus data ini? 
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <a id="btn_hapus" href="#" class="btn btn-danger btn-sm">Hapus</a>
        </div>
        </div>
    </div>
</div> --}}

{{-- @foreach ($biayas as $item)
<div class="modal fade" id="updateModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="updateModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="updateModal-{{$item->id}}Label">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/biaya/update/'.$item->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type-{{$item->id}}" type="text" value="update" hidden>
            <div class="form-group">
                <label>Jenis Biaya <span class='text-danger' title='This field is required'>*</span></label>
                <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="jenis_biaya" style="width: 100%;" required>
                    <option value="">**Pilih</option>
                    <option value="Biaya Pendaftaran" @if("Biaya Pendaftaran" == $item->jenis_biaya) selected @endif>Biaya Pendaftaran</option>
                    <option value="Biaya Daftar Ulang" @if("Biaya Daftar Ulang" == $item->jenis_biaya) selected @endif>Biaya Daftar Ulang</option>
                </select>
                <div class="text-danger"></div>
            </div>
            @if ($web_profil->jenjang_sekolah == "SLTA")
            <div class="form-group">
                <label>Jurusan <span class='text-danger' title='This field is required'>*</span></label>
                <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="program_studi" style="width: 100%;" required>
                    <option value="">**Pilih</option>
                    @foreach ($jurusans as $row)
                    <option value="{{ $row->id }}" @if($row->id == $item->program_studi) selected @endif>{{ $row->name }}</option>    
                    @endforeach
                </select>
                <div class="text-danger"></div>
            </div>
            @endif
            <div class="form-group">
                <label>Biaya  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="biaya" type="number" class="form-control form-control-sm" placeholder="Isi biaya" value="{{$item->biaya}}" required>
                <div class="text-danger"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="deleteModal-{{$item->id}}Label">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Apakah anda yakin untuk menghapus data ini? 
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <a href="{{url('admin/biaya/destroy/'.$item->id)}}" class="btn btn-danger btn-sm">Hapus</a>
        </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        if ('{{old("type-".$item->id)}}' == 'update') {
            $(document).ready(function () {
                $("#updateModal-{{$item->id}}").modal('show');
            });
        }

        $('.select2-update-{{$item->id}}').select2({
            placeholder: "**Silahkan Pilih",
            dropdownParent: $("#updateModal-{{$item->id}}")
        });
    });
</script>
@endforeach --}}
<script>
    $(document).ready(function () {
        // $(".btn_tambah").click(function(){
        //     $('.hasil_clone').append(
        //         '<div class="form-inline" id="inputFormRow">'+
        //             '<input name="nama_tagihan[]" style="width: 50%" type="text" class="form-control form-control-sm mr-2" placeholder="Nama Tagihan">'+
        //             '<input name="nominal[]" type="number" class="form-control form-control-sm mr-2" placeholder="Nominal">'+
        //             '<a class="btn btn-danger btn-xs" id="btn_hapus" style="color: white">X</i></a>'+
        //         '</div>'
        //     );
        // })

        // $("#btn_hapus").click(function(){
        //     $(this).closest('#inputFormRow').remove();
        // })

        $("#addRow").click(function () {
            var html = '';
            html += '<div class="form-inline mb-1" id="inputFormRow">';
            html += '<input name="nama_tagihan[]" style="width: 50%" type="text" class="form-control form-control-sm mr-2" placeholder="Nama Tagihan">';
            html += '<input name="nominal[]" type="number" class="form-control form-control-sm mr-2" placeholder="Nominal">';
            html += '<a class="btn btn-danger btn-xs" id="removeRow" style="color: white">X</i></a>';
            html += '</div>';

            $('#newRow').append(html);
        });

        $(document).on('click', '#removeRow', function () {
            $(this).closest('#inputFormRow').remove();
        });

        $(".mulai_tanggal").change(function() {
            var tgl_mulai = $(this).val();
            var tgl_berakhir = $(this).parent().parent().find('.sampai_tanggal').val();
            if (tgl_berakhir == "") {
                $(this).parent().parent().find('.sampai_tanggal').val(tgl_mulai);
            } else if (tgl_mulai > tgl_berakhir){
                $(this).parent().parent().find('.sampai_tanggal').val(tgl_mulai);
            }
            console.log(tgl_mulai);
            console.log(tgl_berakhir);
        });

        $(".sampai_tanggal").change(function() {
            var tgl_mulai = $(this).parent().parent().find('.mulai_tanggal').val();
            var tgl_berakhir = $(this).val();
            if (tgl_berakhir == "") {
                $(this).parent().parent().find('.mulai_tanggal').val(tgl_berakhir);
            } else if (tgl_mulai > tgl_berakhir){
                $(this).parent().parent().find('.mulai_tanggal').val(tgl_berakhir);
            }
            console.log(tgl_mulai);
            console.log(tgl_berakhir);
        });
    });
</script>
@endsection