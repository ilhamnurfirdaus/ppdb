<form action="{{url('admin/biaya/update/'.$biaya->id)}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal-body">
        <input name="type-{{$biaya->id}}" type="text" value="update" hidden>
        <div class="form-group">
        @if ($web_profil->jenjang_sekolah == "SLTA")
        <div class="form-group">
            <label>Jurusan <span class='text-danger' title='This field is required'>*</span></label>
            <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="program_studi" style="width: 100%;" required>
                <option value="">**Pilih</option>
                @foreach ($jurusans as $row)
                <option value="{{ $row->id }}" @if($row->id == $biaya->program_studi) selected @endif>{{ $row->name }}</option>    
                @endforeach
            </select>
            <div class="text-danger"></div>
        </div>
        @endif
            <label>Jenis Biaya <span class='text-danger' title='This field is required'>*</span></label>
            <input type="text" name="jenis_biaya" class="form-control form-control-sm" value="{{$biaya->jenis_biaya}}">
            {{-- <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="jenis_biaya" style="width: 100%;" required>
                <option value="">**Pilih</option>
                <option value="Biaya Pendaftaran" @if("Biaya Pendaftaran" == $biaya->jenis_biaya) selected @endif>Biaya Pendaftaran</option>
                <option value="Biaya Daftar Ulang" @if("Biaya Daftar Ulang" == $biaya->jenis_biaya) selected @endif>Biaya Daftar Ulang</option>
            </select> --}}
            <div class="text-danger"></div>
            <label>Nominal <span class='text-danger' title='This field is required'>*</span></label>
            <input type="text" name="biaya" class="form-control form-control-sm" value="{{$biaya->biaya}}">
            {{-- <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="jenis_biaya" style="width: 100%;" required>
                <option value="">**Pilih</option>
                <option value="Biaya Pendaftaran" @if("Biaya Pendaftaran" == $biaya->jenis_biaya) selected @endif>Biaya Pendaftaran</option>
                <option value="Biaya Daftar Ulang" @if("Biaya Daftar Ulang" == $biaya->jenis_biaya) selected @endif>Biaya Daftar Ulang</option>
            </select> --}}
            <div class="text-danger"></div>
        </div>
        <div class="form-group">
            <label>Nama Gelombang  <span class='text-danger' title='This field is required'>*</span></label>
            <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="nama_gelombang" style="width: 100%;" required>
                <option value="">**Pilih</option>
                @foreach ($gelombangs as $row)
                <option value="{{ $row->gelombang_id }}" @if($row->nama_gelombang == $biaya->nama_gelombang) selected @endif>{{ $row->nama_gelombang }}</option>    
                @endforeach
            </select>
            <div class="text-danger"></div>
        </div>
        
        {{-- <div class="form-group">
            <label>Biaya Pendaftaran <span class='text-danger' title='This field is required'>*</span></label>
            <input name="biaya_pendaftaran" type="number" class="form-control form-control-sm" placeholder="Isi biaya_pendaftaran" value="{{$biaya->biaya}}" required>
            <div class="text-danger"></div>
        </div> --}}
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-success btn-sm">Simpan</button>
    </div>
</form>

<script>
    $(document).ready(function () {
        $('.select2-biasa').select2({
            placeholder: "**Silahkan Pilih"
        });
        
        $(".mulai_tanggal").change(function() {
            var tgl_mulai = $(this).val();
            var tgl_berakhir = $(this).parent().parent().find('.sampai_tanggal').val();
            if (tgl_berakhir == "") {
                $(this).parent().parent().find('.sampai_tanggal').val(tgl_mulai);
            } else if (tgl_mulai > tgl_berakhir){
                $(this).parent().parent().find('.sampai_tanggal').val(tgl_mulai);
            }
            console.log(tgl_mulai);
            console.log(tgl_berakhir);
        });

        $(".sampai_tanggal").change(function() {
            var tgl_mulai = $(this).parent().parent().find('.mulai_tanggal').val();
            var tgl_berakhir = $(this).val();
            if (tgl_berakhir == "") {
                $(this).parent().parent().find('.mulai_tanggal').val(tgl_berakhir);
            } else if (tgl_mulai > tgl_berakhir){
                $(this).parent().parent().find('.mulai_tanggal').val(tgl_berakhir);
            }
            console.log(tgl_mulai);
            console.log(tgl_berakhir);
        });
    });
</script>