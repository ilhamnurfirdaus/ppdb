<html>
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Data Seleksi Siswa</title>
        <body>
            <style type="text/css">
                .footer { position: fixed; left: 0px; bottom: -50px; right: 0px; height: 50px;text-align: center;font-size: 10px;}
                .footer .pagenum:before { content: counter(page); }

                .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
                .tg td{font-family:Arial;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
                .tg th{font-family:Arial;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
                .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
            </style>

            <div class="footer">
                Page <span class="pagenum"></span>
            </div>
  
            <div style="font-family:Arial; font-size:12px;">
                <h2 style="text-align: center">Data Seleksi Siswa</h2>
            </div>
            <br>
            <table class="tg">
                <tr>
                    <th class="tg-3wr7">Nama</th>
                    @if ($web_profil->jenjang_sekolah == "SLTA")
                    <th class="tg-3wr7">Jurusan</th>
                    @endif
                    <th class="tg-3wr7">Tahun Ajaran</th>
                    <th class="tg-3wr7">Total Nilai</th>
                    <th class="tg-3wr7">Keterangan</th>
                </tr>
                <?php $no=1 ?>
                @foreach($siswas as $item)
                <tr>
                    <td class="tg-rv4w">{{$item->user->name}}</td>
                    @if ($web_profil->jenjang_sekolah == "SLTA")
                    <td>{{$item->jurusanTo->name}}</td>
                    @endif
                    <td class="tg-rv4w">
                        {{ $item->tahun_ajaran }}
                    </td>
                    <td class="tg-rv4w">
                        {{ $item->total_score }}
                    </td>
                    <td class="tg-rv4w">
                        {{ $item->bayar->where('status', 'Lunas')->count() > 0 ? $item->seleksi : "Belum Lunas"}}
                    </td>
                </tr>
                @endforeach
            </table>
        </body>
    </head>
</html>