<div class="form-row">
    <label class="col-md-4">Tanggal</label>
    <label class="col-md-8">{{Carbon\Carbon::parse($sertifikat->tanggal)->isoFormat('D MMMM Y')}}</label>
</div>

<div class="form-row">
    <label class="col-md-4">Tingkat</label>
    <label class="col-md-8">{{$sertifikat->tingkat}}</label>
</div>

<div class="form-row">
    <label class="col-md-4">Peringkat</label>
    <label class="col-md-8">{{$sertifikat->peringkat}}</label>
</div>

@if ("File" == $sertifikat->jenis_file)
<div class="form-row">
    <label class="col-md-4 form-group">Dokumen</label>
    <div class="col-md-8">
        <img id="gambar_sertifikat" class="mx-auto d-block img-fluid" src="{{asset('penyimpanan/user/siswa/sertifikat-'.$sertifikat->siswa_id.'/'.$sertifikat->file)}}" alt="Responsive image">
    </div>
</div>
@else
<div class="form-row">
    <label class="col-md-4 form-group">Link</label>
    <a href="//{{ $sertifikat->keterangan }}"><label for="col-md-8">{{ $sertifikat->keterangan }}</label></a>
</div>
@endif