<div class="modal-body">
    <div class="form-row">
        <label class="col-md-3">Berkas</label>
        <div class="form-group col-md-9">
            <select id="siswa_berkas" class="form-control form-control-sm select2-biasa" style="width: 100%;" required>
                @foreach ($siswa->siswa_berkas as $item)
                <option value="{{ asset('penyimpanan/user/siswa/berkas/'.$item->siswa_id.'/'.$item->file) }}">{{ $item->name }}</option>
                @endforeach
            </select>
            <div class="text-danger"></div>
        </div>
    </div>
    <div class="form-row">
        <label class="col-md-3">Gambar</label>
        <div class="form-group col-md-9">
            <img id="gambar_raport" class="mx-auto d-block img-fluid" src="{{asset('penyimpanan/user/siswa/berkas/'.$siswa->siswa_berkas->first()->siswa_id.'/'.$siswa->siswa_berkas->first()->file)}}" alt="Responsive image">
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#siswa_berkas").on('change', function(){
            $("#gambar_raport").attr("src",$(this).val());
        });
    });
</script>