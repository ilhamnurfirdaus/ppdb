@extends('layouts.admin')
@section('main')
@php
    function rupiah($num) {
        return 'Rp. '.number_format(intval($num), 0, ',', '.');
    }
@endphp
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>Input Poin Tambahan (Kepala Sekolah)
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
                @if (Auth::user()->jenis == "Admin")
                {{-- <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#createModal">
                    Update Lolos
                </button> --}}
                {{-- <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#bukaSeleksiModal">
                    Buka Seleksi
                </button>

                <a href="{{url('admin/seleksi/export_pdf')}}" target="blank" id='import' class='btn-shadow ml-3 btn btn-sm btn-danger'><i class='fa fa-upload'></i> Cetak</a> --}}
                @endif
            </div>
        </div>
        <div class="page-title-actions">
            
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>
@if (Auth::user()->jenis == "NonAdmin" && $web_profil->tampil_seleksi == "Tidak")
<div class="main-card mb-3 card bg-warning text-white">
    <div class="card-body">
        <strong>Maaf, Anda belum bisa mengakses halaman ini - </strong> Tunggu hasil seleksi dibuka.
    </div>
</div>
@else
<div class="main-card mb-3 card">
    <div class="card-body table-responsive-sm">
        @if ($web_profil->jenjang_sekolah == "SLTA" && $jurusans->count() > 0)
        <form class="form form-inline" method="get">
            <select name="jurusan" class="form-control form-control-sm select2-biasa" style="width: 130px;">
                @foreach ($jurusans as $item)
                <option @if(Request::input('jurusan') == $item->kode_akun) selected @endif value="{{$item->id}}">{{$item->name}}</option>
                @endforeach
            </select>

            <button class="ml-2 mr-2 btn btn-sm btn-primary" type="submit">Filter</button>
            <a href="{{url()->current()}}" class="mr-2 btn btn-sm btn-primary">Reset</a>
        </form>
        @endif
        <br>
        <table style="width: 100%;" class="table table-hover table-striped table-bordered">
            <thead>
            <tr>
                <th rowspan="2" class="text-center" style="vertical-align: middle;">Nama</th>
                {{-- @if ($web_profil->jenjang_sekolah == "SLTA")
                <th rowspan="2" class="text-center" style="vertical-align: middle;">Jurusan</th>
                @endif --}}
                <th colspan="2" class="text-center">Raport</th>
                <th colspan="2" class="text-center">Sertifikat</th>
                <th rowspan="2" class="text-center" style="vertical-align: middle;">Soal</th>
                <th rowspan="2" class="text-center" style="vertical-align: middle;width:50px;">Kepsek</th>
                <th rowspan="2" class="text-center" style="vertical-align: middle;">Total</th>
                <th rowspan="2" class="text-center" style="vertical-align: middle;">Seleksi</th>
                @if (Auth::user()->jenis == "Admin")
                <th rowspan="2" class="text-center" style="vertical-align: middle;">Aksi</th>
                @endif
            </tr>
            <tr>
                <th class="text-center">Nilai</th>
                <th class="text-center">Lihat</th>
                <th class="text-center">Nilai</th>
                <th class="text-center">Lihat</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($siswas as $item)
                <tr>
                    <form action="{{url('admin/kepsek/update/'.$item->id)}}" method="POST" enctype="multipart/form-data">
                    @csrf
                        <td>{{$item->user->name}}</td>
                        {{-- @if ($web_profil->jenjang_sekolah == "SLTA")
                        <td>{{$item->jurusanTo->name}}</td>
                        @endif --}}

                        @php
                            $nilai_berkas = 0;
                        @endphp
                        @foreach ($item->siswa_raport as $key => $value)
                            @php
                                $nilai_berkas += $value->nilai;
                            @endphp
                        @endforeach
                        @php
                            if ($item->siswa_raport->count() > 0) {
                                $nilai_berkas = $nilai_berkas / $item->siswa_raport->count();
                            }
                        @endphp

                        <td>
                            {{ number_format($nilai_berkas,2,".",".") }}
                        </td>
                        <td>
                            @if ($item->siswa_raport->count() > 0)
                            <button type="button" class="btn_gambar_berkas_modal btn btn-xs btn-warning" title="Raport" data-toggle="modal" data-target="#gambarModal" data-remote="{{url('admin/kepsek/raports/'.$item->id)}}"><i class="fa fa-eye"></i></button>
                            @else
                            <button type="button" title="Raport" class="btn btn-xs btn-secondary"><i class="fa fa-eye"></i></button>
                            @endif
                        </td>

                        @php
                            $nilai_sertifikat = 0;
                        @endphp
                        @foreach ($item->sertifikat as $key => $value)
                            @php
                                $nilai_sertifikat += $value->score;
                            @endphp
                        @endforeach
                        <td>
                            {{ $nilai_sertifikat }}
                        </td>
                        <td>
                            @if ($item->sertifikat->count() > 0)
                            <button type="button" class="btn_gambar_berkas_modal btn btn-xs btn-success" title="Sertifikat" data-toggle="modal" data-target="#gambarModal" data-remote="{{url('admin/kepsek/sertifikats/'.$item->id)}}"><i class="fa fa-eye"></i></button>
                            @else
                            <button type="button" title="Sertifikat" class="btn btn-xs btn-secondary"><i class="fa fa-eye"></i></button>
                            @endif
                        </td>

                        <td>
                            {{ intval($item->soal_score) }}
                        </td>
                        <td>
                            {{-- {{ isset($item->poin_kepsek) ? $item->poin_kepsek : 0 }} --}}
                            <input name="poin_kepsek" type="number" class="nilai form-control form-control-sm" placeholder="Isi nilai tambahan" min="0" value="{{ isset($item->poin_kepsek) ? $item->poin_kepsek : 0 }}" required>
                        </td>
                        <td>
                            {{ number_format($item->total_score,2,".",".") }}
                        </td>
                        <td>
                            {{ $item->bayar->where('status', 'Lunas')->count() > 0 ? $item->seleksi : "Belum Lunas"}}
                        </td>
                        @if (Auth::user()->jenis == "Admin")
                        <td>
                            {{-- <button type="button" class="btn_update_modal btn btn-xs btn-success" data-toggle="modal" data-target="#updateModal" data-remote="{{url('admin/kepsek/edit/'.$item->id)}}"><i class="fas fa-pencil-alt"></i></button> --}}
                            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
                        </td>
                        @endif
                    </form>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endif
@endsection

@section('modal')
<!-- Modal -->
{{-- <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="createModalLabel">Edit Seleksi</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/seleksi/store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type" type="text" value="create" hidden>
            <div class="form-group">
                <label>Jumlah siswa yang lolos  <span class='text-danger' title='This field is required'>*</span></label>
                <input id="jumlah" name="jumlah" type="number" class="form-control form-control-sm" placeholder="Isi jumlah siswa yang lolos" value="{{$web_profil->jumlah_lolos}}" max="{{ $siswas->count() }}" required>
                <div class="text-danger"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div> --}}

<div class="modal fade" id="bukaSeleksiModal" tabindex="-1" role="dialog" aria-labelledby="bukaSeleksiModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="bukaSeleksiModalLabel">Buka Seleksi</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/seleksi/buka')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type" type="text" value="create" hidden>
            <div class="form-group">
                <label>Apakah anda ingin buka seleksi ke siswa ?</label>
                <select name="tampil_seleksi" id="tampil_seleksi" class="form-control form-control-sm" required>
                    <option value="Tidak" @if ($web_profil->tampil_seleksi == "Tidak") selected @endif>Tidak</option>
                    <option value="Ya" @if ($web_profil->tampil_seleksi == "Ya") selected @endif>Ya</option>
                </select>
                <div class="text-danger"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

@if (Auth::user()->jenis == "Admin")
{{-- @foreach ($siswas as $item)
<div class="modal fade" id="updateModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="updateModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="updateModal-{{$item->id}}Label">Nilai Tambahan</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/kepsek/update/'.$item->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type-{{$item->id}}" type="text" value="update" hidden>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="jenis_kepsek" value="Tidak" @if ($item->jenis_kepsek == "Tidak") checked @endif>
                <label class="form-check-label">
                  Tidak Pakai Nilai Tambahan
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="jenis_kepsek" value="Auto" @if ($item->jenis_kepsek == "Auto") checked @endif>
                <label class="form-check-label">
                  Ranking 1
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="jenis_kepsek" value="Manual" @if ($item->jenis_kepsek == "Manual") checked @endif>
                <label class="form-check-label">
                  Rangking Manual
                </label>
            </div>
            <div class="form-group form-nilai mt-3" @if ($item->jenis_kepsek == "Tidak") style="display: none" @endif>
                <label> Nilai  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="poin_kepsek" type="number" class="nilai form-control form-control-sm" placeholder="Isi nilai tambahan" value="{{ $item->poin_kepsek }}" @if ($item->jenis_kepsek != "Tidak") required @endif>
                <div class="text-danger"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>
@endforeach --}}
@endif
<script>
    
    $(document).ready(function() {
        $("#jumlah").on('change', function(){
            let att = parseInt($(this).attr("max"));
            let val = parseInt($(this).val());
            if (val > att) {
                $(this).val(att);
            }
        });

        if ('{{ $web_profil->jenjang_sekolah == "SLTA" }}') {
            $('.table').DataTable( {
                "lengthChange": false,
                "searching": false,
                language: {
                    searchPlaceholder: "Cari"
                },
                "order": [[ 8, "desc" ]]
            });
        } else {
            $('.table').DataTable( {
                "lengthChange": false,
                "searching": false,
                language: {
                    searchPlaceholder: "Cari"
                },
                "order": [[ 7, "desc" ]]
            });
        }
    });
</script>
@endsection