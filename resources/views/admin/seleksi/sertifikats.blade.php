<div class="modal-body">
    <div class="form-row">
        <label class="col-md-4">Nama Sertifikat</label>
        <div class="form-group col-md-8">
            <select id="sertifikat" class="form-control form-control-sm select2-biasa" style="width: 100%;" required>
                @foreach ($siswa->sertifikat as $item)
                <option value="{{ $item->id }}">{{ $item->name }}</option>
                @endforeach
            </select>
            <div class="text-danger"></div>
        </div>
    </div>
    
    <div id="load_sertifikat">
        <div class="form-row">
            <label class="col-md-4">Tanggal</label>
            <label class="col-md-8">{{Carbon\Carbon::parse($siswa->sertifikat->first()->tanggal)->isoFormat('D MMMM Y')}}</label>
        </div>

        <div class="form-row">
            <label class="col-md-4">Tingkat</label>
            <label class="col-md-8">{{$siswa->sertifikat->first()->tingkat}}</label>
        </div>

        <div class="form-row">
            <label class="col-md-4">Peringkat</label>
            <label class="col-md-8">{{$siswa->sertifikat->first()->peringkat}}</label>
        </div>

        @if ("File" == $siswa->sertifikat->first()->jenis_file)
        <div class="form-row">
            <label class="col-md-4 form-group">Dokumen</label>
            <div class="col-md-8">
                <img id="gambar_sertifikat" class="mx-auto d-block img-fluid" src="{{asset('penyimpanan/user/siswa/sertifikat-'.$siswa->sertifikat->first()->siswa_id.'/'.$siswa->sertifikat->first()->file)}}" alt="Responsive image">
            </div>
        </div>
        @else
        <div class="form-row">
            <label class="col-md-4 form-group">Link</label>
            <a href="//{{ $siswa->sertifikat->first()->keterangan }}"><label for="col-md-8">{{ $siswa->sertifikat->first()->keterangan }}</label></a>
        </div>
        @endif
        
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#sertifikat").on('change', function(){
            let url = "{{ url('admin/kepsek/sertifikat') }}/"+$(this).val();
            $('#load_sertifikat').load(url, function( data, textStatus, xhr ) {
                if ( textStatus == "error" ) {
                    alert( "Koneksi Internet anda terputus." );
                } else {
                    console.log("berhasil");
                }
            });
        });
    });
</script>