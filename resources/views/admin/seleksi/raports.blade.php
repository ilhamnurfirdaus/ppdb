<div class="modal-body">
    <div class="form-row">
        <label class="col-md-3">Raport</label>
        <div class="form-group col-md-9">
            <select id="siswa_raport" class="form-control form-control-sm select2-biasa" style="width: 100%;" required>
                @foreach ($siswa->siswa_raport as $item)
                <option value="{{ asset('penyimpanan/user/siswa/raport/'.$item->siswa_id.'/'.$item->file) }}">{{ $item->nama }} dengan nilai rata-rata {{ $item->nilai }}</option>
                @endforeach
            </select>
            <div class="text-danger"></div>
        </div>
    </div>
    <div class="form-row">
        <label class="col-md-3">Gambar</label>
        <div class="form-group col-md-9">
            <img id="gambar_raport" class="mx-auto d-block img-fluid" src="{{asset('penyimpanan/user/siswa/raport/'.$siswa->siswa_raport->first()->siswa_id.'/'.$siswa->siswa_raport->first()->file)}}" alt="Responsive image">
        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $("#siswa_raport").on('change', function(){
            $("#gambar_raport").attr("src",$(this).val());
        });
    });
</script>