<form action="{{url('admin/kepsek/update/'.$siswa->id)}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal-body">
        <input name="type-{{$siswa->id}}" type="text" value="update" hidden>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="jenis_kepsek" value="Tidak" @if ($siswa->jenis_kepsek == "Tidak") checked @endif>
            <label class="form-check-label">
              Tidak Pakai Nilai Tambahan
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="jenis_kepsek" value="Auto" @if ($siswa->jenis_kepsek == "Auto") checked @endif>
            <label class="form-check-label">
              Ranking 1
            </label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="jenis_kepsek" value="Manual" @if ($siswa->jenis_kepsek == "Manual") checked @endif>
            <label class="form-check-label">
              Rangking Manual
            </label>
        </div>
        {{-- <div id="form-nilai" class="form-row mt-3" @if ($siswa->jenis_kepsek != "Manual") style="display: none" @endif>
            <label class="col-sm-3"> Nilai <span class='text-danger' title='This field is required'>*</span></label>
            <div class="form-group col-sm-9">
                <input name="poin_kepsek" type="number" class="nilai form-control form-control-sm" placeholder="Isi nilai tambahan" value="{{ $siswa->poin_kepsek }}" @if ($siswa->jenis_kepsek == "Manual") required @endif>
                <div class="text-danger"></div>
            </div>
        </div> --}}
        <div id="form-nilai" class="form-group mt-3" @if ($siswa->jenis_kepsek != "Manual") style="display: none" @endif>
            <label> Nilai  <span class='text-danger' title='This field is required'>*</span></label>
            <input name="poin_kepsek" type="number" class="nilai form-control form-control-sm" placeholder="Isi nilai tambahan" value="{{ $siswa->poin_kepsek }}" @if ($siswa->jenis_kepsek == "Manual") required @endif>
            <div class="text-danger"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-success btn-sm">Simpan</button>
    </div>
</form>

<script>
    $(document).ready(function() {
        $('input[type=radio][name=jenis_kepsek]').change(function() {
            let ob = $(this);
            let val = ob.val();

            if (val == "Manual") {
                $("#form-nilai").css("display", "block");
                $("#form-nilai").find("input").attr('required',true);
            } else {
                $("#form-nilai").css("display", "none");
                $("#form-nilai").find("input").attr('required',false);
            }
        });
    });
</script>