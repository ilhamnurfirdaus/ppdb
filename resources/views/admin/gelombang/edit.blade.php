<form action="{{url('admin/gelombang/update/'.$gelombang->gelombang_id)}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal-body">
        <div class="form-group">
            <label>Nama Gelombang <span class='text-danger' title='This field is required'>*</span></label>
            <input name="nama_gelombang" type="text" class="form-control form-control-sm" placeholder="Isi nama gelombang" value="{{ $gelombang->nama_gelombang }}" required>
            <div class="text-danger"></div>
        </div>
        <div class="form-group">
            <label>Tanggal Mulai  <span class='text-danger' title='This field is required'>*</span></label>
            <input name="tanggal_mulai" type="date" class="form-control form-control-sm" placeholder="Isi tanggal mulai" value="{{ $gelombang->tanggal_mulai }}" required>
            <div class="text-danger"></div>
        </div>
        <div class="form-group">
            <label>Tanggal Selesai  <span class='text-danger' title='This field is required'>*</span></label>
            <input name="tanggal_selesai" type="date" class="form-control form-control-sm" placeholder="Isi tanggal selesai" value="{{ $gelombang->tanggal_selesai }}" required>
            <div class="text-danger"></div>
        </div>
        <div class="form-group">
            <label>Tahun Ajaran  <span class='text-danger' title='This field is required'>*</span></label>
            <input name="tahun_ajaran" type="text" class="form-control form-control-sm" placeholder="" value="{{ $gelombang->tahun_ajaran }}" required>
            <div class="text-danger"></div>
        </div>
        <div class="form-group">
            <label>keterangan</label>
            <textarea name="keterangan" id="keterangan" class='form-control form-control-sm' rows='5'>{{$gelombang->keterangan}}</textarea>
            <div class="text-danger"></div>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-success btn-sm">Simpan</button>
    </div>
</form>
