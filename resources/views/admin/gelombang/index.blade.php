@extends('layouts.admin')
@section('main')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div>Master Gelombang
                <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#createModal">
                    Create
                </button>
            </div>
        </div>
        <div class="page-title-actions">
            
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>
<div class="main-card mb-3 card">
    <div class="card-body">
        <table style="width: 100%;" class="table table-hover table-striped table-bordered table-default">
            <thead>
            <tr>
                <th>No</th>
                {{-- <th>Kode Gelombang</th> --}}
                <th>Nama</th>
                <th>Tanggal Mulai</th>
                <th>Tanggal Selesai</th>
                <th>Tahun Ajaran</th>
                <th>Keterangan</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($gelombang as $key=>$item)
                <tr>
                    <td>{{$key+1}}</td>
                    <td>{{$item->nama_gelombang}}</td>
                    <td>{{$item->tanggal_mulai}}</td>
                    <td>{{$item->tanggal_selesai}}</td>
                    <td>{{$item->tahun_ajaran}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>
                        <button type="button" class="btn btn-xs btn-success btn_update_modal" data-toggle="modal" data-target="#updateModal" data-remote="{{url('admin/gelombang/edit/'.$item->gelombang_id)}}"><i class="fas fa-pencil-alt"></i></button>
                        <button type="button" class="btn btn-xs btn-warning btn_delete_modal" data-toggle="modal" data-target="#deleteModal" data-remote="{{url('admin/gelombang/destroy/'.$item->gelombang_id)}}"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
                    
                @endforeach
            </tbody>
            
        </table>
    </div>
</div>
@endsection

@section('modal')
<!-- Modal -->
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="createModalLabel">Buat Data Baru</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/gelombang/store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <div class="form-group">
                <label>Nama Gelombang <span class='text-danger' title='This field is required'>*</span></label>
                <input name="nama_gelombang" type="text" class="form-control form-control-sm" placeholder="Isi nama gelombang" value="{{old('nama_gelombang')}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Tanggal Mulai <span class='text-danger' title='This field is required'>*</span></label>
                <input name="tanggal_mulai" type="date" class="form-control form-control-sm" placeholder="Isi tanggal mulai" value="{{old('tanggal_mulai')}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Tanggal Selesai <span class='text-danger' title='This field is required'>*</span></label>
                <input name="tanggal_selesai" type="date" class="form-control form-control-sm" placeholder="Isi tanggal selesai" value="{{old('tanggal_selesai')}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Tahun Ajaran <span class='text-danger' title='This field is required'>*</span></label>
                <input name="tahun_ajaran" type="text" class="form-control form-control-sm" placeholder="Contoh 2021-2022" value="{{old('tahun_ajaran')}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>keterangan</label>
                <textarea name="keterangan" id="keterangan" class='form-control form-control-sm' rows='5'>{{old('keterangan')}}</textarea>
                <div class="text-danger"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

<script>
    
</script>
@endsection