@extends('layouts.admin')
@section('main')
@php
    function rupiah($num) {
        return 'Rp. '.number_format(intval($num), 0, ',', '.');
    }
@endphp
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>Master Jabatan
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
                <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#createModal">
                    Create
                </button>
            </div>
        </div>
        <div class="page-title-actions">
            
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>
<div class="main-card mb-3 card">
    <div class="card-body">
        <table style="width: 100%;" class="table table-hover table-striped table-bordered table-default">
            <thead>
            <tr>
                <th>Nama</th>
                <th>Keterangan</th>
                <th>Tgl DIbuat</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($jabatans as $item)
                <tr>
                    <td>{{$item->name}}</td>
                    <td>{{$item->keterangan}}</td>
                    <td>
                        {{Carbon\Carbon::parse($item->created_at)->isoFormat('D MMMM Y')}}
                    </td>
                    <td>
                        <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#updateModal-{{$item->id}}"><i class="fas fa-pencil-alt"></i></button>
                        <button type="button" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#deleteModal-{{$item->id}}"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('modal')
<!-- Modal -->
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="createModalLabel">Buat Data Baru</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/jabatan/store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type" type="text" value="create" hidden>
            <div class="form-group">
                <label>Nama  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="name" type="text" class="form-control form-control-sm" placeholder="Isi nama jabatan" value="{{old('name')}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>keterangan</label>
                <textarea name="keterangan" id="keterangan" class='form-control form-control-sm' rows='5'>{{old('keterangan')}}</textarea>
                <div class="text-danger"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

@foreach ($jabatans as $item)
<div class="modal fade" id="updateModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="updateModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="updateModal-{{$item->id}}Label">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/jabatan/update/'.$item->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type-{{$item->id}}" type="text" value="update" hidden>
            <div class="form-group">
                <label>Nama  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="name" type="text" class="form-control form-control-sm" placeholder="Isi nama jabatan" value="{{ $item->name }}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>keterangan</label>
                <textarea name="keterangan" id="keterangan" class='form-control form-control-sm' rows='5'>{{$item->keterangan}}</textarea>
                <div class="text-danger"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="deleteModal-{{$item->id}}Label">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Apakah anda yakin untuk menghapus data ini? 
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <a href="{{url('admin/jabatan/destroy/'.$item->id)}}" class="btn btn-danger btn-sm">Hapus</a>
        </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        if ('{{old("type-".$item->id)}}' == 'update') {
            $(document).ready(function () {
                $("#updateModal-{{$item->id}}").modal('show');
            });
        }

        $('.select2-update-{{$item->id}}').select2({
            placeholder: "**Silahkan Pilih",
            dropdownParent: $("#updateModal-{{$item->id}}")
        });
    });
</script>
@endforeach
@endsection