@extends('layouts.admin')
@section('main')
@php
    function rupiah($num) {
        return 'Rp. '.number_format(intval($num), 0, ',', '.');
    }
@endphp
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>Seleksi
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
            </div>
        </div>
        <div class="page-title-actions">
            
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>
<div class="main-card mb-3 card">
    <div class="card-body">
        <table style="width: 100%;" class="table table-hover table-striped table-bordered">
            <thead>
            <tr>
                <th>Nama</th>
                <th>Jurusan</th>
                <th>Nilai Berkas</th>
                <th>Nilai Soal</th>
                <th>Total Nilai</th>
                <th>Seleksi</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($siswas as $item)
                <tr>
                    <td>{{$item->user->name}}</td>
                    <td>{{$item->jurusanTo->name}}</td>
                    @php
                        $nilai_sertifikat = 0;
                    @endphp
                    @foreach ($item->sertifikat as $row)
                    @php
                        $nilai_sertifikat += intval($row->score);
                    @endphp
                    @endforeach
                    <td>
                        {{intval($item->nilai_rapor_kelas_6) + intval($item->nilai_rapor_kelas_5) + intval($item->nilai_rapor_kelas_4) + $nilai_sertifikat}}
                    </td>
                    @php
                            $nilai_soal = 0;
                        @endphp
                        @foreach ($item->testAnswer as $row)
                        @php
                            $nilai_soal += intval($row->testChoice->score);
                        @endphp
                        @endforeach
                    <td>
                        {{ $nilai_soal }}
                    </td>
                    <td>
                        @if ($item->jenis_kepsek != "Tidak")
                            {{ intval($item->poin_kepsek) }}
                        @else
                        {{ intval($item->nilai_rapor_kelas_6) + intval($item->nilai_rapor_kelas_5) + intval($item->nilai_rapor_kelas_4) + $nilai_sertifikat + $nilai_soal }}
                        @endif
                    </td>
                    <td>
                        @if ($item->jenis_kepsek != "Tidak")
                            Lolos
                        @else
                        {{ isset($item->seleksi) ? $item->seleksi : 'Masih Proses'}}
                        @endif
                    </td>
                    <td>
                        <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#updateModal-{{$item->id}}"><i class="fas fa-pencil-alt"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('modal')
@foreach ($siswas as $item)
<div class="modal fade" id="updateModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="updateModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="updateModal-{{$item->id}}Label">Nilai Tambahan</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/kepsek/update/'.$item->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type-{{$item->id}}" type="text" value="update" hidden>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="jenis_kepsek" value="Tidak" @if ($item->jenis_kepsek == "Tidak") checked @endif>
                <label class="form-check-label">
                  Tidak Pakai Nilai Tambahan
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="jenis_kepsek" value="Auto" @if ($item->jenis_kepsek == "Auto") checked @endif>
                <label class="form-check-label">
                  Ranking 1
                </label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="radio" name="jenis_kepsek" value="Manual" @if ($item->jenis_kepsek == "Manual") checked @endif>
                <label class="form-check-label">
                  Rangking Manual
                </label>
            </div>
            <div class="form-group form-nilai mt-3" @if ($item->jenis_kepsek == "Tidak") style="display: none" @endif>
                <label> Nilai  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="poin_kepsek" type="number" class="nilai form-control form-control-sm" placeholder="Isi nilai tambahan" value="{{ $item->poin_kepsek }}" @if ($item->jenis_kepsek != "Tidak") required @endif>
                <div class="text-danger"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>
@endforeach

<script>
    
    $(document).ready(function() {
        $('input[type=radio][name=jenis_kepsek]').change(function() {
            if (this.value == 'Tidak') {
                $(this).parent().parent().find('.form-nilai').hide();
                $(this).parent().parent().find('.nilai').prop("required", false);
            }
            else if (this.value == 'Auto') {
                $(this).parent().parent().find('.form-nilai').hide();
                $(this).parent().parent().find('.nilai').prop("required", false);
            }
            else if (this.value == 'Manual') {
                $(this).parent().parent().find('.form-nilai').show();
                $(this).parent().parent().find('.nilai').prop("required", true);
            }
        });
        // $("#jumlah").on('change', function(){
        //     let att = parseInt($(this).attr("max"));
        //     let val = parseInt($(this).val());
        //     if (val > att) {
        //         $(this).val(att);
        //     }
        // });

        $('.table').DataTable( {
            language: {
                searchPlaceholder: "Cari"
            },
            "order": [[ 4, "desc" ]]
        });
    });
</script>
@endsection