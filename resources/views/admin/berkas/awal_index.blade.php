@extends('layouts.admin')
@section('main')
@php
    function rupiah($num) {
        return 'Rp. '.number_format(intval($num), 0, ',', '.');
    }
    $request_url = "";
    if (isset(explode("?",request()->fullUrl())[1])) {
        $request_url = "?".explode("?",request()->fullUrl())[1];
    }
@endphp
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>Rekap Pendaftaran 
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
                {{-- <button type="button" class="btn-shadow ml-3 btn btn-xs btn-success" data-toggle="modal" data-target="#createModal">
                    Create
                </button> --}}
                @if (Auth::user()->jenis == "Admin")
                {{-- <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#createModal">
                    Update Lolos
                </button> --}}
                {{-- <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#bukaSeleksiModal">
                    Buka Seleksi
                </button> --}}

                <a href="{!!url('admin/berkas/awal/pdf'.$request_url)!!}" target="blank" id='import' class='btn-shadow ml-3 btn btn-sm btn-danger'><i class='fa fa-upload'></i> Cetak</a>
                <a href="{!!url('admin/berkas/awal/export_excel'.$request_url)!!}" target="blank" class='btn-shadow ml-3 btn btn-sm btn-success'><i class='fa fa-upload'></i> Export Siswa</a>
                @endif
            </div>
        </div>
        <div class="page-title-actions">
            
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>

<div class="row">
    @for ($i = 1; $i < 3; $i++)
    <div class="col-md-6">
        <div class="mb-3 card">
            <div class="card-header-tab card-header bg-danger text-white">
                <div class="card-header-title font-size-md text-capitalize font-weight-normal">
                    <b>Grafik Tahun Ajaran</b>
                    &nbsp;&nbsp;&nbsp;&nbsp;

                    <select class="tahun_ajaran form-control form-control-sm select2-biasa" style="width: 130px;">
                        @foreach ($tahun_ajarans as $item)
                        <option @if(Request::input('tahun_ajaran') != null && Request::input('tahun_ajaran') == $item->tahun_ajaran) selected @elseif($tahun_ajaran->tahun_ajaran == $item->tahun_ajaran) selected @endif value="{{$item->tahun_ajaran}}">{{$item->tahun_ajaran}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="card-body">
                <div id="load_grafik-{{ $i }}" class="load_grafik" data-id="{{ $i }}"></div>
            </div>
        </div>            
    </div>
    @endfor
</div>

<div class="main-card mb-3 card">
    <div class="card-body table-responsive-sm">
        <form class="form form-inline" method="get">
            <select name="tahun_ajaran" class="form-control form-control-sm select2-biasa" style="width: 130px;">
                @foreach ($tahun_ajarans as $item)
                <option @if(Request::input('tahun_ajaran') != null && Request::input('tahun_ajaran') == $item->tahun_ajaran) selected @elseif($tahun_ajaran->tahun_ajaran == $item->tahun_ajaran) selected @endif value="{{$item->tahun_ajaran}}">{{$item->tahun_ajaran}}</option>
                @endforeach
            </select>
            
            &nbsp;
            {{-- <select name="status" class="form-control form-control-sm select2-biasa" style="width: 130px;">
                <option value="">Keterangan</option>
                <option value="Lolos" @if(Request::input('status') == "Lolos") selected @endif>Lolos</option>
                <option value="Tidak Lolos" @if(Request::input('status') == "Tidak Lolos") selected @endif>Tidak Lolos</option>
            </select> --}}
            @php
                $status_pembayarans = ["Belum Lunas", "Masih Proses", "Lunas"];
            @endphp
            <select name="status" class="form-control form-control-sm select2-biasa" style="width: 230px;">
                <option value="">Pilih Status Pembayaran</option>
                @foreach ($status_pembayarans as $status_pembayaran)
                    <option value="{{$status_pembayaran}}" @if(Request::input('status') == $status_pembayaran) selected @endif>{{$status_pembayaran}}</option>
                @endforeach
            </select>

            <button class="ml-2 mr-2 btn btn-sm btn-primary" type="submit">Filter</button>
            <a href="{{url()->current()}}" class="mr-2 btn btn-sm btn-primary">Reset</a>
        </form>
        <br>
        @include('admin.berkas.export_excel')

        <div class="row" style="margin-top: 10px;">
            <div class="col-sm-6">
                {!! $siswas->appends(request()->all())->links('pagination::bootstrap-4') !!}
            </div>
            <div class="col-sm-6">
                
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
{{-- <div class="modal fade" id="bukaSeleksiModal" tabindex="-1" role="dialog" aria-labelledby="bukaSeleksiModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="bukaSeleksiModalLabel">Buka Seleksi</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/seleksi/buka')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type" type="text" value="create" hidden>
            <div class="form-group">
                <label>Apakah anda ingin buka seleksi ke siswa ?</label>
                <select name="tampil_seleksi" id="tampil_seleksi" class="form-control form-control-sm" required>
                    <option value="Tidak" @if ($web_profil->tampil_seleksi == "Tidak") selected @endif>Tidak</option>
                    <option value="Ya" @if ($web_profil->tampil_seleksi == "Ya") selected @endif>Ya</option>
                </select>
                <div class="text-danger"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div> --}}
<script>
    $('.load_grafik').each(function(){
        let ob = $(this);
        let tahun_ajaran = ob.parent().parent().find(".tahun_ajaran").val();
        let id = ob.data("id");

        $('#load_grafik-'+id).load("{{url('admin/berkas')}}/"+id+"/tahun_ajaran/"+tahun_ajaran, function( data, textStatus, xhr ) {            
            if ( textStatus == "error" ) {
                alert( "Koneksi Internet anda terputus." );
            }
        });
    });

    $('.tahun_ajaran').change(function(){
        let ob = $(this);
        
        let tahun_ajaran = ob.val();        
        
        let load_grafik = ob.parent().parent().parent().parent().find('.load_grafik');
        let id = load_grafik.data("id");
        
        load_grafik.load("{{url('admin/berkas')}}/"+id+"/tahun_ajaran/"+tahun_ajaran, function( data, textStatus, xhr ) {
            if ( textStatus == "error" ) {
                alert( "Koneksi Internet anda terputus." );
            }
            console.log("test");
        });
    });
</script>
@endsection