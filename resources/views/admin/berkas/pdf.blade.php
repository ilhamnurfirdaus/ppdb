<html>
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Laporan Pendaftaran</title>
        <body>
            <style type="text/css">
                .footer { position: fixed; left: 0px; bottom: -50px; right: 0px; height: 50px;text-align: center;font-size: 10px;}
                .footer .pagenum:before { content: counter(page); }

                .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
                .tg td{font-family:Arial;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
                .tg th{font-family:Arial;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
                .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
            </style>

            <div class="footer">
                Page <span class="pagenum"></span>
            </div>
  
            <div style="font-family:Arial; font-size:12px;">
                <h2 style="text-align: center">Laporan Pendaftaran</h2>
            </div>
            <br>
            @include('admin.berkas.export_excel')
        </body>
    </head>
</html>