@extends('layouts.admin')
@section('main')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>
                Seleksi Berkas
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
            </div>
        </div>
        <div class="page-title-actions">
            <a href="{{ url('admin/akhir/berkas') }}" class="btn-shadow ml-3 btn btn-xs btn-info">
                Kembali
            </a>
        </div>    
    </div>
</div>

<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="mb-3 card">
            <div class="card-body">
                <table style="width: 100%;" class="table table-hover table-striped table-bordered table-default">
                    <thead>
                    <tr>
                        <th>Nama Berkas</th>
                        <th>File</th>
                        <th>Status</th>
                        <th>Tgl Diperbarui</th>
                        <th>Aksi</th>
                    </tr>
                    </thead>
                    <tbody>
                        @foreach ($setting_berkass as $item)
                            @foreach ($siswa->siswa_berkas->where('jenis_berkas', 'Berkas Daftar Ulang') as $col)
                                @if ($item->nama_berkas == $col->name)
                                <tr>
                                    <td>
                                        {{ $col->name }}
                                    </td>
                                    <td>
                                        <a data-toggle="modal" data-target="#berkasModal-{{$item->id}}" style="cursor: pointer;">
                                            <img src="{{asset('/penyimpanan/user/siswa/berkas/'.$col->file)}}" alt="Responsive image" height="40px">
                                        </a>
                                    </td>
                                    <td>
                                        {{ empty($col->status) ? "Belum Valid" : $col->status }}
                                    </td>
                                    <td>
                                        {{Carbon\Carbon::parse($item->updated_at)->isoFormat('D MMMM Y')}}
                                    </td>
                                    <td>
                                        <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#updateModal-{{$item->id}}"><i class="fas fa-pencil-alt"></i></button>
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
@foreach ($siswa->siswa_berkas->where('jenis_berkas', 'Berkas Daftar Ulang') as $item)
<!-- Image Modal -->
<div class="modal fade" id="berkasModal-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="berkasModal-{{ $item->id }}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h6 class="modal-title" id="berkasModal-{{ $item->id }}Label">Scan File</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body" style="font-size: 12px;">
            <img class="mx-auto d-block img-fluid" src="{{asset('/penyimpanan/user/siswa/berkas/'.$col->file)}}" alt="Responsive image">
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="updateModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="updateModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="updateModal-{{$item->id}}Label">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/berkas/akhir/seleksi-post/'.$item->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type-{{$item->id}}" type="text" value="update" hidden>
            <div class="form-group">
                <label>Status Validasi Berkas</label>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="status" value="Belum Valid" data-id="{{ $item->id }}" @if ($item->status == "Lunas") checked @endif>
                    <label class="form-check-label">Belum Valid</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="status" value="Valid" data-id="{{ $item->id }}" @if ($item->status == "Belum Lunas") checked @endif required>
                    <label class="form-check-label">Valid</label>
                </div>
            </div>

            <div id="form_bayar_notifikasi-{{ $item->id }}" class="form_bayar_notifikasi form-group" @if ($item->status != "Belum Lunas") style="display: none;" @endif>
                <label>Pesan Notifikasi</label>
                <textarea id="bayar_notifikasi-{{ $item->id }}" name="catatan_admin" class="bayar_notifikasi form-control" rows="3" @if ($item->status == "Belum Lunas") required @endif>{{ $item->keterangan }}</textarea>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {        
        $('input[type=radio][name=status]').change(function() {
            if (val == "Belum Valid") {
                $(this).parent().parent().parent().parent().find(".form_bayar_notifikasi").css("display", "block");
                $(this).parent().parent().parent().parent().find(".bayar_notifikasi").attr('required', true);
            } else {
                $(this).parent().parent().parent().parent().find(".form_bayar_notifikasi").css("display", "none");
                $(this).parent().parent().parent().parent().find(".bayar_notifikasi").attr('required', false);
                $(this).parent().parent().parent().parent().find(".bayar_notifikasi").val(null);
            }
        });
    });
</script>
@endforeach
@endsection