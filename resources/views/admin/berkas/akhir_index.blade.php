@extends('layouts.admin')
@section('main')
@php
    function rupiah($num) {
        return 'Rp. '.number_format(intval($num), 0, ',', '.');
    }
@endphp
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>Form Registrasi
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
                {{-- <button type="button" class="btn-shadow ml-3 btn btn-xs btn-success" data-toggle="modal" data-target="#createModal">
                    Create
                </button> --}}
            </div>
        </div>
        <div class="page-title-actions">
            
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>
<div class="main-card mb-3 card">
    <div class="card-body">
        <table style="width: 100%;" class="table table-hover table-striped table-bordered table-default">
            <thead>
            <tr>
                <th>Nama</th>
                @if ($web_profil->jenjang_sekolah == "SLTA")
                <th>Jurusan</th>
                @endif
                <th>Status Valid</th>
                <th>Tgl DIbuat</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($siswas as $item)
                @if ($item->siswa_berkas->count())
                <tr>
                    <td>{{$item->user->name}}</td>
                    @if ($web_profil->jenjang_sekolah == "SLTA")
                    <th>{{$item->jurusanTo->name}}</th>
                    @endif
                    <td>
                        @if ($item->siswa_berkas->where('jenis_berkas', 'Berkas Daftar Ulang')->where('status', 'Valid')->count() == $item->siswa_berkas->where('jenis_berkas', 'Berkas Daftar Ulang')->count())
                        Sudah Valid Semua
                        @else    
                        Belum Valid Semua
                        @endif
                    </td>
                    <td>
                        {{Carbon\Carbon::parse($item->created_at)->isoFormat('D MMMM Y')}}
                    </td>
                    <td>
                        <a href="{{ url('admin/berkas/akhir/seleksi-index/'.$item->id) }}" class="btn btn-xs btn-success"><i class="fas fa-pencil-alt"></i></a>
                    </td>
                </tr>
                @endif
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('modal')

@endsection