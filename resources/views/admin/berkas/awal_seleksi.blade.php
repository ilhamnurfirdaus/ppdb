@extends('layouts.admin')
@section('main')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>
                Seleksi Berkas
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
            </div>
        </div>
        <div class="page-title-actions">
            <a href="{{ url('admin/berkas/awal') }}" class="btn-shadow ml-3 btn btn-xs btn-info">
                Kembali
            </a>
        </div>    
    </div>
</div>

<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="mb-3 card">
            {{-- <div class="card-header-tab card-header bg-danger text-white">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    
                </div>
            </div> --}}
            <div class="card-body">
                <form method="POST" action="{{ url('admin/berkas/awal/seleksi-post/'.$siswa->id) }}" enctype="multipart/form-data">
                    @csrf 
                    
                    @foreach ($setting_berkass as $item)
                        @forelse ($siswa->siswa_berkas->where('jenis_berkas', 'Berkas Pendaftaran') as $col)
                            @if ($item->nama_berkas == $col->name)
                                @if ($item->jurusan_id == $col->siswa->jurusan && $col->jurusan_id == "Sesuai Jurusan Siswa")
                                    
                                @elseif ($item->jurusan_id == "Default" && $col->jurusan_id == "Default")
                                    
                                @endif
                                <div class="form-group">
                                    <label>{{ $col->name }} <a class="btn btn-sm btn-primary text-white" data-toggle="modal" data-target="#berkasModal-{{$item->id}}" style="cursor: pointer;">lihat file</a></label>
                                    <div class="row">
                                        <div class="col-sm-12">
                                            <div class="input-group input-group-sm mb-3">
                                                <div class="input-group-prepend">
                                                  <span class="input-group-text" id="basic-addon1">Nilai</span>
                                                </div>
                                                <input name="berkas_id[]" type="hidden" value="{{ $col->siswa_berkas_id }}">
                                                <input name="berkas_score[]" type="number" class="form-control form-control-sm" placeholder="Isi nilai" value="{{ $col->score }}" required>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @else
                                
                            @endif
                        @empty
                            @if ($item->jurusan_id == $siswa->jurusan)
                            
                            @elseif ($item->jurusan_id == "Default")
                            
                            @endif
                        @endforelse
                    @endforeach
                    {{-- <div class="form-group">
                        <label>Scan KK @if(isset($siswa->file_kk))<a class="btn btn-sm btn-primary text-white" data-toggle="modal" data-target="#file_kkModal" style="cursor: pointer;">lihat file</a>@endif</label>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="input-group input-group-sm mb-3">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="basic-addon1">Nilai</span>
                                    </div>
                                    <input name="nilai_kk" type="number" class="form-control" placeholder="Isi nilai" value="{{ $siswa->nilai_kk }}" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Scan Rapor Kelas 4 Semester 1 @if(isset($siswa->file_kk))<a class="btn btn-sm btn-primary text-white" data-toggle="modal" data-target="#rapor_kelas_4Modal" style="cursor: pointer;">lihat file</a>@endif</label>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="input-group input-group-sm mb-3">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="basic-addon1">Nilai</span>
                                    </div>
                                    <input name="nilai_rapor_kelas_4" type="number" class="form-control form-control-sm" placeholder="Isi nilai" value="{{ $siswa->nilai_rapor_kelas_4 }}" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Scan Rapor Kelas 5 Semester 1 @if(isset($siswa->file_kk))<a class="btn btn-sm btn-primary text-white" data-toggle="modal" data-target="#rapor_kelas_5Modal" style="cursor: pointer;">lihat file</a>@endif</label>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="input-group input-group-sm mb-3">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="basic-addon1">Nilai</span>
                                    </div>
                                    <input name="nilai_rapor_kelas_5" type="number" class="form-control form-control-sm" placeholder="Isi nilai" value="{{ $siswa->nilai_rapor_kelas_5 }}" required>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Scan Rapor Kelas 6 Semester 1 @if(isset($siswa->file_kk))<a class="btn btn-sm btn-primary text-white" data-toggle="modal" data-target="#rapor_kelas_6Modal" style="cursor: pointer;">lihat file</a>@endif</label>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="input-group input-group-sm mb-3">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="basic-addon1">Nilai</span>
                                    </div>
                                    <input name="nilai_rapor_kelas_6" type="number" class="form-control form-control-sm" placeholder="Isi nilai" value="{{ $siswa->nilai_rapor_kelas_6 }}" required>
                                </div>
                            </div>
                        </div>
                    </div> --}}

                    @foreach ($siswa->sertifikat as $item)
                    <div class="form-group">
                        <label>Scan Sertifikat {{ $item->name }}, Tingkat {{ $item->tingkat }} <a class="btn btn-sm btn-primary text-white" data-toggle="modal" data-target="#sertifikatModal-{{$item->id}}" style="cursor: pointer;">lihat file</a></label>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="input-group input-group-sm mb-3">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="basic-addon1">Nilai</span>
                                    </div>
                                    <input name="sertifikat_id[]" type="hidden" value="{{ $item->id }}">
                                    <input name="score[]" type="number" class="form-control form-control-sm" placeholder="Isi nilai" value="{{ $item->score }}" required>
                                </div>
                            </div>
                        </div>
                        {{-- <div class="row">
                            <div class="col-sm-4">
                                <label>Nama Sertifikat</label>
                            </div>
                            <div class="col-sm-8">
                                <label>: {{ $item->name }}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Lihat File</label>
                            </div>
                            <div class="col-sm-8">
                                <label>: 
                                    <a data-toggle="modal" data-target="#sertifikatModal-{{$item->id}}" style="cursor: pointer;">
                                        <img src="{{asset('/penyimpanan/user/siswa/sertifikat-'.$siswa->id.'/'.$item->file)}}" alt="Responsive image" height="40px">
                                    </a>
                                </label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-4">
                                <label>Tingkat</label>
                            </div>
                            <div class="col-sm-8">
                                <label>: {{ $item->tingkat }}</label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="input-group input-group-sm mb-3">
                                    <div class="input-group-prepend">
                                      <span class="input-group-text" id="basic-addon1">Nilai</span>
                                    </div>
                                    <input name="sertifikat_id[]" type="hidden" value="{{ $item->id }}">
                                    <input name="score[]" type="number" class="form-control form-control-sm" placeholder="Isi nilai" value="{{ $item->score }}" required>
                                </div>
                            </div>
                        </div> --}}
                    </div>
                    @endforeach

                    <div class="form-group my-3">
                    <button type="submit" class="btn btn-success">
                        {{ __('Simpan') }}
                    </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<!-- Image Modal -->
{{-- <div class="modal fade" id="file_kkModal" tabindex="-1" role="dialog" aria-labelledby="file_kkModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h6 class="modal-title" id="file_kkModalLabel">Scan File</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body" style="font-size: 12px;">
            <img class="mx-auto d-block img-fluid" src="{{asset('/penyimpanan/user/siswa/file_kk/'.$siswa->file_kk)}}" alt="Responsive image">
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="rapor_kelas_4Modal" tabindex="-1" role="dialog" aria-labelledby="rapor_kelas_4ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h6 class="modal-title" id="rapor_kelas_4ModalLabel">Scan File</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body" style="font-size: 12px;">
            <img class="mx-auto d-block img-fluid" src="{{asset('/penyimpanan/user/siswa/rapor_kelas_4/'.$siswa->rapor_kelas_4)}}" alt="Responsive image">
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="rapor_kelas_5Modal" tabindex="-1" role="dialog" aria-labelledby="rapor_kelas_5ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h6 class="modal-title" id="rapor_kelas_5ModalLabel">Scan File</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body" style="font-size: 12px;">
            <img class="mx-auto d-block img-fluid" src="{{asset('/penyimpanan/user/siswa/rapor_kelas_5/'.$siswa->rapor_kelas_5)}}" alt="Responsive image">
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="rapor_kelas_6Modal" tabindex="-1" role="dialog" aria-labelledby="rapor_kelas_6ModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h6 class="modal-title" id="rapor_kelas_6ModalLabel">Scan File</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body" style="font-size: 12px;">
            <img class="mx-auto d-block img-fluid" src="{{asset('/penyimpanan/user/siswa/rapor_kelas_6/'.$siswa->rapor_kelas_6)}}" alt="Responsive image">
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        </div>
    </div>
    </div>
</div> --}}
@foreach ($siswa->siswa_berkas->where('jenis_berkas', 'Berkas Pendaftaran') as $item)
<!-- Image Modal -->
<div class="modal fade" id="berkasModal-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="berkasModal-{{ $item->id }}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h6 class="modal-title" id="berkasModal-{{ $item->id }}Label">Scan File</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body" style="font-size: 12px;">
            <img class="mx-auto d-block img-fluid" src="{{asset('/penyimpanan/user/siswa/berkas/'.$col->file)}}" alt="Responsive image">
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        </div>
    </div>
    </div>
</div>    
@endforeach
@foreach ($siswa->sertifikat as $item)
<!-- Image Modal -->
<div class="modal fade" id="sertifikatModal-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="sertifikatModal-{{ $item->id }}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h6 class="modal-title" id="sertifikatModal-{{ $item->id }}Label">Scan File</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body" style="font-size: 12px;">
            <img class="mx-auto d-block img-fluid" src="{{asset('/penyimpanan/user/siswa/sertifikat-'.$siswa->id.'/'.$item->file)}}" alt="Responsive image">
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        </div>
    </div>
    </div>
</div>
@endforeach
@endsection