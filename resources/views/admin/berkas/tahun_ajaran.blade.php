<div class="position-relative mb-4">
    <canvas class="bulan-chart" height="200"></canvas>
</div>

<div class="d-flex flex-row justify-content-end">
    <span class="mr-2">
        <i class="fa fa-square text-success"></i> Daftar
    </span>

    <span class="mr-2">
        <i class="fa fa-square text-warning"></i> Lunas
    </span>

    <span>
        <i class="fa fa-square text-danger"></i> Belum Lunas
    </span>
</div>

<script>
    $(function (){
        console.log($(this).data("id"));
        'use strict'

        var ticksStyle = {
            fontColor: '#495057',
            fontStyle: 'bold'
        }

        var mode = 'index'
        var intersect = true

        let ob = $("#load_grafik-{{ $id }}");

        $.get("{{url('admin/berkas/grafik')}}?tahun_ajaran={{ $tahun_ajaran }}", function(data){
            console.log(data);
            let jurusan = [];
            let jumlah_daftar = [];
            let jumlah_gagal = [];
            let jumlah_lolos = [];
            // console.log(data.siswas[1]);
            $.each( data.siswas, function( key, value ) {
                // console.log();
                jurusan.push(data.siswas[data.siswas.length - key - 1].jurusan);
                jumlah_daftar.push(data.siswas[data.siswas.length - key - 1].daftar);
                jumlah_gagal.push(data.siswas[data.siswas.length - key - 1].gagal);
                jumlah_lolos.push(data.siswas[data.siswas.length - key - 1].lolos);
            });

            var $hariChart = ob.find('.bulan-chart');
            // eslint-disable-next-line no-unused-vars
            
            $hariChart.parent().removeAttr('class');
            if (data.siswas.length > 4) {
                $hariChart.parent().attr('class', 'col-md-12 mb-4');
            } else if (data.siswas.length == 1) {
                $hariChart.parent().attr('class', 'col-md-6 mb-4');
            } else {
                $hariChart.parent().attr('class', 'col-md-'+6*parseInt(data.siswas.length)/2+' mb-4');
            }
            
            var hariChart = new Chart($hariChart, {
                type: 'bar',
                data: {
                labels: jurusan,
                datasets: [
                    {
                    backgroundColor: '#3ac47d',
                    borderColor: '#3ac47d',
                    data: jumlah_daftar
                    },
                    {
                    backgroundColor: '#f7b924',
                    borderColor: '#f7b924',
                    data: jumlah_lolos
                    },
                    {
                    backgroundColor: '#d92550',
                    borderColor: '#d92550',
                    data: jumlah_gagal
                    }
                ]
                },
                options: {
                maintainAspectRatio: false,
                tooltips: {
                    mode: mode,
                    intersect: intersect
                },
                hover: {
                    mode: mode,
                    intersect: intersect
                },
                legend: {
                    display: false
                },
                scales: {
                    yAxes: [{
                    // display: false,
                    gridLines: {
                        display: true,
                        lineWidth: '4px',
                        color: 'rgba(0, 0, 0, .2)',
                        zeroLineColor: 'transparent'
                    },
                    ticks: $.extend({
                        beginAtZero: true,
                        suggestedMax: data.maksimal_jumlah
                    }, ticksStyle)
                    }],
                    xAxes: [{
                    display: true,
                    gridLines: {
                        display: false
                    },
                    ticks: ticksStyle
                    }]
                }
                }
            })
        });
    });
</script>