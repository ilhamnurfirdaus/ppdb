<table style="width: 100%;" class="table table-hover table-striped table-bordered tg">
    <thead>
        <tr>
            <th style="{{request()->segment(4) == "export_excel" ? 'text-align:center;font-weight:bold;width:'.(3+3+0.83*(intval(strlen('No')))).'px;' : ''}}">No</th>
            <th style="{{request()->segment(4) == "export_excel" ? 'text-align:center;font-weight:bold;width:'.(3+3+0.83*(intval(strlen('Nama')))).'px;' : ''}}">Nama</th>
            @if ($web_profil->jenjang_sekolah == "SLTA")
            <th style="{{request()->segment(4) == "export_excel" ? 'text-align:center;font-weight:bold;width:'.(3+3+0.83*(intval(strlen('Jurusan')))).'px;' : ''}}">Jurusan</th>
            @endif
            <th style="{{request()->segment(4) == "export_excel" ? 'text-align:center;font-weight:bold;width:'.(3+3+0.83*(intval(strlen('Tahun Ajaran')))).'px;' : ''}}">Tahun Ajaran</th>
            {{-- <th>Total Nilai</th> --}}
            <th style="{{request()->segment(4) == "export_excel" ? 'text-align:center;font-weight:bold;width:'.(3+3+0.83*(intval(strlen('Keterangan')))).'px;' : ''}}">Keterangan</th>
            @if (request()->segment(4) == null)
                <th style="{{request()->segment(4) == "export_excel" ? 'text-align:center;font-weight:bold;width:'.(3+3+0.83*(intval(strlen('Aksi')))).'px;' : ''}}">Aksi</th>
            @endif
        </tr>
    </thead>
    <tbody>
        @php
            $no=1 + (request()->limit ? intval(request()->limit) : 10) * ((request()->page ? intval(request()->page) : 1) - 1);
        @endphp
        @foreach ($siswas as $item)
        <tr>
            <td style="text-align:center;">{{$no++}}</td>
            <td>{{$item->user->name}}</td>
            @if ($web_profil->jenjang_sekolah == "SLTA")
            <td>{{$item->jurusanTo->name}}</td>
            @endif
            <td>
                {{ $item->tahun_ajaran }}
            </td>
            {{-- <td>
                {{ $item->total_score }}
            </td> --}}
            <td>
                {{-- {{ $item->bayar->where('status', 'Lunas')->count() > 0 ? $item->seleksi : "Belum Lunas"}} --}}
                {{ $item->bayar->where('status', 'Lunas')->count() > 0 ? "Lunas" : "Belum Lunas"}}
            </td>
            @if (request()->segment(4) == null)
                <td>
                    <div class="btn-group">
                        @if ($item->siswa_berkas->count() > 0)
                        <button type="button" class="btn_gambar_berkas_modal btn btn-xs btn-primary" title="Berkas" data-toggle="modal" data-target="#gambarModal" data-remote="{{url('admin/kepsek/berkass/'.$item->id)}}"><i class="fa fa-eye"></i></button>
                        @else
                        <button type="button" title="Berkas" class="btn btn-xs btn-secondary"><i class="fa fa-eye"></i></button>
                        @endif
                        @if ($item->siswa_raport->count() > 0)
                        <button type="button" class="btn_gambar_berkas_modal btn btn-xs btn-warning" title="Raport" data-toggle="modal" data-target="#gambarModal" data-remote="{{url('admin/kepsek/raports/'.$item->id)}}"><i class="fa fa-eye"></i></button>
                        @else
                        <button type="button" title="Raport" class="btn btn-xs btn-secondary"><i class="fa fa-eye"></i></button>
                        @endif
                        @if ($item->sertifikat->count() > 0)
                        <button type="button" class="btn_gambar_berkas_modal btn btn-xs btn-success" title="Sertifikat" data-toggle="modal" data-target="#gambarModal" data-remote="{{url('admin/kepsek/sertifikats/'.$item->id)}}"><i class="fa fa-eye"></i></button>
                        @else
                        <button type="button" title="Sertifikat" class="btn btn-xs btn-secondary"><i class="fa fa-eye"></i></button>
                        @endif
                    </div>
                    {{-- <a href="{{ url('admin/berkas/awal/seleksi-index/'.$item->id) }}" class="btn btn-xs btn-success"><i class="fas fa-pencil-alt"></i></a> --}}
                </td>
            @endif
        </tr>
        @endforeach
    </tbody>
</table>