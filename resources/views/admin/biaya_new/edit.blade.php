<form action="{{url('admin/setting-biaya/store')}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal-body">
        <input name="type" type="text" value="create" hidden>
        <div class="form-group">
            <label>Tahun Ajaran </label>
            <input name="tahun_ajaran" type="text" class="form-control form-control-sm" value="{{ $tahun_ajaran->tahun_ajaran }}" readonly>
            <div class="text-danger"></div>
        </div>
        <div class="biaya">
            @if (isset($tahun_ajaran))
            <div class="table-responsive-sm">
                <table class="table table-borderless">
                    @foreach ($tahun_ajaran->biaya
                    ->where('jenis_biaya', 'Biaya Pendaftaran')
                    ->where('program_studi', 'Default') as $key => $item)
                    <tr>
                        <td>{{ $key == 0 ? "Uang Pendaftaran" : ""}}</td>
                        <td>{{ $item->nama_gelombang }}</td>
                        <td>
                            <input name="bp_id[]" type="hidden" value="{{ $item->id }}">
                            <input name="bp_nama_gelombang[]" type="hidden" value="{{ $item->nama_gelombang }}">
                            <input name="nominal_bp[]" type="number" class="nama_berkas form-control form-control-sm"
                             placeholder="Isi nominal pembayaran" min="0" value="{{ $item->biaya }}" required>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
            @endif
            @if (isset($tahun_ajaran))
            <div class="table-responsive-sm">
                <table class="table table-borderless">
                    @foreach ($tahun_ajaran->biaya
                    ->where('jenis_biaya', 'Biaya Daftar Ulang')
                    ->where('program_studi', 'Default') as $key => $item)
                    <tr>
                        <td>{{ $key == 0 ? "Uang Gedung" : ""}}</td>
                        <td>{{ $item->nama_gelombang }}</td>
                        <td>
                            <input name="bu_id[]" type="hidden" value="{{ $item->id }}">
                            <input name="bu_nama_gelombang[]" type="hidden" value="{{ $item->nama_gelombang }}">
                            <input name="nominal_bu[]" type="number" class="nama_berkas form-control form-control-sm" 
                            placeholder="Isi nominal pembayaran" min="0" value="{{ $item->biaya }}" required>
                        </td>
                    </tr>
                    @endforeach
                </table>
            </div>
            @endif
            @if (isset($tahun_ajaran) && $tahun_ajaran->jurusan->count() > 0 && $web_profil->jenjang_sekolah == "SLTA")
            <hr>
            <div class="form-group">
                <label>Biaya Jurusan</label>
                <div class="table-responsive-sm">
                    <table class="table table-borderless">
                        <tr>
                            <td>Jurusan</td>
                            <td>Nama</td>
                            <td>Nominal</td>
                            <td></td>
                        </tr>
                        @foreach ($tahun_ajaran->bjs() as $item)
                        <tr>
                            <td>
                                <input name="bj_id[]" type="hidden" value="{{ $item->id }}">
                                <select class="form-control form-control-sm select2-biasa" name="program_studi[]" style="width: 130px;" required>
                                    <option value="">** Pilih Jurusan</option>
                                    @foreach ($tahun_ajaran->jurusan as $col)
                                    <option value="{{ $col->id }}" @if ($col->id == $item->program_studi) selected @endif>{{ $col->name }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <input name="nama_bj[]" type="text" class="form-control form-control-sm" placeholder="Isi nama biaya" value="{{ $item->jenis_biaya }}" required>
                            </td>
                            <td>
                                <input name="nominal_bj[]" type="number" class="nama_berkas form-control form-control-sm" placeholder="Isi nominal pembayaran" min="0" value="{{ $item->biaya }}" required>
                            </td>
                            <td>
                                <button class="btn btn-sm btn-secondary btn-delete" type="button">X</button>
                            </td>
                        </tr>
                        @endforeach
                        <tr class="row_bj" style="display: none;">
                            <td>
                                <input class="bj_id" type="hidden" value="Baru">
                                <select class="form-control form-control-sm select2-biasa program_studi" style="width: 130px;">
                                    <option value="">** Pilih Jurusan</option>
                                    @foreach ($tahun_ajaran->jurusan as $col)
                                    <option value="{{ $col->id }}">{{ $col->name }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <input type="text" class="form-control form-control-sm nama_bj" placeholder="Isi nama biaya">
                            </td>
                            <td>
                                <input type="number" class="nama_berkas form-control form-control-sm nominal_bj" placeholder="Isi nominal pembayaran" min="0">
                            </td>
                            <td>
                                <button class="btn btn-sm btn-secondary btn-delete" type="button">X</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="form-group">
                <button type="button" class="btn btn-success add_bj">
                    Tambah
                </button>
            </div>
            @endif
            @if (isset($tahun_ajaran))
            <hr>
            <div class="form-group">
                <label>Biaya Lainnya</label>
                <div class="table-responsive-sm">
                    <table class="table table-borderless">
                        <tr>
                            <td>Nama</td>
                            <td>Nominal</td>
                            <td></td>
                        </tr>
                        @foreach ($tahun_ajaran->bls() as $item)
                        <tr>
                            <td>
                                <input name="bl_id[]" type="hidden" value="{{ $item->id }}">
                                <input name="nama_bl[]" type="text" class="form-control form-control-sm" placeholder="Isi nama biaya" value="{{ $item->jenis_biaya }}" required>
                            </td>
                            <td>
                                <input name="nominal_bl[]" type="number" class="nama_berkas form-control form-control-sm" placeholder="Isi nominal pembayaran" min="0" value="{{ $item->biaya }}" required>
                            </td>
                            <td>
                                <button class="btn btn-sm btn-secondary btn-delete" type="button">X</button>
                            </td>
                        </tr>
                        @endforeach
                        <tr class="row_bl" style="display: none;">
                            <td>
                                <input class="bl_id" type="hidden" value="Baru">
                                <input type="text" class="form-control form-control-sm nama_bl" placeholder="Isi nama biaya">
                            </td>
                            <td>
                                <input type="number" class="nama_berkas form-control form-control-sm nominal_bl" placeholder="Isi nominal pembayaran" min="0">
                            </td>
                            <td>
                                <button class="btn btn-sm btn-secondary btn-delete" type="button">X</button>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
            <div class="form-group">
                <button type="button" class="btn btn-success add_bl">
                    Tambah
                </button>
            </div>
            @endif
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-success btn-sm">Simpan</button>
    </div>
</form>

<script>
    $('.select2-biasa').select2();
</script>