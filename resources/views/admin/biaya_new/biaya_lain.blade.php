@php
    function rupiah($num) {
        return 'Rp. '.number_format(intval($num), 0, ',', '.');
    }
@endphp
<div class="modal-body">
    <table style="width: 100%;" class="table table-hover table-striped table-bordered table-default">
        <thead>
        <tr>
            <th>Nama</th>
            <th>Nominal</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($tahun_ajaran->bls() as $key=>$item)
            <tr>
                <td>{{$item->jenis_biaya}}</td>
                <td>{{ rupiah($item->biaya) }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
</div>