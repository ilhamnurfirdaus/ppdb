@extends('layouts.admin')
@section('main')
@php
    function rupiah($num) {
        return 'Rp. '.number_format(intval($num), 0, ',', '.');
    }
@endphp
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>Setting Biaya
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
                <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#createModal">
                    Tambah
                </button>
            </div>
        </div>
        <div class="page-title-actions">
            
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>
<div class="main-card mb-3 card">
    <div class="card-body">
        <table style="width: 100%;" class="table table-hover table-striped table-bordered table-default">
            <thead>
            <tr>
                <th>Tahun Ajaran</th>
                <th>Uang Pendaftaran</th>
                <th>Uang Gedung</th>
                @if ($web_profil->jenjang_sekolah == "SLTA")
                <th>Biaya Jurusan</th>
                @endif
                <th>Biaya Lainnya</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($tahun_ajarans as $item)
                @if ($item->biaya->count() > 0)                
                <tr>
                    <td>{{$item->tahun_ajaran}}</td>
                    <td>
                        <button type="button" class="btn btn-xs btn-info btn_semua_modal" data-toggle="modal" data-target="#semuaModal" data-remote="{{url('admin/setting-biaya/uang-pendaftaran/'.$item->tahun_ajaran)}}" data-title="Uang Pendaftaran"><i class="fas fa-eye"></i></button>
                    </td>
                    <td>
                        <button type="button" class="btn btn-xs btn-info btn_semua_modal" data-toggle="modal" data-target="#semuaModal" data-remote="{{url('admin/setting-biaya/uang-gedung/'.$item->tahun_ajaran)}}" data-title="Uang Gedung"><i class="fas fa-eye"></i></button>
                    </td>
                    @if ($web_profil->jenjang_sekolah == "SLTA")
                    <td>
                        @if ($item->bjs()->count() > 0)
                        <button type="button" class="btn btn-xs btn-info btn_semua_modal" data-toggle="modal" data-target="#semuaModal" data-remote="{{url('admin/setting-biaya/biaya-jurusan/'.$item->tahun_ajaran)}}" data-title="Biaya Jurusan"><i class="fas fa-eye"></i></button>
                        @else
                        <button type="button" class="btn btn-xs btn-secondary"><i class="fas fa-eye"></i></button>
                        @endif
                    </td>
                    @endif
                    <td>
                        @if ($item->bls()->count() > 0)
                        <button type="button" class="btn btn-xs btn-info btn_semua_modal" data-toggle="modal" data-target="#semuaModal" data-remote="{{url('admin/setting-biaya/biaya-lain/'.$item->tahun_ajaran)}}" data-title="Biaya Lain"><i class="fas fa-eye"></i></button>
                        @else
                        <button type="button" class="btn btn-xs btn-secondary"><i class="fas fa-eye"></i></button>
                        @endif
                    </td>
                    <td>
                        <button type="button" class="btn btn-xs btn-success btn_update_modal" data-toggle="modal" data-target="#updateModal" data-remote="{{url('admin/setting-biaya/edit/'.$item->tahun_ajaran)}}"><i class="fas fa-pencil-alt"></i></button>
                        <button type="button" class="btn btn-xs btn-warning btn_delete_modal" data-toggle="modal" data-target="#deleteModal" data-remote="{{url('admin/setting-biaya/destroy/'.$item->tahun_ajaran)}}"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
                @endif
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('modal')
<!-- Modal -->
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="createModalLabel">Buat Data Baru</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/setting-biaya/store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type" type="text" value="create" hidden>
            <div class="form-group">
                <label>Tahun Ajaran <span class='text-danger' title='This field is required'>*</span></label>
                <select class="form-control form-control-sm select2-biasa tahun_ajaran" aria-label="Default select example" name="tahun_ajaran" style="width: 100%;" required>
                    @foreach ($tahun_ajarans as $item)
                        @if ($item->biaya->count() == 0)
                            <option value="{{ $item->tahun_ajaran }}" @if ($item->tahun_ajaran == Carbon\Carbon::now()->isoFormat('Y')."-".Carbon\Carbon::now()->addYear()->isoFormat('Y')) selected @endif>{{ $item->tahun_ajaran }}</option>
                        @endif
                    @endforeach
                </select>
                <div class="text-danger"></div>
            </div>
            <div class="biaya">
                @if (isset($tahun_ajaran))
                <div class="table-responsive-sm">
                    <table class="table table-borderless">
                        @foreach ($tahun_ajaran->gelombang as $key => $item)
                        <tr>
                            <td>{{ $key == 0 ? "Uang Pendaftaran" : ""}}</td>
                            <td>{{ $item->nama_gelombang }}</td>
                            <td>
                                <input name="bp_id[]" type="hidden" value="Baru">
                                <input name="bp_gelombang_id[]" type="hidden" value="{{ $item->gelombang_id }}">
                                <input name="nominal_bp[]" type="number" class="nama_berkas form-control form-control-sm" placeholder="Isi nominal pembayaran" min="0" required>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                @endif
                @if (isset($tahun_ajaran))
                <div class="table-responsive-sm">
                    <table class="table table-borderless">
                        @foreach ($tahun_ajaran->gelombang as $key => $item)
                        <tr>
                            <td>{{ $key == 0 ? "Uang Gedung" : ""}}</td>
                            <td>{{ $item->nama_gelombang }}</td>
                            <td>
                                <input name="bu_id[]" type="hidden" value="Baru">
                                <input name="bu_gelombang_id[]" type="hidden" value="{{ $item->gelombang_id }}">
                                <input name="nominal_bu[]" type="number" class="nama_berkas form-control form-control-sm" placeholder="Isi nominal pembayaran" min="0" required>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </div>
                @endif
                @if (isset($tahun_ajaran) && $tahun_ajaran->jurusan->count() > 0 && $web_profil->jenjang_sekolah == "SLTA")
                <hr>
                <div class="form-group">
                    <label>Biaya Jurusan</label>
                    <div class="table-responsive-sm">
                        <table class="table table-borderless">
                            <tr>
                                <td>Jurusan</td>
                                <td>Nama</td>
                                <td>Nominal</td>
                                <td></td>
                            </tr>
                            {{-- <tr>
                                <td>
                                    <input name="bj_id[]" type="hidden" value="Baru">
                                    <select class="form-control form-control-sm select2-biasa" name="program_studi[]" style="width: 130px;" required>
                                        <option value="">** Pilih Jurusan</option>
                                        @foreach ($tahun_ajaran->jurusan as $col)
                                        <option value="{{ $col->id }}">{{ $col->name }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input name="nama_bj[]" type="text" class="form-control form-control-sm" placeholder="Isi nama biaya" required>
                                </td>
                                <td>
                                    <input name="nominal_bj[]" type="number" class="nama_berkas form-control form-control-sm" placeholder="Isi nominal pembayaran" min="0" required>
                                </td>
                                <td>
                                    
                                </td>
                            </tr> --}}
                            <tr class="row_bj" style="display: none;">
                                <td>
                                    <input class="bj_id" type="hidden" value="Baru">
                                    <select class="form-control form-control-sm select2-biasa program_studi" style="width: 130px;">
                                        <option value="">** Pilih Jurusan</option>
                                        @foreach ($tahun_ajaran->jurusan as $col)
                                        <option value="{{ $col->id }}">{{ $col->name }}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control form-control-sm nama_bj" placeholder="Isi nama biaya">
                                </td>
                                <td>
                                    <input type="number" class="nama_berkas form-control form-control-sm nominal_bj" placeholder="Isi nominal pembayaran" min="0">
                                </td>
                                <td>
                                    <button class="btn btn-sm btn-secondary btn-delete" type="button">X</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-success add_bj">
                        Tambah
                    </button>
                </div>
                @endif
                @if (isset($tahun_ajaran))
                <hr>
                <div class="form-group">
                    <label>Biaya Lainnya</label>
                    <div class="table-responsive-sm">
                        <table class="table table-borderless">
                            <tr>
                                <td>Nama</td>
                                <td>Nominal</td>
                                <td></td>
                            </tr>
                            {{-- <tr>
                                <td>
                                    <input name="bl_id[]" type="hidden" value="Baru">
                                    <input name="nama_bl[]" type="text" class="form-control form-control-sm" placeholder="Isi nama biaya" required>
                                </td>
                                <td>
                                    <input name="nominal_bl[]" type="number" class="nama_berkas form-control form-control-sm" placeholder="Isi nominal pembayaran" min="0" required>
                                </td>
                                <td>
                                    
                                </td>
                            </tr> --}}
                            <tr class="row_bl" style="display: none;">
                                <td>
                                    <input class="bl_id" type="hidden" value="Baru">
                                    <input type="text" class="form-control form-control-sm nama_bl" placeholder="Isi nama biaya">
                                </td>
                                <td>
                                    <input type="number" class="nama_berkas form-control form-control-sm nominal_bl" placeholder="Isi nominal pembayaran" min="0">
                                </td>
                                <td>
                                    <button class="btn btn-sm btn-secondary btn-delete" type="button">X</button>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <div class="form-group">
                    <button type="button" class="btn btn-success add_bl">
                        Tambah
                    </button>
                </div>
                @endif
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

<script>
    $(document).on('click', '.btn-delete', function()
    { 
        $(this).parent().parent().remove(); 
    });

    $(document).on('change', '.tahun_ajaran', function() {
        let url = "{{ url('admin/setting-biaya/biayas') }}/"+$(this).val();
        let biaya = $(this).parent().parent().find('.biaya');

        // biaya.html("");

        biaya.load(url, function( data, textStatus, xhr ) {
            if ( textStatus == "error" ) {
                alert( "Koneksi Internet anda terputus." );
            }
        });
    }); 

    $(document).on('click', '.add_bj', function() {
        $(".select2-biasa").select2("destroy");
        let tambah_input = $(this).parent().parent().find('.row_bj').clone();
        tambah_input.show();
        tambah_input.removeClass("row_bj");
        tambah_input.find(".bj_id").attr('required',true);
        tambah_input.find(".bj_id").attr('name','bj_id[]');
        tambah_input.find(".program_studi").attr('required',true);
        tambah_input.find(".program_studi").attr('name','program_studi[]');
        tambah_input.find(".nama_bj").attr('required',true);
        tambah_input.find(".nama_bj").attr('name','nama_bj[]');
        tambah_input.find(".nominal_bj").attr('required',true);
        tambah_input.find(".nominal_bj").attr('name','nominal_bj[]');
        tambah_input.insertBefore($(this).parent().parent().find(".row_bj"));
        $('.select2-biasa').select2();
    });

    $(document).on('click', '.add_bl', function() {
        let tambah_input = $(this).parent().parent().find('.row_bl').clone();
        tambah_input.show();
        tambah_input.removeClass("row_bl");
        tambah_input.find(".bl_id").attr('required',true);
        tambah_input.find(".bl_id").attr('name','bl_id[]');
        tambah_input.find(".nama_bl").attr('required',true);
        tambah_input.find(".nama_bl").attr('name','nama_bl[]');
        tambah_input.find(".nominal_bl").attr('required',true);
        tambah_input.find(".nominal_bl").attr('name','nominal_bl[]');
        tambah_input.insertBefore($(this).parent().parent().find(".row_bl"));
    });
</script>
@endsection