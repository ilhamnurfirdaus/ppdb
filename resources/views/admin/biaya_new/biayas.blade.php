@if (isset($tahun_ajaran))
    <div class="table-responsive-sm">
        <table class="table table-borderless">
            @foreach ($tahun_ajaran->gelombang as $key => $item)
            <tr>
                <td>{{ $key == 0 ? "Uang Pendaftaran" : ""}}</td>
                <td>{{ $item->nama_gelombang }}</td>
                <td>
                    <input name="bp_id[]" type="hidden" value="Baru">
                    <input name="bp_gelombang_id[]" type="hidden" value="{{ $item->gelombang_id }}">
                    <input name="nominal_bp[]" type="number" class="nama_berkas form-control form-control-sm" placeholder="Isi nominal pembayaran" min="0" required>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
    @endif
    @if (isset($tahun_ajaran))
    <div class="table-responsive-sm">
        <table class="table table-borderless">
            @foreach ($tahun_ajaran->gelombang as $key => $item)
            <tr>
                <td>{{ $key == 0 ? "Uang Gedung" : ""}}</td>
                <td>{{ $item->nama_gelombang }}</td>
                <td>
                    <input name="bu_id[]" type="hidden" value="Baru">
                    <input name="bu_gelombang_id[]" type="hidden" value="{{ $item->gelombang_id }}">
                    <input name="nominal_bu[]" type="number" class="nama_berkas form-control form-control-sm" placeholder="Isi nominal pembayaran" min="0" required>
                </td>
            </tr>
            @endforeach
        </table>
    </div>
    @endif
    @if (isset($tahun_ajaran) && $tahun_ajaran->jurusan->count() > 0 && $web_profil->jenjang_sekolah == "SLTA")
    <hr>
    <div class="form-group">
        <label>Biaya Jurusan</label>
        <div class="table-responsive-sm">
            <table class="table table-borderless">
                <tr>
                    <td>Jurusan</td>
                    <td>Nama</td>
                    <td>Nominal</td>
                    <td></td>
                </tr>
                {{-- <tr>
                    <td>
                        <input name="bj_id[]" type="hidden" value="Baru">
                        <select class="form-control form-control-sm select2-biasa" name="program_studi[]" style="width: 130px;" required>
                            <option value="">** Pilih Jurusan</option>
                            @foreach ($tahun_ajaran->jurusan as $col)
                            <option value="{{ $col->id }}">{{ $col->name }}</option>
                            @endforeach
                        </select>
                    </td>
                    <td>
                        <input name="nama_bj[]" type="text" class="form-control form-control-sm" placeholder="Isi nama biaya" required>
                    </td>
                    <td>
                        <input name="nominal_bj[]" type="number" class="nama_berkas form-control form-control-sm" placeholder="Isi nominal pembayaran" min="0" required>
                    </td>
                    <td>
                        
                    </td>
                </tr> --}}
                <tr class="row_bj" style="display: none;">
                    <td>
                        <input class="bj_id" type="hidden" value="Baru">
                        <select class="form-control form-control-sm select2-biasa program_studi" style="width: 130px;">
                            <option value="">** Pilih Jurusan</option>
                            @foreach ($tahun_ajaran->jurusan as $col)
                            <option value="{{ $col->id }}">{{ $col->name }}</option>
                            @endforeach
                        </select>
                    </td>
                    <td>
                        <input type="text" class="form-control form-control-sm nama_bj" placeholder="Isi nama biaya">
                    </td>
                    <td>
                        <input type="number" class="nama_berkas form-control form-control-sm nominal_bj" placeholder="Isi nominal pembayaran" min="0">
                    </td>
                    <td>
                        <button class="btn btn-sm btn-secondary btn-delete" type="button">X</button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="form-group">
        <button type="button" class="btn btn-success add_bj">
            Tambah
        </button>
    </div>
    @endif
    @if (isset($tahun_ajaran))
    <hr>
    <div class="form-group">
        <label>Biaya Lainnya</label>
        <div class="table-responsive-sm">
            <table class="table table-borderless">
                <tr>
                    <td>Nama</td>
                    <td>Nominal</td>
                    <td></td>
                </tr>
                {{-- <tr>
                    <td>
                        <input name="bl_id[]" type="hidden" value="Baru">
                        <input name="nama_bl[]" type="text" class="form-control form-control-sm" placeholder="Isi nama biaya" required>
                    </td>
                    <td>
                        <input name="nominal_bl[]" type="number" class="nama_berkas form-control form-control-sm" placeholder="Isi nominal pembayaran" min="0" required>
                    </td>
                    <td>
                        
                    </td>
                </tr> --}}
                <tr class="row_bl" style="display: none;">
                    <td>
                        <input class="bl_id" type="hidden" value="Baru">
                        <input type="text" class="form-control form-control-sm nama_bl" placeholder="Isi nama biaya">
                    </td>
                    <td>
                        <input type="number" class="nama_berkas form-control form-control-sm nominal_bl" placeholder="Isi nominal pembayaran" min="0">
                    </td>
                    <td>
                        <button class="btn btn-sm btn-secondary btn-delete" type="button">X</button>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="form-group">
        <button type="button" class="btn btn-success add_bl">
            Tambah
        </button>
    </div>
@endif


<script>
    $('.select2-biasa').select2();
</script>