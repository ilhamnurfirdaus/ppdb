@extends('layouts.admin')
@section('main')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div>Master Periode/Gelombang
                {{-- <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#createModal">
                    Tambah
                </button> --}}
                <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success btn_semua_modal" data-toggle="modal" data-target="#semuaModal" data-remote="{{url('admin/tahun-ajaran/create')}}" data-title="Buat Data Baru">
                    Tambah
                </button>
            </div>
        </div>
        <div class="page-title-actions">
            
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>
<div class="main-card mb-3 card">
    <div class="card-body">
        <table style="width: 100%;" class="table table-hover table-striped table-bordered table-default">
            <thead>
            <tr>
                <th>Tahun Ajaran</th>
                <th>Jml Kuota Penerima</th>
                <th>Nilai Minimal Raport</th>
                <th>Gelombang</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($tahun_ajarans as $key=>$item)
                <tr>
                    <td>{{$item->tahun_ajaran}}</td>
                    <td>{{$item->maksimal_jumlah_siswa}}</td>
                    <td>{{$item->minimal_nilai}}</td>
                    <td>
                        <button type="button" class="btn btn-xs btn-info btn_semua_modal" data-toggle="modal" data-target="#semuaModal" data-remote="{{url('admin/tahun-ajaran/gelombang/'.$item->tahun_ajaran)}}" data-title="Gelombang Tahun Ajaran {{ $item->tahun_ajaran }}"><i class="fas fa-eye"></i></button>
                    </td>
                    <td>
                        <button type="button" class="btn btn-xs btn-success btn_update_modal" data-toggle="modal" data-target="#updateModal" data-remote="{{url('admin/tahun-ajaran/edit/'.$item->tahun_ajaran)}}"><i class="fas fa-pencil-alt"></i></button>
                        <button type="button" class="btn btn-xs btn-warning btn_delete_modal" data-toggle="modal" data-target="#deleteModal" data-remote="{{url('admin/tahun-ajaran/destroy/'.$item->tahun_ajaran)}}"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
                    
                @endforeach
            </tbody>
            
        </table>
    </div>
</div>
@endsection

@section('modal')
<!-- Modal -->
{{-- <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="createModalLabel">Buat Data Baru</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/tahun-ajaran/store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <div class="form-group">
                <label>Tahun Ajaran <span class='text-danger' title='This field is required'>*</span></label>
                <input name="tahun_ajaran" type="text" class="form-control form-control-sm" value="{{$ta}}" readonly>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Nilai Minimal Raport  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="minimal_nilai" type="number" class="form-control form-control-sm" placeholder="Isi Minimal Nilai jurusan" min="0" max="10" step=".01" value="{{ old('minimal_nilai') ? old('minimal_nilai') : '0.0' }}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Jumlah Kuota Penerima  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="maksimal_jumlah_siswa" type="number" class="form-control form-control-sm" placeholder="Isi Maksimal Jumlah Siswa jurusan" min="0" value="{{ old('maksimal_jumlah_siswa') ? old('maksimal_jumlah_siswa') : '0' }}" required>
                <div class="text-danger"></div>
            </div>
            <hr>
            <div class="table-responsive-sm">
                <table class="table table-borderless">
                    <tr>
                        <td>Gelombang</td>
                        <td colspan="3">
                            <input name="nama_gelombang" type="text" class="form-control form-control-sm" placeholder="Isi nama gelombang" required>
                        </td>
                        <td>
                            <button type="button" class="btn btn-xs btn-warning btn-delete"><i class="fa fa-trash"></i></button>
                        </td>
                    </tr>
                    <tr>
                        <td>Range Tgl</td>
                        <td>
                            <div class="input-group input-group-sm" style="width:120px;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text open-datetimepicker"><a><i class='fa fa-calendar '></i></a></span>
                                </div>
                                <input type="text" name="tanggal[]" class="form-control input_date" value="{{Carbon\Carbon::parse(old('tanggal.'.$key))->format('Y-m-d')}}" onkeypress="return false;" focusable="false" required>
                            </div>
                        </td>
                        <td>
                            -
                        </td>
                        <td>
                            <div class="input-group input-group-sm" style="width:120px;">
                                <div class="input-group-prepend">
                                    <span class="input-group-text open-datetimepicker"><a><i class='fa fa-calendar '></i></a></span>
                                </div>
                                <input type="text" name="tanggal[]" class="form-control input_date" value="{{Carbon\Carbon::parse(old('tanggal.'.$key))->format('Y-m-d')}}" onkeypress="return false;" focusable="false" required>
                            </div>
                        </td>
                        <td>
                            
                        </td>
                    </tr>
                </table>
            </div>
            <div class="form-group">
                <button id="btn-tambah" type="button" class="btn btn-success">
                    Tambah
                </button>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div> --}}
<script>
    
</script>
@endsection