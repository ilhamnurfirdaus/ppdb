<div class="modal-body">
    <table style="width: 100%;" class="table table-hover table-striped table-bordered table-default">
        <thead>
        <tr>
            <th>Nama</th>
            <th>Tanggal Mulai</th>
            <th>Tanggal Selesai</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($tahun_ajaran->gelombang as $key=>$item)
            <tr>
                <td>{{$item->nama_gelombang}}</td>
                <td>{{$item->tanggal_mulai}}</td>
                <td>{{$item->tanggal_selesai}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
</div>