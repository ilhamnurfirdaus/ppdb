<style>
    .table-borderless > tbody > tr > td,
    .table-borderless > tbody > tr > th,
    .table-borderless > tfoot > tr > td,
    .table-borderless > tfoot > tr > th,
    .table-borderless > thead > tr > td,
    .table-borderless > thead > tr > th {
        border: none;
    }
</style>

<form action="{{url('admin/tahun-ajaran/update/'.$tahun_ajaran->tahun_ajaran)}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal-body">
        <div class="form-group">
            <label>Tahun Ajaran <span class='text-danger' title='This field is required'>*</span></label>
            <input name="tahun_ajaran" type="text" class="form-control form-control-sm" value="{{$tahun_ajaran->tahun_ajaran}}" readonly>
            <div class="text-danger"></div>
        </div>
        <div class="form-group">
            <label>Nilai Minimal Raport  <span class='text-danger' title='This field is required'>*</span></label>
            <input name="minimal_nilai" type="number" class="form-control form-control-sm" placeholder="Isi Minimal Nilai jurusan" min="0" max="100" step=".01" value="{{ $tahun_ajaran->minimal_nilai }}" required>
            <div class="text-danger"></div>
        </div>
        <div class="form-group">
            <label>Jumlah Kuota Penerima  <span class='text-danger' title='This field is required'>*</span></label>
            <input name="maksimal_jumlah_siswa" type="number" class="form-control form-control-sm" placeholder="Isi Maksimal Jumlah Siswa jurusan" min="0" value="{{ $tahun_ajaran->maksimal_jumlah_siswa }}" required>
            <div class="text-danger"></div>
        </div>
        <hr>
        <div class="table-responsive-sm">
            <table class="table table-borderless">
                @foreach ($tahun_ajaran->gelombang as $item)
                <tr>
                    <td>Gelombang</td>
                    <td colspan="3">
                        <input name="gelombang_id[]" type="hidden" value="{{ $item->gelombang_id }}">
                        <input name="nama_gelombang[]" type="text" class="form-control form-control-sm" placeholder="Isi nama gelombang" value="{{ $item->nama_gelombang }}" required>
                    </td>
                    <td>
                        <button type="button" class="btn btn-xs btn-warning btn-delete" style="display: none;"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
                <tr>
                    <td>Range Tgl</td>
                    <td>
                        <div class="input-group input-group-sm" style="width:120px;">
                            <div class="input-group-prepend">
                                <span class="input-group-text open-datetimepicker"><a><i class='fa fa-calendar '></i></a></span>
                            </div>
                            <input type="text" name="tanggal_mulai[]" class="form-control input_date tanggal_mulai" value="{{Carbon\Carbon::parse($item->tanggal_mulai)->format('Y-m-d')}}" onkeypress="return false;" focusable="false" required>
                        </div>
                    </td>
                    <td>
                        -
                    </td>
                    <td>
                        <div class="input-group input-group-sm" style="width:120px;">
                            <div class="input-group-prepend">
                                <span class="input-group-text open-datetimepicker"><a><i class='fa fa-calendar '></i></a></span>
                            </div>
                            <input type="text" name="tanggal_selesai[]" class="form-control input_date tanggal_selesai" value="{{Carbon\Carbon::parse($item->tanggal_selesai)->format('Y-m-d')}}" onkeypress="return false;" focusable="false" required>
                        </div>
                    </td>
                    <td>
                        
                    </td>
                </tr>
                @endforeach
                <tr id="row-input" style="display: none;">
                    <td>Gelombang</td>
                    <td colspan="3">
                        <input class="gelombang_id" type="hidden" value="Baru">
                        <input type="text" class="form-control form-control-sm nama_gelombang" placeholder="Isi nama gelombang">
                    </td>
                    <td>
                        <button type="button" class="btn btn-xs btn-warning btn-delete"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
                <tr id="row-input2" style="display: none;">
                    <td>Range Tgl</td>
                    <td>
                        <div class="input-group input-group-sm" style="width:120px;">
                            <div class="input-group-prepend">
                                <span class="input-group-text open-datetimepicker"><a><i class='fa fa-calendar '></i></a></span>
                            </div>
                            <input type="text" class="form-control input_date tanggal_mulai" value="{{Carbon\Carbon::now()->format('Y-m-d')}}" onkeypress="return false;" focusable="false">
                        </div>
                    </td>
                    <td>
                        -
                    </td>
                    <td>
                        <div class="input-group input-group-sm" style="width:120px;">
                            <div class="input-group-prepend">
                                <span class="input-group-text open-datetimepicker"><a><i class='fa fa-calendar '></i></a></span>
                            </div>
                            <input type="text" class="form-control input_date tanggal_selesai" value="{{Carbon\Carbon::now()->format('Y-m-d')}}" onkeypress="return false;" focusable="false">
                        </div>
                    </td>
                    <td>
                        
                    </td>
                </tr>
            </table>
        </div>
        <div class="form-group">
            <button id="btn-tambah" type="button" class="btn btn-success">
                Tambah
            </button>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-success btn-sm">Simpan</button>
    </div>
</form>

<script>
$('.input_date').datepicker({
    format: 'yyyy-mm-dd',
    language: 'id'
});

$('.input_date').datepicker('hide');

$('.open-datetimepicker').click(function () {
    $(this).next('.input_date').datepicker('show');
});

$(document).on('change', '.tanggal_mulai', function()
{
    var tanggal_mulai = $(this).val();
    var tanggal_selesai = $(this).parent().parent().parent().find('.tanggal_selesai').val();
    if (tanggal_selesai == "") {
        $(this).parent().parent().parent().find('.tanggal_selesai').val(tanggal_mulai);
    } else if (tanggal_mulai > tanggal_selesai){
        $(this).parent().parent().parent().find('.tanggal_selesai').val(tanggal_mulai);
    }
});

$(document).on('change', '.tanggal_selesai', function()
{
    var tanggal_mulai = $(this).parent().parent().parent().find('.tanggal_mulai').val();
    var tanggal_selesai = $(this).val();
    if (tanggal_selesai == "") {
        $(this).parent().parent().parent().find('.tanggal_mulai').val(tanggal_selesai);
    } else if (tanggal_mulai > tanggal_selesai){
        $(this).parent().parent().parent().find('.tanggal_mulai').val(tanggal_selesai);
    }
});

$(document).on('click', '.btn-delete', function()
{ 
    $(this).parent().parent().next().remove();
    $(this).parent().parent().remove(); 
});

$('#btn-tambah').click(function ()
{        
    let tambah_input = $('#row-input').clone();
    tambah_input.show();
    tambah_input.attr("id", "");
    tambah_input.find(".gelombang_id").attr('required',true);
    tambah_input.find(".gelombang_id").attr('name','gelombang_id[]');
    tambah_input.find(".nama_gelombang").attr('required',true);
    tambah_input.find(".nama_gelombang").attr('name','nama_gelombang[]');
    tambah_input.insertBefore("#row-input"); 

    let tambah_input2 = $('#row-input2').clone();
    tambah_input2.show();
    tambah_input2.attr("id", "");
    tambah_input2.find(".tanggal_mulai").attr('required',true);
    tambah_input2.find(".tanggal_mulai").attr('name','tanggal_mulai[]');
    tambah_input2.find(".tanggal_selesai").attr('required',true);
    tambah_input2.find(".tanggal_selesai").attr('name','tanggal_selesai[]');
    tambah_input2.insertBefore("#row-input");

    $('.input_date').datepicker({
        format: 'yyyy-mm-dd',
        language: 'id'
    });
    $('.input_date').datepicker('hide');
    $('.open-datetimepicker').click(function () {
        $(this).next('.input_date').datepicker('show');
    });
});
</script>