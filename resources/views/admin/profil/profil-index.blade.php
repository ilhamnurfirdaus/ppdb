@extends('layouts.admin')
@section('main')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            <div>Profil
            </div>
        </div>
        <div class="page-title-actions">
            
        </div>    
    </div>
</div>

<div class="main-card mb-3 card">
    <div class="card-body">
        <form method="POST" action="{{ url('profile/update') }}/{{$user->id}}" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label>Nama</label>
                        <input name="nama" type="text" class="form-control form-control-sm" value="{{$user->name}}" required>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>Email</label>
                        <input name="email" type="text" class="form-control form-control-sm" value="{{$user->email}}" required>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-6">
                    <div class="form-group">
                        <label>Username</label>
                        <input name="username" type="text" class="form-control form-control-sm" value="{{$user->username}}"  required>
                    </div>
                </div>
                <div class="col-6">
                    <div class="form-group">
                        <label>Password</label>
                        <input name="password" type="password" class="form-control form-control-sm" value="">
                    </div>
                </div>
            </div>
            <div class="form-group">
                <label>Foto</label>
                <input type="file" class="dropify" name="file" data-max-file-size="2M" data-default-file="{{asset('/profil')}}/{{$user->foto}}" />
                <hr>
                <label>Ketentuan</label>
                <ul>
                    <li>File harus berformat <b>.jpg/.jpeg/.png</b></li>
                    <li>Ukuran file maksimal <b>2 MB</b></li>
                </ul>
            </div>

            <div class="form-group my-3">
            <button type="submit" class="btn btn-success">Simpan</button>
            </div>
        </form>
    </div>
</div>
@endsection