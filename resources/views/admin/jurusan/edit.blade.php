<form action="{{url('admin/jurusan/update/'.$jurusan->id)}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal-body">
        <input name="type-{{$jurusan->id}}" type="text" value="update" hidden>
        <div class="form-group">
            <label>Tahun Ajaran <span class='text-danger' title='This field is required'>*</span></label>
            <select class="form-control form-control-sm select2-biasa tahun_ajaran" aria-label="Default select example" name="tahun_ajaran" style="width: 100%;" required>
                @foreach ($tahun_ajarans as $item)
                    <option value="{{ $item->tahun_ajaran }}" @if ($item->tahun_ajaran == Carbon\Carbon::now()->isoFormat('Y')."-".Carbon\Carbon::now()->addYear()->isoFormat('Y')) selected @endif>{{ $item->tahun_ajaran }}</option>
                @endforeach
            </select>
            <div class="text-danger"></div>
        </div>
        <div class="form-group">
            <label>Nama Jurusan  <span class='text-danger' title='This field is required'>*</span></label>
            <input name="name" type="text" class="form-control form-control-sm" placeholder="Isi nama jurusan" value="{{ $jurusan->name }}" required>
            <div class="text-danger"></div>
        </div>
        @foreach ($jurusan->settingBerkas as $key => $row)
        <div class="form-group">
            @if ($key == '0')
            <label>Berkas Syarat <span class='text-danger' title='This field is required'>*</span></label>
            @endif
            <input name="setting_berkas_id[]" type="hidden" class="setting_berkas_id form-control form-control-sm" value="{{ $row->setting_berkas_id  }}">
            <div class="form-group row">
                <div class="col-sm-10">
                    <input name="nama_berkas[]" type="text" class="nama_berkas form-control form-control-sm" placeholder="Isi nama berkas" value="{{ $row->nama_berkas  }}" required>
                </div>
                @if ($key != '0')
                <div class="col-sm-2">
                    <button class="btn btn-sm btn-secondary btn-delete" type="button">X</button>
                </div>
                @endif
            </div>
        </div>
        @endforeach
        <div class="form-group row-input" style="display: none;">
            <input type="hidden" class="setting_berkas_id form-control form-control-sm" value="Baru">
            <div class="form-group row">
                <div class="col-sm-10">
                    <input type="text" class="nama_berkas form-control form-control-sm" placeholder="Isi nama berkas">
                </div>
                <div class="col-sm-2">
                    <button class="btn btn-sm btn-secondary btn-delete" type="button">X</button>
                </div>
            </div>
        </div>
        <div class="form-group row-add">
            <button type="button" class="btn btn-info btn-sm btn-add">Tambah Berkas</button>
        </div>
        <div class="form-group">
            <label>Kuota Siswa  <span class='text-danger' title='This field is required'>*</span></label>
            <input name="maksimal_jumlah_siswa" type="number" class="form-control form-control-sm maksimal_jumlah_siswa" placeholder="Isi Maksimal Jumlah Siswa jurusan" min="0" max="{{ $sisa_kuota }}" value="{{ $jurusan->maksimal_jumlah_siswa }}" required>
            <div class="text-danger"></div>
        </div>
        <div class="form-group">
            <label>Nilai Minimal Raport  <span class='text-danger' title='This field is required'>*</span></label>
            <input name="minimal_nilai" type="number" class="form-control form-control-sm minimal_nilai" placeholder="Isi Minimal Nilai jurusan" min="{{ $jurusan->tahunAjaran->minimal_nilai }}" max="100" step=".01" value="{{ $jurusan->minimal_nilai }}" required>
            <div class="text-danger"></div>
        </div>
        <div class="form-group">
            <label>Akreditasi <span class='text-danger' title='This field is required'>*</span></label>
            <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="akreditasi" style="width: 100%;" required>
                <option value="">**Pilih</option>
                <option value="Terkakreditasi A" @if("Terkakreditasi A" == $jurusan->akreditasi) selected @endif>Terkakreditasi A</option>
                <option value="Terkakreditasi B" @if("Terkakreditasi B" == $jurusan->akreditasi) selected @endif>Terkakreditasi B</option>
                <option value="Terdaftar" @if("Terdaftar" == $jurusan->akreditasi) selected @endif>Terdaftar</option>
            </select>
            <div class="text-danger"></div>
        </div>
        {{-- <div class="form-group">
            <label>Status <span class='text-danger' title='This field is required'>*</span></label>
            <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="status" style="width: 100%;" required>
                <option value="">**Pilih</option>
                <option value="Tidak Aktif" @if("Tidak Aktif" == $jurusan->status) selected @endif>Tidak Aktif</option>
                <option value="Aktif" @if("Aktif" == $jurusan->status) selected @endif>Aktif</option>
            </select>
            <div class="text-danger"></div>
        </div>
        <div class="form-group">
            <label>keterangan</label>
            <textarea name="keterangan" id="keterangan" class='form-control form-control-sm' rows='5'>{{$jurusan->keterangan}}</textarea>
            <div class="text-danger"></div>
        </div> --}}
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-success btn-sm">Simpan</button>
    </div>
</form>

<script>
    // $(document).on('click', '.btn-delete', function()
    // { 
    //     $(this).parent().parent().parent().remove(); 
    // });

    $(document).ready(function () {
        $('.btn-add').click(function () {
            console.log($(this).parent().parent().find('.row-input'));
            let tambah_input = $(this).parent().parent().find('.row-input').clone();
            tambah_input.show();
            tambah_input.removeClass("row-input");
            // tambah_input.find(".btn-delete").show();
            tambah_input.find(".nama_berkas").attr('required',true);
            tambah_input.find(".nama_berkas").attr('name','nama_berkas[]');
            tambah_input.find(".setting_berkas_id").attr('required',true);
            tambah_input.find(".setting_berkas_id").attr('name','setting_berkas_id[]');
            tambah_input.insertBefore($(this).parent().parent().find(".row-add"));
        });   
    });
</script>