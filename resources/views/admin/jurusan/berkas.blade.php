<div class="modal-body">
    <table style="width: 100%;" class="table table-hover table-striped table-bordered table-default">
        <thead>
        <tr>
            <th>No</th>
            <th>Nama</th>
        </tr>
        </thead>
        <tbody>
            @foreach ($jurusan->settingBerkas as $key=>$item)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{$item->nama_berkas}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
</div>