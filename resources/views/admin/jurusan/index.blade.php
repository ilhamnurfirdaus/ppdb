@extends('layouts.admin')
@section('main')
@php
    function rupiah($num) {
        return 'Rp. '.number_format(intval($num), 0, ',', '.');
    }
@endphp
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>Master Jurusan
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
                <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#createModal">
                    Tambah
                </button>
            </div>
        </div>
        <div class="page-title-actions">
            
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>
<div class="main-card mb-3 card">
    <div class="card-body">
        <table style="width: 100%;" class="table table-hover table-striped table-bordered table-default">
            <thead>
            <tr>
                <th>Tahun Ajaran</th>
                <th>Jurusan</th>
                <th>Kuota Siswa</th>
                <th>Nilai Minimal Raport</th>
                <th>Berkas</th>
                <th>Akreditasi</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($jurusans as $item)
                <tr>
                    <td>{{$item->tahun_ajaran}}</td>
                    <td>{{$item->name}}</td>
                    <td>{{$item->maksimal_jumlah_siswa}}</td>
                    <td>{{$item->minimal_nilai}}</td>
                    <td>
                        <button type="button" class="btn btn-xs btn-info btn_semua_modal" data-toggle="modal" data-target="#semuaModal" data-remote="{{url('admin/jurusan/berkas/'.$item->id)}}" data-title="Berkas"><i class="fas fa-eye"></i></button>
                    </td>
                    <td>{{$item->akreditasi}}</td>
                    <td>
                        <button type="button" class="btn btn-xs btn-success btn_update_modal" data-toggle="modal" data-target="#updateModal" data-remote="{{url('admin/jurusan/edit/'.$item->id)}}"><i class="fas fa-pencil-alt"></i></button>
                        <button type="button" class="btn btn-xs btn-warning btn_delete_modal" data-toggle="modal" data-target="#deleteModal" data-remote="{{url('admin/jurusan/destroy/'.$item->id)}}"><i class="fa fa-trash"></i></button>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('modal')
<!-- Modal -->
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="createModalLabel">Buat Data Baru</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/jurusan/store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type" type="text" value="create" hidden>
            <div class="form-group">
                <label>Tahun Ajaran <span class='text-danger' title='This field is required'>*</span></label>
                <select class="form-control form-control-sm select2-biasa tahun_ajaran" aria-label="Default select example" name="tahun_ajaran" style="width: 100%;" required>
                    @foreach ($tahun_ajarans as $item)
                        <option value="{{ $item->tahun_ajaran }}" @if ($item->tahun_ajaran == Carbon\Carbon::now()->isoFormat('Y')."-".Carbon\Carbon::now()->addYear()->isoFormat('Y')) selected @endif>{{ $item->tahun_ajaran }}</option>
                    @endforeach
                </select>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Nama Jurusan <span class='text-danger' title='This field is required'>*</span></label>
                <input name="name" type="text" class="form-control form-control-sm" placeholder="Isi nama jurusan" value="{{old('name')}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Berkas Syarat <span class='text-danger' title='This field is required'>*</span></label>
                <input name="setting_berkas_id[]" type="hidden" class="setting_berkas_id form-control form-control-sm" value="Baru">
                <div class="form-group row">
                    <div class="col-sm-10">
                        <input name="nama_berkas[]" type="text" class="nama_berkas form-control form-control-sm" placeholder="Isi nama berkas" value="{{old('nama_berkas')}}" required>
                    </div>
                </div>
            </div>
            <div class="form-group row-input" style="display: none;">
                <input type="hidden" class="setting_berkas_id form-control form-control-sm" value="Baru">
                <div class="form-group row">
                    <div class="col-sm-10">
                        <input type="text" class="nama_berkas form-control form-control-sm" placeholder="Isi nama berkas">
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-sm btn-secondary btn-delete" type="button">X</button>
                    </div>
                </div>
            </div>
            <div class="form-group row-add">
                <button type="button" class="btn btn-info btn-sm btn-add">Tambah Berkas</button>
            </div>
            <div class="form-group">
                <label>Kuota Siswa  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="maksimal_jumlah_siswa" type="number" class="form-control form-control-sm maksimal_jumlah_siswa" placeholder="Isi Maksimal Jumlah Siswa jurusan" min="0" max="{{ $sisa_kuota }}" value="{{ old('maksimal_jumlah_siswa') ? old('maksimal_jumlah_siswa') : '0' }}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Nilai Minimal Raport  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="minimal_nilai" type="number" class="form-control form-control-sm minimal_nilai" placeholder="Isi Minimal Nilai jurusan" min="{{ isset($tahun_ajaran) ? $tahun_ajaran->minimal_nilai : 0 }}" max="100" step=".01" value="{{ old('minimal_nilai') ? old('minimal_nilai') : (isset($tahun_ajaran) ? $tahun_ajaran->minimal_nilai : 0) }}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Akreditasi <span class='text-danger' title='This field is required'>*</span></label>
                <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="akreditasi" style="width: 100%;" required>
                    <option value="">**Pilih</option>
                    <option value="Terkakreditasi A" @if("Terkakreditasi A" == old("akreditasi")) selected @endif>Terkakreditasi A</option>
                    <option value="Terkakreditasi B" @if("Terkakreditasi B" == old("akreditasi")) selected @endif>Terkakreditasi B</option>
                    <option value="Terdaftar" @if("Terdaftar" == old("akreditasi")) selected @endif>Terdaftar</option>
                </select>
                <div class="text-danger"></div>
            </div>
            {{-- <div class="form-group">
                <label>Status <span class='text-danger' title='This field is required'>*</span></label>
                <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="status" style="width: 100%;" required>
                    <option value="">**Pilih</option>
                    <option value="Tidak Aktif" @if("Tidak Aktif" == old("status")) selected @endif>Tidak Aktif</option>
                    <option value="Aktif" @if("Aktif" == old("status")) selected @endif>Aktif</option>
                </select>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>keterangan</label>
                <textarea name="keterangan" id="keterangan" class='form-control form-control-sm' rows='5'>{{old('keterangan')}}</textarea>
                <div class="text-danger"></div>
            </div> --}}
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

{{-- <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="updateModalLabel">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div id="model_content_update">

        </div>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="deleteModalLabel">Hapus Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Apakah anda yakin untuk menghapus data ini? 
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <a id="btn_hapus" href="#" class="btn btn-danger btn-sm">Hapus</a>
        </div>
        </div>
    </div>
</div> --}}

{{-- @foreach ($jurusans as $item)
<div class="modal fade" id="updateModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="updateModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="updateModal-{{$item->id}}Label">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/jurusan/update/'.$item->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type-{{$item->id}}" type="text" value="update" hidden>
            <div class="form-group">
                <label>Nama  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="name" type="text" class="form-control form-control-sm" placeholder="Isi nama jurusan" value="{{ $item->name }}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Minimal Nilai  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="minimal_nilai" type="number" class="form-control form-control-sm" placeholder="Isi Minimal Nilai jurusan" min="0" max="100" step=".01" value="{{ $item->minimal_nilai }}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Maksimal Jumlah Siswa  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="maksimal_jumlah_siswa" type="number" class="form-control form-control-sm" placeholder="Isi Maksimal Jumlah Siswa jurusan" min="0" value="{{ $item->maksimal_jumlah_siswa }}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Status <span class='text-danger' title='This field is required'>*</span></label>
                <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="status" style="width: 100%;" required>
                    <option value="">**Pilih</option>
                    <option value="Tidak Aktif" @if("Tidak Aktif" == $item->status) selected @endif>Tidak Aktif</option>
                    <option value="Aktif" @if("Aktif" == $item->status) selected @endif>Aktif</option>
                </select>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>keterangan</label>
                <textarea name="keterangan" id="keterangan" class='form-control form-control-sm' rows='5'>{{$item->keterangan}}</textarea>
                <div class="text-danger"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="deleteModal-{{$item->id}}Label">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Apakah anda yakin untuk menghapus data ini? 
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <a href="{{url('admin/jurusan/destroy/'.$item->id)}}" class="btn btn-danger btn-sm">Hapus</a>
        </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function () {
        if ('{{old("type-".$item->id)}}' == 'update') {
            $(document).ready(function () {
                $("#updateModal-{{$item->id}}").modal('show');
            });
        }

        $('.select2-update-{{$item->id}}').select2({
            placeholder: "**Silahkan Pilih",
            dropdownParent: $("#updateModal-{{$item->id}}")
        });
    });
</script>
@endforeach --}}
<script>
    $(document).on('click', '.btn-delete', function()
    { 
        $(this).parent().parent().parent().remove(); 
    });

    $(document).on('change', '.tahun_ajaran', function() {
        let url = "{{ url('admin/jurusan/tahun_ajaran') }}/"+$(this).val();
        let maksimal_jumlah_siswa = $(this).parent().parent().find(".maksimal_jumlah_siswa");
        let minimal_nilai = $(this).parent().parent().find(".minimal_nilai");

        $.get(url, function(data){
            maksimal_jumlah_siswa.attr('max',data.maksimal_jumlah_siswa);
            maksimal_jumlah_siswa.val(0);
            minimal_nilai.attr('min',data.minimal_nilai);
            minimal_nilai.val(data.minimal_nilai);
        });
    }); 

    $(document).ready(function(){
        // $(".btn_update_modal").click(function() {
        //     $("#model_content_update").empty(); 
        //     let url = $(this).data("remote");
        //     console.log(url);
        //     $('#model_content_update').load(url, function( data, textStatus, xhr ) {
        //         if ( textStatus == "error" ) {
        //             alert( "Koneksi Internet anda terputus." );
        //         } else {
        //             console.log("berhasil");
        //         }
        //     });
        // });

        // $(".btn_delete_modal").click(function() {
        //     let url = $(this).data("remote");
        //     $('#btn_hapus').attr('href', url);
        // }); 

        $('.btn-add').click(function () {
            console.log($(this).parent().parent().find('.row-input'));
            let tambah_input = $(this).parent().parent().find('.row-input').clone();
            tambah_input.show();
            tambah_input.removeClass("row-input");
            // tambah_input.find(".btn-delete").show();
            tambah_input.find(".nama_berkas").attr('required',true);
            tambah_input.find(".nama_berkas").attr('name','nama_berkas[]');
            tambah_input.find(".setting_berkas_id").attr('required',true);
            tambah_input.find(".setting_berkas_id").attr('name','setting_berkas_id[]');
            tambah_input.insertBefore($(this).parent().parent().find(".row-add"));
        });
    });
</script>
@endsection