@extends('layouts.admin')
@section('main')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>
                Informasi Pendaftaran
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
            </div>
        </div>  
    </div>
</div>

<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="mb-3 card">
            {{-- <div class="card-header-tab card-header bg-danger text-white">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    
                </div>
            </div> --}}
            <div class="card-body">
                <form method="POST" action="{{ url('admin/konten-management/update-informasi-pendaftaran/'.$data->id) }}" enctype="multipart/form-data">
                    @csrf 
                    <div class="form-group">
                        <label>Informasi Pendaftaran</label>
                        <textarea name="informasi_pendaftaran" class="summernote" required>{!! $data->informasi_pendaftaran !!}</textarea>
                    </div>

                    <div class="form-group my-3">
                    <button type="submit" class="btn btn-success">
                        {{ __('Simpan') }}
                    </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')

@endsection