@extends('layouts.admin')
@section('main')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>
                Profil Sekolah
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
            </div>
        </div>  
    </div>
</div>

<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="mb-3 card">
            {{-- <div class="card-header-tab card-header bg-danger text-white">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    
                </div>
            </div> --}}
            <div class="card-body">
                <form method="POST" action="{{ url('admin/konten-management/update-web-profil/'.$data->id) }}" enctype="multipart/form-data">
                    @csrf 
                    {{-- <div class="form-group">
                        <label>Nama Sekolah</label>
                        <input name="name" type="text" class="form-control form-control-sm" placeholder="Isi Nama Sekolah" value="{{ old('name') ? old('name') : $data->name }}" required>
                    </div> --}}
                    <div class="form-group">
                        <label>Nama Sekolah</label>
                        <div class="form-row">
                            <div class="col-md-2">
                                <select id="jenjang_sekolah" class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="jenjang_sekolah" style="width: 100%;" required>
                                    <option value="">**Pilih</option>
                                    <option value="PAUD" @if ($data->jenjang_sekolah == "PAUD" ||  old("jenjang_sekolah") == 'PAUD') selected @endif>PAUD</option>
                                    <option value="TK" @if ($data->jenjang_sekolah == "TK" ||  old("jenjang_sekolah") == 'TK') selected @endif>TK</option>
                                    <option value="SD" @if ($data->jenjang_sekolah == "SD" ||  old("jenjang_sekolah") == 'SD') selected @endif>SD</option>
                                    <option value="SLTP" @if ($data->jenjang_sekolah == "SLTP" ||  old("jenjang_sekolah") == 'SLTP') selected @endif>SLTP</option>
                                    <option value="SLTA" @if ($data->jenjang_sekolah == "SLTA" ||  old("jenjang_sekolah") == 'SLTA') selected @endif>SLTA</option>
                                </select>
                            </div>
                            <div id="jenjang_sekolah2" class="col-md-2" @if ($data->jenjang_sekolah != "SLTA") style="display: none;" @endif>
                                <select class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="jenjang_sekolah2" style="width: 100%;" @if ($data->jenjang_sekolah != "SLTA")  @else required @endif>
                                    <option value="SMA" @if ($data->jenjang_sekolah2 == "SMA" ||  old("jenjang_sekolah2") == 'SMA') selected @endif>SMA</option>
                                    <option value="SMK" @if ($data->jenjang_sekolah2 == "SMK" ||  old("jenjang_sekolah2") == 'SMK') selected @endif>SMK</option>
                                    <option value="MA" @if ($data->jenjang_sekolah2 == "MA" ||  old("jenjang_sekolah2") == 'MA') selected @endif>MA</option>
                                </select>
                            </div>
                            <div class="col-md-8">
                                <input name="name" type="text" class="form-control form-control-sm" placeholder="Isi Nama Sekolah" value="{{ old('name') ? old('name') : $data->name }}" required>
                            </div>
                        </div>
                    </div>
                    {{-- <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Jumlah Kelas Raport yang akan diupload</label>
                            <input id="jml_raport" name="jml_raport" type="number" class="form-control form-control-sm" placeholder="Isi Jumlah Raport Sekolah" value="{{ old('jml_raport') ? old('jml_raport') : $data->jml_raport }}" max="6" min="0" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Pakai Test pada Pendaftaran</label>
                            <select id="tampil_test" class="form-control form-control-sm select2-biasa" aria-label="Default select example" name="tampil_test" style="width: 100%;" required>
                                <option value="">**Pilih</option>
                                <option value="Ya" @if ($data->tampil_test == "Ya" ||  old("tampil_test") == 'Ya') selected @endif>Ya</option>
                                <option value="Tidak" @if ($data->tampil_test == "Tidak" ||  old("tampil_test") == 'Tidak') selected @endif>Tidak</option>
                            </select>
                        </div>
                    </div>
                    <div id="form-lolos" @if ($data->jenjang_sekolah == "SLTA") style="display: none;" @endif>
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label>Minimal total nilai yang lolos</label>
                                <input id="minimal_nilai" name="minimal_nilai" type="number" class="form-control form-control-sm" value="{{ old('minimal_nilai') ? old('minimal_nilai') : $data->minimal_nilai }}" min="0" step=".01" @if ($data->jenjang_sekolah != "SLTA") required @endif>
                            </div>
                            <div class="form-group col-md-6">
                                <label>Maksimal jumlah siswa yang lolos</label>
                                <input id="maksimal_jumlah_siswa" name="maksimal_jumlah_siswa" type="number" class="form-control form-control-sm" value="{{ old('maksimal_jumlah_siswa') ? old('maksimal_jumlah_siswa') : $data->maksimal_jumlah_siswa }}" min="0" @if ($data->jenjang_sekolah != "SLTA") required @endif>
                            </div>
                        </div>
                    </div> --}}
                    <div class="form-group">
                        <label>Alamat</label>
                        <textarea name="alamat" class="form-control form-control-sm" rows="3">{{ old('alamat') ? old('alamat') : $data->alamat }}</textarea>
                    </div>
                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label>Email Sekolah</label>
                            <input name="email" type="email" class="form-control form-control-sm" placeholder="Isi Email Sekolah" value="{{ old('email') ? old('email') : $data->email }}" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label>CP Sekolah</label>
                            <input name="cp" type="number" class="form-control form-control-sm" placeholder="Isi CP Sekolah" value="{{ old('cp') ? old('cp') : $data->cp }}" required>
                        </div>
                        <div class="form-group col-md-4">
                            <label>Telp Sekolah</label>
                            <input name="telp" type="text" class="form-control form-control-sm" placeholder="Isi Telp Sekolah" value="{{ old('telp') ? old('telp') : $data->telp }}" required>
                        </div>
                    </div>
                    <div class="form-group">
                        <label>Warna Header</label>
                        <input name="warna_header" type="text" class="form-control demo" value="{{ old('warna_header') ? old('warna_header') : $data->warna_header }}" required>
                        {{-- <div class="form-row">
                            <div class="col-md-6">
                                <input id="demo-input" name="warna_header" type="text" class="form-control form-control-sm" placeholder="Isi Warna Header" value="{{ old('warna_header') ? old('warna_header') : $data->telp }}" required>
                            </div>
                            <div class="col-md-6">
                                <input id="demo" type="text" class="form-control form-control-sm" disabled>
                            </div>
                        </div> --}}
                    </div>
                    <div class="form-group">
                        <label>Logo</label>
                        <input type="file" class="dropify" name="logo" data-max-file-size="2M" data-default-file="{{asset('/templates/landing/images/logo/'.$data->logo)}}" />
                        <hr>
                        <label>Ketentuan</label>
                        <ul>
                            <li>File harus berformat <b>.jpg/.jpeg/.png</b></li>
                            <li>Ukuran file maksimal <b>2 MB</b></li>
                        </ul>
                    </div>

                    <div class="form-group my-3">
                    <button type="submit" class="btn btn-success">
                        {{ __('Simpan') }}
                    </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection

@section('modal')
<script>
    $(document).ready(function () {        
        $('#jenjang_sekolah').change(function() {
            let val =  $(this).val();
            if (val == "SLTA") {
                $("#jenjang_sekolah2").show();
                $("#jenjang_sekolah2").find("select").attr('required', true);
                
                $("#form-lolos").hide();
                $("#minimal_nilai").attr('required', false);
                $("#maksimal_jumlah_siswa").attr('required', false);
            } else {
                $("#jenjang_sekolah2").hide();
                $("#jenjang_sekolah2").find("select").attr('required', false);

                $("#form-lolos").show();
                $("#minimal_nilai").attr('required', true);
                $("#maksimal_jumlah_siswa").attr('required', true);
            }

            if (val == "SLTA") {
                $('#jml_raport').val(3);
                $('#tampil_test').val("Ya").trigger("change");
            } else if (val == "SLTP"){
                $('#jml_raport').val(6);
                $('#tampil_test').val("Ya").trigger("change");
            } else {
                $('#jml_raport').val(0);
                $('#tampil_test').val("Tidak").trigger("change");
            }
        });

        // $('#demo-input').colorpicker();
      
        // // Example using an event, to change the color of the #demo div background:
        // $('#demo-input').on('colorpickerChange', function(event) {
        //     $('#demo').css('background-color', event.color.toString());
        // });

        $('.demo').minicolors({
          control: 'hue',
          theme: 'bootstrap'
        });
    });
</script>
@endsection