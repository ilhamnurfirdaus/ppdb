<html>
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Data Pembayaran Pendaftaran</title>
        <body>
            @php
                function rupiah($num) {
                    return 'Rp. '.number_format(intval($num), 0, ',', '.');
                }
            @endphp
            <style type="text/css">
                .footer { position: fixed; left: 0px; bottom: -50px; right: 0px; height: 50px;text-align: center;font-size: 10px;}
                .footer .pagenum:before { content: counter(page); }

                .tg  {border-collapse:collapse;border-spacing:0;border-color:#ccc;width: 100%; }
                .tg td{font-family:Arial;font-size:12px;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#fff;}
                .tg th{font-family:Arial;font-size:14px;font-weight:normal;padding:10px 5px;border-style:solid;border-width:1px;overflow:hidden;word-break:normal;border-color:#ccc;color:#333;background-color:#f0f0f0;}
                .tg .tg-3wr7{font-weight:bold;font-size:12px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-ti5e{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;;text-align:center}
                .tg .tg-rv4w{font-size:10px;font-family:"Arial", Helvetica, sans-serif !important;}
            </style>

            <div class="footer">
                Page <span class="pagenum"></span>
            </div>
  
            <div style="font-family:Arial; font-size:12px;">
                <h2 style="text-align: center">Data Pembayaran Pendaftaran</h2>
            </div>
            <br>
            <table class="tg">
                <tr>
                    <th class="tg-3wr7">Nama</th>
                    @if ($web_profil->jenjang_sekolah == "SLTA")
                    <th class="tg-3wr7">Jurusan</th>
                    @endif
                    <th class="tg-3wr7">TA</th>
                    <th class="tg-3wr7">Harga</th>
                    <th class="tg-3wr7">Jumlah Bayar</th>
                    <th class="tg-3wr7">Bukti</th>
                    <th class="tg-3wr7">Status</th>
                    <th class="tg-3wr7">Tanggal Bayar</th>
                </tr>
                <?php $no=1 ?>
                @foreach ($bayars as $item)
                <tr>
                    <td>{{ $item->siswa->user->name }}</td>
                    @if ($web_profil->jenjang_sekolah == "SLTA")
                    <td>{{ $item->siswa->jurusanTo->name }}</td>
                    @endif
                    <td>{{ $item->siswa->tahun_ajaran }}</td>
                    @php
                        $harga = 0;
                        foreach ($item->siswa->tagihan as $key => $value) {
                            if ($value['jenis_biaya'] == "Biaya Pendaftaran") {
                                $harga = $value['biaya'];
                            }
                        }
                    @endphp
                    <td>
                        {{rupiah($harga)}}
                    </td>
                    <td>{{rupiah($item->jml_byr + intval($item->tiga_digit_angka))}}</td>
                    <td>
                        <img src="{{public_path('penyimpanan')}}/user/bayars/{{$item->img_bayar}}" alt="Responsive image" height="40px">
                    </td>
                    <td>{{$item->status}}</td>
                    <td>
                        {{Carbon\Carbon::parse($item->created_at)->isoFormat('D MMMM Y')}}
                    </td>
                </tr>
                @endforeach
            </table>
        </body>
    </head>
</html>