@php
    function rupiah($num) {
        return 'Rp. '.number_format(intval($num), 0, ',', '.');
    }
@endphp
<div class="modal-body" style="font-size: 12px;">
    <div class="row">
        <div class="col-5">
            Nama
        </div>
        <div class="col-7">
            {{$bayar->siswa->user->name}}
        </div>
    </div>
    @if ($web_profil->jenjang_sekolah == "SLTA")
    <div class="row">
        <div class="col-5">
            Jurusan
        </div>
        <div class="col-7">
            {{$bayar->siswa->jurusanTo->name}}
        </div>
    </div>
    @endif
    <div class="row">
        <div class="col-5">
            Bank Tujuan
        </div>
        <div class="col-7">
            {{$bayar->rekening->nama_rekening}} No. Rek {{$bayar->rekening->nomor_rekening}} a/n {{$bayar->rekening->atas_nama}}
        </div>
    </div>
    <div class="row">
        <div class="col-5">
            Bank Pengirim
        </div>
        <div class="col-7">
            {{$bayar->nama_bank}}
        </div>
    </div>
    <div class="row">
        <div class="col-5">
            Nomor Rekening
        </div>
        <div class="col-7">
            {{$bayar->nomor_rekening}}
        </div>
    </div>
    <div class="row">
        <div class="col-5">
            Atas Nama
        </div>
        <div class="col-7">
            {{$bayar->atas_nama}}
        </div>
    </div>
    <div class="row">
        <div class="col-5">
            Harga
        </div>
        <div class="col-7">
            @php
                $biaya_pendaftaran = "";
                foreach ($bayar->siswa->tagihan as $key => $value) {
                    if ($value['jenis_biaya'] == "Biaya Pendaftaran") {
                        $biaya_pendaftaran = $value;
                    }
                }
            @endphp
            {{rupiah($biaya_pendaftaran['biaya'])}}
        </div>
    </div>
    <div class="row">
        <div class="col-5">
            Jumlah Bayar
        </div>
        <div class="col-7">
            {{$bayar->jml_byr}}
        </div>
    </div>
    <div class="row">
        <div class="col-5">
            3 Digit Kode Referensi
        </div>
        <div class="col-7">
            {{$bayar->tiga_digit_angka}}
        </div>
    </div>
    <div class="row">
        <div class="col-5">
            Jumlah Transfer
        </div>
        <div class="col-7">
            {{intval($bayar->jml_byr) + intval($bayar->tiga_digit_angka)}}
        </div>
    </div>
    <div class="row">
        <div class="col-5">
            Bukti Pembayaran
        </div>
        <div class="col-7">
            <a href="{{asset('penyimpanan/user/bayars/'.$bayar->img_bayar)}}" target="_blank">Lihat Bukti Pembayaran</a>
        </div>
    </div>
    <div class="row">
        <div class="col-5">
            Tanggal Bayar
        </div>
        <div class="col-7">
            {{Carbon\Carbon::parse($bayar->created_at)->isoFormat('D MMMM Y')}}
        </div>
    </div>
    <div class="row">
        <div class="col-5">
            Catatan User
        </div>
        <div class="col-7">
            {{ $bayar->catatan }}
        </div>
    </div>
    <div class="row">
        <div class="col-5">
            Catatan Admin
        </div>
        <div class="col-7">
            {{ $bayar->keterangan }}
        </div>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Tutup</button>
</div>