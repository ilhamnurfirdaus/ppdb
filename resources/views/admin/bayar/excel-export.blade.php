<table>
    <thead>
    <tr>
        <th>ID Bayar</th>
        <th>Nama</th>
        @if ($web_profil->jenjang_sekolah == "SLTA")
        <th>Jurusan</th>
        @endif
        <th>Tahun Ajaran</th>
        <th>Harga</th>
        <th>Tanggal Bayar</th>
        <th>Bank Tujuan</th>
        <th>Bank</th>
        <th>Nomor Rekening</th>
        <th>Atas Nama</th>
        <th>Jumlah Transfer</th>
        <th>Catatan Dari Pelanggan</th>
        <th>Status Pembayaran</th>
        <th>Pesan Untuk Pelanggan Jika Pembayaran Belum Lunas</th>
    </tr>
    </thead>
    <tbody>
    @foreach ($bayars as $item)
    <tr>
        <td>{{$item->id}}</td>
        <td>{{$item->siswa->user->name}}</td>
        @if ($web_profil->jenjang_sekolah == "SLTA")
        <td>{{$item->siswa->jurusanTo->name}}</td>
        @endif
        <td>{{ $item->siswa->tahun_ajaran }}</td>
        @php
            $harga = 0;
            foreach ($item->siswa->tagihan as $key => $value) {
                if ($value['jenis_biaya'] == "Biaya Pendaftaran") {
                    $harga = $value['biaya'];
                }
            }
        @endphp
        <td>
            {{$harga}}
        </td>
        <td>{{Carbon\Carbon::parse($item->created_at)->isoFormat('D MMMM Y')}}</td>
        <td>{{$item->rekening->nama_rekening}} No. Rek {{$item->rekening->nomor_rekening}} a/n {{$item->rekening->atas_nama}}</td>
        <td>{{$item->nama_bank}}</td>
        <td>{{$item->nomor_rekening}}</td>
        <td>{{$item->atas_nama}}</td>
        <td>{{intval($item->jml_byr) + intval($item->tiga_digit_angka)}}</td>
        <td>{{$item->catatan }}</td>
        <td>{{$item->status}}</td>
        <td></td>
    </tr>
    @endforeach
    </tbody>
</table>