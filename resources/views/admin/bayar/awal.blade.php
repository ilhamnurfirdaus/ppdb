@extends('layouts.admin')
@section('main')
@php
    function rupiah($num) {
        return 'Rp. '.number_format(intval($num), 0, ',', '.');
    }
@endphp
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>Pembayaran Pendaftaran
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
                {{-- <button type="button" class="btn-shadow ml-3 btn btn-xs btn-success" data-toggle="modal" data-target="#createModal">
                    Tambah Pembayaran
                </button> --}}
                <a href="{{url('admin/bayar-awal/export_pdf')}}?tahun_ajaran={{Request::input('tahun_ajaran')}}&status={{Request::input('status')}}" target="blank" id='import' class='btn-shadow ml-3 btn btn-sm btn-danger'><i class='fa fa-upload'></i> Cetak</a>
                <a href="{{url('admin/bayar-awal/export-excel')}}?tahun_ajaran={{Request::input('tahun_ajaran')}}&status={{Request::input('status')}}" target="blank" class='btn-shadow ml-3 btn btn-sm btn-success'><i class='fa fa-upload'></i> Export Pembayaran</a>
            </div>
        </div>
        <div class="page-title-actions">
            
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>

<div class="main-card mb-3 card">
    <div class="card-body table-responsive-sm">
        <form class="form form-inline" method="get">
            <select name="tahun_ajaran" class="form-control form-control-sm select2-biasa" style="width: 130px;">
                @foreach ($tahun_ajarans as $item)
                <option @if(Request::input('tahun_ajaran') != null && Request::input('tahun_ajaran') == $item->tahun_ajaran) selected @elseif($tahun_ajaran->tahun_ajaran == $item->tahun_ajaran) selected @endif value="{{$item->tahun_ajaran}}">{{$item->tahun_ajaran}}</option>
                @endforeach
            </select>
            
            &nbsp;
            <select name="status" class="form-control form-control-sm select2-biasa" style="width: 130px;">
                <option value="">Status </option>
                <option value="Lunas" @if(Request::input('status') == "Lunas") selected @endif>Lunas</option>
                <option value="Belum Lunas" @if(Request::input('status') == "Belum Lunas") selected @endif>Belum Lunas</option>
            </select>

            <button class="ml-2 mr-2 btn btn-sm btn-primary" type="submit">Filter</button>
            <a href="{{url()->current()}}" class="mr-2 btn btn-sm btn-primary">Reset</a>

            @if (Auth::user()->level->name == "Keuangan" || Auth::user()->level->name == "Adnub")
                <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#importModal">
                    Import Status Pembayaran
                </button>
            @endif
        </form>
        <br>
        <table style="width: 100%;" class="table table-hover table-striped table-bordered table-default">
            <thead>
            <tr>
                <th>Nama</th>
                @if ($web_profil->jenjang_sekolah == "SLTA")
                <th>Jurusan</th>
                @endif
                <th>TA</th>
                <th>Harga</th>
                <th>Jumlah Bayar</th>
                <th>Bukti</th>
                <th>
                    Status
                </th>
                <th>Tanggal Bayar</th>
                <th>Aksi</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($bayars as $item)
                <tr>
                    <td>{{ $item->siswa->user->name }}</td>
                    @if ($web_profil->jenjang_sekolah == "SLTA")
                    <td>{{ $item->siswa->jurusanTo->name }}</td>
                    @endif
                    <td>{{ $item->siswa->tahun_ajaran }}</td>
                    @php
                        $harga = 0;
                        foreach ($item->siswa->tagihan as $key => $value) {
                            if ($value['jenis_biaya'] == "Biaya Pendaftaran") {
                                $harga = $value['biaya'];
                            }
                        }
                    @endphp
                    <td>
                        {{rupiah($harga)}}
                    </td>
                    <td>{{rupiah($item->jml_byr + intval($item->tiga_digit_angka))}}</td>
                    <td>
                        <a class="btn_semua_modal" data-toggle="modal" data-target="#semuaModal" data-remote="{{url('bayar/bukti/'.$item->id)}}" data-title="Bukti Pembayaran" style="cursor: pointer;">
                            <img src="{{asset('/penyimpanan/user/bayars/'.$item->img_bayar)}}" alt="Responsive image" height="40px">
                        </a>    
                    </td>
                    <td>{{$item->status}}</td>
                    <td>
                        {{Carbon\Carbon::parse($item->created_at)->isoFormat('D MMMM Y')}}
                    </td>
                    <td>
                        <button type="button" class="btn btn-sm btn-info btn_semua_modal" data-toggle="modal" data-target="#semuaModal" data-remote="{{url('bayar/detil/'.$item->id)}}" data-title="Detil"><i class="fas fa-eye"></i></button>
                        {{-- @if ($item->status == "Masih Proses") --}}
                        {{-- <button type="button" class="btn btn-sm btn-success btn_semua_modal" data-toggle="modal" data-target="#semuaModal" data-remote="{{url('admin/bayar-awal/edit/'.$item->id)}}" data-title="Status Pembayaran"><i class="fas fa-pencil-alt"></i></button> --}}
                        {{-- @endif --}}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('modal')
<!-- Modal -->
{{-- <div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="createModalLabel">Buat Data Baru</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/bayar-awal/store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type" type="text" value="create" hidden>
            <div class="form-group">
                <label>Bank Tujuan *</label>
                <select class="form-control form-control-sm select2-create" aria-label="Default select example" name="rekening_id" style="width: 100%;" required>
                    <option value="">**Pilih tujuan rekening</option>
                    @foreach ($rekenings as $row)
                    <option value="{{$row->id}}" @if($row->id == old("rekening_id")) selected @endif>
                        {{$row->nama_rekening}} No. Rek {{$row->nomor_rekening}} a/n {{$row->atas_nama}}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Bank Pengirim *</label>
                <select class="form-control form-control-sm select2-create" aria-label="Default select example" name="nama_bank" style="width: 100%;" required>
                    <option value="">**Pilih rekening bank anda</option>
                    @foreach ($banks as $row)
                    <option value="{{$row->nama_bank}}" @if($row->nama_bank == old("nama_bank")) selected @endif>
                        {{$row->nama_bank}}
                    </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <label>Nomor Rekening *</label>
                <input id="nomor_rekening" type="number" class="form-control form-control-sm" name="nomor_rekening" placeholder="Isi nomor rekening anda" value="{{old('nomor_rekening')}}" required>                
            </div>
            <div class="form-group">
                <label>Atas Nama *</label>
                <input id="atas_nama" type="text" class="form-control form-control-sm" name="atas_nama" value="{{old('atas_nama')}}" required>
            </div>
            <div class="form-group">
                <label>Jumlah Bayar</label>
                <input name="jml_byr" type="number" class="jml_byr form-control form-control-sm" value="{{ old('jml_byr') ? old('jml_byr') : Auth::user()->siswa->jurusanTo->biayaAwal->biaya }}" required>
            </div>
            <div class="form-group">
                <label>3 Digit Kode Referensi *</label>
                <input type="number" class="form-control form-control-sm tiga_digit_angka" name="tiga_digit_angka" value="{{old('tiga_digit_angka')}}" maxlength="3" required>
            </div>
            <div class="form-group">
                <label>Jumlah Transfer</label>
                <input type="number" class="form-control form-control-sm jumlah_transfer" value="" disabled>
            </div>
            <div class="form-group">
                <label>Upload Bukti Pembayaran *</label>
                <div class="custom-file">
                    <input name="img_bayar"  type="file" class="custom-file-input" id="exampleInputFile" required>
                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                </div>
            </div>
            <div class="form-group">
                <label>Tanggal Bayar *</label>
                <input type="date" class="form-control form-control-sm" value="{{Carbon\Carbon::parse(old('tgl_bayar'))->format('Y-m-d')}}" name="tgl_bayar" required>
            </div>
            <div class="form-group">
                <label>Catatan</label>
                <textarea class="form-control form-control-sm" name="catatan" placeholder="Isi catatan pembayaran, tidak wajib diisi" rows="3">{{old('catatan')}}</textarea>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div> --}}

<!-- Import Modal -->
<div class="modal fade" id="importModal" tabindex="-1" role="dialog" aria-labelledby="importModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h6 class="modal-title" id="importModalLabel">Import Pembayaran</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <form action="{{url('admin/bayar-awal/import-excel')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <div class="form-group">
                <label>Update Status Pembayaran lewat import excel</label>
                <input type="file" name="file" accept=".csv, .xls, .xlsx"/>
            </div>
            <hr>
            {{-- File format yang didukung: xls, xlsx. <a href="{{url('admin/bayar-awal/export-excel')}}">Klik disini</a> unduh contoh format file xls import.  <br> --}}
            <strong>Perhatian =</strong> <br>
            1. Mohon import data pembayaran yang belum pernah diimport sebelumnya. <br>
            2. Mohon tetap menggunakan header kolom sesuai format sampel (contoh) <br>
            3. Ukuran file maksimal <b>2 MB</b> <br>
            4. Pilih salah satu opsi Status Pembayaran antara lain "Belum Lunas", "Masih Proses", "Lunas". <br>
            5. Jika pilih opsi "Belum Lunas", ketik kekurangan dalam pembayaran di kolom "Pesan Untuk Pelanggan Jika Pembayaran Belum Lunas". <br>
            6. Jangan merubah status pembayaran jika tidak ingin merubah apapun.
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-sm btn-secondary" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-sm btn-success">Simpan</button>
        </div>
        </form>
    </div>
    </div>
</div>

{{-- @foreach ($bayars as $item)
<!-- Image Modal -->
<div class="modal fade" id="imageModal-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="imageModal-{{ $item->id }}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h6 class="modal-title" id="imageModal-{{ $item->id }}Label">Bukti Pembayaran</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body" style="font-size: 12px;">
            <img class="mx-auto d-block img-fluid" src="{{asset('/penyimpanan/user/bayars/'.$item->img_bayar)}}" alt="Responsive image">
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="detilModal-{{ $item->id }}" role="dialog" aria-labelledby="detilModal-{{ $item->id }}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="detilModal-{{ $item->id }}Label">Detil Pembayaran</h6>
            <button type="button" class="btn close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" style="font-size: 12px;">
            <div class="row">
                <div class="col-5">
                    Nama
                </div>
                <div class="col-7">
                    {{$item->siswa->user->name}}
                </div>
            </div>
            @if ($web_profil->jenjang_sekolah == "SLTA")
            <div class="row">
                <div class="col-5">
                    Jurusan
                </div>
                <div class="col-7">
                    {{$item->siswa->jurusanTo->name}}
                </div>
            </div>
            @endif
            <div class="row">
                <div class="col-5">
                    Bank Tujuan
                </div>
                <div class="col-7">
                    {{$item->rekening->nama_rekening}} No. Rek {{$item->rekening->nomor_rekening}} a/n {{$item->rekening->atas_nama}}
                </div>
            </div>
            <div class="row">
                <div class="col-5">
                    Bank Pengirim
                </div>
                <div class="col-7">
                    {{$item->nama_bank}}
                </div>
            </div>
            <div class="row">
                <div class="col-5">
                    Nomor Rekening
                </div>
                <div class="col-7">
                    {{$item->nomor_rekening}}
                </div>
            </div>
            <div class="row">
                <div class="col-5">
                    Atas Nama
                </div>
                <div class="col-7">
                    {{$item->atas_nama}}
                </div>
            </div>
            <div class="row">
                <div class="col-5">
                    Harga
                </div>
                <div class="col-7">
                @php
                    $harga = 0;
                    foreach ($item->siswa->tagihan as $key => $value) {
                        if ($value['jenis_biaya'] == "Biaya Pendaftaran") {
                            $harga = $value['biaya'];
                        }
                    }
                @endphp
                    {{ $harga}}
                </div>
            </div>
            <div class="row">
                <div class="col-5">
                    Jumlah Bayar
                </div>
                <div class="col-7">
                    {{$item->jml_byr}}
                </div>
            </div>
            <div class="row">
                <div class="col-5">
                    3 Digit Kode Referensi
                </div>
                <div class="col-7">
                    {{$item->tiga_digit_angka}}
                </div>
            </div>
            <div class="row">
                <div class="col-5">
                    Jumlah Transfer
                </div>
                <div class="col-7">
                    {{intval($item->jml_byr) + intval($item->tiga_digit_angka)}}
                </div>
            </div>
            <div class="row">
                <div class="col-5">
                    Bukti Pembayaran
                </div>
                <div class="col-7">
                    <a href="{{asset('penyimpanan/user/bayars/'.$item->img_bayar)}}" target="_blank">Lihat Bukti Pembayaran</a>
                </div>
            </div>
            <div class="row">
                <div class="col-5">
                    Tanggal Bayar
                </div>
                <div class="col-7">
                    {{Carbon\Carbon::parse($item->created_at)->isoFormat('D MMMM Y')}}
                </div>
            </div>
            <div class="row">
                <div class="col-5">
                    Catatan User
                </div>
                <div class="col-7">
                    {{ $item->catatan }}
                </div>
            </div>
            <div class="row">
                <div class="col-5">
                    Catatan Admin
                </div>
                <div class="col-7">
                    {{ $item->keterangan }}
                </div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal">Tutup</button>
        </div>
        </div>
    </div>
</div>

<div class="modal fade" id="updateModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="updateModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="updateModal-{{$item->id}}Label">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('admin/bayar-awal/update/'.$item->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type-{{$item->id}}" type="text" value="update" hidden>
            
            <div class="form-group">
                <label>Status Pembayaran</label>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="status" value="Lunas" data-id="{{ $item->id }}" @if ($item->status == "Lunas") checked @endif>
                    <label class="form-check-label">Lunas</label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="status" value="Belum Lunas" data-id="{{ $item->id }}" @if ($item->status == "Belum Lunas") checked @endif required>
                    <label class="form-check-label">Belum Lunas</label>
                </div>
            </div>

            <div id="form_bayar_notifikasi-{{ $item->id }}" class="form_bayar_notifikasi form-group" @if ($item->status != "Belum Lunas") style="display: none;" @endif>
                <label>Pesan Notifikasi</label>
                <textarea id="bayar_notifikasi-{{ $item->id }}" name="keterangan" class="bayar_notifikasi form-control" rows="3" @if ($item->status == "Belum Lunas") required @endif>{{ $item->keterangan }}</textarea>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div> --}}

{{-- <div class="modal fade" id="deleteModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="deleteModal-{{$item->id}}Label">Hapus Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Apakah anda yakin untuk menghapus data ini? 
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <a href="{{url('admin/bayar-awal/destroy/'.$item->id)}}" class="btn btn-danger btn-sm">Hapus</a>
        </div>
        </div>
    </div>
</div> --}}

{{-- <script>
    $(document).ready(function () {
        if ('{{old("type-".$item->id)}}' == 'update') {
            $(document).ready(function () {
                $("#updateModal-{{$item->id}}").modal('show');
            });
        }

        $('.select2-update-{{$item->id}}').select2({
            placeholder: "**Silahkan Pilih",
            dropdownParent: $("#updateModal-{{$item->id}}")
        });
    });
</script>
@endforeach --}}
@endsection
@section('script')
<script>
    $(document).ready(function () {
        $(".tiga_digit_angka").on('change', function(){
            let ob = $(this);
            console.log(ob.val());
            let tiga_digit_angka = parseInt(ob.val());
            let jml_byr = parseInt(ob.parent().parent().find('.jml_byr').val());
            ob.parent().parent().find('.jumlah_transfer').val(tiga_digit_angka + jml_byr);
        });

        $(".jml_byr").on('change', function(){
            let ob = $(this);
            let tiga_digit_angka = parseInt(ob.val());
            let jml_byr = parseInt(ob.parent().parent().find('.tiga_digit_angka').val());
            ob.parent().parent().find('.jumlah_transfer').val(tiga_digit_angka + jml_byr);
        });
    });
</script>
@endsection