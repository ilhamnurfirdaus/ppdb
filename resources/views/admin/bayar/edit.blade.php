<form action="{{url('admin/bayar-awal/update/'.$bayar->id)}}" method="POST" enctype="multipart/form-data">
@csrf
<div class="modal-body">
    <div class="form-group">
        <label>Status Pembayaran</label>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="status" value="Lunas" data-id="{{ $bayar->id }}" @if ($bayar->status == "Lunas") checked @endif>
            <label class="form-check-label">Lunas</label>
        </div>
        <div class="form-check">
            <input class="form-check-input" type="radio" name="status" value="Belum Lunas" data-id="{{ $bayar->id }}" @if ($bayar->status == "Belum Lunas") checked @endif required>
            <label class="form-check-label">Belum Lunas</label>
        </div>
    </div>

    <div id="form_bayar_notifikasi-{{ $bayar->id }}" class="form_bayar_notifikasi form-group" @if ($bayar->status != "Belum Lunas") style="display: none;" @endif>
        <label>Pesan Notifikasi</label>
        <textarea id="bayar_notifikasi-{{ $bayar->id }}" name="keterangan" class="bayar_notifikasi form-control" rows="3" @if ($bayar->status == "Belum Lunas") required @endif>{{ $bayar->keterangan }}</textarea>
    </div>
</div>
<div class="modal-footer">
    <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
    <button type="submit" class="btn btn-success btn-sm">Simpan</button>
</div>
</form>

<script>
    $(document).ready(function () {
        $('input[type=radio][name=status]').change(function() {
            let ob = $(this);
            let id = ob.data("id");
            console.log(id);
            let val = ob.val();
            console.log(val);

            if (val == "Belum Lunas") {
                $(this).parent().parent().parent().parent().find(".form_bayar_notifikasi").css("display", "block");
                $(this).parent().parent().parent().parent().find(".bayar_notifikasi").attr('required', true);

                // $(this).css("display", "block");
                // $("#form_bayar_notifikasi-"+id).css("display", "block");
                // $("#bayar_notifikasi-"+id).find("input").attr('required',true);
            } else {
                $(this).parent().parent().parent().parent().find(".form_bayar_notifikasi").css("display", "none");
                $(this).parent().parent().parent().parent().find(".bayar_notifikasi").attr('required', false);
                $(this).parent().parent().parent().parent().find(".bayar_notifikasi").val(null);

                // $("#form_bayar_notifikasi-"+id).css("display", "none");
                // $("#bayar_notifikasi-"+id).find("input").attr('required',false);
            }
        });
    });
</script>