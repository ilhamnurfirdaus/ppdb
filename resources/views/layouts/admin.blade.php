
<!doctype html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta http-equiv="Content-Language" content="en">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Aplikasi Penerimaan Siswa.</title>
    <meta name="viewport"
        content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no, shrink-to-fit=no" />
    <meta name="description" content="Choose between regular React Bootstrap tables or advanced dynamic ones.">

    <!-- Disable tap highlight on IE -->
    <meta name="msapplication-tap-highlight" content="no">

    <link href="{{asset('templates/admin/main.css')}}" rel="stylesheet">
    <!-- DataTables -->
    <link rel="stylesheet" href="{{asset('templates/adminlte/assets/plugins/datatables-bs4/css/dataTables.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('templates/adminlte/assets/plugins/datatables-responsive/css/responsive.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{asset('templates/adminlte/assets/plugins/datatables-buttons/css/buttons.bootstrap4.min.css')}}">
    <link rel="stylesheet" href="{{ asset('templates/adminlte/assets/plugins/fontawesome-free/css/all.min.css') }}">    
    <!-- Select2 -->
    <link rel="stylesheet" href="{{asset('templates/adminlte/assets')}}/plugins/select2/css/select2.min.css">
    <link rel="stylesheet" href="{{asset('templates/adminlte/assets')}}/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
    <!-- summernote -->
    <link rel="stylesheet" href="{{ asset('templates/adminlte/assets/plugins/summernote/summernote-lite.min.css') }}">
    <link rel="stylesheet" href="{{ asset('templates/adminlte/assets/plugins/dropify/dropify.min.css    ') }}">
    {{-- <style>
        .app-header.header-text-light .app-header__logo .logo-src {
            background: url("{{ asset('templates/landing') }}/images/logo-header.png");
        }
    </style> --}}
    {{-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.25/css/jquery.dataTables.min.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/fixedheader/3.1.9/css/fixedHeader.dataTables.min.css"> --}}
    @php
        $pengumumans_template = App\Models\Pengumuman::select('*')->where('status', 'Aktif')->get();

        $web_profils_template = App\Models\WebProfil::all();
        $web_profil_template = App\Models\WebProfil::find($web_profils_template[0]->id);

        $now_template = Carbon\Carbon::now()->isoFormat('Y')."-".Carbon\Carbon::now()->addYear()->isoFormat('Y');
        $tahun_ajaran_template = null;
        if (App\Models\TahunAjaran::where("tahun_ajaran", $now_template)->get()->count() > 0) {
            $tahun_ajaran_template = App\Models\TahunAjaran::find($now_template);
        }

        if (Auth::user()->jenis == "NonAdmin") {
            $tahun_ajaran_template = App\Models\TahunAjaran::find(Auth::user()->siswa->tahun_ajaran);
        }
    @endphp
    <style>
        @media only screen and (min-width: 1250px) {
            
        }
        .modal-footer{
            padding: 10px;
        }
        .app-page-title{
            padding: 15px 30px 15px 30px;
        }

        .card-header{
            background: <?= $web_profil_template->warna_header ?> !important;
            color: rgba(13,27,62,0.7) !important;;
        }
    </style>
<body>
    <div class="app-container app-theme-white body-tabs-shadow fixed-header fixed-sidebar">
        <div class="app-header header-shadow bg-dark header-text-light">
            <div class="app-header__logo">
                {{-- <div class="logo-src"></div> --}}
                <a href="#" class="text-light" style="width: 150px; overflow: hidden;">App Penerimaan Siswa</a>
                {{-- <img src="{{asset('templates/logo/logoinst.jpg')}}" width="35px" height="35px" alt="">
                <label for="" style="margin-left: 10px">{{$hf->bentukinst}} {{$hf->namainst}}</label> --}}
                <div class="header__pane ml-auto">
                    <div>
                        <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
            </div>
            <div class="app-header__mobile-menu">
                <div>
                    <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
            </div>
            <div class="app-header__menu">
                <span>
                    <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                        <span class="btn-icon-wrapper">
                            <i class="fa fa-ellipsis-v fa-w-6"></i>
                        </span>
                    </button>
                </span>
            </div>    
            <div class="app-header__content">
                <marquee class="app-header-left" width="65%" style="margin-top: 12px; margin-left: 8px;"><p></p></marquee>
                <div class="app-header-right">
                    <div class="dropdown">
                        <a type="button" aria-haspopup="true" aria-expanded="false" data-toggle="dropdown"
                        class="p-0 mr-2 btn btn-link">
                            <span class="icon-wrapper icon-wrapper rounded-circle">
                                {{-- <span class="icon-wrapper-bg bg-warning"></span> --}}
                                <i class="icon text-danger icon-anim-pulse ion-android-notifications"></i>
                                @if (Auth::user()->unreadNotifications->count() > 0)
                                <span class="badge badge-warning text-white" style="
                                font-size: .6rem;
                                font-weight: 300;
                                padding: 2px 4px;
                                position: absolute;
                                right: 5px;
                                top: 9px;">{{ Auth::user()->unreadNotifications->count() }}</span>
                                @endif
                            </span>
                        </a>
                        <div tabindex="-1" role="menu" aria-hidden="true" 
                        class="dropdown-menu-xl rm-pointers dropdown-menu dropdown-menu-right">
                        <div class="scroll-area-sm">
                            <div class="scrollbar-container">
                                <div class="p-3">
                                    <div class="vertical-time-simple vertical-without-time vertical-timeline vertical-timeline--one-column">
                                        @foreach (Auth::user()->notifications()->take(8)->get() as $item)
                                        <a href="{{ url($item->data['url_redirect']) }}" style="text-decoration: none;">
                                            <div class="vertical-timeline-item dot-warning vertical-timeline-element">
                                                <div><span class="vertical-timeline-element-icon bounce-in"></span>
                                                    <div class="vertical-timeline-element-content bounce-in">
                                                        <h4 class="timeline-title">{{ $item->data['title'] }}</h4> 
                                                        {!! $item->data['description'] !!}, at <span class="text-success">{{ \Carbon\Carbon::parse($item->created_at)->diffForHumans() }}</span>
                                                        <span class="vertical-timeline-element-date"></span>
                                                        <p>@if (empty($item->read_at))<span class="badge badge-success">NEW</span> @endif <span class="text-success">{{Carbon\Carbon::parse($item->created_at)->isoFormat('D MMMM Y')}}</span></p>
                                                    </div>
                                                </div>
                                            </div>    
                                        </a>
                                        @endforeach
                                        {{-- @if (Auth::user()->jenis == "NonAdmin")
                                        @forelse ($pengumumans_template as $row)
                                        <div class="vertical-timeline-item dot-warning vertical-timeline-element">
                                            <div><span class="vertical-timeline-element-icon bounce-in"></span>
                                                <div class="vertical-timeline-element-content bounce-in">
                                                    <h4 class="timeline-title">{{ $row->judul }} @if (Auth::user()->cek_pengumuman == "1") <span class="badge badge-success">NEW</span> @endif <span class="text-success">{{Carbon\Carbon::parse($row->created_at)->isoFormat('D MMMM Y')}}</span></h4> 
                                                    {!! $row->deskripsi !!}, at <span class="text-success">{{ \Carbon\Carbon::parse($row->created_at)->diffForHumans() }}</span>
                                                    <span class="vertical-timeline-element-date"></span>
                                                </div>
                                            </div>
                                        </div>
                                        @empty
                                        <div class="vertical-timeline-item dot-warning vertical-timeline-element">
                                            <div><span class="vertical-timeline-element-icon bounce-in"></span>
                                                <div class="vertical-timeline-element-content bounce-in">
                                                    <h4 class="timeline-title">Tidak ada Data</h4>
                                                </div>
                                            </div>
                                        </div>
                                        @endforelse
                                        @else
                                        <div class="vertical-timeline-item dot-warning vertical-timeline-element">
                                            <div><span class="vertical-timeline-element-icon bounce-in"></span>
                                                <div class="vertical-timeline-element-content bounce-in">
                                                    <h4 class="timeline-title">Tidak ada Data</h4>
                                                </div>
                                            </div>
                                        </div>
                                        @endif --}}
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        
                    </div>

                    <div class="header-btn-lg pr-0">
                        <div class="widget-content p-0">
                            <div class="widget-content-wrapper">
                                <div class="widget-content-left">
                                    <div class="btn-group">
                                        <a data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" class="p-0 btn">
                                            <img width="42" height="42" class="rounded-circle" src="@if(isset(Auth::user()->foto)) {{asset('profil')}}/{{Auth::user()->foto}} @else {{asset('templates/admin/images/avatars/1.jpg')}} @endif" alt="">
                                            <i class="fa fa-angle-down ml-2 opacity-8"></i>
                                        </a>
                                        <div tabindex="-1" role="menu" aria-hidden="true" class="dropdown-menu dropdown-menu-right">
                                            {{-- <button type="button" tabindex="0" class="dropdown-item">Profil</button> --}}
                                            <a class="dropdown-item" href="{{ url('profile') }}">Profil</a>
                                            <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                            document.getElementById('logout-form').submit();">
                                                {{ __('Logout') }}
                                            </a>
                                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                                @csrf
                                            </form>
                                        </div>
                                    </div>
                                </div>
                                <div class="widget-content-left  ml-3 header-user-info">
                                    <div class="widget-heading">
                                        {{ Auth::user()->name }}
                                    </div>
                                    <div class="widget-subheading">
                                        @if (Auth::user()->jenis == "Admin")
                                        {{ Auth::user()->level->name}}
                                        @else
                                        Siswa
                                        @endif
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>        
                </div>
            </div>
        </div>        
        <div class="ui-theme-settings">
            
        </div>        
        <div class="app-main">
            <div class="app-sidebar sidebar-shadow bg-dark sidebar-text-light">
                <div class="app-header__logo">
                    {{-- <div class="logo-src"></div> --}}
                    <div class="header__pane ml-auto">
                        <div>
                            <button type="button" class="hamburger close-sidebar-btn hamburger--elastic" data-class="closed-sidebar">
                                <span class="hamburger-box">
                                    <span class="hamburger-inner"></span>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
                <div class="app-header__mobile-menu">
                    <div>
                        <button type="button" class="hamburger hamburger--elastic mobile-toggle-nav">
                            <span class="hamburger-box">
                                <span class="hamburger-inner"></span>
                            </span>
                        </button>
                    </div>
                </div>
                <div class="app-header__menu">
                    <span>
                        <button type="button" class="btn-icon btn-icon-only btn btn-primary btn-sm mobile-toggle-header-nav">
                            <span class="btn-icon-wrapper">
                                <i class="fa fa-ellipsis-v fa-w-6"></i>
                            </span>
                        </button>
                    </span>
                </div>    <div class="scrollbar-sidebar">
                    <div class="app-sidebar__inner">
                        <ul class="vertical-nav-menu">
                            @if (Auth::user()->jenis == 'Admin')
                            <li class="app-sidebar__heading">Navigasi</li>
                            <li>
                                <a href="{{url('admin')}}" class="{{Request::is('admin') ? 'mm-active' : '' }}">
                                    <i class="metismenu-icon pe-7s-home"></i>Dashboard
                                </a>
                            </li>
                            @if (Auth::user()->level->name == 'Admin')
                            <li class="{{Request::is('admin/konten-management/edit-web-profil', 'admin/rekening') ? 'mm-active' : '' }}">
                                <a href="#">
                                    <i class="metismenu-icon pe-7s-config"></i>Seting Data Sekolah
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <li>
                                        <a href="{{ url('admin/konten-management/edit-web-profil') }}" class="{{Request::is('admin/konten-management/edit-web-profil') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Profil Sekolah
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('admin/rekening') }}" class="{{Request::is('admin/rekening') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Rekening Sekolah
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="{{Request::is('admin/tahun-ajaran', 'admin/jurusan', 'admin/setting-biaya', 'admin/pendaftaran') ? 'mm-active' : '' }}">
                                <a href="#">
                                    <i class="metismenu-icon pe-7s-config"></i>Master PPDB
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <li>
                                        <a href="{{ url('admin/tahun-ajaran') }}" class="{{Request::is('admin/tahun-ajaran') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Seting Gelombang
                                        </a>
                                    </li>
                                    @if ($web_profil_template->jenjang_sekolah == "SLTA")
                                    <li>
                                        <a href="{{ url('admin/jurusan') }}" class="{{Request::is('admin/jurusan') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Seting Jurusan
                                        </a>
                                    </li>
                                    @endif
                                    <li>
                                        <a href="{{ url('admin/setting-biaya') }}" class="{{Request::is('admin/setting-biaya') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Seting Biaya
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('admin/pendaftaran') }}" class="{{Request::is('admin/pendaftaran') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Seting Pendaftaran
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            <li class="{{Request::is('admin/konten-management/edit-banner', 'admin/konten-management/edit-informasi-pendaftaran', 'admin/alur-pendaftaran', 'admin/syarat-pendaftaran', 'admin/pengumuman') ? 'mm-active' : '' }}">
                                <a href="#">
                                    <i class="metismenu-icon pe-7s-global"></i>Pengelolaan Info PPDB
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <li>
                                        <a href="{{ url('admin/konten-management/edit-banner') }}" class="{{Request::is('admin/konten-management/edit-banner') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Banner
                                        </a>
                                    </li>
                                    {{-- <li>
                                        <a href="{{ url('admin/konten-management/edit-informasi-pendaftaran') }}" class="{{Request::is('admin/konten-management/edit-informasi-pendaftaran') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Informasi Pendaftaran
                                        </a>
                                    </li>  --}}
                                    <li>
                                        <a href="{{ url('admin/alur-pendaftaran') }}" class="{{Request::is('admin/alur-pendaftaran') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Alur Pendaftaran
                                        </a>
                                    </li>
                                    {{-- <li>
                                        <a href="{{ url('admin/syarat-pendaftaran') }}" class="{{Request::is('admin/syarat-pendaftaran') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Syarat Pendaftaran
                                        </a>
                                    </li> --}}
                                    <li>
                                        <a href="{{ url('admin/pengumuman') }}" class="{{Request::is('admin/pengumuman') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Seting Pengumuman
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            @endif
                            @if (Auth::user()->level->name == 'Admin' || Auth::user()->level->name == 'Kepala Sekolah')
                            {{-- Hidden Sementara 24/11/2022 --}}
                            {{-- <li class="{{Request::is('admin/berkas/awal', 'admin/kepsek') ? 'mm-active' : '' }}">
                                <a href="#">
                                    <i class="metismenu-icon pe-7s-tools"></i>Pengelolaan PPDB
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <li>
                                        <a href="{{ url('admin/berkas/awal') }}" class="{{Request::is('admin/berkas/awal', 'admin/berkas/awal/*') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> List Pendaftar
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('admin/kepsek')}}" class="{{Request::is('admin/kepsek') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Poin Kepsek
                                        </a>
                                    </li>
                                </ul>
                            </li> --}}
                            {{-- Hidden Sementara 24/11/2022 --}}
                            <li>
                                <a href="{{ url('admin/berkas/awal') }}" class="{{Request::is('admin/berkas/awal') ? 'mm-active' : '' }}">
                                    <i class="metismenu-icon pe-7s-graph1"></i>Laporan Pendaftaran
                                </a>
                            </li>
                            @endif
                            <li>
                                <a href="{{ url('admin/bayar-awal') }}" class="{{Request::is('admin/bayar-awal') ? 'mm-active' : '' }}">
                                    <i class="metismenu-icon pe-7s-cash"></i>Pembayaran Pendaftaran
                                </a>
                            </li>
                            @if (Auth::user()->level->name == 'Admin')
                            <li class="{{Request::is('admin/jabatan', 'admin/pegawai', 'admin/level', 'admin/user') ? 'mm-active' : '' }}">
                                <a href="#">
                                    <i class="metismenu-icon pe-7s-id"></i>Pengelolaan User
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <li>
                                        <a href="{{ url('admin/jabatan') }}" class="{{Request::is('admin/jabatan') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Master Jabatan
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('admin/pegawai') }}" class="{{Request::is('admin/pegawai') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Master Pegawai
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('admin/level') }}" class="{{Request::is('admin/level') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Master Level
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('admin/user') }}" class="{{Request::is('admin/user') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Master User
                                        </a>
                                    </li>
                                </ul>
                            </li>
                            @endif
                            {{-- <li class="app-sidebar__heading">Konten Managemen</li>
                            <li>
                                <a href="{{url('siswa')}}" class="{{Request::is('siswa') ? 'mm-active' : '' }}">
                                    <i class="metismenu-icon pe-7s-home"></i>Dashboard
                                </a>
                            </li> --}}
                            @else 
                            <li class="app-sidebar__heading">Main Menu</li>
                            <li>
                                <a href="{{url('siswa')}}" class="{{Request::is('siswa') ? 'mm-active' : '' }}">
                                    <i class="metismenu-icon pe-7s-home"></i>Dashboard
                                </a>
                            </li>
                            <li class="{{Request::is('siswa/berkas/awal', 'siswa/daftar/*', 'siswa/berkas/akhir') ? 'mm-active' : '' }}">
                                <a href="#">
                                    <i class="metismenu-icon pe-7s-diskette"></i>Pendaftaran
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <li>
                                        <a href="{{ url('siswa/daftar/awal') }}" class="{{Request::is('siswa/daftar/awal') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Form Pendaftaran
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{url('siswa/berkas/awal')}}" class="{{Request::is('siswa/berkas/awal') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Berkas Daftar
                                        </a>
                                    </li>
                                    {{-- <li>
                                        <a href="{{ url('siswa/daftar/data-diri') }}" class="{{Request::is('siswa/daftar/data-diri', 'siswa/daftar/data-alamat', 'siswa/daftar/data-ortu') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Form Registrasi
                                        </a>
                                    </li> --}}
                                    {{-- <li>
                                        <a href="{{ url('siswa/daftar/data-alamat') }}" class="{{Request::is('siswa/daftar/data-alamat') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Daftar Alamat
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('siswa/daftar/data-ortu') }}" class="{{Request::is('siswa/daftar/data-ortu') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Daftar Ortu
                                        </a>
                                    </li> --}}
                                    {{-- <li>
                                        <a href="{{url('siswa/berkas/akhir')}}" class="{{Request::is('siswa/berkas/akhir') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Berkas Registrasi
                                        </a>
                                    </li> --}}
                                </ul>
                            </li>
                            {{-- Hidden Sementara 24/11/2022 --}}
                            {{-- @if (isset($tahun_ajaran_template) && $tahun_ajaran_template->tampil_test == "Ya")
                            <li>
                                <a href="{{url('siswa/soal')}}" class="{{Request::is('siswa/soal') ? 'mm-active' : '' }}">
                                    <i class="metismenu-icon pe-7s-note2"></i>Soal
                                </a>
                            </li>
                            @endif --}}
                            {{-- Hidden Sementara 24/11/2022 --}}
                            {{-- <li class="{{Request::is('siswa/bayar-awal', 'siswa/bayar-akhir') ? 'mm-active' : '' }}">
                                <a href="#">
                                    <i class="metismenu-icon pe-7s-cash"></i>Pembayaran
                                    <i class="metismenu-state-icon pe-7s-angle-down caret-left"></i>
                                </a>
                                <ul>
                                    <li>
                                        <a href="{{ url('siswa/bayar-awal') }}" class="{{Request::is('siswa/bayar-awal') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Pendaftaran
                                        </a>
                                    </li>
                                    <li>
                                        <a href="{{ url('siswa/bayar-akhir') }}" class="{{Request::is('siswa/bayar-akhir') ? 'mm-active' : '' }}">
                                            <i class="mr-2 far fa-circle"></i> Daftar Ulang
                                        </a>
                                    </li>
                                </ul>
                            </li> --}}
                            <li>
                                <a href="{{ url('siswa/bayar-awal') }}" class="{{Request::is('siswa/bayar-awal') ? 'mm-active' : '' }}">
                                    <i class="metismenu-icon pe-7s-cash"></i> Pembayaran Pendaftaran
                                </a>
                            </li>
                            {{-- Hidden Sementara 24/11/2022 --}}
                            {{-- <li>
                                <a href="{{url('siswa/seleksi')}}" class="{{Request::is('siswa/seleksi') ? 'mm-active' : '' }}">
                                    <i class="metismenu-icon pe-7s-browser"></i>Hasil Seleksi
                                </a>
                            </li> --}}
                            {{-- Hidden Sementara 24/11/2022 --}}
                            <li>
                                <a href="{{url('siswa/pengumuman')}}" class="{{Request::is('siswa/pengumuman') ? 'mm-active' : '' }}">
                                    <i class="metismenu-icon pe-7s-paperclip"></i>Pengumuman 
                                    
                                    {{-- <span class="badge badge-danger">0</span> --}}
                                    
                                </a>
                            </li>
                            @endif
                        </ul>
                    </div>
                </div>
            </div>    
            <div class="app-main__outer">
                <div class="app-main__inner">
                    @yield('main')
                </div>
                <div class="app-wrapper-footer">
                    <div class="app-footer">
                        <div class="app-footer__inner">
                            <div class="app-footer-left">
                                <ul class="nav">
                                    <li class="nav-item">
                                        <a href="javascript:void(0);" class="nav-link">
                                            {{-- Yayaysan {{$hf->namainst}} --}}
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="javascript:void(0);" class="nav-link">
                                            {{-- Footer Link 2 --}}
                                        </a>
                                    </li>
                                </ul>
                            </div>
                            <div class="app-footer-right">
                                <ul class="nav">
                                    <li class="nav-item">
                                        <a href="javascript:void(0);" class="nav-link">
                                            {{-- Footer Link 3 --}}
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="javascript:void(0);" class="nav-link">
                                            <div class="badge badge-success mr-1 ml-0">
                                                {{-- <small>NEW</small> --}}
                                            </div>
                                            App Penerimaan Siswa
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="app-drawer-wrapper">
    </div>
    <div class="app-drawer-overlay d-none animated fadeIn"></div>
    <div class="modal fade" id="updateModal" tabindex="-1" role="dialog" aria-labelledby="updateModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="updateModalLabel">Update Data</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="model_content_update">
    
            </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="pdfModal" tabindex="-1" role="dialog" aria-labelledby="pdfModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="pdfModalLabel">Cetak Invoice 
                    <a href="#" target="_blank" class="btn btn-danger btn-sm">Download Invoice</a>
                </h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="model_content_pdf">
    
            </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="semuaModal" tabindex="-1" role="dialog" aria-labelledby="semuaModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="semuaModalLabel">Modal</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="model_content_semua">
    
            </div>
            </div>
        </div>
    </div>

    <div class="modal fade" id="gambarModal" tabindex="-1" role="dialog" aria-labelledby="gambarModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="gambarModalLabel">Lihat Gambar</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div id="model_content_gambar">
    
            </div>
            </div>
        </div>
    </div>
    
    <div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="deleteModalLabel">Hapus Data</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                Apakah anda yakin untuk menghapus data ini? 
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
                <a id="btn_hapus" href="#" class="btn btn-danger btn-sm">Hapus</a>
            </div>
            </div>
        </div>
    </div>

    
    <script type="text/javascript" src="{{asset('templates/admin/main.js')}}"></script>
    <script type="text/javascript" src="{{asset('templates/adminlte/assets/plugins/jquery/jquery.min.js')}}"></script>
    <!-- Bootstrap 4 -->
    {{-- <script src="{{ asset('templates/adminlte/assets') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script> --}}
    <!-- DataTables  & Plugins -->
    <script src="{{asset('templates/adminlte/assets/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('templates/adminlte/assets/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('templates/adminlte/assets/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('templates/adminlte/assets/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('templates/adminlte/assets/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('templates/adminlte/assets/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('templates/adminlte/assets/plugins/jszip/jszip.min.js')}}"></script>
    <script src="{{asset('templates/adminlte/assets/plugins/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{asset('templates/adminlte/assets/plugins/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{asset('templates/adminlte/assets/plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('templates/adminlte/assets/plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('templates/adminlte/assets/plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
    {{-- <script src="https://cdn.datatables.net/responsive/2.2.9/js/dataTables.responsive.min.js"></script> --}}
    {{-- <script src="https://cdn.datatables.net/fixedheader/3.1.9/js/dataTables.fixedHeader.min.js"></script> --}}
    <!-- Select2 -->
    <script src="{{asset('templates/adminlte/assets')}}/plugins/select2/js/select2.full.min.js"></script>
    <!-- Summernote -->
    <script src="{{ asset('templates/adminlte/assets/plugins/summernote/summernote-lite.min.js') }}"></script>
    <script src="{{asset('templates')}}/jquery.countdown-2.1.0/jquery.countdown.js"></script>
    <script src="{{ asset('templates/adminlte/assets/plugins/dropify/dropify.min.js') }}"></script>

    <!--BOOTSTRAP DATEPICKER-->
    <script src="{{ asset('templates/admin') }}/datepicker/bootstrap-datepicker.js"></script>
    <link rel="stylesheet" href="{{ asset('templates/admin') }}/datepicker/bootstrap-datepicker.min.css">

    <!--COLORPICKER-->
    {{-- <script type="text/javascript" src="{{asset('templates/bootstrap-colorpicker.js')}}"></script> --}}

    <!--MINICOLOR-->
    <script src="{{asset('templates/mini-color')}}/jquery.minicolors.js"></script>
    <link rel="stylesheet" href="{{asset('templates/mini-color')}}/jquery.minicolors.css">



    <script>
        function numberOnly(id) {
            // Get element by id which passed as parameter within HTML element event
            var element = document.getElementById(id);
            // This removes any other character but numbers as entered by user
            element.value = element.value.replace(/[^0-9]/gi, "");
        }

        $('.input_date').datepicker({
            format: 'yyyy-mm-dd',
            language: 'id'
        });

        $('.input_date').datepicker('hide');

        $('.open-datetimepicker').click(function () {
            $(this).next('.input_date').datepicker('show');
        });

        $(document).ready(function () {
            
            $(".btn_pdf_modal").click(function() {
                $("#model_content_pdf").empty();
                let url = $(this).data("remote");
                let href = $(this).data("href");
                $("#pdfModalLabel").find("a").attr('href', '#');
                console.log(url);
                $('#model_content_pdf').load(url, function( data, textStatus, xhr ) {
                    if ( textStatus == "error" ) {
                        alert( "Koneksi Internet anda terputus." );
                    } else {
                        $("#pdfModalLabel").find("a").attr('href', href);
                    }
                });
            });
            
            $(".btn_semua_modal").click(function() {
                $("#model_content_semua").html(""); 
                $("#semuaModalLabel").html("Modal");
                let url = $(this).data("remote");
                let title = $(this).data("title");
                console.log(url);
                $('#model_content_semua').load(url, function( data, textStatus, xhr ) {
                    if ( textStatus == "error" ) {
                        alert( "Koneksi Internet anda terputus." );
                    } else {
                        $("#semuaModalLabel").html(title);
                    }
                });
            });

            $(".btn_update_modal").click(function() {
                $("#model_content_update").empty(); 
                let url = $(this).data("remote");
                console.log(url);
                $('#model_content_update').load(url, function( data, textStatus, xhr ) {
                    if ( textStatus == "error" ) {
                        alert( "Koneksi Internet anda terputus." );
                    } else {
                        console.log("berhasil");
                    }
                });
            });

            $(".btn_gambar_berkas_modal").click(function() {
                $("#model_content_gambar").empty(); 
                let url = $(this).data("remote");
                console.log(url);
                $('#model_content_gambar').load(url, function( data, textStatus, xhr ) {
                    if ( textStatus == "error" ) {
                        alert( "Koneksi Internet anda terputus." );
                    } else {
                        console.log("berhasil");
                    }
                });
            });
            
            $(document).on('click', '.btn_delete_modal', function(){
            // $(".btn_delete_modal").click(function() {
                let url = $(this).data("remote");
                $('#btn_hapus').attr('href', url);
            });

            $('.table-export').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": false,
                "ordering": true,
                "info": false,
                "autoWidth": false,
                "responsive": true,
            });

            $('.table-default').DataTable({
                language: {
                    searchPlaceholder: "Cari"
                }
            });

            $('.table-soal').DataTable({
                "lengthChange": false,
                "searching": false,
                "ordering": false,
                "info": false,
                // "fixedHeader": {
                //     header: true,
                //     footer: true
                // },
            });

            if ('{{old("type")}}' == 'create') {
                $("#createModal").modal('show');
            };

            $('.select2-create').select2({
                placeholder: "**Silahkan Pilih",
                dropdownParent: $("#createModal")
            });

            $('.select2-biasa').select2({
                // placeholder: "**Silahkan Pilih"
            });

            $(".dropify").dropify({
                messages: {
                    'default': 'Pilih File'
                }
            });
        });

        $(function () {
            // Summernote
            $('.summernote').summernote(
                {
                    height: 200,                 // set editor height
                    width: "100%",                 // set editor height
                    // minHeight: null,             // set minimum height of editor
                    // maxHeight: null,             // set maximum height of editor
                    // dialogsInBody: true
                    // toolbar: [],
                    // disableResizeEditor: true,
                }
            );
        });
    </script>
    @yield('modal')
    @yield('script')
</body>
</html>