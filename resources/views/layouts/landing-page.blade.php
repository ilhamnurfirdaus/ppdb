
<!-- DEBUG-VIEW START 2 1 APPPATH/Config/../Views/home/layout/template.php -->
<!-- DEBUG-VIEW START 1 APPPATH/Config/../Views/home/layout/template.php -->
<!DOCTYPE html>
<html lang="zxx">
	<head>
{{-- <script type="text/javascript"  id="debugbar_loader" data-time="1627869773" src="{{ asset('templates/landing/js') }}/index.js"></script><script type="text/javascript"  id="debugbar_dynamic_script"></script><style type="text/css"  id="debugbar_dynamic_style"></style> --}}

		<!-- Meta Tag -->
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name='copyright' content='pavilan'>
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
		<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
		<meta http-equiv="Pragma" content="no-cache" />
		<meta http-equiv="Expires" content="0"/>
		<!-- Title Tag  -->
		<title>PPDB</title>
		
		<!-- Favicon -->
		<link rel="icon" type="image/favicon.png" href="img/favicon.png">
		
		<!-- Web Font -->
		<link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700&display=swap" rel="stylesheet">
		
		<!-- Bizwheel Plugins CSS -->
		<link rel="stylesheet" href="{{ asset('templates/landing/css') }}/animate.min.css">
		<link rel="stylesheet" href="{{ asset('templates/landing/css') }}/bootstrap.min.css">
		<link rel="stylesheet" href="{{ asset('templates/landing/css') }}/cubeportfolio.min.css">
        {{-- <link rel="stylesheet" href="{{ asset('templates/landing/css') }}/font-awesome.css"> --}}
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.14.0/css/all.min.css" integrity="sha512-1PKOgIY59xJ8Co8+NE6FZ+LOAZKjy+KY8iq0G4B3CyeY6wYHN3yt9PW0XpSriVlkMXe40PTKnXrLnZ9+fkDaog==" crossorigin="anonymous" />
		<link rel="stylesheet" href="{{ asset('templates/landing/css') }}/jquery.fancybox.min.css">
		<link rel="stylesheet" href="{{ asset('templates/landing/css') }}/magnific-popup.min.css">
		<link rel="stylesheet" href="{{ asset('templates/landing/css') }}/owl-carousel.min.css">
		<link rel="stylesheet" href="{{ asset('templates/landing/css') }}/slicknav.min.css">
		<!-- Bizwheel Stylesheet -->  
		<link rel="stylesheet" href="{{ asset('templates/landing/css') }}/reset.css">
		<link rel="stylesheet" href="{{ asset('templates/landing/css') }}/style.css">
		<link rel="stylesheet" href="{{ asset('templates/landing/css') }}/responsive.css">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.css" integrity="sha512-vKMx8UnXk60zUwyUnUPM3HbQo8QfmNx7+ltw8Pm5zLusl1XIfwcxo8DbWCqMGKaWeNxWA8yrx5v3SaVpMvR3CA==" crossorigin="anonymous" />
		<!-- Select2 -->
		<link rel="stylesheet" href="{{asset('templates/adminlte/assets')}}/plugins/select2/css/select2.min.css">
		<link rel="stylesheet" href="{{asset('templates/adminlte/assets')}}/plugins/select2-bootstrap4-theme/select2-bootstrap4.min.css">
		@php
			$web_profils_template = App\Models\WebProfil::all();
      		$web_profil_template = App\Models\WebProfil::find($web_profils_template[0]->id);
		@endphp
		<style>
			/* html, body {
				max-width: 100%;
				overflow-x: hidden;
			} */
			
			.video-feature i{
				background: #fff;
				color: #f3a712;
				border-radius: 25px;
				padding: 10px;
			}

			.header .nav li a::before {
				background: #dc3545;
			}

			.header .nav li.active a, .header .nav li:hover a {
				color: #dc3545;
			}

			#scrollUp i{
				color: #dc3545;
			}

			.single-feature:hover, .single-feature.active {
				border-top-color: #dc3545;
			}

			.right-bar:after, .card-header, .middle-header {
				background: <?= $web_profil_template->warna_header ?> !important;
			}

			.pulse {
				animation-duration: 2s!important;
			}

			.header .nav-inner {
				float: left;
				text-align: left;
			}

			.dropdown-item {
				line-height: 30px;
			}
		</style>
	</head>
	<body id="bg">
		<!-- Boxed Layout -->
		<div id="page" class="site boxed-layout"> 
		
		<!-- Preloader -->
		<!-- <div class="preeloader">
			<div class="preloader-spinner"></div>
		</div> -->
		<!--/ End Preloader -->
	
		<!-- Header -->
		<header class="header">
			<!-- Middle Header -->
			<div class="middle-header">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="middle-inner">
								<div class="row">
									<div class="col-lg-2 col-md-3 col-12">
										<!-- Logo -->
										<div class="logo">
											<!-- Image Logo -->
											<div class="img-logo">
												<a href="{{ url('/') }}">
													<img src="{{ isset($web_profil_template->logo) ? asset('templates/landing/images/logo/'.$web_profil_template->logo) : asset('templates/landing/images/logo-header.png')}}" alt="#" style="height: 50px;">
												</a>
											</div>
										</div>								
										<div class="mobile-nav"></div>
									</div>
									<div class="col-lg-10 col-md-9 col-12">
										<div class="menu-area">
											<!-- Main Menu -->
											<nav class="navbar navbar-expand-lg">
												<div class="navbar-collapse">	
													<div class="nav-inner">	
														<div class="menu-home-menu-container">
															<!-- Naviagiton -->
															<ul>
																<li><h4 style="text-transform: capitalize;
																	position: relative;
																	display: block;
																	padding: 30px 0px 0px 0px;
																	font-weight: 600;">{{ $web_profil_template->jenjang_sekolah == "SLTA" ? $web_profil_template->jenjang_sekolah2 : $web_profil_template->jenjang_sekolah }} {{ $web_profil_template->name }}</h4></li>
															</ul>
															<ul id="nav" class="nav main-menu menu navbar-nav">
																<li><a href="{{ url('/') }}">Home</a></li>
																@if (!Auth::check())
																<li><a href="{{ url('pendaftaran') }}">Pendaftaran</a></li>
																@endif
																<li><a href="{{ Request::is('/') ? '' : url('/')}}#informasi-pendaftaran">Informasi Pendaftaran</a></li>
																<li><a href="{{ Request::is('/') ? '' : url('/')}}#jurusan">Jurusan</a></li>
															</ul>
															<!--/ End Naviagiton -->
														</div>
													</div>
												</div>
											</nav>
											<!--/ End Main Menu -->	
											<!-- Right Bar -->
											<div class="right-bar" style="padding-left: 25px; padding-top: 25px;">
												@if (Auth::check())
												<a class="dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="vertical-align: middle;">
													{{ Auth::user()->name }}
												</a>
												<div class="dropdown-menu" aria-labelledby="navbarDropdown">
													<a class="dropdown-item" href="{{ route('logout') }}"
														onclick="event.preventDefault();
																	document.getElementById('logout-form').submit();">
														{{ __('Logout') }}
													</a>

													<form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
														@csrf
													</form>
													@if (Auth::user()->jenis == "NonAdmin")
													<a class="dropdown-item" href="{{ url('siswa') }}">
														Pendaftaran
													</a>
													@else
													<a class="dropdown-item" href="{{ url('admin') }}">
														Web Admin
													</a>
													@endif
												</div>
												@else
												<a href="#" onclick="openLogin();">Login</a>
												@endif
											</div>
											<!--/ End Right Bar -->
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--/ End Middle Header -->
			<!-- Sidebar Popup -->
			<div class="sidebar-popup">
				<div class="cross">
					<a class="btn"><i class="fa fa-close"></i></a>
				</div>
				<div class="single-content">
					<h4>About Bizwheel</h4>
					<p>The main component of a healthy environment for self esteem is that it needs be nurturing. It should provide unconditional warmth.</p>
					<!-- Social Icons -->
					<ul class="social">
						<li><a href="#"><i class="fa fa-facebook"></i></a></li>
						<li><a href="#"><i class="fa fa-twitter"></i></a></li>
						<li><a href="#"><i class="fa fa-linkedin"></i></a></li>
						<li><a href="#"><i class="fa fa-dribbble"></i></a></li>
					</ul>
				</div>
				<div class="single-content">
					<h4>Important Links</h4>   
					<ul class="links">
						<li><a href="#">About Us</a></li>
						<li><a href="#">Our Services</a></li>
						<li><a href="#">Portfolio</a></li>
						<li><a href="#">Pricing Plan</a></li>
						<li><a href="#">Blog & News</a></li>
						<li><a href="#">Contact us</a></li>
					</ul>
				</div>	
			</div>
			<!--/ Sidebar Popup -->	
		</header>
		<!--/ End Header -->

@yield('main')

<!-- Services -->
<!-- <section class="services section-bg section-space">
	<div class="container">
		<div class="row">
			<div class="col-12">
				<div class="section-title style2 text-center">
					<div class="section-top">
						<h2><i class="fas fa-bullhorn"></i> Informasi Tambahan</h2>
					</div>
				</div>
				<hr>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-4 col-md-4 col-12">
				<div class="single-service">
					<div class="service-head">
						<img src="https://via.placeholder.com/555x410" alt="#">
						<div class="icon-bg"><i class="fa fa-info-circle"></i></div>
					</div>
					<div class="service-content">
						<h4><a href="https://pmb.masuk.id/pmb/info/pendaftaran">Informasi Pendaftaran</a></h4>
						<a class="btn" href="https://pmb.masuk.id/pmb/info/pendaftaran"><i class="fas fa-arrow-circle-right"></i>View Service</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-12">
				<div class="single-service">
					<div class="service-head">
						<img src="https://via.placeholder.com/555x410" alt="#">
						<div class="icon-bg"><i class="fas fa-money-check-alt"></i></div>
					</div>
					<div class="service-content">
						<h4><a href="https://pmb.masuk.id/pmb/biaya/pendaftaran">Biaya Pendaftaran</a></h4>
						<a class="btn" href="https://pmb.masuk.id/pmb/biaya/pendaftaran"><i class="fas fa-arrow-circle-right"></i>View Service</a>
					</div>
				</div>
			</div>
			<div class="col-lg-4 col-md-4 col-12">
				<div class="single-service">
					<div class="service-head">
						<img src="https://via.placeholder.com/555x410" alt="#">
						<div class="icon-bg"><i class="fas fa-money-check-alt"></i></div>
					</div>
					<div class="service-content">
						<h4><a href="https://pmb.masuk.id/pmb/biaya/pendidikan">Biaya Pendidikan</a></h4>
						<a class="btn" href="https://pmb.masuk.id/pmb/biaya/pendidikan"><i class="fas fa-arrow-circle-right"></i>View Service</a>
					</div>
				</div>
			</div>
		</div>
	</div>
</section> -->
<!--/ End Services -->

		
		<!-- Footer -->
		<footer class="footer">
			<!-- Footer Top -->
			<div class="footer-top">
				<div class="container">
					<div class="row mb-3">
						<div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 mb-3">
							<h5 class="text-light m-0">PPDB</h5>
							<p>
								{{ $web_profil_template->alamat }}
							</p>
							<hr>
						</div>
						{{-- <div class="col-xl-4 col-lg-4 col-md-6 col-sm-12 mb-3">
							<h5 class="text-light m-0">WAKTU LAYANAN</h5>
							<p>
								Senin – Jum’at : 08.00 s/d 21.00 <br> Sabtu : 08.00 s/d 14.00
							</p>
						</div> --}}
					</div>
				</div>
			</div>
			<!-- Copyright -->
			<div class="copyright">
				<div class="container">
					<div class="row">
						<div class="col-12">
							<div class="copyright-content">
								<!-- Copyright Text -->
								<p>{{ $web_profil_template->jenjang_sekolah == "SLTA" ? $web_profil_template->jenjang_sekolah2 : $web_profil_template->jenjang_sekolah }} {{ $web_profil_template->name }} © Copyright <a href="#"> 2021</a></p>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!--/ End Copyright -->
		</footer>
		
		<!-- Jquery JS -->
        
		<script src="{{ asset('templates/landing') }}/js/jquery.min.js"></script>
		<script src="{{ asset('templates/landing') }}/js/jquery-migrate-3.0.0.js"></script>
        {{-- <script type="text/javascript" src="{{asset('templates/adminlte/assets/plugins/jquery/jquery.min.js')}}"></script> --}}
		<script src="{{ asset('templates/landing') }}/js/popper.min.js"></script>
		<script src="{{ asset('templates/landing') }}/js/bootstrap.min.js"></script>
		<script src="{{ asset('templates/landing') }}/js/modernizr.min.js"></script>
		<script src="{{ asset('templates/landing') }}/js/scrollup.js"></script>
		<script src="{{ asset('templates/landing') }}/js/jquery-fancybox.min.js"></script>
		<script src="{{ asset('templates/landing') }}/js/cubeportfolio.min.js"></script>
		<script src="{{ asset('templates/landing') }}/js/slicknav.min.js"></script>
		<script src="{{ asset('templates/landing') }}/js/slicknav.min.js"></script>
		<script src="{{ asset('templates/landing') }}/js/owl-carousel.min.js"></script>
		<script src="{{ asset('templates/landing') }}/js/easing.js"></script>
		<script src="{{ asset('templates/landing') }}/js/magnific-popup.min.js"></script>
		<script src="{{ asset('templates/landing') }}/js/active.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/toastr.min.js" integrity="sha512-VEd+nq25CkR676O+pLBnDW09R7VQX9Mdiij052gVCp5yVH3jGtH70Ho/UUv4mJDsEdTvqRCFZg0NKGiojGnUCw==" crossorigin="anonymous"></script>
		<!-- Select2 -->
		<script src="{{asset('templates/adminlte/assets')}}/plugins/select2/js/select2.full.min.js"></script>
		<script>
            $(document).ready(function () {
                console.log('ready');
            });

			toastr.options = {
				"closeButton": true,
				"debug": false,
				"newestOnTop": false,
				"progressBar": true,
				"positionClass": "toast-top-center",
				"preventDuplicates": true,
				"onclick": null,
				"showDuration": "300",
				"hideDuration": "1000",
				"timeOut": "5000",
				"extendedTimeOut": "1000",
				"showEasing": "swing",
				"hideEasing": "linear",
				"showMethod": "fadeIn",
				"hideMethod": "fadeOut"
			}
		</script>
		@yield('modal')
		@yield('script')

		<!-- Modal -->
		<div class="modal fade" id="signModal" tabindex="-1" role="dialog" aria-labelledby="signModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
					{{-- <h5 class="modal-title" id="signModalLabel">Sign In / Sign Up</h5> --}}
					<h5 class="modal-title" id="signModalLabel">Login</h5>
					<button type="button" class="btn close" data-dismiss="modal" aria-label="Close" onclick="$('#signModal').modal('hide');">
						<span aria-hidden="true">&times;</span>
					</button>
					</div>
					<div class="modal-body" style="font-size: 12px;">
					{{-- <ul class="nav nav-pills">
						<li class="nav-item">
						<a class="tab-login nav-link active" data-toggle="tab" href="#login">Login</a>
						</li>
						<li class="nav-item">
						<a class="tab-register nav-link" data-toggle="tab" href="#register">Register</a>
						</li>
					</ul>
		
					<div class="tab-content mt-3">
						<div id="login" class="tab-pane fade in active show"> --}}
						<form method="POST" action="{{ route('login') }}">
							@csrf
		
							<input type="hidden" name="type" value="login">
		
							<div class="form-group my-1">
							<label for="login" class="my-1">{{ __('Username') }}</label>
		
							<input id="login" type="text" class="my-1 form-control @if (old("type") == "login") @error('login') is-invalid @enderror @endif" name="login" value="{{ old('login') }}" required autocomplete="login" autofocus placeholder="Isi nama lengkap anda">
		
							@if (old("type") == "login")
								@error('login')
									<span class="invalid-feedback my-1" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
							@endif
							</div>
		
							<div class="form-group my-1">
								<label for="password" class="my-1">{{ __('Password') }}</label>
		
								<input id="password" type="password" class="my-1 form-control @if (old("type") == "login") @error('password') is-invalid @enderror @endif" name="password" required autocomplete="current-password">
		
								@if (old("type") == "login")
								@error('password')
									<span class="invalid-feedback my-1" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
								@endif
							</div>
		
							<div class="form-group my-3">
							<div class="form-check">
								<input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
		
								<label class="form-check-label" for="remember">
									{{ __('Remember Me') }}
								</label>
							</div>
							</div>
		
							<div class="form-group my-3">
							<button type="submit" class="btn btn-primary">
								{{ __('Log In') }}
							</button>
		
							@if (Route::has('password.request'))
								<a class="btn btn-link" href="{{ route('password.request') }}">
									{{ __('Forgot Your Password?') }}
								</a>
							@endif
							</div>
						</form>
						{{-- </div>
						<div id="register" class="tab-pane fade">
						<form method="POST" action="{{ route('register') }}">
							@csrf 
		
							<input type="hidden" name="type" value="register">
		
							<div class="form-group my-1">
								<label for="name" class="my-1">{{ __('Name') }}</label>
		
								<input id="name" type="text" class="my-1 form-control @if (old("type") == "register") @error('name') is-invalid @enderror @endif" name="name" value="{{ old('name') }}" required autocomplete="name" autofocus placeholder="Isi nama lengkap anda">
		
								@if (old("type") == "register")
								@error('name')
									<span class="invalid-feedback my-1" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
								@endif
							</div>
		
							<div class="form-group my-1">
								<label for="email" class="my-1">{{ __('E-Mail Address') }}</label>
		
								<input id="email" type="email" class="my-1 form-control @if (old("type") == "register") @error('email') is-invalid @enderror @endif" name="email" value="{{ old('email') }}" required autocomplete="email" placeholder="Isi email sebagai username akun anda">
		
								@if (old("type") == "register")
								@error('email')
									<span class="invalid-feedback my-1" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
								@endif
							</div>
		
							<div class="form-group my-1">
								<label class="my-1">Nomor Hp</label>
								<input id="phone" name="phone" type="text" class="my-1 form-control @if (old("type") == "register") @error('phone') is-invalid @enderror @endif" placeholder="08 . . ." oninput="numberOnly(this.id);" maxlength="15" value="{{old("phone")}}" required autocomplete="phone">
		
								@if (old("type") == "register")
								@error('phone')
									<span class="invalid-feedback my-1" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
								@endif
							</div>
		
							<div class="form-group my-1">
								<label for="password" class="my-1">{{ __('Password') }}</label>
		
								<input id="password" type="password" class=" my-1 form-control @if (old("type") == "register") @error('password') is-invalid @enderror @endif" name="password" required autocomplete="new-password" placeholder="Isi password untuk akun anda">
		
								@if (old("type") == "register")
								@error('password')
									<span class="invalid-feedback my-1" role="alert">
										<strong>{{ $message }}</strong>
									</span>
								@enderror
								@endif
							</div>
		
							<div class="form-group my-1">
								<label for="password-confirm" class="my-1">{{ __('Confirm Password') }}</label>
								
								<input id="password-confirm" type="password" class="my-1 form-control" name="password_confirmation" required autocomplete="new-password" placeholder="Ketik ulang password akun anda">
							</div>
		
							<div class="form-group my-3">
							<button type="submit" class="btn btn-primary">
								{{ __('Register') }}
							</button>
							</div>
						</form>
						</div>
					</div>
					</div> --}}
				</div>
			</div>
		</div>

		<script>
			function openRegister() {
				$("#signModal").modal('show');
				$(".tab-register").addClass("active");
				$(".tab-login").removeClass("active");
				$("#register").addClass("in active show");
				$("#login").removeClass("in active show");
			}
	  
			function openLogin() { 
				$("#signModal").modal('show');
				// $(".tab-register").removeClass("active");
				// $(".tab-login").addClass("active");
				// $("#register").removeClass("in active show");
				// $("#login").addClass("in active show");
			}
	  
			if ('{{old("type")}}' == 'register') {
				$(document).ready(function () {
					openRegister();
					console.log("register gagal");
				});
			}
			else if ('{{old("type")}}' == 'login') {
				$(document).ready(function () {
					openLogin();
				});
			}
		</script>

	</body>
</html>
<!-- DEBUG-VIEW ENDED 1 APPPATH/Config/../Views/home/layout/template.php -->

<!-- DEBUG-VIEW ENDED 2 1 APPPATH/Config/../Views/home/layout/template.php -->
