@extends('layouts.admin')
@section('main')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>
                Daftar Ulang
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
            </div>
        </div>
        <div class="page-title-actions">

        </div>    
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="mb-3 card">
            <div class="card-header-tab card-header bg-danger text-white">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    Daftar Diri
                </div>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ url('siswa/daftar/update-data-diri/'.$siswa->id) }}">
                    @csrf                     

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Ukuran Baju *</label>
                            <input type="text" value="{{$siswa->ukuran_baju}}" class="form-control" name="ukuran_baju" placeholder="Ukuran Baju" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>NISN *</label>
                            <input type="number" value="{{$siswa->nisn}}" class="form-control" name="nisn" placeholder="NISN" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Ukuran Baju (CM)*</label>
                            <input type="text" value="{{$siswa->ukuran_baju_cm}}" class="form-control" name="ukuran_baju_cm" placeholder="panjang badan/panjang pundak/panjang lengan" required>
                            <small class="form-text text-muted">panjang badan/panjang pundak/panjang lengan</small>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Ukuran Celana (CM)*</label>
                            <input type="text" value="{{$siswa->ukuran_celana_cm}}" class="form-control" name="ukuran_celana_cm" placeholder="panjang celana/lingkar pinggang/lingkar paha" required>
                            <small class="form-text text-muted">panjang celana/lingkar pinggang/lingkar paha</small>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>NIK *</label>
                        <input type="number" class="form-control" name="nik" value="{{$siswa->nik}}" required>
                    </div>

                    <div class="form-group">
                        <label>Nomor KK *</label>
                        <input type="number" class="form-control" name="no_kk" value="{{$siswa->no_kk}}" required>
                    </div>

                    <div class="form-group ">
                        <label for="name">{{ __('Nama Lengkap') }} *</label>

                        <input id="name" type="text" class=" form-control" name="name" value="{{ $siswa->user->name }}" required autocomplete="name" autofocus placeholder="Isi nama lengkap anda">
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="tempat">Tempat Lahir</label>
                            <input type="text" value="{{$siswa->tempat_lahir}}" class="form-control" name="tempat_lahir" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="tgllahir">Tanggal Lahir</label>
                            <input type="date" class="form-control" value="{{Carbon\Carbon::parse($siswa->tanggal_lahir)->format('Y-m-d')}}" name="tanggal_lahir" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Jenis Kelamin *</label>
                            <select class="form-control" name="jenis_kelamin" required>
                                <option value="">--- Pilih Jenis Kelamin ---</option>
                                <option value="Laki-laki" @if ($siswa->jenis_kelamin == "Laki-laki") selected @endif>Laki-Laki</option>
                                <option value="Perempuan" @if ($siswa->jenis_kelamin == "Perempuan") selected @endif>Perempuan</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Nomor Hp *</label>
                            <input id="no_hp" name="no_hp" type="number" class=" form-control" placeholder="08 . . ." maxlength="15" value="{{ $siswa->no_hp }}" required autocomplete="no_hp">
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Asal Sekolah *</label>
                        <input type="text" value="{{$siswa->asal_sekolah}}" class="form-control" name="asal_sekolah" placeholder="Asal Sekolah" required>
                    </div>

                    <div class="form-group">
                        <label>Agama *</label>
                        <select class="form-control" name="agama" required>
                            <option value="">--- Pilih Agama ---</option>
                            <option value="Budha" @if ($siswa->agama == "Budha") selected @endif>Budha</option>
                            <option value="Hindu" @if ($siswa->agama == "Hindu") selected @endif>Hindu</option>
                            <option value="Islam" @if ($siswa->agama == "Islam") selected @endif>Islam</option>
                            <option value="Katholik" @if ($siswa->agama == "Katholik") selected @endif>Katholik</option>
                            <option value="Kristen" @if ($siswa->agama == "Kristen") selected @endif>Kristen</option>
                            <option value="Konghucu" @if ($siswa->agama == "Konghucu") selected @endif>Konghucu</option>
                        </select>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Anak Ke :</label>
                            <input type="number" name="anak_ke" class="form-control" value="{{$siswa->anak_ke}}" placeholder="Anak Ke :" min="0" max="40" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Jumlah Saudara</label>
                            <input type="number" name="jumlah_saudara" class="form-control" value="{{$siswa->jumlah_saudara}}" placeholder="Jumlah Saudara" min="0" max="30" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Tinggi Badan (CM)</label>
                            <input type="number" name="tinggi_badan_cm" class="form-control" value="{{$siswa->tinggi_badan_cm}}" placeholder="Tinggi Badan" min="10" max="270" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>Berat Badan</label>
                            <input type="number" name="berat_badan" class="form-control" value="{{$siswa->berat_badan}}" placeholder="Berat Badan" min="10" max="200" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label>Nomor KIP *</label>
                        <input type="number" class="form-control" name="no_kip" value="{{$siswa->no_kip}}" required>
                    </div>

                    <div class="form-group my-3">
                    <button type="submit" class="btn btn-success">
                        {{ __('Simpan') }}
                    </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        $(document).ready(function(){

        });
    </script>
@endsection