@extends('layouts.admin')
@section('main')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>
                Daftar Ulang
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
            </div>
        </div>
        <div class="page-title-actions">

        </div>    
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="mb-3 card">
            <div class="card-header-tab card-header bg-danger text-white">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    Daftar Alamat
                </div>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ url('siswa/daftar/update-data-alamat/'.$siswa->id) }}">
                    @csrf 

                    <div class="form-group">
                        <label>Alamat *</label>
                        <textarea name="alamat" class="form-control" rows="3" required>{{$siswa->alamat}}</textarea>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>RT *</label>
                            <input type="number" value="{{$siswa->rt}}" class="form-control" name="rt" placeholder="RT" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>RW *</label>
                            <input type="number" value="{{$siswa->rw}}" class="form-control" name="rw" placeholder="RW" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Provinsi *</label>
                            <select id="provinsi" name="provinsi" class="form-control" style="width: 100%;padding: 10px;margin-top: 10px ; font-size: 19px;" required>
                                @if ($siswa->provinsi)
                                <option value="{{ $siswa->provinsi }}" selected>{{ $siswa->provinsi }}</option>
                                @endif
                            </select>
                            
                        </div>
    
                        <div class="form-group col-md-6">
                            <label>Kota / Kabupaten *</label>
                            <select id="kota_kabupaten" name="kota_kabupaten" class="form-control" style="width: 100%;padding: 10px;margin-top: 10px ; font-size: 19px;" @if($siswa->kota_kabupaten) @else disabled @endif required>
                                @if ($siswa->kota_kabupaten)
                                <option value="{{ $siswa->kota_kabupaten }}" selected>{{ $siswa->kota_kabupaten }}</option>
                                @endif
                            </select>    
                        </div>
                    </div>

                    <div class="form-row">                        
                        <div class="form-group col-md-6">
                            <label>Kecamatan *</label>
                            <select id="kecamatan" name="kecamatan" class="form-control" style="width: 100%;padding: 10px;margin-top: 10px ; font-size: 19px;" @if($siswa->kecamatan) @else disabled @endif required>
                                @if ($siswa->kecamatan)
                                <option value="{{ $siswa->kecamatan }}" selected>{{ $siswa->kecamatan }}</option>
                                @endif
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Desa / Kelurahan *</label>
                            <select id="desa_kelurahan" name="desa_kelurahan" class="form-control" style="width: 100%;padding: 10px;margin-top: 10px ; font-size: 19px;" @if($siswa->desa_kelurahan) @else disabled @endif  required>
                                @if ($siswa->desa_kelurahan)
                                <option value="{{ $siswa->desa_kelurahan }}" selected>{{ $siswa->desa_kelurahan }}</option>
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Jarak Ke sekolah (Meter) *</label>
                            <input type="number" value="{{$siswa->rt}}" class="form-control" name="rt" placeholder="RT" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label>RW *</label>
                            <input type="number" value="{{$siswa->rw}}" class="form-control" name="rw" placeholder="RW" required>
                        </div>
                    </div>

                    <div class="form-group my-3">
                    <button type="submit" class="btn btn-success">
                        {{ __('Simpan') }}
                    </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        $(document).ready(function(){
    //alert("cek2");
        $('#provinsi').select2({
            minimumInputLength: 1,
            ajax: {
                url: "{{ url('search/provinsi') }}",
                dataType: 'json',
                delay: 250,
                processResults: pResult,
                cache: true
            },
        });

        $('#provinsi').change(function(){
            $('#kota_kabupaten').val(null).trigger('change');
            $('#kota_kabupaten').prop( "disabled", false );

            $('#kecamatan').val(null).trigger('change');
            $('#kecamatan').prop( "disabled", true );

            $('#desa_kelurahan').val(null).trigger('change');
            $('#desa_kelurahan').prop( "disabled", true );
        });

        $('#kota_kabupaten').select2({
            minimumInputLength: 1,
            ajax: {
                url: function () {
                    return "{{ url('search/kota_kabupaten') }}/"+$('#provinsi').val().split(" ").join("_");
                },
                dataType: 'json',
                delay: 250,
                processResults: pResult,
                cache: true
            },
        });

        $('#kota_kabupaten').change(function(){
            $('#kecamatan').val(null).trigger('change');
            $('#kecamatan').prop( "disabled", false );

            $('#desa_kelurahan').val(null).trigger('change');
            $('#desa_kelurahan').prop( "disabled", true );
        });

        $('#kecamatan').select2({
            minimumInputLength: 1,
            ajax: {
                url: function () {
                    return "{{ url('search/kecamatan') }}/"+$('#kota_kabupaten').val().split(" ").join("_");
                },
                dataType: 'json',
                delay: 250,
                processResults: pResult,
                cache: true
            },
        });

        $('#kecamatan').change(function(){
            $('#desa_kelurahan').val(null).trigger('change');
            $('#desa_kelurahan').prop( "disabled", false );
        });

        $('#desa_kelurahan').select2({
            minimumInputLength: 1,
            ajax: {
                url: function () {
                    return "{{ url('search/desa_kelurahan') }}/"+$('#kecamatan').val().split(" ").join("_");
                },
                dataType: 'json',
                delay: 250,
                processResults: pResult,
                cache: true
            },
        });

        function pResult(data) {
            return {
                results: $.map(data, function (item) {
                    // console.log(item);
                        return {
                            text: item.name,
                            id: item.name
                        }
                })
            };
        }
    })
    </script>
@endsection