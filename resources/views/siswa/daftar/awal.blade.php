@extends('layouts.admin')
@section('main')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>
                Daftar
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
            </div>
        </div>
        <div class="page-title-actions">

        </div>    
    </div>
</div>

<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="mb-3 card">
            <div class="card-header-tab card-header bg-danger text-white">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    Isi Form Pendaftaran
                </div>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ url('siswa/daftar/update-awal/'.$siswa->id) }}">
                    @csrf 

                    <input type="hidden" name="type" value="register">

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="name">{{ __('Nama Lengkap') }} *</label>

                            <input id="name" type="text" class="form-control form-control-sm @if (old("type") == "register") @error('name') is-invalid @enderror @endif" name="name" value="{{ $siswa->user->name }}" required autocomplete="name" autofocus placeholder="Isi nama lengkap anda">

                            @if (old("type") == "register")
                            @error('name')
                                <span class="invalid-feedback " role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            @endif
                        </div>

                        <div class="form-group col-md-6">
                            
                            <label>NIK *</label>
                            <input type="number" class="form-control form-control-sm" name="nik" value="{{$siswa->nik}}" min="0" max="999999999999" required>
                            </div>
                    </div>
                    
                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Provinsi Asal Sekolah*</label>
                            <select id="provinsi" name="provinsi" class="form-control form-control-sm" style="width: 100%;padding: 10px;margin-top: 10px ; font-size: 19px;">
                                <option value="">Cari Provinsi</option>
                                @if (old("provinsi"))
                                <option value="{{ old("provinsi") }}" selected>{{ old("provinsi") }}</option>
                                @endif
                            </select>
                        </div>

                        <div class="form-group col-md-6">
                            <label>Kota / Kabupaten Asal Sekolah*</label>
                            <select id="kota_kabupaten" name="kota_kabupaten" class="form-control form-control-sm" style="width: 100%;padding: 10px;margin-top: 10px ; font-size: 19px;" @if(old("kota_kabupaten")) @else disabled @endif>
                                @if (old("kota_kabupaten"))
                                <option value="{{ old("kota_kabupaten") }}" selected>{{ old("kota_kabupaten") }}</option>
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="name">Asal Sekolah *</label>
                            {{-- <input type="text" class="form-control form-control-sm" name="asal_sekolah" value="{{$siswa->asal_sekolah}}" required> --}}
                            <select id="institusi" name="asal_sekolah" class="form-control form-control-sm" style="width: 100%;padding: 10px;margin-top: 10px ; font-size: 19px;" @if(old("kota_kabupaten")) @else disabled @endif required>
                                <option value="{{ $siswa->asal_sekolah }}" selected>{{ $siswa->asal_sekolah }}</option>
                                
                            </select>
                        </div>
    
                        <div class="form-group col-md-6">
                            <label>Nomor KK *</label>
                            <input type="number" class="form-control form-control-sm" name="no_kk" value="{{$siswa->no_kk}}" min="0" required>
                        </div>
                    </div>

                    {{-- <div class="form-row">
                        <div class="form-group col-md-6">
                            <label for="tempat">Tempat Lahir</label>
                            <input type="text" value="{{$siswa->tempat_lahir}}" class="form-control form-control-sm" name="tempat_lahir" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="tgllahir">Tanggal Lahir</label>
                            <input type="date" class="form-control form-control-sm" value="{{Carbon\Carbon::parse($siswa->tanggal_lahir)->format('Y-m-d')}}" name="tanggal_lahir" required>
                        </div>
                    </div> --}}

                    <div class="form-row">
                        <div class="form-group col-md-4">
                            <label>Jenis Kelamin *</label>
                            <div class="form-row">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="jenis_kelamin" value="Laki-laki" @if ($siswa->jenis_kelamin == "Laki-laki") checked @endif required>
                                    <label class="form-check-label">
                                        Laki-laki
                                    </label>
                                </div>
                                <div class="form-check mx-3">
                                    <input class="form-check-input" type="radio" name="jenis_kelamin" value="Perempuan" @if ($siswa->jenis_kelamin == "Perempuan") checked @endif>
                                    <label class="form-check-label">
                                        Perempuan
                                    </label>
                                </div>
                            </div>
                            {{-- <select class="form-control form-control-sm" name="jenis_kelamin" required>
                                <option value="">--- Pilih Jenis Kelamin ---</option>
                                <option value="Laki-laki" @if ($siswa->jenis_kelamin == "Laki-laki") selected @endif>Laki-Laki</option>
                                <option value="Perempuan" @if ($siswa->jenis_kelamin == "Perempuan") selected @endif>Perempuan</option>
                            </select> --}}
                        </div>
    
                        <div class="form-group col-md-8">
                            <label>Agama *</label>
                            <div class="form-row">
                                <div class="form-check">
                                    <input class="form-check-input" type="radio" name="agama" value="Budha" @if ($siswa->agama == "Budha") checked @endif required>
                                    <label class="form-check-label">
                                        Budha
                                    </label>
                                </div>
                                <div class="form-check mx-2">
                                    <input class="form-check-input" type="radio" name="agama" value="Hindu" @if ($siswa->agama == "Hindu") checked @endif>
                                    <label class="form-check-label">
                                        Hindu
                                    </label>
                                </div>
                                <div class="form-check mx-2">
                                    <input class="form-check-input" type="radio" name="agama" value="Islam" @if ($siswa->agama == "Islam") checked @endif>
                                    <label class="form-check-label">
                                        Islam
                                    </label>
                                </div>
                                <div class="form-check mx-2">
                                    <input class="form-check-input" type="radio" name="agama" value="Katholik" @if ($siswa->agama == "Katholik") checked @endif>
                                    <label class="form-check-label">
                                        Katholik
                                    </label>
                                </div>
                                <div class="form-check mx-2">
                                    <input class="form-check-input" type="radio" name="agama" value="Kristen" @if ($siswa->agama == "Kristen") checked @endif>
                                    <label class="form-check-label">
                                        Kristen
                                    </label>
                                </div>
                                <div class="form-check mx-2">
                                    <input class="form-check-input" type="radio" name="agama" value="Konghucu" @if ($siswa->agama == "Konghucu") checked @endif>
                                    <label class="form-check-label">
                                        Konghucu
                                    </label>
                                </div>
                            </div>
                            {{-- <select class="form-control form-control-sm" name="agama" required>
                                <option value="">--- Pilih Agama ---</option>
                                <option value="Budha" @if ($siswa->agama == "Budha") selected @endif>Budha</option>
                                <option value="Hindu" @if ($siswa->agama == "Hindu") selected @endif>Hindu</option>
                                <option value="Islam" @if ($siswa->agama == "Islam") selected @endif>Islam</option>
                                <option value="Katholik" @if ($siswa->agama == "Katholik") selected @endif>Katholik</option>
                                <option value="Kristen" @if ($siswa->agama == "Kristen") selected @endif>Kristen</option>
                                <option value="Konghucu" @if ($siswa->agama == "Konghucu") selected @endif>Konghucu</option>
                            </select> --}}
                        </div>
                    </div>

                    @if ($siswa->jurusan != "Default")
                    <div class="form-group">
                        <label>Jurusan *</label>
                        <select class="form-control form-control-sm" name="jurusan" required>
                            <option value="">--- Pilih Jurusan ---</option>
                            @foreach ($jurusans as $item)
                            <option value="{{ $item->id }}" @if ($siswa->jurusan == $item->id) selected @endif>{{ $item->name }}</option>    
                            @endforeach
                        </select>
                    </div>
                    @endif

                    {{-- <div class="form-group">
                        <label>Alamat *</label>
                        <textarea name="alamat" class="form-control form-control-sm" rows="3" required>{{$siswa->alamat}}</textarea>                        
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Provinsi *</label>
                            <select id="provinsi" name="provinsi" class="form-control form-control-sm" style="width: 100%;padding: 10px;margin-top: 10px ; font-size: 19px;" required>
                                @if ($siswa->provinsi)
                                <option value="{{ $siswa->provinsi }}" selected>{{ $siswa->provinsi }}</option>
                                @endif
                            </select>
                        </div>
    
                        <div class="form-group col-md-6">
                            <label>Kota / Kabupaten *</label>
                            <select id="kota_kabupaten" name="kota_kabupaten" class="form-control form-control-sm" style="width: 100%;padding: 10px;margin-top: 10px ; font-size: 19px;" @if($siswa->kota_kabupaten) @else disabled @endif required>
                                @if ($siswa->kota_kabupaten)
                                <option value="{{ $siswa->kota_kabupaten }}" selected>{{ $siswa->kota_kabupaten }}</option>
                                @endif
                            </select>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label>Kecamatan *</label>
                            <select id="kecamatan" name="kecamatan" class="form-control form-control-sm" style="width: 100%;padding: 10px;margin-top: 10px ; font-size: 19px;" @if($siswa->kecamatan) @else disabled @endif required>
                                @if ($siswa->kecamatan)
                                <option value="{{ $siswa->kecamatan }}" selected>{{ $siswa->kecamatan }}</option>
                                @endif
                            </select>
                        </div>
    
                        <div class="form-group col-md-6">
                            <label>Desa / Kelurahan *</label>
                            <select id="desa_kelurahan" name="desa_kelurahan" class="form-control form-control-sm" style="width: 100%;padding: 10px;margin-top: 10px ; font-size: 19px;" @if($siswa->desa_kelurahan) @else disabled @endif  required>
                                @if ($siswa->desa_kelurahan)
                                <option value="{{ $siswa->desa_kelurahan }}" selected>{{ $siswa->desa_kelurahan }}</option>
                                @endif
                            </select>
                        </div>
                    </div> --}}

                    {{-- <div class="form-group ">
                        <label for="email">{{ __('E-Mail Address') }}  *</label>

                        <input id="email" type="email" class=" form-control form-control-sm @if (old("type") == "register") @error('email') is-invalid @enderror @endif" name="email" value="{{ $siswa->user->email }}" required autocomplete="email" placeholder="Isi email sebagai username akun anda">

                        @if (old("type") == "register")
                        @error('email')
                            <span class="invalid-feedback " role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        @endif
                    </div> --}}

                    <div class="form-group ">
                        <label>Nomor Hp *</label>
                        <input id="no_hp" name="no_hp" type="text" class=" form-control form-control-sm @if (old("type") == "register") @error('no_hp') is-invalid @enderror @endif" placeholder="08 . . ." oninput="numberOnly(this.id);" maxlength="15" value="{{ $siswa->no_hp }}" required autocomplete="no_hp">

                        @if (old("type") == "register")
                        @error('no_hp')
                            <span class="invalid-feedback " role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        @endif
                    </div>

                    {{-- <div class="form-group ">
                        <label for="password">{{ __('Password') }}  *</label>

                        <input id="password" type="password" class="  form-control form-control-sm @if (old("type") == "register") @error('password') is-invalid @enderror @endif" name="password" autocomplete="new-password" placeholder="Minimal 6 Karakter">

                        @if (old("type") == "register")
                        @error('password')
                            <span class="invalid-feedback " role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                        @endif
                    </div>

                    <div class="form-group ">
                        <label for="password-confirm">{{ __('Confirm Password') }}  *</label>
                        
                        <input id="password-confirm" type="password" class=" form-control form-control-sm" name="password_confirmation" autocomplete="new-password" placeholder="Ketik ulang password akun anda">
                    </div> --}}

                    <div class="form-group my-3">
                    <button type="submit" class="btn btn-sm btn-success">
                        {{ __('Simpan') }}
                    </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<input type="hidden" id="jenjang_sekolah" value="{{$web_profil->jenjang_sekolah}}">
@endsection
@section('script')
<script>
    $(document).ready(function(){
    $.get("https://nyala.sochainformatika.com/public/search/provinsi", function(data){
        $.each(data, function( key, value ){
            // console.log(value.name);
            if ($('#provinsi').val() == value.name) {
                $('#provinsi').append("<option value='"+value.name+"' selected>"+value.name+"</option>");
            } else {
                $('#provinsi').append("<option value='"+value.name+"'>"+value.name+"</option>");
            }
        });
    });

    $('#provinsi').select2({
        placeholder: "Pilih Provinsi",
    });

    $('#provinsi').change(function(){
        $('#kota_kabupaten').val(null).trigger('change');
        $('#kota_kabupaten').prop( "disabled", false );

        $('#jenjang_sekolah').val(null).trigger('change');
        $('#jenjang_sekolah').prop( "disabled", true );
        
        $('#alamat').val(null);
        $('#institusi').val(null).trigger('change');
        $('#institusi').prop( "disabled", true );

        let provinsi = $("#provinsi").val();
        var id_pesan = $(this).data("id_pesan");
        var url  = '{{url("toko/update-provinsi")}}/'+id_pesan;
        var token = '{{ csrf_token() }}';
    });

    $('#kota_kabupaten').select2({
        minimumInputLength: 1,
        language: { 
                inputTooShort: function () { 
                    return 'Cari Kota/Kabupaten asal sekolah anda.'; 
                } 
            },
        ajax: {
            url: "https://nyala.sochainformatika.com/public/search/kota_kabupaten",
            data: function (params) {
                return {
                    q: params.term, // search term
                    provinsi: $('#provinsi').val()
                };
            },
            dataType: 'json',
            delay: 250,
            processResults: pResultUpdate,
            cache: true
        },
    });

    let kota_kabupaten_val_update = "";
    if ($('#kota_kabupaten').val() != null) {
        let kota_kabupaten_explode_update = $('#kota_kabupaten').val().split(' ');
        if (kota_kabupaten_explode_update[0] == "KABUPATEN") {
            kota_kabupaten_val_update = "Kab. "+kota_kabupaten_explode_update[1];
        } else {
            kota_kabupaten_val_update = $('#kota_kabupaten').val();
        }
        console.log(kota_kabupaten_val_update);   
    }

    $('#kota_kabupaten').change(function(){
        $('#jenjang_sekolah').val(null).trigger('change');
        $('#jenjang_sekolah').prop( "disabled", false );

        $('#institusi').val(null).trigger('change');
        $('#institusi').prop( "disabled", false );

        kota_kabupaten_val_update = "";
        console.log(kota_kabupaten_val_update);
        if ($(this).val() != null) {
            let kota_kabupaten_explode_update = $(this).val().split(' ');
            if (kota_kabupaten_explode_update[0] == "KABUPATEN") {
                kota_kabupaten_val_update = "Kab. "+kota_kabupaten_explode_update[1];
            } else {
                kota_kabupaten_val_update = $(this).val();
            }
            console.log(kota_kabupaten_val_update);      
        }
    });

    var jenjang = $("#jenjang_sekolah").val();

    $('#institusi').select2({
        minimumInputLength: 1,
        tags: true,
        language: { 
            inputTooShort: function () { 
                return 'Input bila tidak ada dalam pilihan.'; 
            } 
        },
        ajax: {
            url: "https://nyala.sochainformatika.com/public/search/sekolah",
            data: function (params) {
                if (jenjang == 'SLTA') {
                        return {
                            q: params.term, // search term
                            kota_kabupaten: kota_kabupaten_val_update,
                            jenjang_sekolah: 'SMP'
                        };
                    }else if (jenjang == 'SLTP') {
                        return {
                            q: params.term, // search term
                            kota_kabupaten: kota_kabupaten_val_update,
                            jenjang_sekolah: 'SD'
                        };
                    }else{
                        return {
                            q: params.term, // search term
                            kota_kabupaten: kota_kabupaten_val_update,
                            jenjang_sekolah: 'TK'
                        };
                    }
            },
            dataType: 'json',
            delay: 250,
            processResults: pResultUpdate2,
            cache: true
        },
    });

    function pResultUpdate(data) {
        return {
            results: $.map(data, function (item) {
                // console.log(item);
                    return {
                        text: item.name,
                        id: item.name
                    }
            })
        };
    };

    function pResultUpdate2(data) {
        return {
            results: $.map(data, function (item) {
                    return {
                        text: item.sekolah,
                        // id: item.npsn+" - "+item.sekolah
                        id: item.sekolah
                    }
            })
        };
    };
})
</script>
@endsection