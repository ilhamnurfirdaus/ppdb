@extends('layouts.admin')
@section('main')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>
                Daftar Ulang
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
            </div>
        </div>
        <div class="page-title-actions">

        </div>    
    </div>
</div>
<div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="mb-3 card">
            <div class="card-header-tab card-header bg-danger text-white">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    Data Orang Tua
                </div>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ url('siswa/daftar/update-data-ortu/'.$siswa->id) }}">
                    @csrf 

                    <h6><b></b><span class="d-none d-lg-block"><i class="fas fa-user"></i> Data
                        Lengkap Ayah</span></b></h6>

                    <div class="form-group mt-4">
                        <label id="name" class="form-text text-muted">NIK *</label>
                        <input type="text" class="form-control" name="nik_ayah" value="{{ $siswa->nik_ayah }}" aria-describedby="name"
                            placeholder="NIK" required>
                    </div>

                    <div class="form-group">
                        <label id="name" class="form-text text-muted">Nama *</label>
                        <input type="text" class="form-control" name="nama_ayah" value="{{ $siswa->nama_ayah }}" aria-describedby="name"
                            placeholder="Nama" required>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label id="name" class="form-text text-muted">Tempat Lahir *</label>
                            <input type="text" class="form-control" name="tempat_lahir_ayah" value="{{ $siswa->tempat_lahir_ayah }}" aria-describedby="name"
                            placeholder="Tempat Lahir" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label id="name" class="form-text text-muted">Tanggal Lahir *</label>
                            <input type="date" class="form-control" name="tanggal_lahir_ayah" value="{{ Carbon\Carbon::parse($siswa->tanggal_lahir_ayah)->format('Y-m-d') }}" aria-describedby="name"
                                placeholder="Tanggal Lahir" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label id="name" class="form-text text-muted">Pendidikan *</label>
                            <input type="text" class="form-control" name="pendidikan_ayah" value="{{ $siswa->pendidikan_ayah }}" aria-describedby="name"
                            placeholder="Pendidikan Ayah" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label id="name" class="form-text text-muted">Pekerjaan *</label>
                            <input type="text" class="form-control" name="pekerjaan_ayah" value="{{ $siswa->pekerjaan_ayah }}" aria-describedby="name"
                            placeholder="Pekerjaan Ayah" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label id="name" class="form-text text-muted">Penghasilan *</label>
                            <input type="text" class="form-control" name="penghasilan_ayah" value="{{ $siswa->penghasilan_ayah }}" aria-describedby="name"
                            placeholder="Penghasilan Ayah" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label id="name" class="form-text text-muted">No. HP *</label>
                            <input type="number" class="form-control" name="no_hp_ayah" value="{{ $siswa->no_hp_ayah }}" aria-describedby="name"
                            placeholder="No. HP" required>
                        </div>
                    </div>

                    <hr>

                    <h6><b></b><span class="d-none d-lg-block"><i class="fas fa-user"></i> Data
                        Lengkap Ibu</span></b></h6>

                    <div class="form-group mt-4">
                        <label id="name" class="form-text text-muted">NIK *</label>
                        <input type="text" class="form-control" name="nik_ibu" value="{{ $siswa->nik_ibu }}" aria-describedby="name"
                            placeholder="NIK" required>
                    </div>

                    <div class="form-group">
                        <label id="name" class="form-text text-muted">Nama *</label>
                        <input type="text" class="form-control" name="nama_ibu" value="{{ $siswa->nama_ibu }}" aria-describedby="name"
                            placeholder="Nama" required>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label id="name" class="form-text text-muted">Tempat Lahir *</label>
                            <input type="text" class="form-control" name="tempat_lahir_ibu" value="{{ $siswa->tempat_lahir_ibu }}" aria-describedby="name"
                            placeholder="Tempat Lahir" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label id="name" class="form-text text-muted">Tanggal Lahir *</label>
                            <input type="date" class="form-control" name="tanggal_lahir_ibu" value="{{ Carbon\Carbon::parse($siswa->tanggal_lahir_ibu)->format('Y-m-d') }}" aria-describedby="name"
                                placeholder="Tanggal Lahir" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label id="name" class="form-text text-muted">Pendidikan *</label>
                            <input type="text" class="form-control" name="pendidikan_ibu" value="{{ $siswa->pendidikan_ibu }}" aria-describedby="name"
                            placeholder="Pendidikan Ibu" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label id="name" class="form-text text-muted">Pekerjaan *</label>
                            <input type="text" class="form-control" name="pekerjaan_ibu" value="{{ $siswa->pekerjaan_ibu }}" aria-describedby="name"
                            placeholder="Pekerjaan Ibu" required>
                        </div>
                    </div>

                    <div class="form-row">
                        <div class="form-group col-md-6">
                            <label id="name" class="form-text text-muted">Penghasilan *</label>
                            <input type="text" class="form-control" name="penghasilan_ibu" value="{{ $siswa->penghasilan_ibu }}" aria-describedby="name"
                            placeholder="Penghasilan Ibu" required>
                        </div>
                        <div class="form-group col-md-6">
                            <label id="name" class="form-text text-muted">No. HP *</label>
                            <input type="number" class="form-control" name="no_hp_ibu" value="{{ $siswa->no_hp_ibu }}" aria-describedby="name"
                            placeholder="No. HP" required>
                        </div>
                    </div>

                    
                    <div class="form-group my-3">
                    <button type="submit" class="btn btn-success">
                        {{ __('Simpan') }}
                    </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection
@section('script')
    <script>
        $(document).ready(function(){
    //alert("cek2");
        $('#provinsi').select2({
            minimumInputLength: 1,
            ajax: {
                url: "{{ url('search/provinsi') }}",
                dataType: 'json',
                delay: 250,
                processResults: pResult,
                cache: true
            },
        });

        $('#provinsi').change(function(){
            $('#kota_kabupaten').val(null).trigger('change');
            $('#kota_kabupaten').prop( "disabled", false );

            $('#kecamatan').val(null).trigger('change');
            $('#kecamatan').prop( "disabled", true );

            $('#desa_kelurahan').val(null).trigger('change');
            $('#desa_kelurahan').prop( "disabled", true );
        });

        $('#kota_kabupaten').select2({
            minimumInputLength: 1,
            ajax: {
                url: function () {
                    return "{{ url('search/kota_kabupaten') }}/"+$('#provinsi').val().split(" ").join("_");
                },
                dataType: 'json',
                delay: 250,
                processResults: pResult,
                cache: true
            },
        });

        $('#kota_kabupaten').change(function(){
            $('#kecamatan').val(null).trigger('change');
            $('#kecamatan').prop( "disabled", false );

            $('#desa_kelurahan').val(null).trigger('change');
            $('#desa_kelurahan').prop( "disabled", true );
        });

        $('#kecamatan').select2({
            minimumInputLength: 1,
            ajax: {
                url: function () {
                    return "{{ url('search/kecamatan') }}/"+$('#kota_kabupaten').val().split(" ").join("_");
                },
                dataType: 'json',
                delay: 250,
                processResults: pResult,
                cache: true
            },
        });

        $('#kecamatan').change(function(){
            $('#desa_kelurahan').val(null).trigger('change');
            $('#desa_kelurahan').prop( "disabled", false );
        });

        $('#desa_kelurahan').select2({
            minimumInputLength: 1,
            ajax: {
                url: function () {
                    return "{{ url('search/desa_kelurahan') }}/"+$('#kecamatan').val().split(" ").join("_");
                },
                dataType: 'json',
                delay: 250,
                processResults: pResult,
                cache: true
            },
        });

        function pResult(data) {
            return {
                results: $.map(data, function (item) {
                    // console.log(item);
                        return {
                            text: item.name,
                            id: item.name
                        }
                })
            };
        }
    })
    </script>
@endsection