    @extends('layouts.admin')
@section('main')
@php
	function rupiah($angka){
		$hasil_rupiah = "Rp " . number_format($angka,0,',','.');
		return $hasil_rupiah;
	
	}
@endphp
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>
                Dashboard
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
            </div>
        </div>
        <div class="page-title-actions">

        </div>    
    </div>
</div>
<div class="row justify-content-center">
    <div class="col-sm-12 col-lg-8">
        <div class="mb-3 card">
            <div class="card-header-tab card-header bg-danger text-white">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    Informasi Gelombang Pendaftaran
                </div>
            </div>
            <div class="card-body">
                @foreach ($gelombang as $key=>$item)
                    {{$key+1}}. {{$item->nama_gelombang}} : {{Carbon\Carbon::parse($item->tanggal_mulai)->isoFormat('D MMMM Y')}} s/d {{Carbon\Carbon::parse($item->tanggal_selesai)->isoFormat('D MMMM Y')}} <br>
                @endforeach
                <br>
                Jumlah Kouta Siswa : {{isset($tahun_ajaran) ? $tahun_ajaran->maksimal_jumlah_siswa : 0}}
            </div>
        </div>
        <div class="mb-3 card">
            <div class="card-header-tab card-header bg-danger text-white">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    Informasi Biaya
                </div>
            </div>
            <div class="card-body">
                @if ($data->jenjang_sekolah == 'SLTA')
                @foreach ($jurusan as $item)
                    <b>{{$item->name}}</b> <br>
                    @foreach ($biaya as $key=>$col)
                        @if ($item->id == $col->program_studi)
                        {{$col->nama_gelombang}} {{$col->jenis_biaya}} :  {{rupiah($col->biaya)}}<br>
                        @endif
                    @endforeach
                @endforeach  
                @else
                    @if ($col->program_studi == 'Default')
                    {{$col->nama_gelombang}} {{$col->jenis_biaya}} :  {{rupiah($col->biaya)}}<br>
                    @endif
                @endif
            </div>
        </div>
        <!-- <div class="mb-3 card">
            <div class="card-header-tab card-header bg-danger text-white">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    Informasi Lain-lain
                </div>
            </div>
            <div class="card-body">
                {!! $data->informasi_pendaftaran !!}
            </div>
        </div> -->
    </div>
    {{-- <div class="col-sm-12 col-lg-8">
        <div class="mb-3 card">
            <div class="card-header-tab card-header bg-danger text-white">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    Informasi Pendaftaran
                </div>
            </div>
            <div class="card-body">
                {!! $data->informasi_pendaftaran !!}
            </div>
        </div>
    </div> --}}
    @php
        $no = 1;
    @endphp
    <div class="col-sm-12 col-lg-4">
        <div class="main-card mb-3 card">
            <div class="card-body">
                <h5 class="card-title">Progress Pendaftaran</h5>
                <div class="vertical-time-icons vertical-timeline vertical-timeline--animate vertical-timeline--one-column">
                    <div class="vertical-timeline-item vertical-timeline-element">
                        <div>
                            <a href="{{ url('siswa/daftar/awal') }}" class="vertical-timeline-element-icon bounce-in">
                                <div class="timeline-icon border-primary bg-primary text-white">
                                    <span style="display: block;font-size: 1.1rem;margin: 0 auto;">{{ $no }}</span>
                                </div>
                            </a>
                            <div class="vertical-timeline-element-content bounce-in">
                                <h4 class="timeline-title">Form Pendaftaran</h4>
                                @if (empty(Auth::user()->siswa->nik))
                                <p><a href="{{ url('siswa/daftar/awal') }}" class="btn btn-rounded btn-danger"><i class="fas fa-times-circle"></i> Belum Selesai</a></p>
                                @else
                                <p><a href="{{ url('siswa/daftar/awal') }}" class="btn btn-rounded btn-success"><i class="fas fa-check"></i> Sudah Selesai </a></p>
                                @endif
                            </div>
                        </div>
                    </div>
                    @php
                        $no++;
                    @endphp
                    <div class="vertical-timeline-item vertical-timeline-element">
                        <div>
                            <a href="{{ url('siswa/berkas/awal') }}" class="vertical-timeline-element-icon bounce-in">
                                <div class="timeline-icon border-primary bg-primary text-white">
                                    <span style="display: block;font-size: 1.1rem;margin: 0 auto;">{{ $no }}</span>
                                </div>
                            </a>
                            <div class="vertical-timeline-element-content bounce-in">
                                <h4 class="timeline-title">Form Berkas Daftar</h4>
                                @if (Auth::user()->siswa->siswa_berkas->count() > 0 || Auth::user()->siswa->siswa_raport->count() > 0 || Auth::user()->siswa->sertifikat->count() > 0)
                                <p><a href="{{ url('siswa/berkas/awal') }}" class="btn btn-rounded btn-success"><i class="fas fa-check"></i> Sudah Selesai </a></p>
                                @else
                                <p><a href="{{ url('siswa/berkas/awal') }}" class="btn btn-rounded btn-danger"><i class="fas fa-times-circle"></i> Belum Selesai</a></p>
                                @endif
                            </div>
                        </div>
                    </div>
                    @php
                        $no++;
                    @endphp
                    <div class="vertical-timeline-item vertical-timeline-element">
                        <div>
                            <a href="{{ url('siswa/bayar-awal') }}" class="vertical-timeline-element-icon bounce-in">
                                <div class="timeline-icon border-primary bg-primary text-white">
                                    <span style="display: block;font-size: 1.1rem;margin: 0 auto;">{{ $no }}</span>
                                </div>
                            </a>
                            <div class="vertical-timeline-element-content bounce-in">
                                <h4 class="timeline-title">Form Pembayaran</h4>
                                @if (Auth::user()->siswa->bayar->where('status', 'Lunas')->count() > 0)
                                <p><a href="{{ url('siswa/bayar-awal') }}" class="btn btn-rounded btn-success"><i class="fas fa-check"></i> Sudah Selesai </a></p>
                                @else
                                <p><a href="{{ url('siswa/bayar-awal') }}" class="btn btn-rounded btn-danger"><i class="fas fa-times-circle"></i> Belum Selesai</a></p>
                                @endif
                            </div>
                        </div>
                    </div>
                    {{-- Hidden Sementara 24/11/2022 --}}
                    {{-- @php
                        $no++;
                    @endphp
                    @if (isset($tahun_ajaran) && $tahun_ajaran->tampil_test == "Ya")
                    <div class="vertical-timeline-item vertical-timeline-element">
                        <div>
                            <a href="{{ url('siswa/soal') }}" class="vertical-timeline-element-icon bounce-in">
                                <div class="timeline-icon border-primary bg-primary text-white">
                                    <span style="display: block;font-size: 1.1rem;margin: 0 auto;">{{ $no }}</span>
                                </div>
                            </a>
                            <div class="vertical-timeline-element-content bounce-in">
                                <h4 class="timeline-title">Form Test Pendaftaran</h4>
                                @if (isset(Auth::user()->siswa->soal_score))
                                <p><a href="{{ url('siswa/soal') }}" class="btn btn-rounded btn-success"><i class="fas fa-check"></i> Sudah Selesai </a></p>
                                @else
                                <p><a href="{{ url('siswa/soal') }}" class="btn btn-rounded btn-danger"><i class="fas fa-times-circle"></i> Belum Selesai</a></p>
                                @endif
                            </div>
                        </div>
                    </div>
                    @php
                        $no++;
                    @endphp
                    @endif
                    <div class="vertical-timeline-item vertical-timeline-element">
                        <div>
                            <a href="{{ url('siswa/seleksi') }}" class="vertical-timeline-element-icon bounce-in">
                                <div class="timeline-icon border-primary bg-primary text-white">
                                    <span style="display: block;font-size: 1.1rem;margin: 0 auto;">{{ $no }}</span>
                                </div>
                            </a>
                            <div class="vertical-timeline-element-content bounce-in">
                                <h4 class="timeline-title">Hasil Seleksi</h4>
                                @if (Auth::user()->siswa->bayar->where('status', 'Lunas')->count() > 0)
                                <p><a href="{{ url('siswa/seleksi') }}" class="btn btn-rounded btn-success"><i class="fas fa-check"></i> Hasil Seleksi Sudah Ada </a></p>
                                @else
                                <p><a href="{{ url('siswa/seleksi') }}" class="btn btn-rounded btn-warning"><i class="fas fa-clock"></i> Tunggu Pembayaran Lunas</a></p>
                                @endif
                            </div>
                        </div>
                    </div> --}}
                    {{-- Hidden Sementara 24/11/2022 --}}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection