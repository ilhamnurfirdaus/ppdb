@extends('layouts.admin')
@section('main')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>
                Test Soal
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
            </div>
        </div>
        @if (Auth::user()->siswa->bayar->where('status', 'Lunas')->count() > 0 && empty(Auth::user()->siswa->cek_soal))
        <div class="page-title-actions">
            <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#mulaiModal">
                Mulai
            </button>
        </div>    
        @endif
    </div>
</div>
@if (auth::user()->siswa->bayar->where('status', 'Lunas')->count() > 0)
@if (Auth::user()->siswa->cek_soal == "Masih Proses")
<div class="row justify-content-center">
    <div class="col-sm-12">
        <div class="mb-3 card">
            <div class="card-body">
                <form id="soal-form" action="{{ url('siswa/soal/selesai/'.$siswa->id) }}" method="POST" enctype="multipart/form-data">
                <table style="width: 100%;" class="table table-bordered table-soal">
                    <thead>
                    <tr>
                        <th>
                            Waktu : <span id="hours"></span> <span id="minutes"></span> <span id="seconds"></span>
                            {{-- <div id="countdown"></div> 
                            <div id="clock">
                                <div class="hours"></div>
                                <div class="minutes"></div>
                                <div class="seconds"></div>
                            </div> --}}
                        </th>
                    </tr>
                    </thead>
                    @csrf
                    <tbody>
                        @foreach ($tests as $key => $item)
                        <tr>
                            <td>
                                <div class="form-group">
                                    <label>{!! $item->pertanyaan !!}</label>
                                    <input type="hidden" name="ids[]" value="{{$item->id}}">
                                    @foreach ($item->testChoice->random($item->testChoice->count()) as $row)
                                    <div class="form-check">
                                        <input class="form-check-input" type="radio" name="test_choice_id_{{$key}}[]" value="{{ $row->id }}">
                                        <label class="form-check-label">{{ $row->name }}</label>
                                    </div>
                                    @endforeach
                                </div>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>
                                <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#submitModal">
                                    Selesai
                                </button>
                            </th>
                        </tr>
                    </tfoot>
                </table>
                </form>
            </div>
        </div>
    </div>
</div>
@elseif (Auth::user()->siswa->cek_soal == "Selesai")

<div class="main-card mb-3 card bg-warning text-white">
    <div class="card-body">
        <strong>Anda Sudah mengerjakan halaman soal ini - </strong> Silahkan cek hasil anda di halaman seleksi.
    </div>
</div>

@endif
@else
<div class="main-card mb-3 card bg-warning text-white">
    <div class="card-body">
        <strong>Maaf, Anda belum bisa mengakses halaman ini - </strong> Silahkan melunasi biaya pendaftaran.
    </div>
</div>
@endif
@endsection

@section('modal')
<!-- Modal -->
<div class="modal fade" id="submitModal" tabindex="-1" role="dialog" aria-labelledby="submitModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="submitModalLabel">Selesai Test</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Apakah anda yakin ingin selesai dalam test? Cek ulang lagi jawaban anda!
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            {{-- <button id="real-submit" type="submit" class="btn btn-success btn-sm" form="soal-form">Simpan</button> --}}
            <button type="button" class="btn btn-success btn-sm btn-submit">Simpan</button>
        </div>
        </div>
    </div>
</div>

<div class="modal fade" id="mulaiModal" tabindex="-1" role="dialog" aria-labelledby="mulaiModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="mulaiModalLabel">Mulai Test</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Apakah anda yakin ingin mulai soal test ini?
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <a href="{{ url('siswa/soal/mulai/'.$siswa->id) }}" class="btn btn-success btn-sm">Ya</a>
        </div>
        </div>
    </div>
</div>
@endsection

@section('script')
@if (auth::user()->siswa->bayar->where('status', 'Lunas')->count() > 0 && Auth::user()->siswa->cek_soal == "Masih Proses")
<script>
    var end = new Date('{{$siswa->end_date}}');
    var _second = 1000;
    var _minute = _second * 60;
    var _hour = _minute * 60;
    var _day = _hour * 24;
    var timer;
    function showRemaining() {
        var now = new Date();
        console.log(now);
        var distance = end - now;
        if (distance < 0) {
            $("#soal-form").submit();
            clearInterval(timer);
            
            return;
        }
        var days = Math.floor(distance / _day);
        var hours = Math.floor((distance % _day) / _hour);
        var minutes = Math.floor((distance % _hour) / _minute);
        var seconds = Math.floor((distance % _minute) / _second);

        $("#hours").html(hours + "<span>Jam</span>");
        $("#minutes").html(minutes + "<span>Menit</span>");
        $("#seconds").html(seconds + "<span>Detik</span>");		
    }
    timer = setInterval(showRemaining, 1000);

    // function makeTimer() {

    //     // var endTime = new Date("29 April 2018 9:56:00 GMT+01:00");	
    //     var endTime = new Date("{{$siswa->end_date}}");			
    //     endTime = (Date.parse(endTime) / 1000);

    //     var now = new Date();
    //     now = (Date.parse(now) / 1000);

    //     var timeLeft = endTime - now;

    //     var days = Math.floor(timeLeft / 86400); 
    //     var hours = Math.floor((timeLeft - (days * 86400)) / 3600);
    //     var minutes = Math.floor((timeLeft - (days * 86400) - (hours * 3600 )) / 60);
    //     var seconds = Math.floor((timeLeft - (days * 86400) - (hours * 3600) - (minutes * 60)));

    //     if (hours < "10") { hours = "0" + hours; }
    //     if (minutes < "10") { minutes = "0" + minutes; }
    //     if (seconds < "10") { seconds = "0" + seconds; }

    //     $("#days").html(days + "<span>Days</span>");
    //     $("#hours").html(hours + "<span>Hours</span>");
    //     $("#minutes").html(minutes + "<span>Minutes</span>");
    //     $("#seconds").html(seconds + "<span>Seconds</span>");		

    // }

    // setInterval(function() { makeTimer(); }, 1000);

    $(document).on('click', '.btn-submit', function() {
        $('#soal-form').submit();
    });
</script>
@endif
@endsection