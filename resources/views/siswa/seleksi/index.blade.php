@extends('layouts.admin')
@section('main')
@php
    function rupiah($num) {
        return 'Rp. '.number_format(intval($num), 0, ',', '.');
    }
@endphp
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>Hasil Seleksi
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
            </div>
        </div>
        <div class="page-title-actions">
            
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>
<div class="main-card mb-3 card">
    <div class="card-body">
        <table style="width: 100%;" class="table table-hover table-striped table-bordered">
            <thead>
            <tr>
                <th>Nama</th>
                <th>Jurusan</th>
                @if (Auth::user()->siswa->bayar->where('status', 'Lunas')->count() > 0)
                <th>Total Nilai</th>
                @endif
                <th>Keterangan</th>
            </tr>
            </thead>
            <tbody>
                @foreach ($siswas as $item)
                <tr>
                    <td>{{$item->user->name}}</td>
                    <td>{{$item->jurusanTo->name}}</th>
                    @if (Auth::user()->siswa->bayar->where('status', 'Lunas')->count() > 0)
                    <td>{{ number_format($item->total_score,2,".",".") }}</td>
                    @endif
                    <td>
                        {{ $item->bayar->where('status', 'Lunas')->count() > 0 ? $item->seleksi : "Belum Lunas"}}
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection

@section('modal')
<script>
    
    $(document).ready(function() {
        $("#jumlah").on('change', function(){
            let att = parseInt($(this).attr("max"));
            let val = parseInt($(this).val());
            if (val > att) {
                $(this).val(att);
            }
        });

        $('.table').DataTable( {
            language: {
                searchPlaceholder: "Cari"
            },
            "order": [[ 2, "desc" ]]
        });
    });
</script>
@endsection