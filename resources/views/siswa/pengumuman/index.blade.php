@extends('layouts.admin')
@section('main')
@php
    function rupiah($num) {
        return 'Rp. '.number_format(intval($num), 0, ',', '.');
    }
@endphp
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div> Pengumuman
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
                {{-- <button type="button" class="btn-shadow ml-3 btn btn-xs btn-success" data-toggle="modal" data-target="#createModal">
                    Create
                </button> --}}
            </div>
        </div>
        <div class="page-title-actions">
            
        </div>    
    </div>
</div>
<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>
<div class="main-card mb-3 card">
    <div class="card-body">
        <div class="vertical-time-simple vertical-without-time vertical-timeline vertical-timeline--one-column">
            {{-- @if ($web_profil->tampil_seleksi == "Ya")
                @if (Auth::user()->siswa->seleksi == "Lolos")
                <div class="vertical-timeline-item dot-warning vertical-timeline-element">
                    <div><span class="vertical-timeline-element-icon bounce-in"></span>
                        <div class="vertical-timeline-element-content bounce-in">
                            <h4 class="timeline-title">Pengumuman Lolos / Tidak Lolos @if (Auth::user()->cek_pengumuman == "1") <span class="badge badge-success">NEW</span> @endif <span class="text-success">{{Carbon\Carbon::parse($row->created_at)->isoFormat('D MMMM Y')}}</span></h4> 
                            {!! $row->deskripsi !!}, at <span class="text-success">{{ \Carbon\Carbon::parse($row->created_at)->diffForHumans() }}</span>
                            <span class="vertical-timeline-element-date"></span>
                        </div>
                    </div>
                </div>
                @else
                @endif
            @endif --}}
            @forelse ($pengumumans as $row)
            <div class="vertical-timeline-item dot-warning vertical-timeline-element">
                <div><span class="vertical-timeline-element-icon bounce-in"></span>
                    <div class="vertical-timeline-element-content bounce-in">
                        <h4 class="timeline-title">{{ $row->judul }} <span class="text-success">{{Carbon\Carbon::parse($row->created_at)->isoFormat('D MMMM Y')}}</span></h4> 
                        {!! $row->deskripsi !!}, at <span class="text-success">{{ \Carbon\Carbon::parse($row->created_at)->diffForHumans() }}</span>
                        <span class="vertical-timeline-element-date"></span>
                    </div>
                </div>
            </div>
            @empty
                <p>Tidak ada Data</p>
            @endforelse

            {{-- <div class="vertical-time-simple vertical-without-time vertical-timeline vertical-timeline--one-column">
                @forelse ($pengumuman as $row)
                <div class="vertical-timeline-item dot-warning vertical-timeline-element">
                    <div><span class="vertical-timeline-element-icon bounce-in"></span>
                        <div class="vertical-timeline-element-content bounce-in">
                            <h4 class="timeline-title">{{ $row->judul }} <a href="" class="mb-2 mr-2 btn btn-primary btn-sm">Open</a> <span class="badge badge-success">NEW</span> at <span class="text-success">{{$row->created_at}}</span> By {{ $row->pemberi_pengumuman }}</h4> 
                            {!! $row->deskripsi !!}, at <span class="text-success">{{ \Carbon\Carbon::parse($row->created_at)->diffForHumans() }}</span>
                            <span class="vertical-timeline-element-date"></span>
                        </div>
                    </div>
                </div>
                @empty
                    <p>Tidak ada Data</p>
                @endforelse
            </div> --}}
        </div>
    </div>
</div>
@endsection

@section('modal')

@endsection