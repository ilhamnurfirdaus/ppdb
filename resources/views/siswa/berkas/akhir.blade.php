@extends('layouts.admin')
@section('main')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>
                Berkas Daftar Ulang
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
            </div>
        </div>
        <div class="page-title-actions">

        </div>    
    </div>
</div>

<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-sm-12 col-lg-12">
        {{-- @if (Auth::user()->siswa->siswa_berkas->where('jenis_berkas', 'Berkas Daftar Ulang')->where('status', 'Valid')->count() < Auth::user()->siswa->siswa_berkas->where('jenis_berkas', 'Berkas Daftar Ulang')->count())
        <div class="mb-3 card bg-warning text-white">
            <div class="card-body">
                <strong>Maaf, Anda belum bisa mengakses halaman ini - </strong> Silahkan lengkapi form Daftar Ulang
            </div>
        </div>
        @else --}}
        <div class="mb-3 card">
            <div class="card-header-tab card-header bg-danger text-white">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    Upload Berkas Daftar Ulang
                </div>
            </div>
            <div class="card-body">
                <form method="POST" action="{{ url('siswa/berkas/update-akhir/'.$siswa->id) }}" enctype="multipart/form-data">
                    @csrf 
                    
                    @php
                        $nama_berkas_array = [];
                    @endphp
                    @foreach ($setting_berkass as $item)
                        @forelse ($siswa->siswa_berkas->where('jenis_berkas', 'Berkas Daftar Ulang') as $col)
                            @if ($item->nama_berkas == $col->name)
                                @if ($item->jurusan_id == $col->siswa->jurusan && $col->jurusan_id == "Sesuai Jurusan Siswa")
                                    <label>{{ $col->name }}</label>
                                    <input type="hidden" name="berkas_id[]" value="{{ $col->siswa_berkas_id }}">
                                    <input type="hidden" name="name[]" value="{{ $col->name }}">
                                    <input type="file" class="dropify" name="file[]" data-max-file-size="1M" data-default-file="{{asset('/penyimpanan/user/siswa/berkas/'.$col->file)}}"/>
                                    <input type="hidden" name="jurusan_id[]" value="Sesuai Jurusan Siswa">
                                    <hr>
                                @elseif ($item->jurusan_id == "Default" && $col->jurusan_id == "Default")
                                    <label>{{ $col->name }}</label>
                                    <input type="hidden" name="berkas_id[]" value="{{ $col->siswa_berkas_id }}">
                                    <input type="hidden" name="name[]" value="{{ $col->name }}">
                                    <input type="file" class="dropify" name="file[]" data-max-file-size="1M" data-default-file="{{asset('/penyimpanan/user/siswa/berkas/'.$col->file)}}"/>
                                    <input type="hidden" name="jurusan_id[]" value="Default">
                                    <hr>
                                @endif
                                @php
                                    array_push($nama_berkas_array, $item->nama_berkas);
                                @endphp
                            @endif
                        @empty
                           
                        @endforelse
                        @if (in_array($item->nama_berkas, $nama_berkas_array) == false)
                            @if ($item->jurusan_id == $siswa->jurusan)
                            <div class="form-group">
                                <label>{{ $item->nama_berkas }}</label>
                                <input type="hidden" name="berkas_id[]" value="Baru">
                                <input type="hidden" name="name[]" value="{{ $item->nama_berkas }}">
                                <input type="file" class="dropify" name="file[]" data-max-file-size="1M" data-default-file="" required/>
                                <input type="hidden" name="jurusan_id[]" value="Sesuai Jurusan Siswa">
                                <hr>
                            </div>
                            @elseif ($item->jurusan_id == "Default")
                            <div class="form-group">
                                <label>{{ $item->nama_berkas }}</label>
                                <input type="hidden" name="berkas_id[]" value="Baru">
                                <input type="hidden" name="name[]" value="{{ $item->nama_berkas }}">
                                <input type="file" class="dropify" name="file[]" data-max-file-size="1M" data-default-file="" required/>
                                <input type="hidden" name="jurusan_id[]" value="Default">
                                <hr>
                            </div>
                            @endif
                        @endif
                    @endforeach

                    <div class="form-group my-3">
                    <button type="submit" class="btn btn-success">
                        {{ __('Simpan') }}
                    </button>
                    </div>

                    <div class="form-group">
                        <hr>
                        <label>Ketentuan</label>
                        <ul>
                            <li>File harus berformat <b>.jpg/.jpeg/.png</b></li>
                            <li>Ukuran file maksimal <b>1 MB</b></li>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
        {{-- @endif --}}
    </div>
</div>
@endsection

@section('script')
    <script>

    $(document).ready(function(){
        
    });
    </script>
@endsection