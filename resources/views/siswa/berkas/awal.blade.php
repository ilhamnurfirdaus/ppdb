@extends('layouts.admin')
@section('main')
<div class="app-page-title">
    <div class="page-title-wrapper">
        <div class="page-title-heading">
            {{-- <div class="page-title-icon">
                <i class="pe-7s-medal icon-gradient bg-tempting-azure"></i>
            </div> --}}
            <div>
                Berkas Daftar
                {{-- <div class="page-title-subheading">Choose between regular React Bootstrap tables or advanced dynamic ones.</div> --}}
            </div>
        </div>
        <div class="page-title-actions">

        </div>    
    </div>
</div>

<div class="row">
    <div class="col-12">
        @if (session('message'))
        <div class="alert alert-success alert-dismissible" role="alert">
            <h4 class="alert-heading">Berhasil!</h4>
            {{ session('message') }}
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        @endif

        @if ($errors->any())
            <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Gagal!</h4>
                @foreach ($errors->all() as $error)
                    <p>{{ $error }}</p>
                @endforeach
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
    </div>
</div>

@if (isset($tahun_ajaran))
@if ($tahun_ajaran->tampil_berkas == "Ya")
<div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="mb-3 card">
            <div class="card-header-tab card-header bg-danger text-white">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    Upload Berkas
                </div>
            </div>
            <div class="card-body table-responsive-sm">
                <form method="POST" action="{{ url('siswa/berkas/update-awal/'.$siswa->id) }}" enctype="multipart/form-data">
                    @csrf 
                    <table style="width: 100%;" class="table table-hover table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Nama Berkas</th>
                            <th>Upload File</th>
                            @if ($siswa->siswa_berkas->where('jenis_berkas', 'Berkas Pendaftaran')->count() > 0)
                            <th>Lihat File</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                            @php
                                $nama_berkas_array = [];
                            @endphp
                            @foreach ($setting_berkass as $item)
                                @forelse ($siswa->siswa_berkas->where('jenis_berkas', 'Berkas Pendaftaran') as $col)
                                    @if ($item->nama_berkas == $col->name)
                                        @if ($item->jurusan_id == $col->siswa->jurusan && $col->jurusan_id == "Sesuai Jurusan Siswa")
                                            <tr>
                                                <td>{{ $col->name }}</td>
                                                <td>
                                                    <input type="file" class="form-control-file" name="file[]">
                                                    {{-- <input type="file" class="dropify" name="file[]" data-max-file-size="1M" data-default-file="{{asset('/penyimpanan/user/siswa/berkas/'.$col->file)}}"/> --}}
                                                    <input type="hidden" name="berkas_id[]" value="{{ $col->siswa_berkas_id }}">
                                                    <input type="hidden" name="name[]" value="{{ $col->name }}">
                                                    <input type="hidden" name="jurusan_id[]" value="Sesuai Jurusan Siswa">
                                                </td>
                                                <td>
                                                    <button type="button" class="btn_gambar_berkas_modal btn btn-xs btn-primary" data-toggle="modal" data-target="#gambarModal" data-remote="{{url('gambar_berkas/'.$col->siswa_berkas_id)}}"><i class="fa fa-eye"></i></button>
                                                </td>
                                            </tr>  
                                        @elseif ($item->jurusan_id == "Default" && $col->jurusan_id == "Default")
                                            <tr>
                                                <td>{{ $col->name }}</td>
                                                <td>
                                                    <input type="file" class="form-control-file" name="file[]">
                                                    <input type="hidden" name="berkas_id[]" value="{{ $col->siswa_berkas_id }}">
                                                    <input type="hidden" name="name[]" value="{{ $col->name }}">
                                                    <input type="hidden" name="jurusan_id[]" value="Default">
                                                </td>
                                                <td>
                                                    <button type="button" class="btn_gambar_berkas_modal btn btn-xs btn-primary" data-toggle="modal" data-target="#gambarModal" data-remote="{{url('gambar_berkas/'.$col->siswa_berkas_id)}}"><i class="fa fa-eye"></i></button>
                                                </td>
                                            </tr>
                                        @endif
                                        @php
                                            array_push($nama_berkas_array, $item->nama_berkas);
                                        @endphp
                                    @endif
                                @empty
                                    
                                @endforelse
                                @if (in_array($item->nama_berkas, $nama_berkas_array) == false)
                                    @if ($item->jurusan_id == $siswa->jurusan)
                                    <tr>
                                        <td>{{ $item->nama_berkas }}</td>
                                        <td>
                                            <input type="file" class="form-control-file" name="file[]" required>
                                            <input type="hidden" name="berkas_id[]" value="Baru">
                                            <input type="hidden" name="name[]" value="{{ $item->nama_berkas }}">
                                            {{-- <input type="file" class="dropify" name="file[]" data-max-file-size="1M" data-default-file="" required/> --}}
                                            <input type="hidden" name="jurusan_id[]" value="Sesuai Jurusan Siswa">
                                        </td>
                                    </tr>
                                    @elseif ($item->jurusan_id == "Default")
                                    <tr>
                                        <td>{{ $item->nama_berkas }}</td>
                                        <td>
                                            <input type="file" class="form-control-file" name="file[]" required>
                                            <input type="hidden" name="berkas_id[]" value="Baru">
                                            <input type="hidden" name="name[]" value="{{ $item->nama_berkas }}">
                                            <input type="hidden" name="jurusan_id[]" value="Default">
                                        </td>
                                    </tr>
                                    @endif
                                @endif
                            @endforeach
                        </tbody>
                    </table>

                    {{-- <div class="form-group">
                        <label>Sertifikat</label>
                        <div class="row mt-3 row-input" style="display: none;">
                            <div class="col-sm-4">
                                <input type="text" class='name form-control' placeholder="Nama">
                            </div>
                            <div class="col-sm-3">
                                <div class="custom-file">
                                    <input type="file" class="file custom-file-input" id="exampleInputFile">
                                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <select class="tingkat form-control">
                                    <option value="">** Pilih Tingkat</option>
                                    <option value="Kecamatan">Kecamatan</option>
                                    <option value="Kelurahan">Kelurahan</option>
                                    <option value="Kota">Kota</option>
                                    <option value="Provinsi">Provinsi</option>
                                </select> --}}
                                {{-- <textarea class='keterangan form-control form-control-sm' rows='3' placeholder="keterangan"></textarea> --}}
                            {{-- </div>
                            <div class="col-sm-1">
                                
                            </div>
                            <div class="col-sm-1">
                                <button class="btn btn-sm btn-secondary btn-delete" type="button" style="display: none;">X</button>
                            </div>
                        </div>
                        <div class="row mt-3 row-add">
                            <div class="col-sm-12">
                                <button type="button" class="btn btn-sm btn-info btn-add">Tambah Sertifikat</button>
                            </div>                        
                        </div>
                    </div> --}}

                    <div class="form-group my-3">
                    <button type="submit" class="btn btn-success">
                        {{ __('Simpan') }}
                    </button>
                    </div>

                    <div class="form-group">
                        <hr>
                        <label>Ketentuan</label>
                        <ul>
                            <li>File harus berformat <b>.jpg/.jpeg/.png</b></li>
                            <li>Ukuran file maksimal <b>1 MB</b></li>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endif

@if ($tahun_ajaran->jml_raport > 0 && $tahun_ajaran->tampil_raport == "Ya")
<div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="mb-3 card">
            <div class="card-header-tab card-header bg-danger text-white">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    Upload Raport
                </div>
            </div>
            <div class="card-body table-responsive-sm">
                <form id="form_upload_raport" method="POST" action="{{ url('siswa/berkas/update-raport/'.$siswa->id) }}" enctype="multipart/form-data">
                    @csrf 
                    <table style="width: 100%;" class="table table-hover table-striped table-bordered">
                        <thead>
                        <tr>
                            <th>Nama Raport</th>
                            <th>Nilai Rata-rata</th>
                            <th>Upload File</th>
                            @if ($siswa->siswa_raport->count() > 0)
                            <th>Lihat File</th>
                            @endif
                        </tr>
                        </thead>
                        <tbody>
                        @if ($web_profil->jenjang_sekolah == "SLTA")
                        <input id="minimal_nilai" type="number" value="{{ Auth::user()->siswa->jurusanTo->minimal_nilai }}" style="display: none;">
                        @else
                        <input id="minimal_nilai" type="number" value="{{ $tahun_ajaran->minimal_nilai }}" style="display: none;">
                        @endif
                            @php
                                $nama_berkas_array = [];
                            @endphp
                            @forelse ($siswa->siswa_raport as $item)
                            <tr>
                                <td>{{ $item->nama }}</td>
                                <td>
                                    {{-- <select class="form-control form-control-sm select2-biasa" name="nilai[]" style="width: 300px;" required>
                                        <option value="">** Pilih Range Nilai</option>
                                        @foreach ($nilai_berkass as $row)
                                        <option value="{{ $row->nilai_berkas_id }}" @if($row->nilai_berkas_id == $item->nilai) selected @endif>{{ $row->min_nilai }} - {{ $row->max_nilai }}</option>
                                        @endforeach
                                    </select> --}}
                                    <input name="nilai[]" type="number" class="form-control form-control-sm nilai_raport" min="0" max="100" step=".01" value="{{$item->nilai}}" required>
                                    <div class="invalid-feedback">
                                        Nilai Anda dibawah syarat nilai!
                                    </div>
                                </td>
                                <td>
                                    <input type="file" class="form-control-file" name="file[]">
                                    <input type="hidden" name="siswa_raport_id[]" value="{{ $item->siswa_raport_id }}">
                                    <input type="hidden" name="nama[]" value="{{ $item->nama }}">
                                </td>
                                <td>
                                    <button type="button" class="btn_gambar_berkas_modal btn btn-xs btn-primary" data-toggle="modal" data-target="#gambarModal" data-remote="{{url('gambar_raport/'.$item->siswa_raport_id)}}"><i class="fa fa-eye"></i></button>
                                </td>
                            </tr>
                            @empty
                                @foreach ($setting_raports as $i=>$item)
                                <tr>
                                    <td>{{ $item->nama_raport }}</td>
                                    <td>
                                        <input name="nilai[]" type="number" class="form-control form-control-sm nilai_raport" min="0" max="100" step=".01" value="{{old("nilai.".$i)}}" required>
                                        <div class="invalid-feedback">
                                            Nilai Anda dibawah syarat nilai!
                                        </div>
                                    </td>
                                    <td>
                                        <input type="file" class="form-control-file" name="file[]" required>
                                        <input type="hidden" name="siswa_raport_id[]" value="Baru">
                                        <input type="hidden" name="nama[]" value="{{ $item->nama_raport }}">
                                    </td>
                                </tr>
                                @endforeach
                                {{-- @for ($i = 0; $i < $tahun_ajaran->jml_raport; $i++)
                                <tr>
                                    <td>Raport Kelas {{ $i+1 }}</td>
                                    <td>
                                        <input name="nilai[]" type="number" class="form-control form-control-sm nilai_raport" min="0" max="100" step=".01" value="{{old("nilai.".$i)}}" required>
                                        <div class="invalid-feedback">
                                            Nilai Anda dibawah syarat nilai!
                                        </div>
                                    </td>
                                    <td>
                                        <input type="file" class="form-control-file" name="file[]" required>
                                        <input type="hidden" name="siswa_raport_id[]" value="Baru">
                                        <input type="hidden" name="nama[]" value="Raport Kelas {{ $i+1 }}">
                                    </td>
                                </tr>
                                @endfor --}}
                            @endforelse
                        </tbody>
                    </table>

                    <div class="form-group my-3">
                    <button id="submit_form_upload_raport" type="submit" class="btn btn-success" style="display: none;">
                        Simpan
                    </button>
                    <button id="button_form_upload_raport" type="button" class="btn btn-success">
                        Simpan
                    </button>
                    </div>

                    <div class="form-group">
                        <hr>
                        <label>Ketentuan</label>
                        <ul>
                            <li>File harus berformat <b>.jpg/.jpeg/.png</b></li>
                            <li>Ukuran file maksimal <b>1 MB</b></li>
                        </ul>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endif

@if ($tahun_ajaran->tampil_sertifikat == "Ya")
<div class="row">
    <div class="col-sm-12 col-lg-12">
        <div class="mb-3 card">
            <div class="card-header-tab card-header bg-danger text-white">
                <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
                    Upload Sertifikat Tambahan

                    {{-- <button type="button" class="btn-shadow ml-3 btn btn-sm btn-success" data-toggle="modal" data-target="#createModal">
                        Tambah
                    </button> --}}
                </div>
            </div>
            <div class="card-body table-responsive-sm">
                <form action="{{url('siswa/berkas/update-sertifikat/'.Auth::user()->siswa->id)}}" method="POST" enctype="multipart/form-data">
                @csrf
                <table style="width: 100%;" class="table table-hover table-striped table-bordered">
                    <thead>
                    <tr>
                        <th rowspan="2" class="text-center" style="vertical-align: middle;">Tanggal Sertifikat</th>
                        <th rowspan="2" class="text-center" style="vertical-align: middle;">Nama Kejuaraan</th>
                        <th rowspan="2" class="text-center" style="vertical-align: middle;">Tingkat</th>
                        <th rowspan="2" class="text-center" style="vertical-align: middle;">Peringkat</th>
                        @if ($siswa->sertifikat->count() > 0)
                        <th colspan="3" class="text-center">Upload Sertifikat</th>
                        @else
                        <th colspan="2" class="text-center">Upload Sertifikat</th>
                        @endif
                        <th rowspan="2" class="text-center" style="vertical-align: middle;">Aksi</th>
                    </tr>
                    <tr>
                        <th class="text-center">File</th>
                        <th class="text-center">Link</th>
                        @if ($siswa->sertifikat->count() > 0)
                        <th class="text-center">Lihat</th>
                        @endif
                    </tr>
                    </thead>
                    <tbody>
                        @forelse (Auth::user()->siswa->sertifikat as $key => $item)
                        {{-- <tr>
                            <td>{{Carbon\Carbon::parse($item->tanggal)->isoFormat('D MMMM Y')}}</td>
                            <td>{{$item->name}}</td>
                            <td>{{$item->tingkat}}</td>
                            <td>{{$item->peringkat}}</td>
                            <td>
                                <button type="button" class="btn_gambar_berkas_modal btn btn-xs btn-primary" data-toggle="modal" data-target="#gambarModal" data-remote="{{url('gambar_sertifikat/'.$item->id)}}"><i class="fa fa-eye"></i></button>
                            </td>
                            <td>
                                <button type="button" class="btn btn-xs btn-success" data-toggle="modal" data-target="#updateModal-{{$item->id}}"><i class="fas fa-pencil-alt"></i></button>
                                <button type="button" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#deleteModal-{{$item->id}}"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr> --}}
                        <tr>
                            <td>
                                <input class="id" type="hidden" name="id[]" value="{{ $item->id }}">
                                <div class="input-group input-group-sm" style="width:120px;">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text open-datetimepicker"><a><i class='fa fa-calendar '></i></a></span>
                                    </div>
                                    <input type="text" name="tanggal[]" class="form-control input_date" value="{{Carbon\Carbon::parse($item->tanggal)->format('Y-m-d')}}" onkeypress="return false;" focusable="false" required>
                                </div>
                            </td>
                            <td>
                                <input name="name[]" type="text" class="form-control form-control-sm" placeholder="Isi nama sertifikat" value="{{$item->name}}" required>
                            </td>
                            <td>
                                <select class="form-control form-control-sm select2-biasa" name="tingkat[]" style="width: 130px;" required>
                                    <option value="">** Pilih Tingkat</option>
                                    @foreach ($tingkats as $col)
                                    <option value="{{ $col->nama_tingkat }}" @if($col->nama_tingkat == $item->tingkat) selected @endif>{{ $col->nama_tingkat }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <select class="form-control form-control-sm select2-biasa" name="peringkat[]" style="width: 50px;" required>
                                    <option value="">No</option>
                                    @foreach ($peringkats as $col)
                                    <option value="{{ $col->peringkat }}" @if($col->peringkat == $item->peringkat) selected @endif>{{ $col->peringkat }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <input type="hidden" class="hidden_file" value="{{ $item->file }}">
                                <div class="form-check">
                                    <input class="form-check-input file_checkbox" type="checkbox" value="File" name="jenis_file[]" @if ("File" == $item->jenis_file) checked @endif>
                                    <label class="form-check-label">
                                        <input type="file" name="file[]" class="form-control-file file" @if("File" == $item->jenis_file) @if (empty($item->file)) required @endif @else readonly style="display: none;" @endif>
                                        <input type="file" class="form-control-file file2" disabled @if("File" == $item->jenis_file) style="display: none;" @else @endif>
                                    </label>
                                </div>
                                {{-- <select class="form-control form-control-sm jenis_file" name="jenis_file[]" style="width: 100px;" required>
                                    <option value="File"  @if("File" == $item->jenis_file) selected @endif>Upload</option>
                                    <option value="Link"  @if("Link" == $item->jenis_file) selected @endif>Link</option>
                                </select> --}}
                            </td>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input link_checkbox" type="checkbox" value="Link" name="jenis_file[]" @if ("Link" == $item->jenis_file) checked @endif>
                                    <label class="form-check-label">
                                        <input name="link[]" type="text" class="form-control form-control-sm link" placeholder="Isi link sertifikat" value="{{$item->keterangan}}" @if("Link" == $item->jenis_file) required @else readonly @endif>
                                    </label>
                                </div>
                            </td>
                            <td>
                                @if ("File" == $item->jenis_file)
                                <button type="button" class="btn_gambar_berkas_modal btn btn-xs btn-primary view_file" data-toggle="modal" data-target="#gambarModal" data-remote="{{url('gambar_sertifikat/'.$item->id)}}"><i class="fa fa-eye"></i></button>
                                @endif
                                @if ("Link" == $item->jenis_file)
                                <a href="//{{ $item->keterangan }}" class="btn btn-xs btn-primary view_link"><i class="fa fa-eye"></i></a>
                                @endif
                            </td>
                            <td>
                                @if ($key != 0)
                                <button type="button" class="btn btn-xs btn-warning btn-delete"><i class="fa fa-trash"></i></button>
                                @endif
                            </td>
                        </tr>
                        @empty
                        <tr>
                            <td>
                                <input type="hidden" name="id[]" value="Baru">
                                <div class="input-group input-group-sm" style="width:120px;">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text open-datetimepicker"><a><i class='fa fa-calendar '></i></a></span>
                                    </div>
                                    <input type="text" name="tanggal[]" class="form-control input_date" value="{{Carbon\Carbon::now()->format('Y-m-d')}}" onkeypress="return false;" focusable="false" required>
                                </div>
                            </td>
                            <td>
                                <input name="name[]" type="text" class="form-control form-control-sm" placeholder="Isi nama sertifikat" required>
                            </td>
                            <td>
                                <select class="form-control form-control-sm select2-biasa" name="tingkat[]" style="width: 130px;" required>
                                    <option value="">** Pilih Tingkat</option>
                                    @foreach ($tingkats as $col)
                                    <option value="{{ $col->nama_tingkat }}">{{ $col->nama_tingkat }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <select class="form-control form-control-sm select2-biasa" name="peringkat[]" style="width: 50px;" required>
                                    <option value="">No</option>
                                    @foreach ($peringkats as $col)
                                    <option value="{{ $col->peringkat }}">{{ $col->peringkat }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input file_checkbox" type="checkbox" value="File" name="jenis_file[]" checked>
                                    <label class="form-check-label">
                                        <input type="file" name="file[]" class="form-control-file file" required>
                                        <input type="file" class="form-control-file file2" disabled style="display: none;">
                                    </label>
                                </div>
                                {{-- <select class="form-control form-control-sm jenis_file" name="jenis_file[]" style="width: 100px;" required>
                                    <option value="File">Upload</option>
                                    <option value="Link" >Link</option>
                                </select> --}}
                            </td>
                            <td>
                                {{-- <input type="file" name="file[]" class="form-control-file file" required>
                                <input name="link[]" type="text" class="form-control form-control-sm link" placeholder="Isi link sertifikat" style="display: none;"> --}}
                                <div class="form-check">
                                    <input class="form-check-input link_checkbox" type="checkbox" value="Link" name="jenis_file[]">
                                    <label class="form-check-label">
                                        <input name="link[]" type="text" class="form-control form-control-sm link" placeholder="Isi link sertifikat" readonly>
                                    </label>
                                </div>
                            </td>
                            {{-- <td>

                            </td> --}}
                            <td>
                                {{-- <button type="button" class="btn btn-xs btn-warning btn-delete"><i class="fa fa-trash"></i></button> --}}
                            </td>
                        </tr>
                        @endforelse
                        @if (old("id") != null)
                            @foreach (old('id') as $key => $item)
                                @if ($item == "Baru")
                                <tr>
                                    <td>
                                        <input class="id" type="hidden" name="id[]" value="Baru">
                                        <div class="input-group input-group-sm" style="width:120px;">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text open-datetimepicker"><a><i class='fa fa-calendar '></i></a></span>
                                            </div>
                                            <input type="text" name="tanggal[]" class="form-control input_date" value="{{Carbon\Carbon::parse(old('tanggal.'.$key))->format('Y-m-d')}}" onkeypress="return false;" focusable="false" required>
                                        </div>
                                    </td>
                                    <td>
                                        <input name="name[]" type="text" class="form-control form-control-sm" placeholder="Isi nama sertifikat" value="{{old('name')[$key]}}" required>
                                    </td>
                                    <td>
                                        <select class="form-control form-control-sm select2-biasa" name="tingkat[]" style="width: 130px;" required>
                                            <option value="">** Pilih Tingkat</option>
                                            @foreach ($tingkats as $col)
                                            <option value="{{ $col->nama_tingkat }}" @if($col->nama_tingkat == old('tingkat.'.$key)) selected @endif>{{ $col->nama_tingkat }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <select class="form-control form-control-sm select2-biasa" name="peringkat[]" style="width: 50px;" required>
                                            <option value="">No</option>
                                            @foreach ($peringkats as $col)
                                            <option value="{{ $col->peringkat }}" @if($col->peringkat == old("peringkat.".$key)) selected @endif>{{ $col->peringkat }}</option>
                                            @endforeach
                                        </select>
                                    </td>
                                    <td>
                                        <div class="form-check">
                                            <input class="form-check-input file_checkbox" type="checkbox" value="File" name="jenis_file[]" @if ("File" == old("jenis_file.".$key)) checked @endif>
                                            <label class="form-check-label">
                                                <input type="file" name="file[]" class="form-control-file file" @if("File" == old("jenis_file.".$key)) required @else readonly style="display: none;" @endif>
                                                <input type="file" class="form-control-file file2" disabled @if("File" == old("jenis_file.".$key)) style="display: none;" @else @endif>
                                            </label>
                                        </div>
                                        {{-- <select class="form-control form-control-sm jenis_file" name="jenis_file[]" style="width: 100px;" required>
                                            <option value="File"  @if("File" == old("jenis_file.".$key)) selected @endif>Upload</option>
                                            <option value="Link"  @if("Link" == old("jenis_file.".$key)) selected @endif>Link</option>
                                        </select> --}}
                                    </td>
                                    <td>
                                        <div class="form-check">
                                            <input class="form-check-input link_checkbox" type="checkbox" value="Link" name="jenis_file[]" @if ("Link" == old("jenis_file.".$key)) checked @endif>
                                            <label class="form-check-label">
                                                <input name="link[]" type="text" class="form-control form-control-sm link" placeholder="Isi link sertifikat" value="{{old("keterangan.".$key)}}" @if("Link" == old("jenis_file.".$key)) required @else readonly @endif>
                                            </label>
                                        </div>
                                    </td>
                                    @if ($siswa->sertifikat->count() > 0)
                                    <td>

                                    </td>
                                    @endif
                                    <td>
                                        <button type="button" class="btn btn-xs btn-warning btn-delete"><i class="fa fa-trash"></i></button>
                                    </td>
                                </tr>
                                @endif
                            @endforeach
                        @endif
                        <tr id="row-input" style="display: none;">
                            <td>
                                <input class="id" type="hidden" value="Baru">
                                <div class="input-group input-group-sm" style="width:120px;">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text open-datetimepicker"><a><i class='fa fa-calendar '></i></a></span>
                                    </div>
                                    <input type="text" class="form-control input_date tanggal" value="{{Carbon\Carbon::now()->format('Y-m-d')}}" onkeypress="return false;" focusable="false">
                                </div>
                            </td>
                            <td>
                                <input type="text" class="form-control form-control-sm name" placeholder="Isi nama sertifikat">
                            </td>
                            <td>
                                <select class="form-control form-control-sm select2-biasa tingkat" style="width: 130px;">
                                    <option value="">** Pilih Tingkat</option>
                                    @foreach ($tingkats as $col)
                                    <option value="{{ $col->nama_tingkat }}">{{ $col->nama_tingkat }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <select class="form-control form-control-sm select2-biasa peringkat" style="width: 50px;">
                                    <option value="">No</option>
                                    @foreach ($peringkats as $col)
                                    <option value="{{ $col->peringkat }}">{{ $col->peringkat }}</option>
                                    @endforeach
                                </select>
                            </td>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input file_checkbox" type="checkbox" value="File" checked>
                                    <label class="form-check-label">
                                        <input type="file" class="form-control-file file">
                                        <input type="file" class="form-control-file file2" disabled style="display: none;">
                                    </label>
                                </div>
                                {{-- <select class="form-control form-control-sm jenis_file" style="width: 100px;">
                                    <option value="File">Upload</option>
                                    <option value="Link" >Link</option>
                                </select> --}}
                            </td>
                            <td>
                                <div class="form-check">
                                    <input class="form-check-input link_checkbox" type="checkbox" value="Link">
                                    <label class="form-check-label">
                                        <input type="text" class="form-control form-control-sm link" placeholder="Isi link sertifikat" readonly>
                                    </label>
                                </div>
                                {{-- <input type="file" class="form-control-file file">
                                <input type="text" class="form-control form-control-sm link" placeholder="Isi link sertifikat" style="display: none;"> --}}
                            </td>
                            @if ($siswa->sertifikat->count() > 0)
                            <td>

                            </td>
                            @endif
                            <td>
                                <button type="button" class="btn btn-xs btn-warning btn-delete"><i class="fa fa-trash"></i></button>
                            </td>
                        </tr>
                    </tbody>
                </table>

                <div class="form-group my-3">
                    <button id="btn-tambah" type="button" class="btn btn-success">
                        {{ __('Tambah') }}
                    </button>
                    <button type="submit" class="btn btn-success">
                        {{ __('Simpan') }}
                    </button>
                </div>
                </form>

                <div class="form-group">
                    <hr>
                    <label>Ketentuan</label>
                    <ul>
                        <li>File harus berformat <b>.jpg/.jpeg/.png</b></li>
                        <li>Ukuran file maksimal <b>512 KB</b></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endif
{{-- @else
<div class="main-card mb-3 card bg-warning text-white">
    <div class="card-body">
        <strong>Maaf, Anda belum bisa mengakses halaman ini - </strong> Silahkan lengkapi form Pembayaran
    </div>
</div> --}}
@endif 
@endsection

@section('modal')
<!-- Image Modal -->

<!-- Modal -->
<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="createModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="createModalLabel">Buat Data Baru</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('siswa/berkas/sertifikat-store')}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type" type="text" value="create" hidden>
            <div class="form-group">
                <label>Tanggal Sertifikat  <span class='text-danger' title='This field is required'>*</span></label>
                {{-- <input name="tanggal" type="date" class="form-control form-control-sm" placeholder="Isi Tanggal Sertifikat" value="{{old('tanggal')}}" required> --}}
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Nama Kejuaraan  <span class='text-danger' title='This field is required'>*</span></label>
                {{-- <input name="name" type="text" class="form-control form-control-sm" placeholder="Isi nama sertifikat" value="{{old('name')}}" required> --}}
                <div class="text-danger"></div>
            </div>
            {{-- <div class="form-group">
                <label>Tingkat Kejuaraan  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="tingkat" type="text" class="form-control form-control-sm" placeholder="Isi Tingkat sertifikat" value="{{old('tingkat')}}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Peringkat  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="peringkat" type="number" class="form-control form-control-sm" placeholder="Isi peringkat sertifikat" value="{{old('peringkat')}}" required>
                <div class="text-danger"></div>
            </div> --}}
            <div class="form-group">
                <label>Status <span class='text-danger' title='This field is required'>*</span></label>
                <select class="form-control form-control-sm select2-create" name="tingkat" style="width: 100%;" required>
                    <option value="">** Pilih Tingkat</option>
                    @foreach ($nilai_sertifikats as $col)
                    {{-- <option value="{{ $col->nilai_sertifikats_id }}" @if($col->nilai_sertifikats_id == old("tingkat")) selected @endif>{{ $col->nama_tingkat }} Peringkat {{ $col->peringkat }}</option> --}}
                    @endforeach
                </select>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>File <span class="text-danger"> *</span>(format: jpeg,png,jpg. maksimal ukuran 512 KB)</label>
                {{-- <div class="custom-file"> --}}
                    <input type="file" name="file" id="" required>
                    {{-- <label class="custom-file-label" for="exampleInputFile">Choose file</label> --}}
                {{-- </div> --}}
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Link Sertifikat</label>
                {{-- <input name="link" type="text" class="form-control form-control-sm" placeholder="Isi link sertifikat" value="{{old('link')}}"> --}}
                <div class="text-danger"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

{{-- @foreach ($sertifikats as $item)
<!-- Image Modal -->
<div class="modal fade" id="sertifikatModal-{{ $item->id }}" tabindex="-1" role="dialog" aria-labelledby="sertifikatModal-{{ $item->id }}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
        <h6 class="modal-title" id="sertifikatModal-{{ $item->id }}Label">Scan File</h6>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
        </button>
        </div>
        <div class="modal-body" style="font-size: 12px;">
            <img class="mx-auto d-block img-fluid" src="{{asset('/penyimpanan/user/siswa/sertifikat-'.Auth::user()->siswa->id.'/'.$item->file)}}" alt="Responsive image">
        </div>
        <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        </div>
    </div>
    </div>
</div>

<div class="modal fade" id="updateModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="updateModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="updateModal-{{$item->id}}Label">Update Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <form action="{{url('siswa/berkas/sertifikat-update/'.$item->id)}}" method="POST" enctype="multipart/form-data">
        @csrf
        <div class="modal-body">
            <input name="type-{{$item->id}}" type="text" value="update" hidden>
            <div class="form-group">
                <label>Nama  <span class='text-danger' title='This field is required'>*</span></label>
                <input name="name" type="text" class="form-control form-control-sm" placeholder="Isi nama sertifikat" value="{{ $item->name }}" required>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>File *(format: jpeg,png,jpg. maksimal ukuran 512 KB)</label>
                <div class="custom-file">
                    <input type="file" name="file" class="custom-file-input" id="exampleInputFile">
                    <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                </div>
                <div class="text-danger"></div>
            </div>
            <div class="form-group">
                <label>Status <span class='text-danger' title='This field is required'>*</span></label>
                <select class="form-control form-control-sm select2-create" name="tingkat" style="width: 100%;" required>
                    <option value="">** Pilih Tingkat</option>
                    @foreach ($nilai_sertifikats as $col)
                    <option value="{{ $col->nilai_sertifikats_id }}" @if($col->nilai_sertifikats_id == $item->tingkat) selected @endif>{{ $col->nama_tingkat }}</option>
                    @endforeach
                </select>
                <div class="text-danger"></div>
            </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <button type="submit" class="btn btn-success btn-sm">Simpan</button>
        </div>
        </form>
        </div>
    </div>
</div>

<div class="modal fade" id="deleteModal-{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModal-{{$item->id}}Label" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h6 class="modal-title" id="deleteModal-{{$item->id}}Label">Hapus Data</h6>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            Apakah anda yakin untuk menghapus data ini? 
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
            <a href="{{url('siswa/berkas/sertifikat-destroy/'.$item->id)}}" class="btn btn-danger btn-sm">Hapus</a>
        </div>
        </div>
    </div>
</div>
@endforeach --}}
@endsection

@section('script')
    <script>
    cekJumlahFile();
    $(document).on('click', '.btn-delete', function()
    { 
        $(this).parent().parent().remove(); 
        cekJumlahFile();
    });

    $(document).on('click', '.file_checkbox', function()
    { 
        let file = $(this).parent().parent().parent().find(".file");
        let file2 = $(this).parent().parent().parent().find(".file2");
        let link = $(this).parent().parent().parent().find(".link");
        let id = $(this).parent().parent().parent().find(".id");

        $(this).parent().parent().parent().find(".link_checkbox").prop('checked', false);
        $(this).prop('checked',true);

        file.prop('required', true);
        file.prop('readonly', false);
        file.show();
        file2.hide();

        link.prop('required', false);
        link.prop('readonly', true);

        if (id.val() != "Baru") {
            let hidden_file = $(this).parent().parent().parent().find(".hidden_file");
            if (hidden_file.val() != "") {
                file.prop('required',false);
            }
        }
    });

    $(document).on('click', '.link_checkbox', function()
    { 
        let file = $(this).parent().parent().parent().find(".file");
        let file2 = $(this).parent().parent().parent().find(".file2");
        let link = $(this).parent().parent().parent().find(".link");

        $(this).parent().parent().parent().find(".file_checkbox").prop('checked',false);
        $(this).prop('checked',true);

        file.prop('required', false);
        file.prop('readonly', true);
        file.hide();
        file2.show();

        link.prop('required', true);
        link.prop('readonly', false);
    });

    $(document).on('change', '.jenis_file', function(){ 
        console.log($(this).val());
        let file = $(this).parent().parent().find(".file");
        let link = $(this).parent().parent().find(".link");
        let id = $(this).parent().parent().find(".id");
        if ($(this).val() == "Link") {
            link.css("display","block");
            link.attr('required',true);
            file.css("display","none");
            file.attr('required',false);
        } else {
            file.css("display","block");
            file.attr('required',true);
            link.css("display","none");
            link.attr('required',false);
            if (id.val() != "Baru") {
                let hidden_file = $(this).parent().parent().find(".hidden_file");
                if (hidden_file.val() != "") {
                    file.attr('required',false);
                }
            }
        }
    });

    $(document).on('change', '.nilai_raport', function(){
        let nilai_raport = parseFloat($(this).val());
        let minimal_nilai = parseFloat($("#minimal_nilai").val());

        console.log(nilai_raport);
        console.log(minimal_nilai);
        console.log(nilai_raport < minimal_nilai);
        if (nilai_raport < minimal_nilai) {
            $("#submit_form_upload_raport").attr('id','submit_form_upload_raport_disabled');
            $(this).addClass('is-invalid');
        } else {
            $("#submit_form_upload_raport_disabled").attr('id','submit_form_upload_raport');
            $(this).removeClass('is-invalid');
        }
    });

    $(document).on('click', '#button_form_upload_raport', function(){
        $("#submit_form_upload_raport").trigger('click');
    });

    $(document).on('click', '#btn-tambah', function()
    {        
        $('#row-input').find(".select2-biasa").select2("destroy");
        let tambah_input = $('#row-input').clone();
        tambah_input.show();
        tambah_input.attr("id", "");
        // tambah_input.find(".btn-delete").show();
        // tambah_input.find(".kode_tugas_verifikator").attr('name','kode_tugas_verifikator[]');
        tambah_input.find(".id").attr('required',true);
        tambah_input.find(".id").attr('name','id[]');
        tambah_input.find(".name").attr('required',true);
        tambah_input.find(".name").attr('name','name[]');
        tambah_input.find(".tanggal").attr('required',true);
        tambah_input.find(".tanggal").attr('name','tanggal[]');
        tambah_input.find(".tingkat").attr('required',true);
        tambah_input.find(".tingkat").attr('name','tingkat[]');
        tambah_input.find(".peringkat").attr('required',true);
        tambah_input.find(".peringkat").attr('name','peringkat[]');
        // tambah_input.find(".jenis_file").attr('required',true);
        // tambah_input.find(".jenis_file").attr('name','jenis_file[]');
        tambah_input.find(".file_checkbox").attr('name','jenis_file[]');
        tambah_input.find(".link_checkbox").attr('name','jenis_file[]');
        tambah_input.find(".file").attr('required',true);
        tambah_input.find(".file").attr('name','file[]');
        tambah_input.find(".link").attr('required',false);
        tambah_input.find(".link").attr('name','link[]');
        tambah_input.insertAfter("#row-input");
        $('.select2-biasa').select2();
        $('.input_date').datepicker({
            format: 'yyyy-mm-dd',
            language: 'id'
        });
        $('.input_date').datepicker('hide');
        $('.open-datetimepicker').click(function () {
            $(this).next('.input_date').datepicker('show');
        });

        cekJumlahFile();
    });

    function cekJumlahFile() {
        let jumlah_class = $('.file').length - 1;
        $(".file").each(function(index) {
            if (jumlah_class != parseInt(index)) {
                console.log(index);
                $(this).attr('name', 'file['+index+']');
                $(this).parent().find(".link").attr('name', 'file['+index+']');
            }
        });
    }
    

    $(document).ready(function(){
        // $('.btn-add').click(function () {
        //     console.log($(this).parent().parent().parent().find('.row-input'));
        //     let tambah_input = $(this).parent().parent().parent().find('.row-input').clone();
        //     tambah_input.show();
        //     tambah_input.removeClass("row-input");
        //     tambah_input.find(".btn-delete").show();
        //     tambah_input.find(".name").attr('required',true);
        //     tambah_input.find(".name").attr('name','name[]');
        //     tambah_input.find(".file").attr('required',true);
        //     tambah_input.find(".file").attr('name','file[]');
        //     tambah_input.find(".tingkat").attr('required',true);
        //     tambah_input.find(".tingkat").attr('name','tingkat[]');
        //     // tambah_input.find(".keterangan").attr('name','keterangan[]');
        //     tambah_input.insertBefore($(this).parent().parent().parent().find(".row-add"));
        // });    
    });
    </script>
@endsection