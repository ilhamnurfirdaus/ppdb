<form action="{{url('siswa/bayar-awal/update/'.$bayar->id)}}" method="POST" enctype="multipart/form-data">
    @csrf
    <div class="modal-body">
        <input name="type-{{$bayar->id}}" type="text" value="update" hidden>
        <div class="form-group">
            <label>Bank Tujuan *</label>
            <select class="form-control form-control-sm select2-update" aria-label="Default select example" name="rekening_id" style="width: 100%;" required>
                @foreach ($rekenings as $row)
                <option value="{{$row->id}}" @if($row->id == $bayar->rekening_id) selected @endif>
                    {{$row->nama_rekening}} No. Rek {{$row->nomor_rekening}} a/n {{$row->atas_nama}}
                </option>
                @endforeach
            </select>
        </div>
        <div class="form-group">
            <label>Bank Pengirim *</label>
            <select id="nama_bank2" class="form-control form-control-sm select2-update" aria-label="Default select example" name="nama_bank" style="width: 100%;" required>
                <option value="">**Pilih rekening bank anda</option>
            </select>
        </div>
        <div class="form-group">
            <label>Nomor Rekening *</label>
            <input id="nomor_rekening" type="number" class="form-control form-control-sm" name="nomor_rekening" placeholder="Isi nomor rekening anda" value="{{$bayar->nomor_rekening}}" required>                
        </div>
        <div class="form-group">
            <label>Atas Nama *</label>
            <input id="atas_nama" type="text" class="form-control form-control-sm" name="atas_nama" value="{{$bayar->atas_nama}}" required>
        </div>
        <div class="form-group">
            <label>Jumlah Bayar</label>
            <input name="jml_byr" type="number" class="jml_byr form-control form-control-sm" value="{{$bayar->jml_byr}}" required>
        </div>
        <div class="form-group">
            <label>3 Digit Kode Referensi *</label>
            <input type="number" class="form-control form-control-sm tiga_digit_angka" name="tiga_digit_angka" value="{{$bayar->tiga_digit_angka}}" maxlength="3" required>
        </div>
        <div class="form-group">
            <label>Jumlah Transfer</label>
            <input type="number" class="form-control form-control-sm jumlah_transfer" value="{{ intval($bayar->jml_byr) + intval($bayar->tiga_digit_angka) }}" disabled>
        </div>
        <div class="form-group">
            <label>Upload Bukti Pembayaran *</label>
            <div class="custom-file">
                <input name="img_bayar" type="file" class="custom-file-input" id="file-field" @if (empty($bayar->img_bayar)) required @endif>
                <label class="custom-file-label" for="file-field">Pilih File</label>
            </div>
        </div>
        <div class="form-group">
            <label>Tanggal Bayar *</label>
            <input type="date" class="form-control form-control-sm" value="{{Carbon\Carbon::parse($bayar->tgl_bayar)->format('Y-m-d')}}" name="tgl_bayar" required>
        </div>
        <div class="form-group">
            <label>Catatan</label>
            <textarea class="form-control form-control-sm" name="catatan" placeholder="Isi catatan pembayaran, tidak wajib diisi" rows="3">{{$bayar->catatan}}</textarea>
        </div>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Tutup</button>
        <button type="submit" class="btn btn-success btn-sm">Simpan</button>
    </div>
</form>

<script>
    $(document).ready(function () {
        $.get("https://nyala.sochainformatika.com/public/search/bank", function(data){
            $.each(data, function( key, value ){
                $('#nama_bank2').append("<option value='"+value.name+"'>"+value.name+"</option>");
            });

            $('.select2-update').select2();
            $('#nama_bank2').val("{{ $bayar->nama_bank }}").trigger('change');
        });

        $(".tiga_digit_angka").on('change', function(){
            let ob = $(this);
            console.log(ob.val());
            let tiga_digit_angka = parseInt(ob.val());
            let jml_byr = parseInt(ob.parent().parent().find('.jml_byr').val());
            ob.parent().parent().find('.jumlah_transfer').val(tiga_digit_angka + jml_byr);
        });

        $(".jml_byr").on('change', function(){
            let ob = $(this);
            let tiga_digit_angka = parseInt(ob.val());
            let jml_byr = parseInt(ob.parent().parent().find('.tiga_digit_angka').val());
            ob.parent().parent().find('.jumlah_transfer').val(tiga_digit_angka + jml_byr);
        });
    });
</script>