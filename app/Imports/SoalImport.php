<?php

namespace App\Imports;

use App\Models\Test;
use App\Models\TestChoice;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Str;

class SoalImport implements ToModel, WithHeadingRow
{
    /**
    * @param Collection $collection
    */
    
    public function model(array $row)
    {
        $soal = new Test();
        $soal->id = Str::uuid();
        $soal->pertanyaan = $row['soal'];
        $soal->save();

        for($i=1;$i<=4;$i++){
            $test_choice = new TestChoice;
            $test_choice->id = Str::uuid();
            $test_choice->test_id = $soal->id;
            $test_choice->name =$row['jawaban_'.$i];
            if($i == $row['kunci_jawaban']){
                $test_choice->score = 1;
            }else{
                $test_choice->score = 0;
            }
            $test_choice->save();
        }
    }
}
