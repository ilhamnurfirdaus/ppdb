<?php

namespace App\Imports;

use App\Models\Bayar;
use App\Models\Notification;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;

class BayarAkhirsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        $bayars = Bayar::select('*')->where('id', $row['id_bayar'])->get();
        if ($bayars->count() > 0 && $row['status'] != "") {
            $bayar = Bayar::find($row['id_bayar']);
            $bayar->status = $row['status'];
            $bayar->keterangan = $row['catatan_admin'];
            $bayar->save();

            if ($bayar->status == "Lunas") {
                $notifikasi = New Notification;
                $notifikasi->id = Str::uuid();
                $notifikasi->user_id = $bayar->siswa->user->id;
                $notifikasi->title = "Pembayaran Daftar Ulang Lunas";
                $notifikasi->description = "Pembayaran anda sudah lunas";
                $notifikasi->url_redirect = "siswa/bayar-akhir";
                $notifikasi->status = "unread";
                $notifikasi->model_id = $bayar->siswa->user->id;
                $notifikasi->nama_model = "Pembayaran Akhir";
                $notifikasi->save();
            } else if ($bayar->status == "Belum Lunas") {
                $notifikasi = New Notification;
                $notifikasi->id = Str::uuid();
                $notifikasi->user_id = $bayar->siswa->user->id;
                $notifikasi->title = "Pembayaran Daftar Ulang Belum Lunas";
                $notifikasi->description = $bayar->keterangan;
                $notifikasi->url_redirect = "siswa/bayar-akhir";
                $notifikasi->status = "unread";
                $notifikasi->model_id = $bayar->siswa->user->id;
                $notifikasi->nama_model = "Pembayaran Akhir";
                $notifikasi->save();
            }    
        }
    }
}
