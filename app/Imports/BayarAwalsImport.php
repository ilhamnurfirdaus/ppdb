<?php

namespace App\Imports;

use App\Models\Bayar;
use App\Models\Notification;
use App\Models\User;
use App\Notifications\KirimNotifikasi;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Maatwebsite\Excel\Imports\HeadingRowFormatter;

HeadingRowFormatter::default('none');

class BayarAwalsImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */

    public function model(array $row)
    {
        
        $bayars = Bayar::select('*')->where('id', $row['ID Bayar'])->get();
        if ($bayars->count() > 0 && $row['Status Pembayaran'] != "") {
            $bayar = Bayar::find($row['ID Bayar']);
            $bayar->status = $row['Status Pembayaran'];
            $bayar->keterangan = $row['Pesan Untuk Pelanggan Jika Pembayaran Belum Lunas'];
            $bayar->save();

            Notification::whereNull('read_at')
            ->where('data', 'like', '%"model_id":"'.$row['ID Bayar'].'"%')->where('data', 'like', '%"nama_model":"Bayar"%')
            ->update(['read_at' => Carbon::now()->format('Y-m-d h:i:s')]);

            if ($bayar->status == "Lunas") {
                $title = "Pembayaran Pendaftaran Lunas";
                $description = "Pembayaran anda sudah lunas, lanjut ke tahap selanjutnya.";
                $url_redirect = "siswa/seleksi";
                $model_id = $bayar->siswa->user->siswa->jurusanTo->id;
                $nama_model = "Berkas Awal";
            } else if ($bayar->status == "Belum Lunas") {
                $title = "Pembayaran Pendaftaran Belum Lunas";
                $description = $bayar->keterangan;
                $url_redirect = "siswa/bayar-awal";
                $model_id = $bayar->siswa->user->siswa->jurusanTo->id;
                $nama_model = "Bayar Awal";
            }

            $toUser = User::find($bayar->siswa->user->id);
            $toUser->notify(new KirimNotifikasi($title, $description, $url_redirect, $model_id, $nama_model));

            // if ($bayar->status == "Lunas") {
            //     $notifikasi = New Notification;
            //     $notifikasi->id = Str::uuid();
            //     $notifikasi->user_id = $bayar->siswa->user->id;
            //     $notifikasi->title = "Pembayaran Pendaftaran Lunas";
            //     $notifikasi->description = "Pembayaran anda sudah lunas, isi berkas untuk melanjutkan pendaftaran.";
            //     $notifikasi->url_redirect = "siswa/berkas/awal";
            //     $notifikasi->status = "unread";
            //     $notifikasi->model_id = $bayar->siswa->user->id;
            //     $notifikasi->nama_model = "Pembayaran Awal";
            //     $notifikasi->save();
            // } else if ($bayar->status == "Belum Lunas") {
            //     $notifikasi = New Notification;
            //     $notifikasi->id = Str::uuid();
            //     $notifikasi->user_id = $bayar->siswa->user->id;
            //     $notifikasi->title = "Pembayaran Pendaftaran Belum Lunas";
            //     $notifikasi->description = $bayar->keterangan;
            //     $notifikasi->url_redirect = "siswa/bayar-awal";
            //     $notifikasi->status = "unread";
            //     $notifikasi->model_id = $bayar->siswa->user->id;
            //     $notifikasi->nama_model = "Pembayaran Awal";
            //     $notifikasi->save();
            // }
        }
    }
}
