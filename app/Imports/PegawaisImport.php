<?php

namespace App\Imports;

use App\Models\Pegawai;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Illuminate\Support\Str;

class PegawaisImport implements ToModel, WithHeadingRow
{
    /**
    * @param array $row
    *
    * @return \Illuminate\Database\Eloquent\Model|null
    */
    public function model(array $row)
    {
        if (isset($row['nama'])) {
            $pegawai = new Pegawai;
            $pegawai->id = Str::uuid();
            $pegawai->tanggal_masuk = $row['tanggal_masuk'];
            $pegawai->nip = $row['nip'];
            $pegawai->nama = $row['nama'];
            $pegawai->tempat_lahir = $row['tempat_lahir'];
            $pegawai->tanggal_lahir = $row['tanggal_lahir'];
            $pegawai->jabatan_id = $row['jabatan'];
            $pegawai->telp = $row['telp'];
            $pegawai->email = $row['email'];
            $pegawai->save();
        }
    }
}
