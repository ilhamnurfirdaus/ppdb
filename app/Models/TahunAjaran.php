<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TahunAjaran extends Model
{
    use HasFactory;

     /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Get the auto-incrementing key type.
     *
     * @return string
     */
    public function getKeyType()
    {
        return 'string';
    }

    protected $primaryKey = 'tahun_ajaran';

    public function gelombang()
    {
    	return $this->hasMany("App\Models\Gelombang", "tahun_ajaran")->orderBy('gelombang_id', 'asc');
    }

    public function jurusan(){
        return $this->hasMany("App\Models\Jurusan", "tahun_ajaran");
    }

    public function biaya(){
        return $this->hasMany("App\Models\Biaya", "tahun_ajaran");
    }

    public function bjs(){
        $bjs = Biaya::select('*')->where("tahun_ajaran", $this->tahun_ajaran)
            ->where('id', 'LIKE', "BJ%") 
            ->orderBy("id", "asc")->get();

        return $bjs;
    }

    public function bls(){
        $bls = Biaya::select('*')->where("tahun_ajaran", $this->tahun_ajaran)
            ->where('id', 'LIKE', "BL%") 
            ->orderBy("id", "asc")->get();

        return $bls;
    }
}
