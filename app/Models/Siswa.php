<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Siswa extends Model
{
    use HasFactory;
    use SoftDeletes;

     /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Get the auto-incrementing key type.
     *
     * @return string
     */
    public function getKeyType()
    {
        return 'string';
    }

    protected $casts=[
        'tagihan'=>'array'
    ];

    public function jurusanTo()
    {
    	return $this->belongsTo("App\Models\Jurusan", "jurusan");
    }

    public function bayar()
    {
    	return $this->hasMany("App\Models\Bayar");
    }

    public function sertifikat()
    {
    	return $this->hasMany("App\Models\Sertifikat");
    }

    public function siswa_berkas()
    {
    	return $this->hasMany("App\Models\SiswaBerkas", "siswa_id");
    }

    public function siswa_raport()
    {
    	return $this->hasMany("App\Models\SiswaRaport", "siswa_id")->orderBy("nama", "asc");
    }

    public function user()
    {
        // return $this->hasOne("App\Models\User", "profil_id", "id");
    	return $this->hasOne("App\Models\User", "profil_id")->where('jenis', 'NonAdmin');
    }

    public function testAnswer()
    {
    	return $this->hasMany("App\Models\TestAnswer");
    }

    public static function seleksi($id){
        $siswa = Siswa::find($id);
        $nilai_sertifikat = 0;
        foreach ($siswa->sertifikat as $key => $value) {
            $nilai_sertifikat += $value->score;
        }

        $nilai_berkas = 0;
        // foreach ($siswa->siswa_berkas as $key => $value) {
        //     if ($value->score != 1) {
        //         $nilai_berkas += $value->nilai_berkas->nilai;
        //     } else {
        //         $nilai_berkas += 1;
        //     }
        // }

        if ($siswa->bayar->where('status', 'Lunas')->count() > 0) {
            # code...
        }
        foreach ($siswa->siswa_raport as $key => $value) {
            $nilai_berkas += $value->nilai;
        }

        if ($siswa->siswa_raport->count() > 0) {
            $nilai_berkas = $nilai_berkas / $siswa->siswa_raport->count();
        }

        $nilai_soal = 0;
        if (isset($siswa->soal_score)) {
            $nilai_soal = $siswa->soal_score;
        }
        
        $nilai_kepsek = 0;
        if (isset($siswa->poin_kepsek)) {
            $nilai_kepsek = $siswa->poin_kepsek;
        }

        $siswa->total_score = $nilai_berkas + $nilai_sertifikat + $nilai_soal + $nilai_kepsek;
        $siswa->save();

        $tahun_ajarans = TahunAjaran::select('*')->get();

        $jurusan = null;
        if ($tahun_ajarans->count() > 0) {
            $jurusan = $tahun_ajarans->last();
            $now = Carbon::now()->isoFormat('Y')."-".Carbon::now()->addYear()->isoFormat('Y');
            if ($tahun_ajarans->where("tahun_ajaran", $now)->count() > 0) {
                $jurusan = TahunAjaran::find($now);
            }
        }

        // $jurusan = WebProfil::all()->first();
        if ($siswa->jurusan != "Default") {
            $jurusan = $siswa->jurusanTo;
        }

        if (isset($jurusan)) {
            $siswas = Siswa::select('*')->whereHas('bayar', function ($q){
                $q->where('status', 'Lunas');
            })->where('jurusan', $siswa->jurusan)->get();
    
            $sisa = 0;
            if ($jurusan->maksimal_jumlah_siswa <= $siswas->count()) {
                $sisa = $siswas->count() - $jurusan->maksimal_jumlah_siswa;
            }
    
            Siswa::whereHas('bayar', function ($q){
                $q->where('status', 'Lunas');
            })->where('jurusan', $siswa->jurusan)->orderBy('total_score', 'desc')->take($jurusan->maksimal_jumlah_siswa)->update(['seleksi' => 'Lolos']);
            Siswa::whereHas('bayar', function ($q){
                $q->where('status', 'Lunas');
            })->where('jurusan', $siswa->jurusan)->orderBy('total_score', 'asc')->take($sisa)->update(['seleksi' => 'Tidak Lolos']);
    
            $siswa->seleksi = 'Tidak Lolos';
            if ($siswa->total_score >= $jurusan->minimal_nilai) {
                $siswa->seleksi = 'Lolos';
            }   
        }

        return $siswa->save();
    }
}
