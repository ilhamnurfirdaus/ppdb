<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;
    use SoftDeletes;

     /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Get the auto-incrementing key type.
     *
     * @return string
     */
    public function getKeyType()
    {
        return 'string';
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name',
        'email',
        'password',
        'profil_id',
        'jenis',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // public function profil()
    // {
    // 	return $this->belongsTo("App\Models\Pegawai", "profil_id", "id");
    // }

    public function pegawai()
    {
        // $pegawai = User::select('*')->where('jenis', 'Admin')->profil->get()->first();
        // return $pegawai;
    	return $this->belongsTo("App\Models\Pegawai", "profil_id");
    }

    public function level()
    {
    	return $this->belongsTo("App\Models\Level", "level_id");
    }

    public function siswa()
    {
        // $pegawai = User::select('*')->where('jenis', 'NonAdmin')->profil->get()->first();
        // return $pegawai;
    	return $this->belongsTo("App\Models\Siswa", "profil_id");
    }
}
