<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Test extends Model
{
    use HasFactory;
    use SoftDeletes;

     /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Get the auto-incrementing key type.
     *
     * @return string
     */
    public function getKeyType()
    {
        return 'string';
    }

    public function jurusanTo()
    {
    	return $this->belongsTo("App\Models\Jurusan", "jurusan");
    }

    public function testAnswer()
    {
    	return $this->hasMany("App\Models\TestAnswer");
    }

    public function testChoice()
    {
    	return $this->hasMany("App\Models\TestChoice");
    }

    public function testChoiceAdmin()
    {
    	return $this->hasMany("App\Models\TestChoice")->orderBy("name", "asc");
    }
}
