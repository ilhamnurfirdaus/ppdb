<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TestAnswer extends Model
{
    use HasFactory;
    use SoftDeletes;

     /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Get the auto-incrementing key type.
     *
     * @return string
     */
    public function getKeyType()
    {
        return 'string';
    }

    public function siswa()
    {
    	return $this->belongsTo("App\Models\Siswa", "siswa_id");
    }

    public function test()
    {
    	return $this->belongsTo("App\Models\Test", "test_id");
    }

    public function testChoice()
    {
    	return $this->belongsTo("App\Models\TestChoice", "test_choice_id");
    }

    
}
