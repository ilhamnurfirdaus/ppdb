<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Biaya extends Model
{
    use HasFactory;
    // use SoftDeletes;

     /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Get the auto-incrementing key type.
     *
     * @return string
     */
    public function getKeyType()
    {
        return 'string';
    }

    public function jurusanTo()
    {
    	return $this->belongsTo("App\Models\Jurusan", "program_studi");
    }

    public function gelombang(){
        $gelombang = Gelombang::select("*")->where("tahun_ajaran", $this->tahun_ajaran)->where("nama_gelombang", $this->nama_gelombang)->get()->first();

        return $gelombang;
    }

    // public function jurusan_awal()
    // {
    // 	return $this->where('jenis_biaya', 'Biaya Pendaftaran')->belongsTo("App\Models\Jurusan", "program_studi", "id");
    // }

    // public function jurusan_ulang()
    // {
    // 	return $this->where('jenis_biaya', 'Biaya Daftar Ulang')->belongsTo("App\Models\Jurusan", "program_studi", "id");
    // }
}
