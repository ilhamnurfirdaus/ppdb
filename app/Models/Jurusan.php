<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Jurusan extends Model
{
    use HasFactory;
    use SoftDeletes;

     /**
     * Get the value indicating whether the IDs are incrementing.
     *
     * @return bool
     */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Get the auto-incrementing key type.
     *
     * @return string
     */
    public function getKeyType()
    {
        return 'string';
    }

    public function siswa()
    {
        return $this->hasMany("App\Models\Siswa", 'jurusan');
    }

    // public function biaya()
    // {
    //     return $this->hasMany("App\Models\Biaya");
    // }

    public function biayaAwal()
    {
        return $this->hasOne("App\Models\Biaya", "program_studi")->where('jenis_biaya', 'Biaya Pendaftaran');
    }

    public function biayaAkhir()
    {
        return $this->hasOne("App\Models\Biaya", "program_studi")->where('jenis_biaya', 'Biaya Daftar Ulang');
    }

    public function settingBerkas()
    {
    	return $this->hasMany("App\Models\SettingBerkas", "jurusan_id");
    }

    public function biaya()
    {
    	return $this->hasMany("App\Models\Biaya", "program_studi");
    }

    public function delete()
    {
        // delete all related photos 
        $this->settingBerkas()->delete();
        // as suggested by Dirk in comment,
        // it's an uglier alternative, but faster
        // Photo::where("user_id", $this->id)->delete()

        // delete the user
        return parent::delete();
    }

    public function tahunAjaran()
    {
    	return $this->belongsTo("App\Models\TahunAjaran", "tahun_ajaran");
    }
}
