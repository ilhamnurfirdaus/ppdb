<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PeriodeBayar extends Model
{
    use HasFactory;

    protected $primaryKey = 'periode_bayar_id';
    public function getKeyType(){
        return 'string';
    }
}
