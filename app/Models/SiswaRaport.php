<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SiswaRaport extends Model
{
    use HasFactory;

    /**
    * Get the value indicating whether the IDs are incrementing.
    *
    * @return bool
    */
    public function getIncrementing()
    {
        return false;
    }

    /**
     * Get the auto-incrementing key type.
     *
     * @return string
     */
    public function getKeyType()
    {
        return 'string';
    }

    protected $primaryKey = 'siswa_raport_id';

    public function siswa()
    {
    	return $this->belongsTo("App\Models\Siswa", "siswa_id");
    }

    public function nilai_berkas()
    {
    	return $this->belongsTo("App\Models\NilaiBerkas", "nilai");
    }
}
