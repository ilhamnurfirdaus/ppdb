<?php

namespace App\Exports;

use App\Models\Bayar;
use App\Models\WebProfil;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;

class BayarAkhirsExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */

    public function view(): View
    {
        $web_profils = WebProfil::all();
        $web_profil = WebProfil::find($web_profils[0]->id);

        $bayars = Bayar::select('*')->whereNotNull('img_bayar')->where('jenis_biaya', 'Biaya Daftar Ulang');

        $bayars = $bayars->where('status', 'Masih Proses');

        if ($web_profil->jenjang_sekolah == "SMK") {
            $bayars = $bayars->whereHas('siswa', function ($q) {
                        $q->where('jurusan', '!=', 'Default');
                    });
        } else {
            $bayars = $bayars->whereHas('siswa', function ($q) {
                $q->where('jurusan', 'Default');
            });
        }

        $bayars = $bayars->get();

        return view('admin.bayar.excel-export', compact('bayars','web_profil'));
    }
}
