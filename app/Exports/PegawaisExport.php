<?php

namespace App\Exports;

use App\Models\Jabatan;
use App\Models\Pegawai;
use Maatwebsite\Excel\Concerns\FromCollection;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class PegawaisExport implements FromView
{
    /**
    * @return \Illuminate\Support\Collection
    */
    // public function collection()
    // {
    //     return Pegawai::all();
    // }

    public function view(): View
    {
        $jabatans = Jabatan::all();

        return view('admin.pegawai.export-excel', [
            'jabatans' => $jabatans
        ]);
    }
}
