<?php

namespace App\Exports;

use Illuminate\Contracts\View\View;
use Illuminate\Support\Facades\Auth;
use Maatwebsite\Excel\Concerns\Exportable;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Maatwebsite\Excel\Concerns\RegistersEventListeners;
use Maatwebsite\Excel\Concerns\WithCustomValueBinder;
use Maatwebsite\Excel\Concerns\WithEvents;
use Maatwebsite\Excel\Concerns\WithStyles;
use Maatwebsite\Excel\Events\AfterSheet;
use Maatwebsite\Excel\Events\BeforeExport;
use Maatwebsite\Excel\Events\BeforeSheet;
use Maatwebsite\Excel\Events\BeforeWriting;
use PhpOffice\PhpSpreadsheet\Cell\Cell;
use PhpOffice\PhpSpreadsheet\Cell\DataType;
use PhpOffice\PhpSpreadsheet\Cell\DefaultValueBinder;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class TemplatesExport implements FromView, WithStyles, WithEvents
{
    public function __construct($return, $style = [], $fixed_header = null)
    {
        $this->return = $return;
        $this->style = $style;
        $this->fixed_header = $fixed_header;
    }

    public function view(): View {
        $return = $this->return;

        return $return;
    }

    public function registerEvents(): array
    {
        return [
            BeforeExport::class => function(BeforeExport $event) {
                
            },
            BeforeWriting::class => function(BeforeWriting $event) {
                
            },
            BeforeSheet::class => function(BeforeSheet $event) {
                
            },
            AfterSheet::class => function(AfterSheet $event) {
                $workSheet = $event->sheet->getDelegate();
                if (isset($this->fixed_header)) {
                    $workSheet->freezePane('A'.$this->fixed_header);
                }
            },
        ];
    }

    public function styles(Worksheet $sheet)
    {
        return $this->style;
    }
}
