<?php

namespace App\Exports;

use App\Models\Bayar;
use App\Models\TahunAjaran;
use App\Models\WebProfil;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;

class BayarAwalsExport implements FromView
{
    public function __construct($status, $tahun_ajaran)
    {
        $this->status = $status;
        $this->tahun_ajaran = $tahun_ajaran;
    }
    
    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        $tahun_ajarans = TahunAjaran::all();

        $tahun_ajaran = null;
        if ($tahun_ajarans->count() > 0) {
            $tahun_ajaran = $tahun_ajarans->last();
            $now = Carbon::now()->isoFormat('Y')."-".Carbon::now()->addYear()->isoFormat('Y');
            if ($tahun_ajarans->where("tahun_ajaran", $now)->count() > 0) {
                $tahun_ajaran = TahunAjaran::find($now);
            }
        }

        $web_profils = WebProfil::all();
        $web_profil = WebProfil::find($web_profils[0]->id);        

        $bayars = Bayar::select('*')->where('jenis_biaya', 'Biaya Pendaftaran');

        if ($this->status) {
            if ($this->status == "Lunas") {
                $bayars = $bayars->where('status', $this->status);
            } else if ($this->status != "Lunas") {
                $bayars = $bayars->where('status', '!=', "Lunas");
            }
        }

        if ($this->tahun_ajaran) {
            $bayars = $bayars->whereHas('siswa', function ($q) {
                $q->where('tahun_ajaran', $this->tahun_ajaran);
            });
        } else {
            $bayars = $bayars->whereHas('siswa', function ($q) use($tahun_ajaran) {
                $q->where('tahun_ajaran', $tahun_ajaran->tahun_ajaran);
            });
        }
        $bayars = $bayars->get();

        return view('admin.bayar.excel-export', [
            'bayars' => $bayars,
            'web_profil' => $web_profil
        ]);
    }
}
