<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use App\Models\Siswa;
use App\Models\TahunAjaran;
use App\Models\WebProfil;
use Carbon\Carbon;
use Illuminate\Contracts\View\View;
use Maatwebsite\Excel\Concerns\FromView;

class SiswaExport implements FromView
{
    public function __construct($status, $tahun_ajaran)
    {
        $this->status = $status;
        $this->tahun_ajaran = $tahun_ajaran;
    }

    /**
    * @return \Illuminate\Support\Collection
    */
    public function view(): View
    {
        //
        $web_profils = WebProfil::all();
        $web_profil = WebProfil::find($web_profils[0]->id);
        
        $tahun_ajarans = TahunAjaran::all();

        $tahun_ajaran = null;
        if ($tahun_ajarans->count() > 0) {
            $tahun_ajaran = $tahun_ajarans->last();
            $now = Carbon::now()->isoFormat('Y')."-".Carbon::now()->addYear()->isoFormat('Y');
            if ($tahun_ajarans->where("tahun_ajaran", $now)->count() > 0) {
                $tahun_ajaran = TahunAjaran::find($now);
            }
        }

        $siswas = Siswa::select('*');

        if ($this->status) {
            $siswas = $siswas->whereHas('bayar', function($q){
                $q->where('status', 'Lunas');
            }, '>', 0)->where('seleksi', $this->status);
        }

        if ($this->tahun_ajaran) {
            $siswas = $siswas->where('tahun_ajaran', $this->tahun_ajaran);
        } else {
            $siswas = $siswas->where('tahun_ajaran', $tahun_ajaran->tahun_ajaran);
        }

        $siswas = $siswas->get();        

        return view('admin.bayar.siswa_export', compact('siswas'));
    }
}
