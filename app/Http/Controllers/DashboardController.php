<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Siswa;
use App\Models\Jurusan;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class DashboardController extends Controller
{
    public function index(Request $request){
        // if (Session::has('access_token')) {
        //     $response = Http::post('http://127.0.0.1:9000/api/auth/me', [
        //         'token' => Session::get('access_token'),
        //     ]);
        //     dd($response->json());
        // }
        $siswa = Siswa::groupBy('asal_sekolah')->select('asal_sekolah')->get();
        $siswa_all = Siswa::select('*')->get();
        $jurusan = Jurusan::select('*')->get();
        $jk['lk'] = Siswa::where('jenis_kelamin', 'Laki-laki')->count();
        $jk['pr'] = Siswa::where('jenis_kelamin', 'Perempuan')->count();
        // dd();
        
        return view('admin.dashboard.index',$jk, compact('siswa','siswa_all','jurusan'));
    }
}
