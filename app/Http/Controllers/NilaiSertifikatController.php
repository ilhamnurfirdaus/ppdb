<?php

namespace App\Http\Controllers;

use App\Models\NilaiSertifikat;
use Illuminate\Http\Request;

class NilaiSertifikatController extends Controller
{
    public function index(Request $request){
        $nilai_sertifikats = NilaiSertifikat::select('*')->get();

        return view('admin.nilai_sertifikat.index', compact('nilai_sertifikats'));
    }

    public function update(Request $request, $id){
        $nilai_sertifikat = NilaiSertifikat::find($id);
        $nilai_sertifikat->nilai = $request->nilai;

        $nilai_sertifikat->save();

        return back()->with('message','Data Berhasil Diupdate');
    }
}
