<?php

namespace App\Http\Controllers;

use App\Exports\TemplatesExport;
use App\Models\Jurusan;
use App\Models\Notification;
use App\Models\Sertifikat;
use App\Models\SettingBerkas;
use App\Models\Siswa;
use App\Models\SiswaBerkas;
use App\Models\TahunAjaran;
use App\Models\WebProfil;
use Barryvdh\DomPDF\Facade;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;

class BerkassController extends Controller
{
    public function awal_index(Request $request){
        $web_profils = WebProfil::all();
        $web_profil = WebProfil::find($web_profils[0]->id);
        
        $tahun_ajarans = TahunAjaran::all();

        $tahun_ajaran = null;
        if ($tahun_ajarans->count() > 0) {
            $tahun_ajaran = $tahun_ajarans->last();
            $now = Carbon::now()->isoFormat('Y')."-".Carbon::now()->addYear()->isoFormat('Y');
            if ($tahun_ajarans->where("tahun_ajaran", $now)->count() > 0) {
                $tahun_ajaran = TahunAjaran::find($now);
            }
        }

        $siswas = Siswa::select('*');

        $siswas = $this->queryControllerKastem($siswas, $tahun_ajaran);

        $siswas = $siswas->paginate(10);

        $sertifikats = Sertifikat::select('*')->get();

        return view('admin.berkas.awal_index', compact('siswas', 'sertifikats', 'web_profil', 'tahun_ajarans', 'tahun_ajaran'));
    }

    public function awal_seleksi_index($id){
        $siswa = Siswa::find($id);
        $setting_berkass = SettingBerkas::select('*')->where('jenis_berkas', 'Berkas Pendaftaran')->get();

        return view('admin.berkas.awal_seleksi', compact('siswa', 'setting_berkass'));
    }

    public function awal_seleksi_post(Request $request, $id){
        $siswa = Siswa::find($id);
        // $siswa->nilai_kk = $request->nilai_kk;
        // $siswa->nilai_rapor_kelas_4 = $request->nilai_rapor_kelas_4;
        // $siswa->nilai_rapor_kelas_5 = $request->nilai_rapor_kelas_5;
        // $siswa->nilai_rapor_kelas_6 = $request->nilai_rapor_kelas_6;

        $nilai_berkas = 0;
        foreach ($request->berkas_id as $key => $value) {
            $siswa_berkas = SiswaBerkas::find($value);
            $siswa_berkas->score = $request->berkas_score[$key];
            $siswa_berkas->save();

            $nilai_berkas += $siswa_berkas->score;
        }

        $nilai_sertifikat = 0;
        if (isset($request->sertifikat_id)) {
            foreach ($request->sertifikat_id as $key => $value) {
                $sertifikat = Sertifikat::find($value);
                $sertifikat->score = $request->score[$key];
                $sertifikat->save();

                $nilai_sertifikat += $sertifikat->score;
            }   
        }

        $siswa->berkas_score = $nilai_berkas + $nilai_sertifikat;
        if ($siswa->jenis_kepsek == "Tidak") {
            $siswa->poin_kepsek = $siswa->soal_score + $siswa->berkas_score;
        }
        $siswa->save();

        $web_profils = WebProfil::all();
        $web_profil = WebProfil::find($web_profils[0]->id);

        $siswas = Siswa::select('*')->get();
        $sisa = $siswas->count() - $web_profil->jumlah_lolos;
        if (isset($web_profil->jumlah_lolos)) {
            Siswa::orderBy('poin_kepsek', 'desc')->take($web_profil->jumlah_lolos)->update(['seleksi' => 'Lolos']);
            Siswa::orderBy('poin_kepsek', 'asc')->take($sisa)->update(['seleksi' => 'Tidak Lolos']);
        }

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function akhir_index(Request $request){
        $web_profils = WebProfil::all();
        $web_profil = WebProfil::find($web_profils[0]->id);
        
        $siswas = Siswa::select('*');

        // if ($web_profil->jenjang_sekolah == "SLTA") {
        //     $siswas = $siswas->where('jurusan', '!=', 'Default');
        // } else {
        //     $siswas = $siswas->where('jurusan', 'Default');
        // }

        $siswas = $siswas->get();

        return view('admin.berkas.akhir_index', compact('siswas', 'web_profil'));
    }

    public function akhir_seleksi_index($id){
        $siswa = Siswa::find($id);
        $setting_berkass = SettingBerkas::select('*')->where('jenis_berkas', 'Berkas Daftar Ulang')->get();

        return view('admin.berkas.akhir_seleksi', compact('siswa', 'setting_berkass'));
    }

    public function akhir_seleksi_post(Request $request, $siswa_berkas_id){
        $siswa_berkas = SiswaBerkas::find($siswa_berkas_id);
        $siswa_berkas->status = $request->status;
        $siswa_berkas->catatan_admin = $request->catatan_admin;
        $siswa_berkas->save();

        if ($siswa_berkas->siswa->siswa_berkas->where('jenis_berkas', 'Berkas Daftar Ulang')->where('status', 'Valid')->count() == $siswa_berkas->siswa->siswa_berkas->where('jenis_berkas', 'Berkas Daftar Ulang')->count()) {
            $notifikasi = New Notification;
            $notifikasi->id = Str::uuid();
            $notifikasi->user_id = $siswa_berkas->siswa->user->id;
            $notifikasi->title = "Berkas Daftar Ulang Valid";
            $notifikasi->description = "Berkas anda Sudah Valid semua. silahkan lanjutkan pembayaran uang gedung";
            $notifikasi->url_redirect = "siswa/bayar-akhir";
            $notifikasi->status = "unread";
            $notifikasi->model_id = $siswa_berkas->siswa->user->id;
            $notifikasi->nama_model = "Berkas Akhir";
            $notifikasi->save();
        }

        if ($siswa_berkas->status == "Valid") {
            // $notifikasi = New Notification;
            // $notifikasi->id = Str::uuid();
            // $notifikasi->user_id = $siswa_berkas->siswa->user->id;
            // $notifikasi->title = "Berkas Daftar Ulang Valid";
            // $notifikasi->description = "Berkas ".$siswa_berkas->name." Sudah Valid.";
            // $notifikasi->url_redirect = "siswa/bayar-akhir";
            // $notifikasi->status = "unread";
            // $notifikasi->model_id = $siswa_berkas->siswa->user->id;
            // $notifikasi->nama_model = "Berkas Akhir";
            // $notifikasi->save();
        } else if ($siswa_berkas->status == "Belum Valid") {
            $notifikasi = New Notification;
            $notifikasi->id = Str::uuid();
            $notifikasi->user_id = $siswa_berkas->siswa->user->id;
            $notifikasi->title = "Berkas Daftar Ulang Belum Valid";
            $notifikasi->description = $siswa_berkas->catatan_admin;
            $notifikasi->url_redirect = "siswa/berkas/akhir";
            $notifikasi->status = "unread";
            $notifikasi->model_id = $siswa_berkas->siswa->user->id;
            $notifikasi->nama_model = "Berkas Akhir";
            $notifikasi->save();
        }

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function grafik(Request $request){
        $tahun_ajarans = TahunAjaran::all();

        $tahun_ajaran = null;
        if ($tahun_ajarans->count() > 0) {
            $tahun_ajaran = $tahun_ajarans->last();
            $now = Carbon::now()->isoFormat('Y')."-".Carbon::now()->addYear()->isoFormat('Y');
            if ($tahun_ajarans->where("tahun_ajaran", $now)->count() > 0) {
                $tahun_ajaran = TahunAjaran::find($now);
            }
        }

        $siswas = Siswa::select('*');
        $jurusans = Jurusan::select('*');

        if ($request->tahun_ajaran) {
            $siswas = $siswas->where('tahun_ajaran', $request->tahun_ajaran);
            $jurusans = $jurusans->where('tahun_ajaran', $request->tahun_ajaran);
        } else {
            $siswas = $siswas->where('tahun_ajaran', $tahun_ajaran->tahun_ajaran);
            $jurusans = $jurusans->where('tahun_ajaran', $tahun_ajaran->tahun_ajaran);
        }

        $siswas = $siswas->get();
        $jurusans = $jurusans->get();

        $jumlah_daftar = 0;
        $jumlah_lolos = 0;
        $jumlah_gagal = 0;

        foreach ($jurusans as $key => $row) {
            if ($siswas->count() > 0) {
                $daftar_count = 0;
                $lolos_count = 0;
                $gagal_count = 0;
                foreach ($siswas->where('jurusan', $row->id) as $key => $value) {
                    $daftar_count += 1;
                    if ($value->bayar->where('status', "Lunas")->count() > 0) {
                        $lolos_count += 1;
                    } else {
                        $gagal_count += 1;
                    }
                }
                $data['siswas'][]= [
                    'jurusan'  => $row->name,
                    'daftar'  => $daftar_count,
                    'lolos'  => $lolos_count,
                    'gagal'  => $gagal_count,
                ];
            } else {
                $daftar_count = 0;
                $lolos_count = 0;
                $gagal_count = 0;
                $data['siswas'][]= [
                    'jurusan'  => $row->name,
                    'daftar'  => $daftar_count,
                    'lolos'  => $lolos_count,
                    'gagal'  => $gagal_count,
                ];
            }
            $jumlah_daftar = $jumlah_daftar >= $daftar_count ? $jumlah_daftar : $daftar_count;
            $jumlah_lolos = $jumlah_lolos >= $lolos_count ? $jumlah_lolos : $lolos_count;
            $jumlah_gagal = $jumlah_gagal >= $gagal_count ? $jumlah_gagal : $gagal_count;
        }
        $data['maksimal_jumlah'] = $jumlah_daftar;
        
        if ($data['maksimal_jumlah'] <= $jumlah_lolos) {
            $data['maksimal_jumlah'] = $jumlah_lolos;
        }

        if ($data['maksimal_jumlah'] <= $jumlah_gagal) {
            $data['maksimal_jumlah'] = $jumlah_gagal;
        }

        if ($data['maksimal_jumlah'] == 0) {
            $data['maksimal_jumlah'] = 1;
        }

        return response()->json($data);
    }

    public function tahun_ajaran(Request $request, $id, $tahun_ajaran){
        
        return view('admin.berkas.tahun_ajaran', compact('id', 'tahun_ajaran'));
    }

    public function pdf()
    {
        $tahun_ajarans = TahunAjaran::all();

        $tahun_ajaran = null;
        if ($tahun_ajarans->count() > 0) {
            $tahun_ajaran = $tahun_ajarans->last();
            $now = Carbon::now()->isoFormat('Y')."-".Carbon::now()->addYear()->isoFormat('Y');
            if ($tahun_ajarans->where("tahun_ajaran", $now)->count() > 0) {
                $tahun_ajaran = TahunAjaran::find($now);
            }
        }

        $siswas = Siswa::select('*');

        $siswas = $this->queryControllerKastem($siswas, $tahun_ajaran);

        $siswas = $siswas->get();

        $web_profils = WebProfil::all();
        $web_profil = WebProfil::find($web_profils[0]->id);

        $pdf = Facade::loadview('admin.berkas.pdf',['siswas'=>$siswas, 'web_profil'=>$web_profil]);
        return $pdf->stream('laporan-seleksi.pdf');
    }

    public function export_excel()
    {
        $tahun_ajarans = TahunAjaran::all();

        $tahun_ajaran = null;
        if ($tahun_ajarans->count() > 0) {
            $tahun_ajaran = $tahun_ajarans->last();
            $now = Carbon::now()->isoFormat('Y')."-".Carbon::now()->addYear()->isoFormat('Y');
            if ($tahun_ajarans->where("tahun_ajaran", $now)->count() > 0) {
                $tahun_ajaran = TahunAjaran::find($now);
            }
        }

        $siswas = Siswa::select('*');

        $siswas = $this->queryControllerKastem($siswas, $tahun_ajaran);

        $siswas = $siswas->get();

        $web_profils = WebProfil::all();
        $web_profil = WebProfil::find($web_profils[0]->id);

        $return = view('admin.berkas.export_excel', ['siswas'=>$siswas, 'web_profil'=>$web_profil]);

        $th_row = 1;
        $styles[$th_row]['fill'] = [
            'fillType' => Fill::FILL_SOLID,
            'rotation' => 90,
            'startColor' => [
                'argb' => 'CCDEAE',
            ],
            'endColor' => [
                'argb' => 'CCDEAE',
            ],
        ];
        $styles[$th_row]['alignment'] = [
            'vertical' =>Alignment::VERTICAL_CENTER,
        ];
        $styles[$th_row]['borders'] = [
            'allBorders' => [
                'borderStyle' => Border::BORDER_THIN,
                'color' => ['argb' => '000'],
            ],
            'top' => [
                'borderStyle' => Border::BORDER_THIN,
                'color' => ['argb' => '000'],
            ],
            'bottom' => [
                'borderStyle' => Border::BORDER_THIN,
                'color' => ['argb' => '000'],
            ],
            'left' => [
                'borderStyle' => Border::BORDER_THIN,
                'color' => ['argb' => '000'],
            ],
            'right' => [
                'borderStyle' => Border::BORDER_THIN,
                'color' => ['argb' => '000'],
            ],
        ];
        
        $total_td_row = $siswas->count() + $th_row;
        for ($i=($th_row+1); $i <= $total_td_row; $i++) { 
            if ($i%2 == 1) {
                $styles[$i]['fill'] = [
                    'fillType' => Fill::FILL_SOLID,
                    'rotation' => 90,
                    'startColor' => [
                        'argb' => 'EEE8AA',
                    ],
                    'endColor' => [
                        'argb' => 'EEE8AA',
                    ],
                ];
            }
            $styles[$i]['borders'] = [
                'allBorders' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => '000'],
                ],
                'top' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => '000'],
                ],
                'bottom' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => '000'],
                ],
                'left' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => '000'],
                ],
                'right' => [
                    'borderStyle' => Border::BORDER_THIN,
                    'color' => ['argb' => '000'],
                ],
            ];
        }

        return Excel::download(new TemplatesExport($return, $styles, ($th_row+1)), 'Laporan Pendaftaran Siswa ('.Carbon::now()->isoFormat('D MMMM Y').').xlsx');
    }

    private function queryControllerKastem($query, $tahun_ajaran){
        // if ($web_profil->jenjang_sekolah == "SLTA") {
        //     $query = $query->where('jurusan', '!=', 'Default');
        // } else {
        //     $query = $query->where('jurusan', 'Default');
        // }

        // Hidden Sementara 24/11/2022
        // if (request()->status) {
        //     $query = $query->whereHas('bayar', function($q){
        //         $q->where('status', 'Lunas');
        //     }, '>', 0)->where('seleksi', request()->status);
        // }
        if (request()->status) {
            $query = $query->whereHas('bayar', function($q) {
                $q->where('status', request()->status);
            });
        }
        // Hidden Sementara 24/11/2022

        if (request()->tahun_ajaran) {
            $query = $query->where('tahun_ajaran', request()->tahun_ajaran);
        } else {
            $query = $query->where('tahun_ajaran', $tahun_ajaran->tahun_ajaran);
        }

        return $query;
    }
}
