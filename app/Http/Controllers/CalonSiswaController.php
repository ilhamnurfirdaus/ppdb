<?php

namespace App\Http\Controllers;

use App\Models\Siswa;
use App\Models\WebProfil;
use Illuminate\Http\Request;

class CalonSiswaController extends Controller
{
    public function index(Request $request){
        $web_profils = WebProfil::all();
        $web_profil = WebProfil::find($web_profils[0]->id);

        $siswas = Siswa::select('*');

        // if ($web_profil->jenjang_sekolah == "SLTA") {
        //     $siswas = $siswas->where('jurusan', '!=', 'Default');
        // } else {
        //     $siswas = $siswas->where('jurusan', 'Default');
        // }

        $siswas = $siswas->get();

        return view('admin.calon_siswa.index', compact('siswas', 'web_profil'));
    }
}
