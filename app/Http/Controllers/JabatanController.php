<?php

namespace App\Http\Controllers;

use App\Models\Jabatan;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class JabatanController extends Controller
{
    public function index(Request $request){
        $jabatans = Jabatan::select('*')->get();

        return view('admin.jabatan.index', compact('jabatans'));
    }

    public function store(Request $request){
        $jabatan = new Jabatan;
        $jabatan->id = Str::uuid();
        $jabatan->name = $request->name;
        $jabatan->keterangan = $request->keterangan;

        $jabatan->save();

        return back()->with('message','Data Berhasil Dibuat');
    }

    public function update(Request $request, $id){
        $jabatan = Jabatan::find($id);
        $jabatan->name = $request->name;
        $jabatan->keterangan = $request->keterangan;

        $jabatan->save();

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function destroy($id){
        $jabatan = Jabatan::find($id);
        $jabatan->delete();

        return back()->with('message','Data Berhasil Dihapus');
    }
}
