<?php

namespace App\Http\Controllers;

use App\Models\Gelombang;
use App\Models\NilaiSertifikat;
use App\Models\TahunAjaran;
use Carbon\Carbon;
use Illuminate\Http\Request;

class TahunAjaranController extends Controller
{
    public function index(Request $request){
        $tahun_ajarans = TahunAjaran::select('*');

        $tahun_ajarans = $tahun_ajarans->orderBy("tahun_ajaran", "ASC")->get();

        return view('admin.tahun_ajaran.index', compact('tahun_ajarans'));
    }

    public function create(){
        $tahun_ajarans = TahunAjaran::select('*');

        $tahun_ajarans = $tahun_ajarans->orderBy("tahun_ajaran", "ASC")->get();

        $ta = Carbon::now()->isoFormat('Y')."-".Carbon::now()->addYear()->isoFormat('Y');
        if ($tahun_ajarans->count() > 0) {
            $x = 1;
            do {
                $ta_id = str_split($tahun_ajarans->first()->tahun_ajaran, 4);
                $ta = (intval($ta_id[0])+$x)."-".(intval($ta_id[0])+$x+1);
                $list_ta = TahunAjaran::where("tahun_ajaran", $ta)->get();
                $x++;
            }while ( $list_ta->count() > 0);
        }

        $now = Carbon::now()->addYear($x-1)->format('Y-m-d');

        return view('admin.tahun_ajaran.create', compact('ta', 'now'));
    }

    public function store(Request $request){
        $tahun_ajaran = new TahunAjaran;
        $tahun_ajaran->tahun_ajaran = $request->tahun_ajaran;
        $tahun_ajaran->waktu_soal = 2;
        $tahun_ajaran->jml_raport = 6;
        $tahun_ajaran->minimal_nilai = $request->minimal_nilai;
        $tahun_ajaran->maksimal_jumlah_siswa = $request->maksimal_jumlah_siswa;
        $tahun_ajaran->save();

        foreach ($request->gelombang_id as $key => $value) {
            $gelombang = new Gelombang;
            $digit = "";
            if (strlen($key+1) >= 2) {
                $digit = $key+1;
            } elseif (strlen($key+1) == 1) {
                $digit = "0".($key+1);
            }
            $gelombang->gelombang_id = "GEL".$request->tahun_ajaran.$digit;
            $gelombang->nama_gelombang = $request->nama_gelombang[$key];
            $gelombang->tanggal_mulai = $request->tanggal_mulai[$key];
            $gelombang->tanggal_selesai = $request->tanggal_selesai[$key];
            $gelombang->tahun_ajaran = $request->tahun_ajaran;
            $gelombang->keterangan = $request->keterangan[$key];
            $gelombang->save();
        }

        $tahun_ajaran = str_split($request->tahun_ajaran, 4);

        for ($i=1; $i <= 15; $i++) {
            $no = $i;
            
            if (strlen($i) == 1) {
                $no = "0".$i;
            }

            $nilai_sertifikat = new NilaiSertifikat;
            $nilai_sertifikat->nilai_sertifikats_id = "NISE".$tahun_ajaran.$no;

            if ($i >= 1 && $i <= 3) {
                $nilai_sertifikat->nama_tingkat = "Kecamatan";
            } else if ($i >= 4 && $i <= 6) {
                $nilai_sertifikat->nama_tingkat = "Kota/Kabupaten";
            } else if ($i >= 7 && $i <= 9) {
                $nilai_sertifikat->nama_tingkat = "Provinsi";
            } else if ($i >= 10 && $i <= 12) {
                $nilai_sertifikat->nama_tingkat = "Nasional";
            } else if ($i >= 13 && $i <= 15) {
                $nilai_sertifikat->nama_tingkat = "Internasional";
            } 

            if ($i >= 1 && $i <= 3 || $i >= 7 && $i <= 9 || $i >= 13 && $i <= 15) {
                if ($i % 3 == 0) {
                    $nilai_sertifikat->peringkat = 1;
                } else if ($i % 2 == 0) {
                    $nilai_sertifikat->peringkat = 2;
                } else {
                    $nilai_sertifikat->peringkat = 3;
                }
            } else {
                if ($i % 3 == 0) {
                    $nilai_sertifikat->peringkat = 1;
                } else if ($i % 2 == 0) {
                    $nilai_sertifikat->peringkat = 3;
                } else {
                    $nilai_sertifikat->peringkat = 2;
                }
            }

            $nilai_sertifikat->nilai = $i;
            $nilai_sertifikat->tahun_ajaran = "2021-2022";
            $nilai_sertifikat->save();
        }

        return back()->with('message','Data Berhasil Dibuat');
    }

    public function gelombang(TahunAjaran $tahun_ajaran){

        return view('admin.tahun_ajaran.gelombang', compact('tahun_ajaran'));
    }

    public function edit(TahunAjaran $tahun_ajaran){

        return view('admin.tahun_ajaran.edit', compact('tahun_ajaran'));
    }

    public function update(Request $request, TahunAjaran $tahun_ajaran){
        $tahun_ajaran->tahun_ajaran = $request->tahun_ajaran;
        $tahun_ajaran->waktu_soal = $request->waktu_soal;
        $tahun_ajaran->minimal_nilai = $request->minimal_nilai;
        $tahun_ajaran->maksimal_jumlah_siswa = $request->maksimal_jumlah_siswa;
        $tahun_ajaran->save();

        $jumlah_id = [];
        foreach ($request->gelombang_id as $key => $value) {
            if ($value == "Baru") {
                $gelombang = new Gelombang;

                // $last_gelombang = Gelombang::where("tahun_ajaran", $request->tahun_ajaran)->orderBy('gelombang_id', 'desc')->get()->first();
                $last_gelombang = $tahun_ajaran->gelombang->last();
                $array_id = str_split($last_gelombang->gelombang_id, 3);
                $gelombang_id = "GEL".(intval($array_id[1].$array_id[2]) + 1);

                $gelombang->gelombang_id = $gelombang_id;
                $gelombang->nama_gelombang = $request->nama_gelombang[$key];
                $gelombang->tanggal_mulai = $request->tanggal_mulai[$key];
                $gelombang->tanggal_selesai = $request->tanggal_selesai[$key];
                $gelombang->tahun_ajaran = $request->tahun_ajaran;
                $gelombang->keterangan = $request->keterangan[$key];
                $gelombang->save();

                array_push($jumlah_id, $gelombang_id);
            } else {
                $gelombang = Gelombang::find($value);
                $gelombang->nama_gelombang = $request->nama_gelombang[$key];
                $gelombang->tanggal_mulai = $request->tanggal_mulai[$key];
                $gelombang->tanggal_selesai = $request->tanggal_selesai[$key];
                $gelombang->tahun_ajaran = $request->tahun_ajaran;
                $gelombang->keterangan = $request->keterangan[$key];
                $gelombang->save();

                array_push($jumlah_id, $value);
            }
        }

        Gelombang::where("tahun_ajaran", $request->tahun_ajaran)->whereNotIn("gelombang_id", $jumlah_id)->delete();

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function destroy(TahunAjaran $tahun_ajaran){
        NilaiSertifikat::where("tahun_ajaran", $tahun_ajaran->tahun_ajaran)->delete();
        Gelombang::where("tahun_ajaran", $tahun_ajaran->tahun_ajaran)->delete();
        TahunAjaran::find($tahun_ajaran->tahun_ajaran)->delete();

        return back()->with('message','Data Berhasil Dihapus');
    }
}
