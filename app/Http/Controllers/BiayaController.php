<?php

namespace App\Http\Controllers;

use App\Models\Biaya;
use App\Models\Gelombang;
use App\Models\Jurusan;
use App\Models\WebProfil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BiayaController extends Controller
{
    public function index(Request $request){
        $web_profils = WebProfil::all();
        $web_profil = WebProfil::find($web_profils[0]->id);

        $biayas = Biaya::select('*');
        $jurusans = Jurusan::select('*')->get();
        $gelombangs = Gelombang::select('*')->get();

        // if ($web_profil->jenjang_sekolah == "SLTA") {
        //     $biayas = $biayas->where('program_studi', '!=', 'Default');
        // } else {
        //     $biayas = $biayas->where('program_studi', 'Default');
        // }
        $biayas = $biayas->get();

        return view('admin.biaya.index', compact('biayas', 'jurusans', 'web_profil', 'gelombangs'));
    }

    public function store(Request $request){
        // dd($request->all());
        foreach($request->nama_tagihan as $key=>$val){
            $biaya = new Biaya;
            $biaya->id = Str::uuid();
            $biaya->jenis_biaya = $val;
            if (isset($request->program_studi)) {
                $biaya->program_studi = $request->program_studi;
            } else {
                $biaya->program_studi = "Default";
            }
            $biaya->biaya = $request->nominal[$key];
            $gelombang = Gelombang::find($request->nama_gelombang);
            $biaya->nama_gelombang = $gelombang->nama_gelombang;
            $biaya->mulai_tanggal = $gelombang->tanggal_mulai;
            $biaya->sampai_tanggal = $gelombang->tanggal_selesai;        
            $biaya->tahun_ajaran = $gelombang->tahun_ajaran;
            $biaya->save();
        }
        return back()->with('message','Data Berhasil Dibuat');
    }

    public function edit($id){
        $biaya = Biaya::find($id);
        
        $web_profils = WebProfil::all();
        $web_profil = WebProfil::find($web_profils[0]->id);

        $jurusans = Jurusan::select('*')->get();
        $gelombangs = Gelombang::select('*')->get();

        return view('admin.biaya.edit', compact('biaya', 'jurusans', 'web_profil', 'gelombangs'));
    }

    public function update(Request $request, $id){
        $biaya = Biaya::find($id);
        $biaya->jenis_biaya = $request->jenis_biaya;
        if (isset($request->program_studi)) {
            $biaya->program_studi = $request->program_studi;
        } else {
            $biaya->program_studi = "Default";
        }
        $biaya->biaya = $request->biaya;
        $gelombang = Gelombang::find($request->nama_gelombang);
        $biaya->nama_gelombang = $gelombang->nama_gelombang;
        $biaya->mulai_tanggal = $gelombang->tanggal_mulai;
        $biaya->sampai_tanggal = $gelombang->tanggal_selesai; 

        $biaya->save();

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function destroy($id){
        $biaya = Biaya::find($id);
        $biaya->delete();

        return back()->with('message','Data Berhasil Dihapus');
    }
}
