<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Indonesia;

class SearchController extends Controller
{
    public function provinsi(Request $request) {
        if ($request->has('q')) {
            $q = $request->q;

            $data = Indonesia::search($q)->allProvinces();

            return response()->json($data);
        }
    }
    
    public function kota_kabupaten(Request $request, $provinsi) {
        if ($request->has('q')) {
            $q = $request->q;
            $perkata = explode("_", $provinsi);
            $nama = implode(" ", $perkata);
            $provinsi_id = Indonesia::search($nama)->allProvinces()[0]->id;
            $data = Indonesia::search($q)->allCities()->where('province_id', $provinsi_id);

            return response()->json($data);
        }
    }

    public function kecamatan(Request $request, $kota_kabupaten) {
        if ($request->has('q')) {
            $q = $request->q;

            $perkata = explode("_", $kota_kabupaten);
            $nama = implode(" ", $perkata);
            $kota_kabupaten_id = Indonesia::search($nama)->allCities()[0]->id;
            $data = Indonesia::search($q)->allDistricts()->where('city_id', $kota_kabupaten_id);

            return response()->json($data);
        }
    }

    public function desa_kelurahan(Request $request, $kecamatan) {
        if ($request->has('q')) {
            $q = $request->q;

            $perkata = explode("_", $kecamatan);
            $nama = implode(" ", $perkata);
            $kecamatan_id = Indonesia::search($nama)->allDistricts()[0]->id;
            $data = Indonesia::search($q)->allVillages()->where('district_id', $kecamatan_id);

            return response()->json($data);
        }
    }

    public function kotas(Request $request) {
        if ($request->has('q')) {
            $q = $request->q;

            $data = Indonesia::search($q)->allCities();

            return response()->json($data);
        }
    }
}
