<?php

namespace App\Http\Controllers;

use App\Exports\BayarAwalsExport;
use App\Imports\BayarAwalsImport;
use App\Models\Bank;
use App\Models\Bayar;
use App\Models\Notification;
use App\Models\Rekening;
use App\Models\User;
use App\Models\Siswa;
use App\Models\TahunAjaran;
use App\Models\WebProfil;
use App\Notifications\KirimNotifikasi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use PDF;

class BayarsAwalController extends Controller
{
    public function index(Request $request){
        $tahun_ajarans = TahunAjaran::all();

        $tahun_ajaran = null;
        if ($tahun_ajarans->count() > 0) {
            $tahun_ajaran = $tahun_ajarans->last();
            $now = Carbon::now()->isoFormat('Y')."-".Carbon::now()->addYear()->isoFormat('Y');
            if ($tahun_ajarans->where("tahun_ajaran", $now)->count() > 0) {
                $tahun_ajaran = TahunAjaran::find($now);
            }
        }

        $web_profils = WebProfil::all();
        $web_profil = WebProfil::find($web_profils[0]->id);

        $bayars = Bayar::select('*')->where('jenis_biaya', 'Biaya Pendaftaran');
        $rekenings = Rekening::select('*')->get();
        $banks = Bank::select('*')->get();

        // if ($web_profil->jenjang_sekolah == "SLTA") {
        //     $bayars = $bayars->whereHas('siswa', function ($q) {
        //                 $q->where('jurusan', '!=', 'Default');
        //             });
        // } else {
        //     $bayars = $bayars->whereHas('siswa', function ($q) {
        //                 $q->where('jurusan', 'Default');
        //             });
        // }

        if ($request->status) {
            if ($request->status == "Lunas") {
                $bayars = $bayars->where('status', $request->status);
            } else if ($request->status != "Lunas") {
                $bayars = $bayars->where('status', '!=', "Lunas");
            }
        }

        if ($request->tahun_ajaran) {
            $bayars = $bayars->whereHas('siswa', function ($q) use($request) {
                $q->where('tahun_ajaran', $request->tahun_ajaran);
            });
        } else {
            $bayars = $bayars->whereHas('siswa', function ($q) use($tahun_ajaran) {
                $q->where('tahun_ajaran', $tahun_ajaran->tahun_ajaran);
            });
        }

        $bayars = $bayars->get();

        return view('admin.bayar.awal', compact('banks', 'bayars', 'rekenings', 'web_profil', 'tahun_ajarans', 'tahun_ajaran'));
    }

    public function bukti(Bayar $bayar){

        return view('admin.bayar.bukti', compact('bayar'));
    }

    public function detil(Bayar $bayar){
        $web_profil = WebProfil::all()->first();

        return view('admin.bayar.detil', compact('bayar', 'web_profil'));
    }

    public function edit(Bayar $bayar){

        return view('admin.bayar.edit', compact('bayar'));
    }

    public function invoice(Bayar $bayar){

        return view('admin.bayar.invoice', compact('bayar'));
    }

    public function update(Request $request, $id){
        $web_profil = WebProfil::all()->first();

        $bayar = Bayar::find($id);
        $bayar->status = $request->status;
        $bayar->keterangan = $request->keterangan;
        $bayar->save();

        Notification::whereNull('read_at')
        ->where('data', 'like', '%"model_id":"'.$id.'"%')->where('data', 'like', '%"nama_model":"Bayar"%')
        ->update(['read_at' => Carbon::now()->format('Y-m-d h:i:s')]);

        if ($bayar->status == "Lunas") {
            $title = "Pembayaran Pendaftaran Lunas";
            $description = "Pembayaran anda sudah lunas, lanjut ke tahap selanjutnya.";
            $url_redirect = "siswa/seleksi";
            $model_id = $bayar->siswa->user->siswa->jurusanTo->id;
            $nama_model = "Berkas Awal";
        } else if ($bayar->status == "Belum Lunas") {
            $title = "Pembayaran Pendaftaran Belum Lunas";
            $description = $bayar->keterangan;
            $url_redirect = "siswa/bayar-awal";
            $model_id = $bayar->siswa->user->siswa->jurusanTo->id;
            $nama_model = "Bayar Awal";
        }

        $toUser = User::find($bayar->siswa->user->id);
        $toUser->notify(new KirimNotifikasi($title, $description, $url_redirect, $model_id, $nama_model));

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function export_excel(Request $request) 
    {
        $status = $request->status;
        $tahun_ajaran = $request->tahun_ajaran;

        return Excel::download(new BayarAwalsExport($status, $tahun_ajaran), 'list_pembayaran.xlsx');
    }

    public function import_excel(Request $request) 
    {
        // validasi
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);

        $file = $request->file;
        $file_name = $file->getClientOriginalName();

        $file_name_only = pathinfo($file_name, PATHINFO_FILENAME);
        $file_type = pathinfo($file_name, PATHINFO_EXTENSION);

        $new_file_name = $file_name_only.'.'.$file_type;

        $file = $request->file('file');
        $tujuan_upload = public_path('penyimpanan/excel/bayars');
        
        $file->move($tujuan_upload, $new_file_name);

        Excel::import(new BayarAwalsImport, public_path('/penyimpanan/excel/bayars/'.$file_name));
        
        return back()->with('message','Data Berhasil Diupdate');
    }

    public function buka(Request $request){
        $web_profils = WebProfil::all();
        $web_profil = WebProfil::find($web_profils[0]->id);
        $web_profil->tampil_seleksi = $request->tampil_seleksi;
        $web_profil->waktu_seleski = Carbon::now()->format("Y-m-d");
        $web_profil->save();

        

        return back()->with('message','Data Berhasil Dibuat');
    }

    public function export_pdf(Request $request){
        $tahun_ajarans = TahunAjaran::all();

        $tahun_ajaran = null;
        if ($tahun_ajarans->count() > 0) {
            $tahun_ajaran = $tahun_ajarans->last();
            $now = Carbon::now()->isoFormat('Y')."-".Carbon::now()->addYear()->isoFormat('Y');
            if ($tahun_ajarans->where("tahun_ajaran", $now)->count() > 0) {
                $tahun_ajaran = TahunAjaran::find($now);
            }
        }

        $web_profils = WebProfil::all();
        $web_profil = WebProfil::find($web_profils[0]->id);

        $bayars = Bayar::select('*')->where('jenis_biaya', 'Biaya Pendaftaran');

        if ($request->status) {
            if ($request->status == "Lunas") {
                $bayars = $bayars->where('status', $request->status);
            } else if ($request->status != "Lunas") {
                $bayars = $bayars->where('status', '!=', "Lunas");
            }
        }

        if ($request->tahun_ajaran) {
            $bayars = $bayars->whereHas('siswa', function ($q) use($request) {
                $q->where('tahun_ajaran', $request->tahun_ajaran);
            });
        } else {
            $bayars = $bayars->whereHas('siswa', function ($q) use($tahun_ajaran) {
                $q->where('tahun_ajaran', $tahun_ajaran->tahun_ajaran);
            });
        }

        $bayars = $bayars->get();

        $pdf = PDF::loadview('admin.bayar.bayar_pdf',['bayars'=>$bayars, 'web_profil'=>$web_profil]);
        return $pdf->stream('laporan-pembayaran.pdf');
    }
}
