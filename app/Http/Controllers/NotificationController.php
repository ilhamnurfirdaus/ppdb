<?php

namespace App\Http\Controllers;

use App\Models\Pengumuman;
use App\Models\Siswa;
use App\Models\WebProfil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class NotificationController extends Controller
{
    public function index(Request $request){
        $web_profil = WebProfil::all()->first();
        $siswa = Siswa::find(Auth::user()->siswa->id);
        // if ($web_profil->tampil_seleksi == "Ya") {
        //     $siswa->cek_pengumuman = "0";
        // }
        $siswa->save();

        $pengumumans = Pengumuman::select('*')->where('status', 'Aktif')->get();

        return view('siswa.pengumuman.index', compact('pengumumans', 'web_profil'));
    }
}
