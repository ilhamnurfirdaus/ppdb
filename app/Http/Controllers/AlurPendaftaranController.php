<?php

namespace App\Http\Controllers;

use App\Models\AlurPendaftaran;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class AlurPendaftaranController extends Controller
{
    public function index(Request $request){
        $alur_pendaftarans = AlurPendaftaran::select('*')->get();

        return view('admin.alur_pendaftaran.index', compact('alur_pendaftarans'));
    }

    public function store(Request $request){
        $alur_pendaftaran = new AlurPendaftaran;
        $alur_pendaftaran->id = Str::uuid();
        $alur_pendaftaran->name = $request->name;
        $alur_pendaftaran->keterangan = $request->keterangan;

        $alur_pendaftaran->save();

        return back()->with('message','Data Berhasil Dibuat');
    }

    public function update(Request $request, $id){
        $alur_pendaftaran = AlurPendaftaran::find($id);
        $alur_pendaftaran->name = $request->name;
        $alur_pendaftaran->keterangan = $request->keterangan;

        $alur_pendaftaran->save();

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function destroy($id){
        $alur_pendaftaran = AlurPendaftaran::find($id);
        $alur_pendaftaran->delete();

        return back()->with('message','Data Berhasil Dihapus');
    }
}
