<?php

namespace App\Http\Controllers;

use App\Models\Pengumuman;
use App\Models\TahunAjaran;
use App\Models\WebProfil;
use App\Models\Gelombang;
use App\Models\Biaya;
use App\Models\Jurusan;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class BerandaController extends Controller
{
    public function index(Request $request){
        $list = WebProfil::all();
        $data = WebProfil::find($list[0]->id);
        $gelombang = gelombang::all();
        $biaya = Biaya::select("*")->orderBy('nama_gelombang', 'asc')->get();
        $jurusan = Jurusan::all();
        // $tahun_ajaran = TahunAjaran::select("*")->first();
        $tahun_ajarans = TahunAjaran::select('*')->get();

        $tahun_ajaran = null;
        if ($tahun_ajarans->count() > 0) {
            $tahun_ajaran = $tahun_ajarans->last();
            if ($tahun_ajarans->where("tahun_ajaran", Auth::user()->siswa->tahun_ajaran)->count() > 0) {
                $tahun_ajaran = TahunAjaran::find(Auth::user()->siswa->tahun_ajaran);
            }
        }
        // dd($data);

        return view('siswa.dashboard.index', compact('jurusan','data', 'gelombang', 'biaya', 'tahun_ajaran'));
    }
}
