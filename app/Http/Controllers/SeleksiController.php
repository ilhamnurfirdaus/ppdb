<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\Sertifikat;
use App\Models\Siswa;
use App\Models\WebProfil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class SeleksiController extends Controller
{
    public function index(Request $request){
      Notification::whereNull('read_at')
        ->where('data', 'like', '%"model_id":"'.Auth::user()->siswa->jurusanTo->id.'"%')->where('data', 'like', '%"nama_model":"Berkas Awal"%')
        ->update(['read_at' => Carbon::now()->format('Y-m-d h:i:s')]);

      $siswas = Siswa::select('*')
      // ->where("jurusan", Auth::user()->siswa->jurusan)
      ->where("tahun_ajaran", Auth::user()->siswa->tahun_ajaran)->get();

      $web_profils = WebProfil::all();
      $web_profil = WebProfil::find($web_profils[0]->id);

      return view('siswa.seleksi.index', compact('siswas', 'web_profil'));
    }
}
