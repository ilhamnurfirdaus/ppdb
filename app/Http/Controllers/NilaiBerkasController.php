<?php

namespace App\Http\Controllers;

use App\Models\NilaiBerkas;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NilaiBerkasController extends Controller
{
    public function index(Request $request){
        $nilai_berkass = NilaiBerkas::select('*')->get();

        return view('admin.nilai_berkas.index', compact('nilai_berkass'));
    }

    public function store(Request $request){
        $nilai_berkas = new NilaiBerkas;
        $x = 1;
        do {
            $digit = "";
            if (strlen($x) >= 6) {
                $digit = $x;
            } elseif (strlen($x) == 5) {
                $digit = "0".$x;
            } elseif (strlen($x) == 4) {
                $digit = "00".$x;
            } elseif (strlen($x) == 3) {
                $digit = "000".$x;
            } elseif (strlen($x) == 2) {
                $digit = "0000".$x;
            } elseif (strlen($x) == 1) {
                $digit = "00000".$x;
            }
            $nilai_berkas_id = "NIBE".$digit;
            $nilai_berkass = DB::table('nilai_berkas')->where('nilai_berkas_id', $nilai_berkas_id)->get();
            $x++;
        }while ( $nilai_berkass->count() != 0);

        $nilai_berkas->nilai_berkas_id = $nilai_berkas_id;
        $nilai_berkas->min_nilai = $request->min_nilai;
        $nilai_berkas->max_nilai = $request->max_nilai;
        $nilai_berkas->nilai = $request->nilai;

        $nilai_berkas->save();

        return back()->with('message','Data Berhasil Dibuat');
    }

    public function edit(Request $request, $id){
        $nilai_berkas = NilaiBerkas::find($id);

        return view('admin.nilai_berkas.edit', compact('nilai_berkas'));
    }

    public function update(Request $request, $id){
        $nilai_berkas = NilaiBerkas::find($id);
        $nilai_berkas->min_nilai = $request->min_nilai;
        $nilai_berkas->max_nilai = $request->max_nilai;
        $nilai_berkas->nilai = $request->nilai;

        $nilai_berkas->save();

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function destroy($id){
        $nilai_berkas = NilaiBerkas::find($id);
        $nilai_berkas->delete();

        return back()->with('message','Data Berhasil Dihapus');
    }
}
