<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Gelombang;
use Illuminate\Support\Str;

class GelombangController extends Controller
{
    //
    public function index(){
        $gelombang = Gelombang::select("*")->get();
        return view('admin.gelombang.index', compact('gelombang'));
    }

    public function store(Request $request){
        $gelombang = new Gelombang();
        $gelombang->gelombang_id = Str::uuid();
        $gelombang->nama_gelombang = $request->nama_gelombang;
        $gelombang->tanggal_mulai = $request->tanggal_mulai;
        $gelombang->tanggal_selesai = $request->tanggal_selesai;
        $gelombang->tahun_ajaran = $request->tahun_ajaran;
        $gelombang->keterangan = $request->keterangan;
        $gelombang->save();

        return back()->with('message', 'Data berhasil disimpan');
    }

    public function edit($id){
        $gelombang = Gelombang::find($id);
        return view('admin.gelombang.edit', compact('gelombang'));
    }

    public function update(Request $request, $id){
        $gelombang = Gelombang::find($id);
        $gelombang->nama_gelombang = $request->nama_gelombang;
        $gelombang->tanggal_mulai = $request->tanggal_mulai;
        $gelombang->tanggal_selesai = $request->tanggal_selesai;
        $gelombang->tahun_ajaran = $request->tahun_ajaran;
        $gelombang->keterangan = $request->keterangan;
        $gelombang->save();

        return back()->with('message', 'Data berhasil disimpan');
    }

    public function destroy($id)
    {
        $gelombang = Gelombang::find($id);
        $gelombang->delete();

        return back()->with('message', 'Data berhasil dihapus');
    }

}
