<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfilController extends Controller
{
    //
    public function index(){
        $user = Auth::user();
        // dd($user);
        return view('admin.profil.profil-index', compact('user'));
    }

    public function update(Request $request, $id){
        // dd($request->all());
        $user = User::find($id);
        $user->name = $request->nama;
        $user->email = $request->email;
        $user->username = $request->username;
        $user->password = Hash::make($request->password);
        if(isset($request->file)){
            $this->validate($request, [
                'file.*' => 'required|mimes:jpeg,png,jpg|max:500',
            ]);

            $image = $request->file;
            $img_name = $image->getClientOriginalName();

            $img_name_only = pathinfo($img_name, PATHINFO_FILENAME);
            $img_type = pathinfo($img_name, PATHINFO_EXTENSION);

            // $new_image_name = $img_name_only.'('.$sertifikat->id.').'.$img_type;
            $new_image_name = Auth::user()->id.'.'.$img_type;

            $file = $request->file('file');
            $tujuan_upload = public_path('profil/');
            
            $file->move($tujuan_upload, $new_image_name);

            $user->foto = $new_image_name;
        }
        $user->save();

        return back()->with('simpan', 'Data berhasil disimpan');
    }
}
