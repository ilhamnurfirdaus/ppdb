<?php

namespace App\Http\Controllers;

use App\Models\AlurPendaftaran;
use App\Models\Jurusan;
use App\Models\Biaya;
use App\Models\SettingBerkas;
use App\Models\SyaratPendaftaran;
use App\Models\TahunAjaran;
use App\Models\WebProfil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Barryvdh\DomPDF\Facade as PDF;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $now = Carbon::now()->isoFormat('Y')."-".Carbon::now()->addYear()->isoFormat('Y');

        $web_profils = WebProfil::all();
        $web_profil = WebProfil::find($web_profils[0]->id);

        $alur_pendaftarans = AlurPendaftaran::select('*')->orderBy('name', 'asc')->get();
        $syarat_pendaftarans = SyaratPendaftaran::select('*')->get();

        $setting_berkass = SettingBerkas::select('*')->where("tahun_ajaran", $now)->where('jenis_berkas', 'Berkas Pendaftaran')->where('jurusan_id', 'Default')->get();

        $jurusans = Jurusan::select('*')->where("tahun_ajaran", $now)->get();

        $no = 0;

        $jurusan = [];

        for ($i = 0; $i < $jurusans->count(); $i = $i + 3) {
            $jurusan[$no] = [];
            for ($j = 0 + $i; $j < 3 + $i; $j++){
                if (empty($jurusans[$j])){
                    break;
                }
                array_push($jurusan[$no], $jurusans[$j]);
            }
            $no = $no + 1;
        }

        $biaya = Biaya::orderBy('nama_gelombang', 'asc')->get();
        $jenis_biaya = Biaya::whereNull('deleted_at')->where('program_studi', '!=', 'Default')->groupBy('jenis_biaya')->select('jenis_biaya')->orderBy('jenis_biaya', 'desc')->get();
        // dd($jenis_biaya);

        $tahun_ajarans = TahunAjaran::select('*')->get();

        $tahun_ajaran = null;
        if ($tahun_ajarans->count() > 0) {
            $tahun_ajaran = $tahun_ajarans->last();
            if ($tahun_ajarans->where("tahun_ajaran", $now)->count() > 0) {
                $tahun_ajaran = TahunAjaran::find($now);
            }
        }

        return view('landing-page.home', compact('jenis_biaya','biaya', 'jurusan', 'jurusans', 'web_profil', 'alur_pendaftarans', 'syarat_pendaftarans', 'setting_berkass', 'tahun_ajaran'));
    }

    public function informasi_pendaftaran_pdf(TahunAjaran $tahun_ajaran){

        $pdf = PDF::loadview('landing-page.informasi_pdf',['tahun_ajaran'=>$tahun_ajaran]);
        return $pdf->stream('informasi-pendaftaran-seleksi.pdf');
    }

    public function pendaftaran(Request $request)
    {
        $tahun_ajarans = TahunAjaran::select('*')->get();

        $tahun_ajaran = null;
        if ($tahun_ajarans->count() > 0) {
            $tahun_ajaran = $tahun_ajarans->last();
            $now = Carbon::now()->isoFormat('Y')."-".Carbon::now()->addYear()->isoFormat('Y');
            if ($tahun_ajarans->where("tahun_ajaran", $now)->count() > 0) {
                $tahun_ajaran = TahunAjaran::find($now);
            }
        }

        $web_profils = WebProfil::all();
        $web_profil = WebProfil::find($web_profils[0]->id);

        $tutup_pendaftaran = "Ya";
        if (isset($tahun_ajaran)) {
            foreach ($tahun_ajaran->biaya as $key => $value) {
                if ($tutup_pendaftaran != "Tidak") {
                    if ($value->sampai_tanggal >= Carbon::now()->format('Y-m-d') && $value->mulai_tanggal <= Carbon::now()->format('Y-m-d')) {
                        $tutup_pendaftaran = "Tidak";
                    }
                } 
            }
        }

        $jurusans = Jurusan::select('*')->where("tahun_ajaran", $now)->get();

        return view('landing-page.pendaftaran', compact('jurusans', 'web_profil', 'tutup_pendaftaran'));
    }
}
