<?php

namespace App\Http\Controllers;

use App\Models\Jurusan;
use App\Models\Siswa;
use App\Models\User;
use App\Models\WebProfil;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class DaftarController extends Controller
{
    public function awal()
    {
        $web_profils = WebProfil::all();
        $web_profil = WebProfil::find($web_profils[0]->id);

        $jurusans = Jurusan::select('*')->where("tahun_ajaran", Auth::user()->siswa->tahun_ajaran)->get();

        $user = User::find(Auth::user()->id);
        $siswa = Siswa::find($user->profil_id);

        return view('siswa.daftar.awal', compact('siswa', 'jurusans', 'web_profil'));
    }

    public function update_awal(Request $request,$id)
    {
        $siswa = Siswa::find($id);

        $user = User::find($siswa->user->id);
        $user->name = $request->name;
        // if ($user->email != $request->email) {
        //     $this->validate($request, [
        //         'email'=>'required|string|max:255|email|unique:users'
        //     ]);
        //     $user->email = $request->email;
        // }
        // if (isset($request->password)) {
        //     $this->validate($request, [
        //     'password'=>'required|string|confirmed|min:5',
        //     ]);

        //     $user->password = Hash::make($request->password);
        // }
        $user->save();

        $siswa->nik = $request->nik;
        // $siswa->tempat_lahir = $request->tempat_lahir;
        // $siswa->tanggal_lahir = $request->tanggal_lahir;
        $siswa->no_kk = $request->no_kk;
        $siswa->jenis_kelamin = $request->jenis_kelamin;
        $siswa->agama = $request->agama;
        if (isset($request->jurusan)) {
            $siswa->jurusan = $request->jurusan;
        } else {
            $siswa->jurusan = 'Default';
        }
        // $siswa->alamat = $request->alamat;
        // $siswa->provinsi = $request->provinsi;
        // $siswa->kota_kabupaten = $request->kota_kabupaten;
        // $siswa->kecamatan = $request->kecamatan;
        // $siswa->desa_kelurahan = $request->desa_kelurahan;
        $siswa->no_hp = $request->no_hp;
        $siswa->asal_sekolah = $request->asal_sekolah;
        $siswa->save();

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function data_diri()
    {
        $jurusans = Jurusan::select('*')->where('status', "Aktif")->get();

        $user = User::find(Auth::user()->id);
        $siswa = Siswa::find($user->profil_id);

        return view('siswa.daftar.data_diri', compact('siswa', 'jurusans'));
    }

    public function update_data_diri(Request $request,$id)
    {
        $siswa = Siswa::find($id);
        $user = User::find($siswa->user->id);

        $siswa->ukuran_baju = $request->ukuran_baju;
        $siswa->nisn = $request->nisn;
        $siswa->ukuran_baju_cm = $request->ukuran_baju_cm;
        $siswa->ukuran_celana_cm = $request->ukuran_celana_cm;
        $siswa->nik = $request->nik;
        $siswa->no_kk = $request->no_kk;
        $user->name = $request->name;
        $siswa->tempat_lahir = $request->tempat_lahir;
        $siswa->tanggal_lahir = $request->tanggal_lahir;
        $siswa->jenis_kelamin = $request->jenis_kelamin;
        $siswa->no_hp = $request->no_hp;
        $siswa->asal_sekolah = $request->asal_sekolah;
        $siswa->agama = $request->agama;
        $siswa->anak_ke = $request->anak_ke;
        $siswa->jumlah_saudara = $request->jumlah_saudara;
        $siswa->tinggi_badan_cm = $request->tinggi_badan_cm;
        $siswa->berat_badan = $request->berat_badan;
        $siswa->no_kip = $request->no_kip;

        $user->save();
        $siswa->save();

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function data_alamat()
    {
        $jurusans = Jurusan::select('*')->where('status', "Aktif")->get();

        $user = User::find(Auth::user()->id);
        $siswa = Siswa::find($user->profil_id);

        return view('siswa.daftar.data_alamat', compact('siswa', 'jurusans'));
    }

    public function update_data_alamat(Request $request,$id)
    {
        $siswa = Siswa::find($id);

        $siswa->alamat = $request->alamat;
        $siswa->rt = $request->rt;
        $siswa->rw = $request->rw;
        $siswa->provinsi = $request->provinsi;
        $siswa->kota_kabupaten = $request->kota_kabupaten;
        $siswa->kecamatan = $request->kecamatan;
        $siswa->desa_kelurahan = $request->desa_kelurahan;
        $siswa->jarak_ke_sekolah_meter = $request->jarak_ke_sekolah_meter;
        $siswa->transport = $request->transport;

        $siswa->save();

        return back()->with('message','Data Berhasil Diupdate');
    }

    
    public function data_ortu()
    {
        $jurusans = Jurusan::select('*')->where('status', "Aktif")->get();

        $user = User::find(Auth::user()->id);
        $siswa = Siswa::find($user->profil_id);

        return view('siswa.daftar.data_ortu', compact('siswa', 'jurusans'));
    }

    public function update_data_ortu(Request $request,$id)
    {
        $siswa = Siswa::find($id);

        $siswa->nik_ayah = $request->nik_ayah;
        $siswa->nama_ayah = $request->nama_ayah;
        $siswa->tempat_lahir_ayah = $request->tempat_lahir_ayah;
        $siswa->tanggal_lahir_ayah = $request->tanggal_lahir_ayah;
        $siswa->pendidikan_ayah = $request->pendidikan_ayah;
        $siswa->pekerjaan_ayah = $request->pekerjaan_ayah;
        $siswa->penghasilan_ayah = $request->penghasilan_ayah;
        $siswa->no_hp_ayah = $request->no_hp_ayah;

        $siswa->nik_ibu = $request->nik_ibu;
        $siswa->nama_ibu = $request->nama_ibu;
        $siswa->tempat_lahir_ibu = $request->tempat_lahir_ibu;
        $siswa->tanggal_lahir_ibu = $request->tanggal_lahir_ibu;
        $siswa->pendidikan_ibu = $request->pendidikan_ibu;
        $siswa->pekerjaan_ibu = $request->pekerjaan_ibu;
        $siswa->penghasilan_ibu = $request->penghasilan_ibu;
        $siswa->no_hp_ibu = $request->no_hp_ibu;

        $siswa->save();

        return back()->with('message','Data Berhasil Diupdate');
    }
}
