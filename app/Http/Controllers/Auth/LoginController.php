<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Session;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    protected $username;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
        // if (request()->is('logout') && Session::has('access_token')) {
        //     $response = Http::post('http://127.0.0.1:9000/api/auth/logout', [
        //         'token' => Session::get('access_token'),
        //     ]);
        //     if ($response->successful()) {
        //         Session::forget('access_token');
        //     }
        // }
        $this->username = $this->findUsername();
    }

    public function findUsername()
    {
        $login = request()->input('login');
 
        $fieldType = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
 
        request()->merge([$fieldType => $login]);

        // $response = Http::post('http://127.0.0.1:9000/api/auth/login', [
        //     'username' => 'admin',
        //     'password' => '123456',
        // ]);

        // if ($response->successful()) {
        //     Session::put('access_token', $response->json()['access_token']);
        // }
        return $fieldType;
    }

    /**
     * Get username property.
     *
     * @return string
     */
    public function username()
    {
        return $this->username;
    }

    protected function redirectTo()
    { 
        $user = Auth::user(); 
        if ( $user->jenis == 'Admin' )
        {
            return '/admin';
        } 
        // else if ( $user->jenis == 'NonAdmin' )
        // {
        //     return '/siswa';
        // }
        else
        {
            return $this->redirectTo;
        }
    }
}
