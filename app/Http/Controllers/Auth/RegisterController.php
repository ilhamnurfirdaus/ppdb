<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\Siswa;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Models\Biaya;
use App\Models\Gelombang;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:6', 'confirmed'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function create(array $data)
    {
        // $date = Carbon::now()->format('Y-m-d');
        $date = '2021-03-03';
        // dd($date);
        $siswa = new Siswa;
        $siswa->id = Str::uuid();
        $siswa->nik = $data['nik'];
        $siswa->no_kk = $data['no_kk'];
        $siswa->jenis_kelamin = $data['jenis_kelamin'];
        $siswa->agama = $data['agama'];
        $siswa->asal_sekolah = $data['asal_sekolah'];
        if (isset($data['jurusan'])) {
            $siswa->jurusan = $data['jurusan'];
        } else {
            $siswa->jurusan = 'Default';
        }
        $siswa->no_hp = $data['no_hp'];
        $siswa->cek_pengumuman = "0";
        $siswa->total_score = 0;
        $siswa->seleksi = "Tidak Lolos";
        // $siswa->tahun_ajaran = Carbon::now()->isoFormat('Y')."/".Carbon::now()->addYear()->isoFormat('Y');

        // $cari_gelombang = Biaya::select('*')
        // ->where('tahun_ajaran', '2021-2022')
        // ->where(function($q) use($siswa){
        //     $q->where('program_studi', 'Default')
        //     ->orWhere('program_studi', $siswa->jurusan);
        // })
        // ->get();

        $cari_gelombang = Biaya::select('*')
        ->where('sampai_tanggal', '>=', Carbon::now()->format('Y-m-d'))
        ->where('mulai_tanggal', '<=', Carbon::now()->format('Y-m-d'))
        ->get();

        $biaya_lain = Biaya::select('*')
        ->where('tahun_ajaran', $cari_gelombang[0]->tahun_ajaran)
        ->where('jenis_biaya', '!=', "Biaya Pendaftaran")
        ->where('jenis_biaya', '!=', "Biaya Daftar Ulang")
        ->where(function($q) use($data){
            $q->where('program_studi', 'Default')
            ->orWhere('program_studi', $data['jurusan']);
        })
        ->get();

        foreach ($cari_gelombang as $key => $value) {
            $tagihan[] = $value;
        }

        foreach ($biaya_lain as $key => $value) {
            $tagihan[] = $value;
        }

        $siswa->tagihan = $tagihan;
        $siswa->tahun_ajaran = $cari_gelombang[0]->tahun_ajaran;
        
        $siswa->save();

        return User::create([
            'id' => Str::uuid(),
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'profil_id' => $siswa->id,
            'jenis' => 'NonAdmin',
        ]);
    }
}
