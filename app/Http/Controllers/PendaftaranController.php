<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\TahunAJaran;
use App\Models\NilaiSertifikat;
use App\Models\SettingBerkas;
use App\Models\SettingRaport;
// use App\Models\NilaiSertifikat;

class PendaftaranController extends Controller
{
    //
    public function pendaftaran(Request $request){
        $tahun_ajaran_all = TahunAjaran::all();
        $tahun_ajaran = TahunAjaran::select("*")->first();
        if($request->tahun_ajaran){
            $tahun_ajaran = TahunAjaran::where('tahun_ajaran', $request->tahun_ajaran)->first();
        }
        return view('admin.pendaftaran.index', compact('tahun_ajaran','tahun_ajaran_all'));
    }

    public function save_switch_raport($id){
        $data = TahunAjaran::find($id);
        if($data->tampil_raport == 'Ya'){
            $data->tampil_raport = 'Tidak';
        }elseif($data->tampil_raport == 'Tidak'){
            $data->tampil_raport = 'Ya';
        }
        $data->save();
        return response()->json($data);
    }

    public function save_switch_sertifikat($id){
        $data = TahunAjaran::find($id);
        if($data->tampil_sertifikat == 'Ya'){
            $data->tampil_sertifikat = 'Tidak';
        }elseif($data->tampil_sertifikat == 'Tidak'){
            $data->tampil_sertifikat = 'Ya';
        }
        $data->save();
        return response()->json($data);
    }

    public function save_switch_berkas($id){
        $data = TahunAjaran::find($id);
        if($data->tampil_berkas == 'Ya'){
            $data->tampil_berkas = 'Tidak';
        }elseif($data->tampil_berkas == 'Tidak'){
            $data->tampil_berkas = 'Ya';
        }
        $data->save();
        return response()->json($data);
    }

    public function save_switch_test($id){
        $data = TahunAjaran::find($id);
        if($data->tampil_test == 'Ya'){
            $data->tampil_test = 'Tidak';
        }elseif($data->tampil_test == 'Tidak'){
            $data->tampil_test = 'Ya';
        }
        $data->save();
        return response()->json($data);
    }

    public function save_waktu_soal(Request $request, $id){
        $data = TahunAjaran::find($id);
        $data->waktu_soal = $request->waktu_soal;
        $data->save();
        return back()->with('message', 'Data berhasil disimpan');
    }

    public function save_jml_raport(Request $request, $id){
        $data = TahunAjaran::find($id);
        $data->jml_raport = $request->jml_raport;
        $data->save();
        return back()->with('message', 'Data berhasil disimpan');
    }

    public function get_nilai_sertifikat(Request $request){
        
        $nilai_sertifikats = NilaiSertifikat::select('*')->get();

        return view('admin.pendaftaran.get_nilai_sertifikat', compact('nilai_sertifikats'));
    }
    
    public function get_syarat_pendaftaran(Request $request){
        $setting_berkass = SettingBerkas::select('*')->where('jenis_berkas', 'Berkas Pendaftaran')->where('jurusan_id', 'Default')->get();

        return view('admin.pendaftaran.get_syarat_pendaftaran', compact('setting_berkass'));
    }

    public function get_setting_raport(Request $request){
        if ($request->tahun_ajaran) {
            $setting_raports = SettingRaport::select('*')->where("tahun_ajaran", $request->tahun_ajaran)->get();
            return view('admin.pendaftaran.get_setting_raport', compact('setting_raports'));
        }
    }

    public function save_setting_raport(Request $request){
        // dd($request->all());
        $tahun_ajaran = str_split($request->tahun_ajaran, 4);
        $sr_id_array = [];
        foreach($request->setting_raport_id as $key => $value){
            $setting_raports = SettingRaport::select('*')->where("tahun_ajaran", $request->tahun_ajaran)->get();
            if ($value == "Baru") {
                $setting_raport = new SettingRaport;
                $id = "SR".$tahun_ajaran[0]."01";
                if ($setting_raports->count() > 0) {
                    $array_id = str_split($setting_raports->last()->setting_raport_id, 2);
                    $id = "SR".(intval($array_id[1].$array_id[2].$array_id[3]) + 1);
                }
                $setting_raport->setting_raport_id = $id;
                $setting_raport->tahun_ajaran = $request->tahun_ajaran;
            } else {
                $setting_raport = SettingRaport::find($value);
            }
            $setting_raport->nama_raport = $request->nama_raport[$key];
            $setting_raport->keterangan = $request->keterangan[$key];
            $setting_raport->save();

            array_push($sr_id_array, $setting_raport->setting_raport_id);
        }

        SettingRaport::where("tahun_ajaran", $request->tahun_ajaran)
        ->whereNotIn("setting_raport_id", $sr_id_array)
        ->delete();

        return back()->with('message','Data Berhasil Dibuat');
    }
    
    public function edit_sertifikat($id){
        $sertifikat = NilaiSertifikat::find($id);

        return view('admin.nilai_sertifikat.edit', compact('sertifikat'));
    }

    public function update_sertifikat(Request $request, $id){
        $sertifikat = NilaiSertifikat::find($id);
        $sertifikat->nilai = $request->nilai;
        $sertifikat->save();

        return back()->with('message', 'Data berhasil disimpan');
    }

    public function add_syarat_pendaftaran(){
        return view('admin.pendaftaran.add_syarat_pendaftaran');
    }
}
