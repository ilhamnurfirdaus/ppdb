<?php

namespace App\Http\Controllers;

use App\Models\Biaya;
use App\Models\Gelombang;
use App\Models\TahunAjaran;
use App\Models\WebProfil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BiayaNewController extends Controller
{
    public function index(Request $request){
        $tahun_ajarans = TahunAjaran::select('*');

        $tahun_ajarans = $tahun_ajarans->orderBy("tahun_ajaran", "ASC")->get();

        $tahun_ajaran = null;
        if ($tahun_ajarans->count() > 0) {
            $tahun_ajaran = $tahun_ajarans->last();
            $now = Carbon::now()->isoFormat('Y')."-".Carbon::now()->addYear()->isoFormat('Y');
            if ($tahun_ajarans->where("tahun_ajaran", $now)->count() > 0) {
                $tahun_ajaran = TahunAjaran::find($now);
                if ($tahun_ajaran->biaya->count() > 0) {
                    $tahun_ajaran = TahunAjaran::select('*')->has("biaya", "==", 0)->orderBy("tahun_ajaran", "ASC")->get()->first();
                }
            }
        }

        $web_profil = WebProfil::all()->first();

        return view('admin.biaya_new.index', compact('tahun_ajarans', 'tahun_ajaran', 'web_profil'));
    }

    public function uang_pendaftaran(TahunAjaran $tahun_ajaran){

        return view('admin.biaya_new.uang_pendaftaran', compact('tahun_ajaran'));
    }

    public function uang_gedung(TahunAjaran $tahun_ajaran){

        return view('admin.biaya_new.uang_gedung', compact('tahun_ajaran'));
    }

    public function biaya_jurusan(TahunAjaran $tahun_ajaran){

        return view('admin.biaya_new.biaya_jurusan', compact('tahun_ajaran'));
    }

    public function biaya_lain(TahunAjaran $tahun_ajaran){

        return view('admin.biaya_new.biaya_lain', compact('tahun_ajaran'));
    }

    public function biayas(TahunAjaran $tahun_ajaran){
        $web_profil = WebProfil::all()->first();

        return view('admin.biaya_new.biayas', compact('tahun_ajaran', 'web_profil'));
    }

    public function store(Request $request){
        // dd($request->all());
        $tahun_ajaran = str_split($request->tahun_ajaran, 4);

        $biaya_id_array = [];
        if (isset($request->bp_id)) {
            foreach($request->bp_id as $key => $value){
                $bps = Biaya::select('*')->where("tahun_ajaran", $request->tahun_ajaran)
                ->where('jenis_biaya', 'Biaya Pendaftaran')
                ->where('program_studi', 'Default')->orderBy("id", "asc")->get();
    
                if ($value == "Baru") {
                    $biaya = new Biaya;
                    $id = "BP".$tahun_ajaran[0]."01";
                    if ($bps->count() > 0) {
                        $array_id = str_split($bps->last()->id, 2);
                        $id = "BP".(intval($array_id[1].$array_id[2].$array_id[3]) + 1);
                    }
                    $biaya->id = $id;
                    $biaya->jenis_biaya = "Biaya Pendaftaran";
                    $biaya->program_studi = "Default";   
    
                    
                    $gelombang = Gelombang::find($request->bp_gelombang_id[$key]);
                    $biaya->nama_gelombang = $gelombang->nama_gelombang;
                    $biaya->mulai_tanggal = $gelombang->tanggal_mulai;
                    $biaya->sampai_tanggal = $gelombang->tanggal_selesai;        
                    $biaya->tahun_ajaran = $gelombang->tahun_ajaran;
                } else {
                    $biaya = Biaya::find($value);
                    $gelombang = $biaya->gelombang();
                    $biaya->nama_gelombang = $gelombang->nama_gelombang;
                    $biaya->mulai_tanggal = $gelombang->tanggal_mulai;
                    $biaya->sampai_tanggal = $gelombang->tanggal_selesai;        
                    $biaya->tahun_ajaran = $gelombang->tahun_ajaran;
                }
    
                $biaya->biaya = $request->nominal_bp[$key];
                $biaya->save();
    
                array_push($biaya_id_array, $biaya->id);
            }   
        }

        if (isset($request->bu_id)) {
            foreach($request->bu_id as $key => $value){
                $bus = Biaya::select('*')->where("tahun_ajaran", $request->tahun_ajaran)
                ->where('jenis_biaya', 'Biaya Daftar Ulang')
                ->where('program_studi', 'Default')->orderBy("id", "asc")->get();
    
                if ($value == "Baru") {
                    $biaya = new Biaya;
                    $id = "BU".$tahun_ajaran[0]."01";
                    if ($bus->count() > 0) {
                        $array_id = str_split($bus->last()->id, 2);
                        $id = "BU".(intval($array_id[1].$array_id[2].$array_id[3]) + 1);
                    }
                    $biaya->id = $id;
                    $biaya->jenis_biaya = "Biaya Daftar Ulang";
                    $biaya->program_studi = "Default";
                    $gelombang = Gelombang::find($request->bu_gelombang_id[$key]);
                    $biaya->nama_gelombang = $gelombang->nama_gelombang;
                    $biaya->mulai_tanggal = $gelombang->tanggal_mulai;
                    $biaya->sampai_tanggal = $gelombang->tanggal_selesai;        
                    $biaya->tahun_ajaran = $gelombang->tahun_ajaran;
                } else {
                    $biaya = Biaya::find($value);
                    $gelombang = $biaya->gelombang();
                    $biaya->nama_gelombang = $gelombang->nama_gelombang;
                    $biaya->mulai_tanggal = $gelombang->tanggal_mulai;
                    $biaya->sampai_tanggal = $gelombang->tanggal_selesai;        
                    $biaya->tahun_ajaran = $gelombang->tahun_ajaran;
                }
                $biaya->biaya = $request->nominal_bu[$key];
                $biaya->save();
    
                array_push($biaya_id_array, $biaya->id);
            }
        }

        if (isset($request->bj_id)) {
            foreach($request->bj_id as $key => $value){
                $bjs = Biaya::select('*')->where("tahun_ajaran", $request->tahun_ajaran)
                ->where('id', 'LIKE', "BJ%") 
                ->orderBy("id", "asc")->get();
    
                if ($value == "Baru") {
                    $biaya = new Biaya;
                    $id = "BJ".$tahun_ajaran[0]."01";
                    if ($bjs->count() > 0) {
                        $array_id = str_split($bjs->last()->id, 2);
                        $id = "BJ".(intval($array_id[1].$array_id[2].$array_id[3]) + 1);
                    }
                    $biaya->id = $id;
                    $biaya->tahun_ajaran = $request->tahun_ajaran;
                } else {
                    $biaya = Biaya::find($value);
                }
    
                $biaya->jenis_biaya = $request->nama_bj[$key];
                $biaya->program_studi = $request->program_studi[$key];
                $biaya->biaya = $request->nominal_bj[$key];
                $biaya->save();
    
                array_push($biaya_id_array, $biaya->id);
            }
        }

        if (isset($request->bl_id)) {
            foreach($request->bl_id as $key => $value){
                $bls = Biaya::select('*')->where("tahun_ajaran", $request->tahun_ajaran)
                ->where('id', 'LIKE', "BL%") 
                ->orderBy("id", "asc")->get();
    
                if ($value == "Baru") {
                    $biaya = new Biaya;
                    $id = "BL".$tahun_ajaran[0]."01";
                    if ($bls->count() > 0) {
                        $array_id = str_split($bls->last()->id, 2);
                        $id = "BL".(intval($array_id[1].$array_id[2].$array_id[3]) + 1);
                    }
                    $biaya->id = $id;
                    $biaya->program_studi = "Default";
                    $biaya->tahun_ajaran = $request->tahun_ajaran;
                } else {
                    $biaya = Biaya::find($value);
                }
                $biaya->jenis_biaya = $request->nama_bl[$key];
                $biaya->biaya = $request->nominal_bl[$key];
                $biaya->save();
    
                array_push($biaya_id_array, $biaya->id);
            }
        }

        $web_profil = WebProfil::all()->first();

        if ($web_profil->jenjang_sekolah == "SLTA") {
            Biaya::where("tahun_ajaran", $request->tahun_ajaran)
            ->whereNotIn("id", $biaya_id_array)->delete();
        } else {
            Biaya::where("tahun_ajaran", $request->tahun_ajaran)
            ->where('id', 'NOT LIKE', "BJ%")
            ->whereNotIn("id", $biaya_id_array)->delete();
        }
        
        return back()->with('message','Data Berhasil Dibuat');
    }

    public function edit(TahunAjaran $tahun_ajaran){
        $web_profil = WebProfil::all()->first();

        return view('admin.biaya_new.edit', compact('tahun_ajaran', 'web_profil'));
    }

    public function destroy(TahunAjaran $tahun_ajaran){        
        Biaya::where("tahun_ajaran", $tahun_ajaran->tahun_ajaran)->delete();

        return back()->with('message','Data Berhasil Dihapus');
    }
    
}
