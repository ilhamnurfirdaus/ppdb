<?php

namespace App\Http\Controllers;

use App\Models\Test;
use App\Models\TestChoice;
use App\Models\WebProfil;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\SoalImport;

class TestsController extends Controller
{
    public function index(Request $request){
        $web_profil = WebProfil::all()->first();

        $tests = Test::select('*')->get();

        return view('admin.test.index', compact('tests', 'web_profil'));
    }

    public function waktu_soal(Request $request){
        $web_profil = WebProfil::all()->first();
        $web_profil->waktu_soal = $request->waktu_soal;
        $web_profil->save();

        return back()->with('message','Data Berhasil Dibuat');
    }

    public function store(Request $request){
        $test = new Test;
        $test->id = Str::uuid();
        $test->pertanyaan = $request->pertanyaan;
        $test->save();

        $score = str_split($request->score);

        foreach ($request->name as $key => $value) {
            $test_choice = new TestChoice;
            $test_choice->id = Str::uuid();
            $test_choice->test_id = $test->id;
            $test_choice->name = $request->name[$key];
            $test_choice->score = $score[$key];
            $test_choice->save();
        }

        return back()->with('message','Data Berhasil Dibuat');
    }

    public function edit(Test $test){
        return view('admin.test.edit', compact('test'));
    }

    public function update(Request $request, $id){
        $score = str_split($request->score);
        // dd($score);
        TestChoice::where('test_id', $id)->delete();

        $test = Test::find($id);
        $test->pertanyaan = $request->pertanyaan;
        $test->save();

        foreach ($request->name as $key => $value) {
            $test_choice = new TestChoice;
            $test_choice->id = Str::uuid();
            $test_choice->test_id = $test->id;
            $test_choice->name = $request->name[$key];
            $test_choice->score = $score[$key];
            $test_choice->save();
        }

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function destroy($id){
        TestChoice::where('test_id', $id)->delete();

        $test = Test::find($id);
        $test->delete();

        return back()->with('message','Data Berhasil Dihapus');
    }
    
    public function import(Request $request){
        Excel::import(new SoalImport, $request->file('file')->store('temp'));
        return back()->with('message', 'Data berhasil disimpan!');
    }
}
