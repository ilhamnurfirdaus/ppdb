<?php

namespace App\Http\Controllers;

use App\Models\Jurusan;
use App\Models\SettingBerkas;
use App\Models\TahunAjaran;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class JurusanController extends Controller
{
    public function index(Request $request){
        $jurusans = Jurusan::select('*')->get();
        $tahun_ajarans = TahunAjaran::select('*')->get();

        $tahun_ajaran = null;
        if ($tahun_ajarans->count() > 0) {
            $tahun_ajaran = $tahun_ajarans->last();
            $now = Carbon::now()->isoFormat('Y')."-".Carbon::now()->addYear()->isoFormat('Y');
            if ($tahun_ajarans->where("tahun_ajaran", $now)->count() > 0) {
                $tahun_ajaran = TahunAjaran::find($now);
            }
        }

        $sisa_kuota = 0;
        if (isset($tahun_ajaran)) {
            $sisa_kuota = $tahun_ajaran->maksimal_jumlah_siswa;
            foreach ($tahun_ajaran->jurusan as $key => $value) {
                $sisa_kuota -= $value->maksimal_jumlah_siswa;
            }
        }

        return view('admin.jurusan.index', compact('jurusans', 'tahun_ajarans', 'tahun_ajaran', 'sisa_kuota'));
    }

    public function berkas(Jurusan $jurusan){
        
        return view('admin.jurusan.berkas', compact('jurusan'));
    }
    
    public function tahun_ajaran(TahunAjaran $tahun_ajaran){
        $sisa_kuota = $tahun_ajaran->maksimal_jumlah_siswa;
        foreach ($tahun_ajaran->jurusan as $key => $value) {
            $sisa_kuota -= $value->maksimal_jumlah_siswa;
        }

        $data['maksimal_jumlah_siswa'] = $sisa_kuota;
        $data['minimal_nilai'] = $tahun_ajaran->minimal_nilai;

        return response()->json($data);
    }

    public function store(Request $request){
        $jurusan = new Jurusan;
        $jurusan->id = Str::uuid();
        $jurusan->name = $request->name;
        // $jurusan->status = $request->status;
        // $jurusan->keterangan = $request->keterangan;
        $jurusan->tahun_ajaran = $request->tahun_ajaran;
        $jurusan->akreditasi = $request->akreditasi;
        
        $jurusan->minimal_nilai = $request->minimal_nilai;
        $jurusan->maksimal_jumlah_siswa = $request->maksimal_jumlah_siswa;

        $jurusan->save();

        $tahun_ajaran = str_split($request->tahun_ajaran, 4);

        $setting_berkas_id_array = [];
        
        foreach ($request->setting_berkas_id as $key => $value) {
            $list_setting_berkas = SettingBerkas::select('*')->where("tahun_ajaran", $request->tahun_ajaran)->where('jurusan_id', $jurusan->id)->orderBy("setting_berkas_id", "asc")->get();
            if ($value == "Baru") {
                $setting_berkas = new SettingBerkas;
                
                $setting_berkas_id = "SB".$tahun_ajaran[0]."01";
                if ($list_setting_berkas->count() > 0) {
                    $array_id = str_split($list_setting_berkas->last()->setting_berkas_id, 2);
                    $setting_berkas_id = "SB".(intval($array_id[1].$array_id[2].$array_id[3]) + 1);
                }

                // $x = 1;
                // do {
                //     $digit = "";
                //     if (strlen($x) >= 6) {
                //         $digit = $x;
                //     } elseif (strlen($x) == 5) {
                //         $digit = "0".$x;
                //     } elseif (strlen($x) == 4) {
                //         $digit = "00".$x;
                //     } elseif (strlen($x) == 3) {
                //         $digit = "000".$x;
                //     } elseif (strlen($x) == 2) {
                //         $digit = "0000".$x;
                //     } elseif (strlen($x) == 1) {
                //         $digit = "00000".$x;
                //     }
                //     $setting_berkas_id = "SB".$digit;
                //     $setting_berkass = DB::table('setting_berkas')->select('*')->where('setting_berkas_id', $setting_berkas_id)->get();
                //     $x++;
                // }while ( $setting_berkass->count() > 0);
                $setting_berkas->setting_berkas_id  = $setting_berkas_id;
                $setting_berkas->jenis_berkas  = "Berkas Pendaftaran";
                $setting_berkas->nama_berkas = $request->nama_berkas[$key];
                $setting_berkas->jurusan_id  = $jurusan->id;
                $setting_berkas->save();

                array_push($setting_berkas_id_array, $setting_berkas->setting_berkas_id);
            } else {
                $setting_berkas = SettingBerkas::find($value);
                $setting_berkas->nama_berkas = $request->nama_berkas[$key];                
                $setting_berkas->save();

                array_push($setting_berkas_id_array, $setting_berkas->setting_berkas_id);
            }
        }

        foreach ($list_setting_berkas as $key => $value) {
            if (in_array($value->setting_berkas_id, $setting_berkas_id_array) == false) {
                $setting_berkas = SettingBerkas::find($value);
                $setting_berkas->delete();
            }
        }

        return back()->with('message','Data Berhasil Dibuat');
    }

    public function edit($id){
        $jurusan = Jurusan::find($id);
        $tahun_ajarans = TahunAjaran::select('*')->get();

        $tahun_ajaran = $jurusan->tahunAjaran;

        $sisa_kuota = $tahun_ajaran->maksimal_jumlah_siswa;
        foreach ($tahun_ajaran->jurusan as $key => $value) {
            if ($jurusan->id != $value->id) {
                $sisa_kuota -= $value->maksimal_jumlah_siswa;
            }
        }

        return view('admin.jurusan.edit', compact('jurusan', 'tahun_ajarans', 'sisa_kuota'));
    }

    public function update(Request $request, $id){
        $jurusan = Jurusan::find($id);
        $jurusan->name = $request->name;
        // $jurusan->status = $request->status;
        // $jurusan->keterangan = $request->keterangan;
        $jurusan->tahun_ajaran = $request->tahun_ajaran;
        $jurusan->akreditasi = $request->akreditasi;

        $jurusan->minimal_nilai = $request->minimal_nilai;
        $jurusan->maksimal_jumlah_siswa = $request->maksimal_jumlah_siswa;

        $jurusan->save();

        $tahun_ajaran = str_split($request->tahun_ajaran, 4);

        $setting_berkas_id_array = [];
        $list_setting_berkas = SettingBerkas::select('*')->where("tahun_ajaran", $request->tahun_ajaran)->where('jurusan_id', $jurusan->id)->orderBy("setting_berkas_id", "asc")->get();
        foreach ($request->setting_berkas_id as $key => $value) {
            if ($value == "Baru") {
                $setting_berkas = new SettingBerkas;

                $setting_berkas_id = "SB".$tahun_ajaran[0]."01";
                if ($list_setting_berkas->count() > 0) {
                    $array_id = str_split($list_setting_berkas->last()->setting_berkas_id, 2);
                    $setting_berkas_id = "SB".(intval($array_id[1].$array_id[2].$array_id[3]) + 1);
                }

                // $x = 1;
                // do {
                //     $digit = "";
                //     if (strlen($x) == 6) {
                //         $digit = $x;
                //     } elseif (strlen($x) == 5) {
                //         $digit = "0".$x;
                //     } elseif (strlen($x) == 4) {
                //         $digit = "00".$x;
                //     } elseif (strlen($x) == 3) {
                //         $digit = "000".$x;
                //     } elseif (strlen($x) == 2) {
                //         $digit = "0000".$x;
                //     } elseif (strlen($x) == 1) {
                //         $digit = "00000".$x;
                //     }
                //     $setting_berkas_id = "SB".$digit;
                //     $setting_berkass = DB::table('setting_berkas')->select('*')->where('setting_berkas_id', $setting_berkas_id)->get();
                //     $x++;
                // }while ( $setting_berkass->count() > 0);
                $setting_berkas->setting_berkas_id  = $setting_berkas_id;
                $setting_berkas->jenis_berkas  = "Berkas Pendaftaran";
                $setting_berkas->nama_berkas = $request->nama_berkas[$key];
                $setting_berkas->jurusan_id  = $jurusan->id;
                $setting_berkas->save();

                array_push($setting_berkas_id_array, $setting_berkas->setting_berkas_id);
            } else {
                $setting_berkas = SettingBerkas::find($value);
                $setting_berkas->nama_berkas = $request->nama_berkas[$key];                
                $setting_berkas->save();

                array_push($setting_berkas_id_array, $setting_berkas->setting_berkas_id);
            }
        }
        
        foreach ($list_setting_berkas as $key => $value) {
            if (in_array($value->setting_berkas_id, $setting_berkas_id_array) == false) {
                $setting_berkas = SettingBerkas::find($value->setting_berkas_id);
                $setting_berkas->delete();
            }
        }

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function destroy($id){
        $jurusan = Jurusan::find($id);
        $jurusan->delete();

        return back()->with('message','Data Berhasil Dihapus');
    }

    public function update_status($id, $status){
        $jurusan = Jurusan::find($id);
        $jurusan->status = $status;
        $jurusan->save();

        return response()->json(['message' => 'status berhasil diupdate', 'status' => 1]);
    }
}
