<?php

namespace App\Http\Controllers;

use App\Models\Level;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class LevelController extends Controller
{
    public function index(Request $request){
        $levels = Level::select('*')->get();

        return view('admin.level.index', compact('levels'));
    }

    public function store(Request $request){
        $level = new Level;
        $level->id = Str::uuid();
        $level->name = $request->name;
        $level->scope = $request->keterangan;

        $level->save();

        return back()->with('message','Data Berhasil Dibuat');
    }

    public function update(Request $request, $id){
        $level = Level::find($id);
        $level->name = $request->name;
        $level->scope = $request->keterangan;

        $level->save();

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function destroy($id){
        $level = Level::find($id);
        $level->delete();

        return back()->with('message','Data Berhasil Dihapus');
    }
}
