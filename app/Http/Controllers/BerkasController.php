<?php

namespace App\Http\Controllers;

use App\Models\NilaiBerkas;
use App\Models\NilaiSertifikat;
use App\Models\Notification;
use App\Models\Sertifikat;
use App\Models\SettingBerkas;
use App\Models\SettingRaport;
use App\Models\Siswa;
use App\Models\SiswaBerkas;
use App\Models\SiswaRaport;
use App\Models\TahunAjaran;
use App\Models\User;
use App\Models\WebProfil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class BerkasController extends Controller
{
    public function awal()
    {
        // $tahun_ajarans = TahunAjaran::select('*')->get();

        // $tahun_ajaran = null;
        // if ($tahun_ajarans->count() > 0) {
        //     $tahun_ajaran = $tahun_ajarans->last();
        //     $now = Carbon::now()->isoFormat('Y')."-".Carbon::now()->addYear()->isoFormat('Y');
        //     if ($tahun_ajarans->where("tahun_ajaran", $now)->count() > 0) {
        //         $tahun_ajaran = TahunAjaran::find($now);
        //     }
        // }

        $tahun_ajaran = TahunAjaran::find(Auth::user()->siswa->tahun_ajaran);

        $user = User::find(Auth::user()->id);
        $siswa = Siswa::find($user->profil_id);

        $web_profil = WebProfil::all()->first();

        $sertifikats = Sertifikat::select('*')->where('siswa_id', $siswa->id)->get();

        $nilai_berkass = NilaiBerkas::select('*')->get();
        $nilai_sertifikats = NilaiSertifikat::select('*')->get();

        $setting_berkass = SettingBerkas::select('*')->where("tahun_ajaran", $tahun_ajaran->tahun_ajaran)->where('jenis_berkas', 'Berkas Pendaftaran')->get();
        $setting_raports = SettingRaport::select('*')->where("tahun_ajaran", $tahun_ajaran->tahun_ajaran)->get();

        $tingkats = NilaiSertifikat::select('nama_tingkat')->where("tahun_ajaran", $tahun_ajaran->tahun_ajaran)->orderBy('nilai_sertifikats_id', 'ASC')->distinct()->get();
        $peringkats = NilaiSertifikat::select('peringkat')->where("tahun_ajaran", $tahun_ajaran->tahun_ajaran)->orderBy('peringkat', 'ASC')->distinct()->get();
        
        return view('siswa.berkas.awal', compact('siswa', 'sertifikats', 'setting_berkass', 'setting_raports','nilai_berkass', 'nilai_sertifikats', 'web_profil', 'tingkats', 'peringkats', 'tahun_ajaran'));
    }

    public function update_awal(Request $request,$id)
    {
        // $siswa = Siswa::find($id);
        // SiswaBerkas::where('siswa_id', $id)->where('jenis_berkas', 'Berkas Pendaftaran')->delete();
        // if ($siswa->siswa_berkas->count() > 0) {
        foreach ($request->berkas_id as $key => $value) {
            $siswa_berkas = "";
            if ($value == "Baru") {
                $siswa_berkas_id = Str::uuid();
                $siswa_berkas = New SiswaBerkas;
                $siswa_berkas->siswa_berkas_id = $siswa_berkas_id;
                $siswa_berkas->siswa_id = $id;
                $siswa_berkas->name = $request->name[$key];
                // $siswa_berkas->score = $request->score[$key];
                if(isset($request->file[$key])){
                    $this->validate($request, [
                        'file.'.$key => 'required|mimes:jpeg,png,jpg|max:1024',
                    ]);
    
                    $image = $request->file[$key];
                    $img_name = $image->getClientOriginalName();
    
                    $img_name_only = pathinfo($img_name, PATHINFO_FILENAME);
                    $img_type = pathinfo($img_name, PATHINFO_EXTENSION);
    
                    $new_image_name = $img_name_only.'('.$siswa_berkas_id.').'.$img_type;
    
                    $file = $request->file('file.'.$key);
                    $tujuan_upload = public_path('penyimpanan/user/siswa/berkas/'.$id);
                    
                    $file->move($tujuan_upload, $new_image_name);
    
                    $siswa_berkas->file = $new_image_name;
                    
                }
                $siswa_berkas->jenis_berkas = "Berkas Pendaftaran";
                $siswa_berkas->jurusan_id = $request->jurusan_id[$key];
                $siswa_berkas->save();
            } else {
                $siswa_berkas = SiswaBerkas::find($value);
                if(isset($request->file[$key])){
                    $this->validate($request, [
                        'file.'.$key => 'required|mimes:jpeg,png,jpg|max:1024',
                    ]);
    
                    $image = $request->file[$key];
                    $img_name = $image->getClientOriginalName();
    
                    $img_name_only = pathinfo($img_name, PATHINFO_FILENAME);
                    $img_type = pathinfo($img_name, PATHINFO_EXTENSION);
    
                    $new_image_name = $img_name_only.'('.$siswa_berkas->siswa_berkas_id.').'.$img_type;
    
                    $file = $request->file('file.'.$key);
                    $tujuan_upload = public_path('penyimpanan/user/siswa/berkas/'.$id);
                    
                    $file->move($tujuan_upload, $new_image_name);
    
                    $siswa_berkas->file = $new_image_name;
                    // $siswa_berkas->score = $request->score[$key];
                }
                $siswa_berkas->save();
            }
        }

        $users = User::select('*')->where('jenis', 'Admin')->get();
        
        // foreach ($users as $key => $value) {
        //     $notifikasi = New Notification;
        //     $notifikasi->id = Str::uuid();
        //     $notifikasi->user_id = $value->id;
        //     $notifikasi->title = "Validasi Berkas Registrasi Masih Proses";
        //     $notifikasi->description = "Siswa dengan nama ".$siswa_berkas->siswa->user->name.", sudah mengirim berkas registrasi";
        //     $notifikasi->url_redirect = "admin/berkas/awal/seleksi-index/".$id;
        //     $notifikasi->status = "unread";
        //     $notifikasi->model_id = $value->id;
        //     $notifikasi->nama_model = "Berkas Awal";
        //     $notifikasi->save();
        // }

        // Siswa::seleksi($id);

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function gambar_berkas(SiswaBerkas $siswa_berkas){
        return view('siswa.berkas.gambar_berkas', compact('siswa_berkas'));
    }

    public function update_raport(Request $request, $id){
        foreach ($request->siswa_raport_id as $key => $value) {
            if ($value == "Baru") {
                $siswa_raport_id = Str::uuid();
                $siswa_raport = New SiswaRaport;
                $siswa_raport->siswa_raport_id = $siswa_raport_id;
                $siswa_raport->siswa_id = $id;
                $siswa_raport->nama = $request->nama[$key];
                $siswa_raport->nilai = $request->nilai[$key];
                if(isset($request->file[$key])){
                    $this->validate($request, [
                        'file.'.$key => 'required|mimes:jpeg,png,jpg|max:1024',
                    ]);
    
                    $image = $request->file[$key];
                    $img_name = $image->getClientOriginalName();
    
                    $img_name_only = pathinfo($img_name, PATHINFO_FILENAME);
                    $img_type = pathinfo($img_name, PATHINFO_EXTENSION);
    
                    $new_image_name = $img_name_only.'('.$siswa_raport_id.').'.$img_type;
    
                    $file = $request->file('file.'.$key);
                    $tujuan_upload = public_path('penyimpanan/user/siswa/raport/'.$id);
                    
                    $file->move($tujuan_upload, $new_image_name);
    
                    $siswa_raport->file = $new_image_name;
                    
                }
                $siswa_raport->save();
            } else {
                $siswa_raport = SiswaRaport::find($value);
                if(isset($request->file[$key])){
                    $this->validate($request, [
                        'file.'.$key => 'required|mimes:jpeg,png,jpg|max:1024',
                    ]);
    
                    $image = $request->file[$key];
                    $img_name = $image->getClientOriginalName();
    
                    $img_name_only = pathinfo($img_name, PATHINFO_FILENAME);
                    $img_type = pathinfo($img_name, PATHINFO_EXTENSION);
    
                    $new_image_name = $img_name_only.'('.$siswa_raport->siswa_raport_id.').'.$img_type;
    
                    $file = $request->file('file.'.$key);
                    $tujuan_upload = public_path('penyimpanan/user/siswa/raport/'.$id);
                    
                    $file->move($tujuan_upload, $new_image_name);
    
                    $siswa_raport->file = $new_image_name;
                }
                $siswa_raport->nilai = $request->nilai[$key];
                $siswa_raport->save();
            }
        }

        Siswa::seleksi($id);

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function gambar_raport(SiswaRaport $siswa_raport){
        return view('siswa.berkas.gambar_raport', compact('siswa_raport'));
    }

    public function sertifikat_store(Request $request){
        $nilai_sertifikat = NilaiSertifikat::find($request->tingkat);

        $sertifikat = new Sertifikat;
        $sertifikat->id = Str::uuid();
        $sertifikat->siswa_id = Auth::user()->siswa->id;
        $sertifikat->tanggal = $request->tanggal;
        $sertifikat->name = $request->name;
        $sertifikat->tingkat = $nilai_sertifikat->nama_tingkat;
        $sertifikat->peringkat = $nilai_sertifikat->peringkat;
        $sertifikat->keterangan = $request->link;
        if(isset($request->file)){
            $this->validate($request, [
                'file.*' => 'required|mimes:jpeg,png,jpg|max:512',
            ]);

            $image = $request->file;
            $img_name = $image->getClientOriginalName();

            $img_name_only = pathinfo($img_name, PATHINFO_FILENAME);
            $img_type = pathinfo($img_name, PATHINFO_EXTENSION);

            $new_image_name = $img_name_only.'('.$sertifikat->id.').'.$img_type;

            $file = $request->file('file');
            $tujuan_upload = public_path('penyimpanan/user/siswa/sertifikat-'.Auth::user()->siswa->id);
            
            $file->move($tujuan_upload, $new_image_name);

            $sertifikat->file = $new_image_name;
            $sertifikat->score = $nilai_sertifikat->nilai;
        }

        $sertifikat->save();

        Siswa::seleksi(Auth::user()->siswa->id);

        return back()->with('message','Data Berhasil Dibuat');
    }

    public function sertifikat_update(Request $request, $id){
        $sertifikat = Sertifikat::find($id);
        $sertifikat->name = $request->name;
        $sertifikat->tingkat = $request->tingkat;
        // $sertifikat->keterangan = $request->keterangan;
        if(isset($request->file)){
            $this->validate($request, [
                'file.*' => 'required|mimes:jpeg,png,jpg|max:512',
            ]);

            $image = $request->file;
            $img_name = $image->getClientOriginalName();

            $img_name_only = pathinfo($img_name, PATHINFO_FILENAME);
            $img_type = pathinfo($img_name, PATHINFO_EXTENSION);

            $new_image_name = $img_name_only.'('.$sertifikat->id.').'.$img_type;

            $file = $request->file('file');
            $tujuan_upload = public_path('penyimpanan/user/siswa/sertifikat-'.Auth::user()->siswa->id);
            
            $file->move($tujuan_upload, $new_image_name);

            $sertifikat->file = $new_image_name;
            $sertifikat->score = "0";
        }

        $sertifikat->save();

        Siswa::seleksi(Auth::user()->siswa->id);

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function sertifikat_destroy($id){
        $sertifikat = Sertifikat::find($id);
        $sertifikat->delete();

        Siswa::seleksi(Auth::user()->siswa->id);

        return back()->with('message','Data Berhasil Dihapus');
    }

    public function update_sertifikat(Request $request, $id){
        $jumlah_id = [];
        foreach ($request->id as $key => $value) {
            $nilai_sertifikat = NilaiSertifikat::where('nama_tingkat', $request->tingkat[$key])->where('peringkat', $request->peringkat[$key])->get()->first();
            if ($value == "Baru") {
                $sertifikat = new Sertifikat;
                $sertifikat_id = Str::uuid();
                $sertifikat->id = $sertifikat_id;
                $sertifikat->siswa_id = $id;
                $sertifikat->tanggal = $request->tanggal[$key];
                $sertifikat->name = $request->name[$key];
                $sertifikat->tingkat = $nilai_sertifikat->nama_tingkat;
                $sertifikat->peringkat = $nilai_sertifikat->peringkat;
                $sertifikat->jenis_file = $request->jenis_file[$key];
                if ($request->jenis_file[$key] == "Link") {
                    $sertifikat->keterangan = $request->link[$key];
                } else {
                    if(isset($request->file)){
                        $this->validate($request, [
                            'file.*' => 'required|mimes:jpeg,png,jpg|max:512',
                        ]);
    
                        $image = $request->file[$key];
                        $img_name = $image->getClientOriginalName();
    
                        $img_name_only = pathinfo($img_name, PATHINFO_FILENAME);
                        $img_type = pathinfo($img_name, PATHINFO_EXTENSION);
    
                        $new_image_name = $img_name_only.'('.$sertifikat->id.').'.$img_type;
    
                        $file = $request->file('file.'.$key);
                        $tujuan_upload = public_path('penyimpanan/user/siswa/sertifikat-'.$id);
                        
                        $file->move($tujuan_upload, $new_image_name);
    
                        $sertifikat->file = $new_image_name;
                    }
                }
                $sertifikat->score = $nilai_sertifikat->nilai;
                $sertifikat->save();

                array_push($jumlah_id, $sertifikat_id);
            } else {
                $sertifikat = Sertifikat::find($value);
                $sertifikat->tanggal = $request->tanggal[$key];
                $sertifikat->name = $request->name[$key];
                $sertifikat->tingkat = $nilai_sertifikat->nama_tingkat;
                $sertifikat->peringkat = $nilai_sertifikat->peringkat;
                $sertifikat->jenis_file = $request->jenis_file[$key];
                if ($request->jenis_file[$key] == "Link") {
                    $sertifikat->keterangan = $request->link[$key];
                } else {
                    if(isset($request->file)){
                        $this->validate($request, [
                            'file.*' => 'required|mimes:jpeg,png,jpg|max:512',
                        ]);
    
                        $image = $request->file[$key];
                        $img_name = $image->getClientOriginalName();
    
                        $img_name_only = pathinfo($img_name, PATHINFO_FILENAME);
                        $img_type = pathinfo($img_name, PATHINFO_EXTENSION);
    
                        $new_image_name = $img_name_only.'('.$sertifikat->id.').'.$img_type;
    
                        $file = $request->file('file.'.$key);
                        $tujuan_upload = public_path('penyimpanan/user/siswa/sertifikat-'.$id);
                        
                        $file->move($tujuan_upload, $new_image_name);
    
                        $sertifikat->file = $new_image_name;
                    }
                }
                $sertifikat->score = $nilai_sertifikat->nilai;
                $sertifikat->save();

                array_push($jumlah_id, $value);
            }
        }

        Sertifikat::where("siswa_id", $id)->whereNotIn("id", $jumlah_id)->delete();

        Siswa::seleksi($id);

        return back()->with('message','Data Berhasil Diupdate');
    }
    

    public function gambar_sertifikat(Sertifikat $sertifikat){
        return view('siswa.berkas.gambar_sertifikat', compact('sertifikat'));
    }

    public function akhir()
    {
        // Notification::where('user_id', Auth::user()->id)->where('model_id', Auth::user()->id)->where('nama_model', 'Berkas Akhir')->where('status', 'unread')->update(['status' => 'read']);

        $user = User::find(Auth::user()->id);
        $siswa = Siswa::find($user->profil_id);

        $setting_berkass = SettingBerkas::select('*')->where('jenis_berkas', 'Berkas Daftar Ulang')->get();

        return view('siswa.berkas.akhir', compact('siswa', 'setting_berkass'));
    }

    public function update_akhir(Request $request,$id)
    {
        foreach ($request->berkas_id as $key => $value) {
            $siswa_berkas = "";
            if ($value == "Baru") {
                $siswa_berkas = New SiswaBerkas;
                $siswa_berkas->siswa_berkas_id = Str::uuid();
                $siswa_berkas->siswa_id = $id;
                $siswa_berkas->name = $request->name[$key];
                $siswa_berkas->status = "Masih Proses";
                if(isset($request->file[$key])){
                    $this->validate($request, [
                        'file.'.$key => 'required|mimes:jpeg,png,jpg|max:1024',
                    ]);

                    $image = $request->file[$key];
                    $img_name = $image->getClientOriginalName();

                    $img_name_only = pathinfo($img_name, PATHINFO_FILENAME);
                    $img_type = pathinfo($img_name, PATHINFO_EXTENSION);

                    $new_image_name = $img_name_only.'('.$siswa_berkas->siswa_berkas_id.').'.$img_type;

                    $file = $request->file('file.'.$key);
                    $tujuan_upload = public_path('penyimpanan/user/siswa/berkas');
                    
                    $file->move($tujuan_upload, $new_image_name);

                    $siswa_berkas->file = $new_image_name;
                    
                }
                $siswa_berkas->jenis_berkas = "Berkas Daftar Ulang";
                $siswa_berkas->jurusan_id = $request->jurusan_id[$key];
                $siswa_berkas->save();

            } else {
                $siswa_berkas = SiswaBerkas::find($value);
                if(isset($request->file[$key])){
                    $this->validate($request, [
                        'file.'.$key => 'required|mimes:jpeg,png,jpg|max:1024',
                    ]);

                    $image = $request->file[$key];
                    $img_name = $image->getClientOriginalName();

                    $img_name_only = pathinfo($img_name, PATHINFO_FILENAME);
                    $img_type = pathinfo($img_name, PATHINFO_EXTENSION);

                    $new_image_name = $img_name_only.'('.Str::uuid().').'.$img_type;

                    $file = $request->file('file.'.$key);
                    $tujuan_upload = public_path('penyimpanan/user/siswa/berkas');
                    
                    $file->move($tujuan_upload, $new_image_name);

                    $siswa_berkas->file = $new_image_name;
                    $siswa_berkas->status = "Masih Proses";
                }
                $siswa_berkas->save();
            }
        }

        $users = User::select('*')->where('jenis', 'Admin')->get();
        
        // foreach ($users as $key => $value) {
        //     $notifikasi = New Notification;
        //     $notifikasi->id = Str::uuid();
        //     $notifikasi->user_id = $value->id;
        //     $notifikasi->title = "Validasi Berkas Daftar Ulang Masih Proses";
        //     $notifikasi->description = "Siswa dengan nama ".$siswa_berkas->siswa->user->name.", sudah mengirim berkas daftar ulang";
        //     $notifikasi->url_redirect = "admin/berkas/akhir/seleksi-index/".$id;
        //     $notifikasi->status = "unread";
        //     $notifikasi->model_id = $value->id;
        //     $notifikasi->nama_model = "Berkas Akhir";
        //     $notifikasi->save();
        // }

        return back()->with('message','Data Berhasil Diupdate');
    }
}
