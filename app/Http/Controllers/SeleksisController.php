<?php

namespace App\Http\Controllers;

use App\Models\Jurusan;
use App\Models\Sertifikat;
use App\Models\Siswa;
use App\Models\TahunAjaran;
use App\Models\WebProfil;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use PDF;

class SeleksisController extends Controller
{
    public function index(Request $request){
        $web_profils = WebProfil::all();
        $web_profil = WebProfil::find($web_profils[0]->id);

        $siswas = Siswa::select('*');

        $siswas = $siswas->where("tahun_ajaran", Carbon::now()->isoFormat('Y')."/".Carbon::now()->addYear()->isoFormat('Y'));

        // if ($web_profil->jenjang_sekolah == "SLTA") {
        //     $siswas = $siswas->where('jurusan', '!=', 'Default');
        // } else {
        //     $siswas = $siswas->where('jurusan', 'Default');
        // }

        $siswas = $siswas->get();

        return view('admin.seleksi.index', compact('siswas', 'web_profil'));
    }

    public function store(Request $request){
        $web_profils = WebProfil::all();
        $web_profil = WebProfil::find($web_profils[0]->id);
        $web_profil->jumlah_lolos = $request->jumlah;
        $web_profil->save();

        $siswas = Siswa::select('*')->get();
        $sisa = $siswas->count() - $request->jumlah;
        Siswa::orderBy('poin_kepsek', 'desc')->take($request->jumlah)->update(['seleksi' => 'Lolos']);
        Siswa::orderBy('poin_kepsek', 'asc')->take($sisa)->update(['seleksi' => 'Tidak Lolos']);

        // foreach ($siswas as $key => $value) {
        //     $nilai_berkas = 0;
        //     foreach ($value->siswa_berkas as $key => $value) {
        //         $nilai_berkas += $value->score;
        //     }

        //     $nilai_sertifikat = 0;
        //     foreach ($value->sertifikat as $key => $value) {
        //         $nilai_sertifikat += $value->score;
        //     }

        //     $siswa = Siswa::find($value->id);
            
        // }

        return back()->with('message','Data Berhasil Dibuat');
    }
    
    public function buka(Request $request){
        $web_profils = WebProfil::all();
        $web_profil = WebProfil::find($web_profils[0]->id);
        $web_profil->tampil_seleksi = $request->tampil_seleksi;
        $web_profil->waktu_seleski = Carbon::now()->format("Y-m-d");
        $web_profil->save();

        

        return back()->with('message','Data Berhasil Dibuat');
    }

    public function export_pdf(Request $request){
        $tahun_ajarans = TahunAjaran::all();

        $tahun_ajaran = null;
        if ($tahun_ajarans->count() > 0) {
            $tahun_ajaran = $tahun_ajarans->last();
            $now = Carbon::now()->isoFormat('Y')."-".Carbon::now()->addYear()->isoFormat('Y');
            if ($tahun_ajarans->where("tahun_ajaran", $now)->count() > 0) {
                $tahun_ajaran = TahunAjaran::find($now);
            }
        }

        $siswas = Siswa::select('*');

        if ($request->status) {
            $siswas = $siswas->whereHas('bayar', function($q){
                $q->where('status', 'Lunas');
            }, '>', 0)->where('seleksi', $request->status);
        }

        if ($request->tahun_ajaran) {
            $siswas = $siswas->where('tahun_ajaran', $request->tahun_ajaran);
        } else {
            $siswas = $siswas->where('tahun_ajaran', $tahun_ajaran->tahun_ajaran);
        }

        $siswas = $siswas->get();
        
        $web_profils = WebProfil::all();
        $web_profil = WebProfil::find($web_profils[0]->id);

        $pdf = PDF::loadview('admin.seleksi.laporan_pdf',['siswas'=>$siswas, 'web_profil'=>$web_profil]);
        return $pdf->stream('laporan-seleksi.pdf');
    }
}
