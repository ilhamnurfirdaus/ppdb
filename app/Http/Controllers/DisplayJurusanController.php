<?php

namespace App\Http\Controllers;

use App\Models\Jurusan;
use Illuminate\Http\Request;

class DisplayJurusanController extends Controller
{
    public function index(Request $request){
        $jurusans = Jurusan::select('*')->get();

        return view('admin.jurusan.index', compact('jurusans'));
    }
}
