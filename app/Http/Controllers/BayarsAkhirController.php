<?php

namespace App\Http\Controllers;

use App\Exports\BayarAkhirsExport;
use App\Exports\SiswaExport;
use App\Imports\BayarAkhirsImport;
use App\Models\Bank;
use App\Models\Bayar;
use App\Models\Notification;
use App\Models\Rekening;
use App\Models\User;
use App\Models\Siswa;
use App\Models\WebProfil;
use App\Notifications\KirimNotifikasi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;

class BayarsAkhirController extends Controller
{
    public function index(Request $request){
        $web_profils = WebProfil::all();
        $web_profil = WebProfil::find($web_profils[0]->id);

        $bayars = Bayar::select('*')->where('jenis_biaya', 'Biaya Daftar Ulang');
        $rekenings = Rekening::select('*')->get();
        $banks = Bank::select('*')->get();

        // if ($web_profil->jenjang_sekolah == "SLTA") {
        //     $bayars = $bayars->whereHas('siswa', function ($q) {
        //                 $q->where('jurusan', '!=', 'Default');
        //             });
        // } else {
        //     $bayars = $bayars->whereHas('siswa', function ($q) {
        //                 $q->where('jurusan', 'Default');
        //             });
        // }

        $bayars = $bayars->get();

        return view('admin.bayar.akhir', compact('banks', 'bayars', 'rekenings', 'web_profil'));
    }

    public function update(Request $request, $id){
        // dd();
        $bayar = Bayar::find($id);
        $bayar->status = $request->status;
        $bayar->keterangan = $request->keterangan;
        $bayar->save();

        Notification::whereNull('read_at')
        ->where('data', 'like', '%"model_id":"'.$id.'"%')->where('data', 'like', '%"nama_model":"Bayar"%')
        ->update(['read_at' => Carbon::now()->format('Y-m-d h:i:s')]);

        if ($bayar->status == "Lunas") {
            $title = "Pembayaran Daftar Ulang Lunas";
            $description = "Pembayaran anda sudah lunas";
            $url_redirect = "siswa/berkas/akhir";
            $model_id = $bayar->siswa->user->siswa->jurusanTo->id;
            $nama_model = "Berkas Akhir";
        } else if ($bayar->status == "Belum Lunas") {
            $title = "Pembayaran Daftar Ulang Belum Lunas";
            $description = $bayar->keterangan;
            $url_redirect = "siswa/bayar-akhir";
            $model_id = $bayar->siswa->user->siswa->jurusanTo->id;
            $nama_model = "Bayar Akhir";
        }
        $toUser = User::find($bayar->siswa->user->id);
        $toUser->notify(new KirimNotifikasi($title, $description, $url_redirect, $model_id, $nama_model));

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function export_excel() 
    {
        return Excel::download(new BayarAkhirsExport, 'list_pembayaran.xlsx');
    }

    public function export_siswa(Request $request) 
    {
        $status = $request->status;
        $tahun_ajaran = $request->tahun_ajaran;
        return Excel::download(new SiswaExport($status, $tahun_ajaran), 'SiswaExport.xlsx');
    }

    public function generate_nis() 
    {
        // dd();
        $bayars = Bayar::select('*')->where('jenis_biaya', 'Biaya Daftar Ulang')->get();
        $jmlh = $bayars->count();
        // dd($bayars->siswa);
        foreach($bayars as $key=>$val){
            $siswa = Siswa::find($val->siswa->id);
            if(isset($siswa->nis)){

            }else{
                $siswa->nis = Carbon::now()->format('Y').($jmlh+$key+1);
                $siswa->save();
            }
            // dd($siswa);
        }

        return back()->with('message', 'NIS berhasil digenerate');
    }

    public function import_excel(Request $request) 
    {
        // validasi
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);

        $file = $request->file;
        $file_name = $file->getClientOriginalName();

        $file_name_only = pathinfo($file_name, PATHINFO_FILENAME);
        $file_type = pathinfo($file_name, PATHINFO_EXTENSION);

        $new_file_name = $file_name_only.'.'.$file_type;

        $file = $request->file('file');
        $tujuan_upload = public_path('penyimpanan/excel/bayars');
        
        $file->move($tujuan_upload, $new_file_name);

        Excel::import(new BayarAkhirsImport, public_path('/penyimpanan/excel/bayars/'.$file_name));
        
        return back()->with('message','Data Berhasil Diupdate');
    }
}
