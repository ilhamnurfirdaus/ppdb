<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\Bayar;
use App\Models\Notification;
use App\Models\Rekening;
use App\Models\User;
use App\Models\Siswa;
use App\Notifications\KirimNotifikasi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;

class BayarAkhirController extends Controller
{
    public function index(Request $request){
        $user = User::find(Auth::user()->id);

        $bayars = Bayar::select('*')->where('siswa_id', $user->profil_id)->where('jenis_biaya', 'Biaya Daftar Ulang')->get();
        $rekenings = Rekening::select('*')->get();
        $banks = Bank::select('*')->get();

        return view('siswa.bayar.akhir', compact('banks', 'user', 'bayars', 'rekenings'));
    }

    public function store(Request $request){
        $user = User::find(Auth::user()->id);

        $user->notifications()->whereNull('read_at')
        ->where('data', 'like', '"model_id":"'.$user->siswa->jurusanTo->id.'"')->where('data', 'like', '"nama_model":"Bayar Awal"')
        ->get()->markAsRead();
        
        $bayar = new Bayar;
        $bayar->id = Str::uuid();
        $bayar->siswa_id = $user->profil_id;
        $bayar->rekening_id = $request->rekening_id;
        $bayar->nama_bank = $request->nama_bank;
        $bayar->nomor_rekening = $request->nomor_rekening;
        $bayar->atas_nama = $request->atas_nama;
        $bayar->tiga_digit_angka = $request->tiga_digit_angka;
        $bayar->status = "Masih Proses";
        $bayar->tgl_bayar = $request->tgl_bayar;
        $bayar->jml_byr = $request->jml_byr;
        $bayar->jenis_biaya = "Biaya Daftar Ulang";
        $bayar->catatan = $request->catatan;

        if(isset($request->img_bayar)){
            $this->validate($request, [
                'img_bayar' => 'required|mimes:jpeg,png,jpg|max:500'
            ]);

            $image = $request->img_bayar;
            $img_name = $image->getClientOriginalName();

            $img_name_only = pathinfo($img_name, PATHINFO_FILENAME);
            $img_type = pathinfo($img_name, PATHINFO_EXTENSION);

            $new_image_name = $img_name_only.'('.$bayar->id.').'.$img_type;
            
            $file = $request->file('img_bayar');
            $tujuan_upload = public_path('penyimpanan/user/bayars');
            
            $file->move($tujuan_upload, $new_image_name);

            $bayar->img_bayar = $new_image_name;
        }

        $bayar->save();

        $users = User::select('*')->where('jenis', 'Admin')->get();
        
        $title = "Pembayaran Daftar Ulang Masih Proses";
        $description = "Siswa dengan nama ".$bayar->siswa->user->name.", sudah membayar biaya daftar ulang";
        $url_redirect = "admin/bayar-akhir";
        $model_id = $bayar->id;
        $nama_model = "Bayar";

        foreach ($users as $key => $value) {
            $toUser = User::find($value->id);
            $toUser->notify(new KirimNotifikasi($title, $description, $url_redirect, $model_id, $nama_model));
        }

        return back()->with('message','Data Berhasil Dibuat');
    }

    public function update(Request $request, $id){
        $bayar = Bayar::find($id);
        $bayar->rekening_id = $request->rekening_id;
        $bayar->nama_bank = $request->nama_bank;
        $bayar->nomor_rekening = $request->nomor_rekening;
        $bayar->atas_nama = $request->atas_nama;
        $bayar->tiga_digit_angka = $request->tiga_digit_angka;
        $bayar->status = "Masih Proses";
        $bayar->tgl_bayar = $request->tgl_bayar;
        $bayar->jml_byr = $request->jml_byr;
        $bayar->jenis_biaya = "Biaya Daftar Ulang";
        $bayar->catatan = $request->catatan;

        if(isset($request->img_bayar)){
            $this->validate($request, [
                'img_bayar' => 'required|mimes:jpeg,png,jpg|max:500'
            ]);

            $image = $request->img_bayar;
            $img_name = $image->getClientOriginalName();

            $img_name_only = pathinfo($img_name, PATHINFO_FILENAME);
            $img_type = pathinfo($img_name, PATHINFO_EXTENSION);

            $new_image_name = $img_name_only.'('.$bayar->id.').'.$img_type;
            
            $file = $request->file('img_bayar');
            $tujuan_upload = public_path('penyimpanan/user/bayars');
            
            $file->move($tujuan_upload, $new_image_name);

            $bayar->img_bayar = $new_image_name;
        }

        $bayar->save();

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function destroy($id){
        $bayar = Bayar::find($id);
        $bayar->delete();

        return back()->with('message','Data Berhasil Dihapus');
    }
}
