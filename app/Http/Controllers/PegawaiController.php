<?php

namespace App\Http\Controllers;

use App\Models\Jabatan;
use App\Models\Pegawai;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\PegawaisImport;
use App\Exports\PegawaisExport;
use Carbon\Carbon;
use PDF;

class PegawaiController extends Controller
{
    public function index(Request $request){
        $pegawais = Pegawai::select('*');

        $q = $request->q;

        if ($q != null) {
            $pegawais = $pegawais->where('name', 'like', "%" . $q . "%")
                ->orWhere('telp', 'like', "%" . $q . "%")
                ->orWhere('email', 'like', "%" . $q . "%")
                ->orWhere('nip', 'like', "%" . $q . "%")
                ->orWhere('tempat_lahir', 'like', "%" . $q . "%")
                ->orWhereHas('jabatan', function ($j) use ($q) {
                    $j->where('name', 'like', "%" . $q . "%");
                })
                ;
        }

        $tgl_mulai = $request->tgl_mulai;
        $tgl_berakhir = $request->tgl_berakhir;

        if ($pegawais->get()->count() > 0) {
            if ($tgl_mulai == '' ) {
                $pegawais = $pegawais->where('tanggal_masuk', '>=', $pegawais->get()->first()->tanggal_masuk);
            } else {
                $pegawais = $pegawais->where('tanggal_masuk', '>=', Carbon::parse($tgl_mulai)->format('Y-m-d 00:00:00'));
            }
    
            if ($tgl_berakhir == '' ) {
                $pegawais = $pegawais->where('tanggal_masuk', '<=', $pegawais->get()->last()->tanggal_masuk);
            } else {
                $pegawais = $pegawais->where('tanggal_masuk', '<=', Carbon::parse($tgl_berakhir)->format('Y-m-d 23:59:59'));
            }
        }

        $pegawais = $pegawais->get();

        $jabatans = Jabatan::select('*')->get();

        return view('admin.pegawai.index', compact('pegawais', 'jabatans'));
    }

    public function store(Request $request){
        $pegawai = new Pegawai;
        $pegawai->id = Str::uuid();
        $pegawai->tanggal_masuk = $request->tanggal_masuk;
        $pegawai->nip = $request->nip;
        $pegawai->name = $request->name;
        $pegawai->tempat_lahir = $request->tempat_lahir;
        $pegawai->tanggal_lahir = $request->tanggal_lahir;
        $pegawai->jabatan_id = $request->jabatan_id;
        $pegawai->telp = $request->telp;
        $pegawai->email = $request->email;

        $pegawai->save();

        return back()->with('message','Data Berhasil Dibuat');
    }

    public function update(Request $request, $id){
        $pegawai = Pegawai::find($id);
        $pegawai->tanggal_masuk = $request->tanggal_masuk;
        $pegawai->nip = $request->nip;
        $pegawai->name = $request->name;
        $pegawai->tempat_lahir = $request->tempat_lahir;
        $pegawai->tanggal_lahir = $request->tanggal_lahir;
        $pegawai->jabatan_id = $request->jabatan_id;
        $pegawai->telp = $request->telp;
        $pegawai->email = $request->email;

        $pegawai->save();

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function destroy($id){
        $pegawai = Pegawai::find($id);
        $pegawai->delete();

        return back()->with('message','Data Berhasil Dihapus');
    }

    public function export_excel()
    {
        return Excel::download(new PegawaisExport, 'pegawais.xlsx');   
    }

    public function import_excel(Request $request){        
		$this->validate($request, [
			'file' => 'required|mimes:csv,xls,xlsx'
		]);
        
        $file = $request->file;
        $file_name = $file->getClientOriginalName();

        $file_name_only = pathinfo($file_name, PATHINFO_FILENAME);
        $file_type = pathinfo($file_name, PATHINFO_EXTENSION);

        $new_file_name = $file_name_only.'.'.$file_type;

        $file = $request->file('file');
        $tujuan_upload = public_path('penyimpanan/excel/pegawais');
        
        $file->move($tujuan_upload, $new_file_name);

        Excel::import(new PegawaisImport, public_path('/penyimpanan/excel/pegawais/'.$file_name));

        return back()->with('message', 'Excel berhasil diimport');
    }

    public function export_pdf($q, $tgl_mulai, $tgl_berakhir){
        $pegawais = Pegawai::select('*');

        if ($q != "kosong") {
            $pegawais = $pegawais->where('name', 'like', "%" . $q . "%")
                ->orWhere('telp', 'like', "%" . $q . "%")
                ->orWhere('email', 'like', "%" . $q . "%")
                ->orWhere('nip', 'like', "%" . $q . "%")
                ->orWhere('tempat_lahir', 'like', "%" . $q . "%")
                ->orWhereHas('jabatan', function ($j) use ($q) {
                    $j->where('name', 'like', "%" . $q . "%");
                })
                ;
        }


        if ($pegawais->get()->count() > 0) {
            if ($tgl_mulai == "kosong" ) {
                $pegawais = $pegawais->where('tanggal_masuk', '>=', $pegawais->get()->first()->tanggal_masuk);
            } else {
                $pegawais = $pegawais->where('tanggal_masuk', '>=', Carbon::parse($tgl_mulai)->format('Y-m-d 00:00:00'));
            }
    
            if ($tgl_berakhir == "kosong" ) {
                $pegawais = $pegawais->where('tanggal_masuk', '<=', $pegawais->get()->last()->tanggal_masuk);
            } else {
                $pegawais = $pegawais->where('tanggal_masuk', '<=', Carbon::parse($tgl_berakhir)->format('Y-m-d 23:59:59'));
            }
        }

        $pegawais = $pegawais->get();
        $jabatans = Jabatan::select('*')->get();

        $pdf = PDF::loadview('admin.pegawai.laporan_pdf',['jabatans'=>$jabatans, 'pegawais'=>$pegawais]);
        return $pdf->stream('laporan-pegawai.pdf');
    }
}
