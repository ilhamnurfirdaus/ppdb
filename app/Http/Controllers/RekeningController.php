<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\Rekening;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class RekeningController extends Controller
{
    public function index(Request $request){
        $rekenings = Rekening::select('*')->get();

        $banks = Bank::select('*')->get();

        return view('admin.rekening.index', compact('rekenings', 'banks'));
    }

    public function store(Request $request){
        $rekening = new Rekening;
        $rekening->id = Str::uuid();
        $rekening->nama_rekening = $request->nama_rekening;
        $rekening->nomor_rekening = $request->nomor_rekening;
        $rekening->atas_nama = $request->atas_nama;

        if(isset($request->image)){
            // dd($request->image->getClientOriginalName());
            $this->validate($request, [
                'image' => 'required|mimes:jpeg,png,jpg|max:500'
            ]);

            $image = $request->image;
            $img_name = $image->getClientOriginalName();

            $img_name_only = pathinfo($img_name, PATHINFO_FILENAME);
            $img_type = pathinfo($img_name, PATHINFO_EXTENSION);

            $new_image_name = $img_name_only.'('.$request->id.').'.$img_type;

            $file = $request->file('image');
            $tujuan_upload = public_path('penyimpanan/rekenings');
            
            $file->move($tujuan_upload, $new_image_name);

            $rekening->logo = $new_image_name;
        }

        $rekening->save();

        return back()->with('message','Data Berhasil Dibuat');
    }

    public function edit (Rekening $rekening){

        return view('admin.rekening.edit', compact('rekening'));
    }
    public function update(Request $request, $id){
        $rekening = Rekening::find($id);
        $rekening->nama_rekening = $request->nama_rekening;
        $rekening->nomor_rekening = $request->nomor_rekening;
        $rekening->atas_nama = $request->atas_nama;

        if(isset($request->image)){
            // dd($request->image->getClientOriginalName());
            $this->validate($request, [
                'image' => 'required|mimes:jpeg,png,jpg|max:500'
            ]);

            $image = $request->image;
            $img_name = $image->getClientOriginalName();

            $img_name_only = pathinfo($img_name, PATHINFO_FILENAME);
            $img_type = pathinfo($img_name, PATHINFO_EXTENSION);

            $new_image_name = $img_name_only.'('.$request->id.').'.$img_type;

            $file = $request->file('image');
            $tujuan_upload = public_path('penyimpanan/rekenings');
            
            $file->move($tujuan_upload, $new_image_name);

            $rekening->logo = $new_image_name;
        }
        $rekening->save();

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function destroy($id){
        $rekening = Rekening::find($id);
        $rekening->delete();

        return back()->with('message','Data Berhasil Dihapus');
    }
}
