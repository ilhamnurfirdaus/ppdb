<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Siswa;
use App\Models\Jurusan;

class GrafikController extends Controller
{
    //
    public function grafik_sekolah(){
        $data = Siswa::groupBy('asal_sekolah')->select('asal_sekolah')->orderBy('asal_sekolah')->get();
        $siswa_all = Siswa::all();
        $data_arr = [];
        foreach ($data as $item){
            $data_arr[] = Siswa::where('asal_sekolah', $item->asal_sekolah)->count();
        }
        // dd($data_arr);
        return response()->json([
            'label' => $data,
            'isi' => $data_arr
        ]);
    }

    public function grafik_jurusan(){
        $jurusan = Jurusan::all();
        $label = [];
        foreach($jurusan as $itm){
            $label[] = $itm->name;
        }
        $isi = [];
        foreach ($jurusan as $item){
            $isi[] = Siswa::where('jurusan', $item->id)->count();
        }
        return response()->json([
            'label' => $label,
            'isi' => $isi
        ]);

    }

    public function grafik_jenis_kelamin(){
        $data['lk'] = Siswa::where('jenis_kelamin', 'Laki-laki')->count();
        $data['pr'] = Siswa::where('jenis_kelamin', 'Perempuan')->count();

        return response()->json($data);
    }
}
