<?php

namespace App\Http\Controllers;

use App\Models\Level;
use App\Models\Pegawai;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function index(Request $request){
        $users = User::select('*')->where('jenis', 'Admin')->get();
        $pegawais = Pegawai::select('*')->get();
        $levels = Level::select('*')->get();
        

        return view('admin.user.index', compact('users','pegawais','levels'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'email'=>'required|string|max:255|email|unique:users',
            'password'=>'required|string|confirmed|min:5',
            'username'=>'required|string|max:255|unique:users'
        ]);

        $user = new User;
        $user->id = Str::uuid();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->profil_id = $request->profil_id;
        $user->level_id = $request->level_id;
        $user->jenis = "Admin";
        $user->username = $request->username;
        $user->password = Hash::make($request->password);
        $user->save();

        return back()->with('message','Data Berhasil Dibuat');
    }

    public function update(Request $request, $id){
        $user = User::find($id);
        $user->name = $request->name;
        $user->level_id = $request->level_id;
        if ($user->email != $request->email) {
            $this->validate($request, [
                'email'=>'required|string|max:255|email|unique:users'
            ]);
            $user->email = $request->email;
        }
        $user->profil_id = $request->profil_id;
        $user->level_id = $request->level_id;
        $user->jenis = "Admin";
        if ($user->username != $request->username) {
            $this->validate($request, [
                'username'=>'required|string|max:255|unique:users'
            ]);
            $user->username = $request->username;
        }
        if (isset($request->password)) {
            $this->validate($request, [
            'password'=>'required|string|confirmed|min:5',
            ]);

            $user->password = Hash::make($request->password);
        }
        $user->save();

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function destroy($id){
        $user = User::find($id);
        $user->delete();

        return back()->with('message','Data Berhasil Dihapus');
    }
}
