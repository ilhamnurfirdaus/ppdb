<?php

namespace App\Http\Controllers;

use App\Models\Notification;
use App\Models\Siswa;
use App\Models\TahunAjaran;
use Illuminate\Http\Request;
use App\Models\Test;
use App\Models\TestAnswer;
use App\Models\TestChoice;
use Illuminate\Support\Str;
use App\Models\User;
use App\Models\WebProfil;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class TestController extends Controller
{
    public function __construct()
    {
        $this->middleware(function ($request, $next) {
            // $tahun_ajarans = TahunAjaran::select('*')->get();
            // $tahun_ajaran = null;
            // if ($tahun_ajarans->count() > 0) {
            //     $tahun_ajaran = $tahun_ajarans->last();
            //     $now = Carbon::now()->isoFormat('Y')."-".Carbon::now()->addYear()->isoFormat('Y');
            //     if ($tahun_ajarans->where("tahun_ajaran", $now)->count() > 0) {
            //         $tahun_ajaran = TahunAjaran::find($now);
            //     }
            // }

            $tahun_ajaran = TahunAjaran::find(Auth::user()->siswa->tahun_ajaran); 
            
            if (isset($tahun_ajaran) && $tahun_ajaran->tampil_test == "Ya") {
                return $next($request);
            }
            abort(404);
        });
    }

    public function index(Request $request){
        Notification::whereNull('read_at')
        ->where('data', 'like', '%"model_id":"'.Auth::user()->siswa->jurusanTo->id.'"%')->where('data', 'like', '%"nama_model":"Berkas Awal"%')
        ->update(['read_at' => Carbon::now()->format('Y-m-d h:i:s')]);
        
        $siswa = Siswa::find(Auth::user()->siswa->id);
        $tests = Test::select('*')->inRandomOrder()->get();

        // $test = Test::all()->random(Test::all()->count());

        // dd($test);
        $web_profil = WebProfil::all()->first();

        return view('siswa.test.index', compact('tests', 'siswa', 'web_profil'));
    }

    public function selesai(Request $request, $id){
        $total_score = 0;
        // dd($request->all());
        for ($i = 0; $i < count($request->ids); $i++) {
            if (TestChoice::all()->where("test_id", $request->ids[$i])->count() > 0) {
                if (isset($request->all()["test_choice_id_" . $i][0])) {
                    $test_choice = TestChoice::find($request->all()["test_choice_id_" . $i][0]);
                    $test_answer = new TestAnswer;
                    $test_answer->id = Str::uuid();
                    $test_answer->siswa_id = $id;
                    $test_answer->test_choice_id = $test_choice->id;
                    $test_answer->save();

                    $total_score += $test_choice->score;
                }
            }
        }

        $siswa = Siswa::find($id);
        $siswa->cek_soal = "Selesai";
        $siswa->soal_score = $total_score;
        $siswa->save();

        Siswa::seleksi($siswa->id);

        return back()->with('message','Data Berhasil Dibuat');
    }

    public function mulai($id){
        $web_profil = WebProfil::all()->first();
        $tahun_ajaran = TahunAjaran::find(Auth::user()->siswa->tahun_ajaran); 
        $siswa = Siswa::find($id);
        if (empty($siswa->cek_soal)) {
            $siswa->cek_soal = "Masih Proses";
            $siswa->start_date = Carbon::now();
            $siswa->end_date = Carbon::now()->addHours($tahun_ajaran->waktu_soal);
        }
        $siswa->save();

        return back()->with('message','Mulai Mengerjakan Soal');
    }
}
