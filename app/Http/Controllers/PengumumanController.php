<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pengumuman;
use App\Models\Siswa;
use Illuminate\Support\Str;

class PengumumanController extends Controller
{
    public function index(Request $request){
        $pengumumans = Pengumuman::select('*')->get();

        return view('admin.pengumuman.index', compact('pengumumans'));
    }

    public function store(Request $request){
        $this->validate($request, [
            'deskripsi'=>'required|string',
        ]);

        $pengumuman = new Pengumuman;
        $pengumuman->id = Str::uuid();
        $pengumuman->judul = $request->judul;
        $pengumuman->deskripsi = $request->deskripsi;
        $pengumuman->status = $request->status;

        Siswa::all()->update(['cek_pengumuman' => '1']);
        
        $pengumuman->save();

        return back()->with('message','Data Berhasil Dibuat');
    }

    public function update(Request $request, $id){
        $this->validate($request, [
            'deskripsi'=>'required|string',
        ]);
        
        $pengumuman = Pengumuman::find($id);
        $pengumuman->judul = $request->judul;
        $pengumuman->deskripsi = $request->deskripsi;
        $pengumuman->status = $request->status;

        Siswa::all()->update(['cek_pengumuman' => '1']);

        $pengumuman->save();

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function destroy($id){
        $pengumuman = Pengumuman::find($id);
        $pengumuman->delete();

        return back()->with('message','Data Berhasil Dihapus');
    }
}
