<?php

namespace App\Http\Controllers;

use App\Models\Bank;
use App\Models\Bayar;
use App\Models\Rekening;
use App\Models\User;
use App\Models\Siswa;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Str;
use PDF;

class InvoiceController extends Controller
{
    public function invoice($id, $type){
        $bayar = Bayar::find($id);

        $pdf = PDF::loadview('landing-page.invoice', ['bayar'=>$bayar]);

        if ($type == "view") {
            return $pdf->stream('invoice.pdf');
        } elseif ($type == "download") {
            return $pdf->download('invoice.pdf');
        }
    }
}
