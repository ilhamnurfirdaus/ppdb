<?php

namespace App\Http\Controllers;

use App\Models\Jurusan;
use App\Models\Sertifikat;
use App\Models\Siswa;
use App\Models\WebProfil;
use Carbon\Carbon;
use Illuminate\Http\Request;

class KepsekController extends Controller
{
    public function index(Request $request){
        $jurusans = Jurusan::select('*')->whereHas("siswa.bayar", function($q){
            $q->where("status", "Lunas");
        }, '>', 0)->get();

        $web_profil = WebProfil::all()->first();

        $siswas = Siswa::select('*')
        ->whereHas("bayar", function($q){
            $q->where("status", "Lunas");
        }, '>', 0)
        ->where("tahun_ajaran", Carbon::now()->isoFormat('Y')."-".Carbon::now()->addYear()->isoFormat('Y'));
        
        if ($web_profil->jenjang_sekolah == "SLTA") {
            if ($request->jurusan != null) {
                $siswas = $siswas->where("jurusan", $request->jurusan);
            } else {
                if ($jurusans->count() > 0) {
                    $siswas = $siswas->where("jurusan", $jurusans->first()->id);
                }
            }
        } else {
            $siswas = $siswas->where('jurusan', 'Default');
        }

        $siswas = $siswas->get();

        return view('admin.seleksi.index', compact('siswas', 'web_profil', 'jurusans'));
    }

    public function edit(Siswa $siswa){
        return view('admin.seleksi.edit', compact('siswa'));
    }

    public function update(Request $request, $id){
        $siswa = Siswa::find($id);

        // $siswa->jenis_kepsek = $request->jenis_kepsek;
        // if ($request->jenis_kepsek == "Auto") {
        //     $peringkat_satu = Siswa::select('*')->orderBy('total_score', 'desc')->get()->first();
        //     if ($peringkat_satu->seleksi == "Lolos") {
        //         $siswa->poin_kepsek = ($peringkat_satu->total_score - ($siswa->total_score - $siswa->poin_kepsek)) + 1;
        //     } else {
        //         $siswa->poin_kepsek = ($siswa->jurusanTo->minimal_nilai - ($siswa->total_score - $siswa->poin_kepsek)) + 1;
        //     }
        // } else if ($request->jenis_kepsek == "Manual") {
        //     $siswa->poin_kepsek = $request->poin_kepsek;
        // } else {
        //     $siswa->poin_kepsek = 0;
        // }
        $siswa->poin_kepsek = $request->poin_kepsek;
        $siswa->save();
        
        Siswa::seleksi($id);

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function berkass(Siswa $siswa){
        return view('admin.seleksi.berkass', compact('siswa'));
    }

    public function raports(Siswa $siswa){
        return view('admin.seleksi.raports', compact('siswa'));
    }

    public function sertifikats(Siswa $siswa){
        return view('admin.seleksi.sertifikats', compact('siswa'));
    }

    public function sertifikat(Sertifikat $sertifikat){
        return view('admin.seleksi.sertifikat', compact('sertifikat'));
    }
}
