<?php

namespace App\Http\Controllers;

use App\Models\SettingBerkas;
use App\Models\SyaratPendaftaran;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class SyaratPendaftaranController extends Controller
{
    public function index(Request $request){
        $setting_berkass = SettingBerkas::select('*')->where('jenis_berkas', 'Berkas Pendaftaran')->where('jurusan_id', 'Default')->get();

        return view('admin.syarat_pendaftaran.index', compact('setting_berkass'));
    }

    public function store(Request $request){
        $setting_berkas = new SettingBerkas;
        $x = 1;
        do {
            $digit = "";
            if (strlen($x) >= 6) {
                $digit = $x;
            } elseif (strlen($x) == 5) {
                $digit = "0".$x;
            } elseif (strlen($x) == 4) {
                $digit = "00".$x;
            } elseif (strlen($x) == 3) {
                $digit = "000".$x;
            } elseif (strlen($x) == 2) {
                $digit = "0000".$x;
            } elseif (strlen($x) == 1) {
                $digit = "00000".$x;
            }
            $setting_berkas_id = "NIBE".$digit;
            $setting_berkass = SettingBerkas::where('setting_berkas_id', $setting_berkas_id)->get();
            $x++;
        }while ( $setting_berkass->count() != 0);
        $setting_berkas->setting_berkas_id  = $setting_berkas_id;
        $setting_berkas->jenis_berkas  = "Berkas Pendaftaran";
        $setting_berkas->nama_berkas = $request->nama_berkas;
        $setting_berkas->keterangan = $request->keterangan;
        $setting_berkas->jurusan_id  = "Default";
        $setting_berkas->save();

        return back()->with('message','Data Berhasil Dibuat');
    }

    public function edit($id){
        $setting_berkas = SettingBerkas::find($id);

        return view('admin.syarat_pendaftaran.edit', compact('setting_berkas'));
    }

    public function update(Request $request, $id){
        $setting_berkas = SettingBerkas::find($id);
        $setting_berkas->nama_berkas = $request->nama_berkas;
        $setting_berkas->keterangan = $request->keterangan;
        $setting_berkas->save();

        return back()->with('message','Data Berhasil Diupdate');
    }

    public function destroy($id){
        $setting_berkas = SettingBerkas::find($id);
        $setting_berkas->delete();

        return back()->with('message','Data Berhasil Dihapus');
    }
}
