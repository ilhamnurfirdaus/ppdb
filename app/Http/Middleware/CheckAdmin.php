<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        if (!Auth::check()) {
            // abort(404);
            return redirect('/');
        }
        $user = Auth::user() ; 
        if ($user->jenis != 'Admin' )
        {
            // abort(404);
            return redirect('/');
        }
        return $next($request);
    }
}
